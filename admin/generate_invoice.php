<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$page = "company-data";
$path = '../';

$servicios = new Servicios();

$companies = $servicios->getPendingStatementsCompanies();
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src='../js/accordion.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/admin.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/accordion.css" />

        <style type="text/css">
            .title {
                font-size: 2rem;
                text-align: center;
                width: 100%;
                margin-top: 1rem;
                margin-bottom: 1rem;
            }

            .accordion {
                margin-left: 10%;
                width: 80% !important;
                border-bottom: 1px solid black;
                margin-bottom: 0.1rem;
            }

            .accordion img {
                width: 2rem;
                float:right;
            }

            .statementsList {
                background-color: white;
                margin-left: 15%;
                width: 80%;
            }

            .statementsList li {
                margin-top: 1rem;
                height: 1rem;
                background-color: white;
            }

            .statementsList li .checkStatement {
                margin-right: 1rem;
            }

            .liGenerateButton {
                height: 2rem !important;
                float: right;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">

                        <div id="menu-client"><?php include_once($path . 'includes/menu-admin.php'); ?></div>

                        <div class="title"><p>Generate Invoices</p></div>

                        <?php
                        if (count($companies) > 0) {
                            foreach ($companies as $company) {
                                echo "<button class='accordion'>" . strtoupper($company->company_name) . "<img src='../img/down_arrow.png'/></button>";
                                $statements = $servicios->getPendingStatements($company->company_id);

                                echo "<div id='statements_" . $company->company_id . "' class = 'panel'>";
                                echo "  <ul class='statementsList'>";
                                foreach ($statements as $statement) {
                                    echo "<li><input type='checkbox' class='checkStatement' checked id='" . $statement->payment_id . "' />";
                                    echo "Test #" . $statement->test_id . " generated on " . $statement->payment_date . " at " . $statement->test_address . ", " . $statement->test_city . "</li>";
                                }
                                echo "<li class='liGenerateButton'><input type='button' value='Generate Invoice' onclick='generateInvoice(" . $company->company_id . ")'/></li>";
                                echo "  </ul>";
                                echo "</div>";
                            }
                        } else {
                            echo "<div style='text-align:center'> There are not statements to process </div>";
                        }
                        ?>

                    </div>
                    <form id="form" method="post" action="generate_invoice_process.php"> <!-- target="_blank"  -->
                        <input type="hidden" value="" id="data" name="data" />
                        <input type="hidden" value="" id="companyId" name="companyId" />
                    </form>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
//        $(document).ready(function () {
//            $('#form').submit(function () {
//                setTimeout(function () {
//                    document.location.href = "generate_invoice.php";
//                }, 500);
//            });
//        });


        $("#data_device").validate({
            rules: {
                serial_number: "required"
            }, messages: {
                serial_number: "Please insert serial number"
            },
            errorLabelContainer: $("#data_device div.error")
        });

        function generateInvoice(companyId) {
            statements = new Array();
            $("#statements_" + companyId + " li input:checked").each(function (index, element) {
                statements.push(element.attributes.id.value);
            });
            $("#data").val(statements);
            $("#companyId").val(companyId);
            document.getElementById("form").submit();
        }
    </script>
</html>   