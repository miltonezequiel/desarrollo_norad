<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();

//Pagination
//$perpage = $GLOBAL_SETTINGS['admin.companies.max'];
//$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
//$from = (($currentPage * $perpage - $perpage));
//$total = count($servicios->getAgreements());
$vAgreements = $servicios->getAgreements();

//$pagination = new Pagination($currentPage, $total, 5, $perpage, "d=" . ($disabled) ? "1" : "0");
$page = "agreements";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>

        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/admin.css" />
        <style>

            #table-t {
                border-collapse: collapse;
                margin: 0;
                width: 100%;
            }

            .actions-list li a {
                padding: 0.4rem;
            }

            .actions-list ul {
                width: 10rem;
            }

        </style>


    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <!--<div class="conttxt"><a href="<?php echo $path; ?>admin/companies_add.php"><img src="<?php echo $path; ?>images/admin-btn-create.jpg" class="img"></a></div>-->
                            <table id="table-t" >
                                <tr>
                                    <th>ID</th>
                                    <th class="mid">Text</th>
                                    <!--<th>Actions</th>-->
                                </tr>
                                <?php
                                foreach ($vAgreements as $oAgreement) {
                                    echo '<tr>';
                                    echo '  <td style="text-align: left;">' . $oAgreement->agreement_id . '</td>';
                                    echo '  <td style="text-align: left;">' . nl2br($oAgreement->agreement_content) . '</td>';
                                    echo '<tr>';
                                }
                                ?>

                            </table>
                            <?php //echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   