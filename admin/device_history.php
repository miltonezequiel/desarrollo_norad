<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/entidades/device_reset.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';
session_start();

$idDevice = $_GET['id'];
if (is_null($idDevice)) {
    header("Location: index.php");
}

$servicios = new Servicios();
$oDevice = $servicios->getDeviceById($idDevice);

//Pagination
$perpage = $GLOBAL_SETTINGS['company.devices.history.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
echo 'test:' . $idCompany;
$total = count($servicios->getDevicesByIdCompany($idCompany, TRUE));
//$vDevicesCompany = $servicios->getDevicesByIdCompanyPag($idCompany, $from, $perpage, TRUE);

$pagination = new Pagination($currentPage, $total, 5, $perpage, "idc=" . $idCompany);

$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <style>
            #table-t{
                width: 80%;
                border-collapse: collapse;
				margin:10px 0 0 10%;
            }

            #table-t th{
                border: 1px solid #a9a9a9;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			 #table-t th.mid {
                border-bottom:1px solid #a9a9a9;
				border-top:1px solid #a9a9a9;
				border-left:hidden;
				border-right:hidden;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }

            #table-t td{
                border: 1px solid #a9a9a9;
                background-color: #fff;
				font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			#table-t td a {
               font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:none;
            }
			
			#table-t td a:hover {
                font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:underline;
            }
       
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <div class="conttxt"><p>Device S/N: <?php echo $oDevice->getSerialNumber(); ?></p></div>
                            <table id="table-t">
                                <tr>
                                    <th>Id</th>
                                    <th class="mid">Company</th>
                                    <th class="mid"># Tests</th>
                                    <th class="mid"># Resets</th>
                                    <th class="mid">Last used</th>
                                    <th>Cal date</th>
                                </tr>
                                <?php
                                //foreach ($vDevicesCompany as $oDeviceCompany) {
                                    $calibrationDate = $servicios->getCalibrationDate($oDevice);
                                    $lastCompanyId = $servicios->getLastUseCompany($oDevice);
                                    $oCompany = $servicios->getCompanyById($lastCompanyId[0]->dc_company_id);
                                    
                                    $lastDateUsed = $lastCompanyId[0]->test_report_date;
                                    $resets = $servicios->getCountResetsByDevice($oDevice->getId());
                                    $tests = $servicios->getCountTestDevice($oDevice->getId());
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $oDevice->getSerialNumber(); ?></td>
                                        <td><?php echo $oCompany->getName() ?></td>
                                        <td><?php echo $tests ?></td>
                                        <td><?php echo (!is_null($resets)) ? $resets : "0"; ?></td>
                                        <td> <?php echo (!is_null($lastDateUsed)) ? Utilidades::dateFormat($lastDateUsed) : "-"; ?> </td>
                                        <td> <?php echo (!is_null($calibrationDate)) ? Utilidades::dateFormat($calibrationDate, "m-d-Y H:i:s") : "-"; ?> </td>
                                    </tr>
                                    <?php
                                //}
                                ?>

                            </table>
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   