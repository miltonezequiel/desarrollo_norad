<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/controllers/users_controller.php';
include_once ROOT_DIR . '/controllers/companies_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$usersController = new UsersController();
$companiesController = new CompaniesController();
$servicios = new Servicios();

$idCompany = $_POST['id'];
$password = $_POST['password'];

$oCompany = $servicios->getCompanyById($idCompany);
$oUser = $servicios->getUserById($oCompany->getUser());

$oUser->setPassword(md5($password));
$servicios->editPasswordUser($oUser);

$redirectLocation = 'companies.php';

header('Location:' . $redirectLocation);
?>