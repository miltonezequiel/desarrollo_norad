<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/entity.php';
session_start();

$idInspector = $_GET['id'];

if (!isset($idInspector) || is_null($idInspector)) {
    header("Location: inspectors.php");
}

$servicios = new Servicios();
$vEntities = $servicios->getEntities();

$oInspector = $servicios->getInspectorById($idInspector);
$oUser = $servicios->getUserById($oInspector->getUser());
$oEntityInspector = $servicios->getEntityById($oInspector->getEntity());
$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_user" action="inspectors_process.php?action=edit">
                                <input type="hidden" name="idInspector" class="box" value="<?php echo $oInspector->getId(); ?>"/><br/>
                                <input type="hidden" name="idUser" class="box" value="<?php echo $oUser->getId(); ?>"/><br/>
                                <input type="text" name="name" class="box" placeholder="Inspector name" value="<?php echo $oInspector->getName(); ?>"/><br/>
                                <input type="text" name="identifier" class="box" placeholder="Inspector identifier" value="<?php echo $oInspector->getIdentifier(); ?>"/><br/>
                                <select name="entity">
                                    <?php
                                    foreach ($vEntities as $oEntity) {
                                        $selected = ($oEntity->getId() == $oEntityInspector->getId()) ? "selected" : "";
                                        echo "<option value='{$oEntity->getId()}' {$selected}>{$oEntity->getName()}</option>";
                                    }
                                    ?>
                                </select><br/>
                                <input type="text" name="username" id="username" class="box" placeholder="User" value="<?php echo $oUser->getUsername(); ?>" readonly="true"/><br/>
                                <input type="password" name="password"  class="box" placeholder="New password" /><br/>
                                <div class="error">
                                </div>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $( "#data_user" ).validate({
            rules: {
                name: "required",
                identifier: "required",
                username: "required"
            },messages: {
                name: "Please insert name",
                identifier: "Please insert identifier",
                username: "Please insert username"
            },
            errorLabelContainer: $("#data_user div.error")
        });
    </script>
</html>   