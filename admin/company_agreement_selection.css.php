<?php
header("Content-type: text/css; charset: UTF-8");

if (!isset($SLIDER_ITEMS_COUNT)) {
    $SLIDER_ITEMS_COUNT = 5;
}
?>
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Config
2.0 General
3.0 Slider
4.0 All Tutorials
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Config
--------------------------------------------------------------*/
/*! sanitize.css | CC0 Public Domain | github.com/jonathantneal/sanitize.css */
pre, textarea {
    overflow: auto;
}

[hidden], audio:not([controls]), template {
    display: none;
}

details, main, summary {
    display: block;
}

input[type=number] {
    width: auto;
}

input[type=search], input[type=text], input[type=email] {
    -webkit-appearance: none;
}

input[type="*"] {
    -webkit-appearance: none;
}

input[type=search]::-webkit-search-cancel-button, input[type=search]::-webkit-search-decoration {
    -webkit-appearance: none;
}

progress {
    display: inline-block;
}

small {
    font-size: 100%;
}

textarea {
    resize: vertical;
}

[unselectable] {
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

*, ::after, ::before {
    box-sizing: inherit;
    border-style: solid;
    border-width: 0;
}

* {
    font-size: inherit;
    line-height: inherit;
    margin: 0;
    padding: 0;
}

::after, ::before {
    text-decoration: inherit;
    vertical-align: inherit;
}

:root {
    -ms-overflow-style: -ms-autohiding-scrollbar;
    overflow-y: scroll;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    text-size-adjust: 100%;
    box-sizing: border-box;
    cursor: default;
    font: 16px/1.5 sans-serif;
    text-rendering: optimizeLegibility;
}

a {
    text-decoration: none;
}

audio, canvas, iframe, img, svg, video {
    vertical-align: middle;
}

button, input, select, textarea {
    background-color: transparent;
    color: inherit;
    font-family: inherit;
    font-style: inherit;
    font-weight: inherit;
    min-height: 1.5em;
}

code, kbd, pre, samp {
    font-family: monospace, monospace;
}

nav ol, nav ul {
    list-style: none;
}

ul li {
    list-style: none;
}

select {
    -moz-appearance: none;
    -webkit-appearance: none;
}

select::-ms-expand {
    display: none;
}

select::-ms-value {
    color: currentColor;
}

table {
    border-collapse: collapse;
    border-spacing: 0;
}

::-moz-selection {
    background-color: #B3D4FC;
    text-shadow: none;
}

::selection {
    background-color: #B3D4FC;
    text-shadow: none;
}

@media screen {
    [hidden~=screen] {
        display: inherit;
    }

    [hidden~=screen]:not(:active):not(:focus):not(:target) {
        clip: rect(0 0 0 0) !important;
        position: absolute !important;
    }
}

/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Font
2.0 Trasnitions
3.0 Number of Slides
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Font
--------------------------------------------------------------*/
/*--------------------------------------------------------------
2.0 Transitions
--------------------------------------------------------------*/
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Config
2.0 Translate
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Transition
--------------------------------------------------------------*/
/*--------------------------------------------------------------
2.0 Translate
--------------------------------------------------------------*/
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Neutral
2.0 Main Colors
3.0 Color Pallete
3.0 Grey Scale
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Neutral
--------------------------------------------------------------*/
/*--------------------------------------------------------------
2.0 Main color
--------------------------------------------------------------*/
/*--------------------------------------------------------------
3.0 Color Pallete
--------------------------------------------------------------*/
/*--------------------------------------------------------------
4.0 Grey Scale
--------------------------------------------------------------*/
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 HTML Settings
2.0 General Settings
3.0 Heading 2
4.0 Serif
5.0 Heading 3
6.0 Text
7.0 Links
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 HTML settings
--------------------------------------------------------------*/
html {
    -webkit-overflow-scrolling: touch;
    box-sizing: border-box;
    -webkit-tap-highwhite-color: transparent;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    text-rendering: optimizeLegibility;
}

/*--------------------------------------------------------------
2.0 General Settings
--------------------------------------------------------------*/
h1,
h2,
h3,
h4,
h5,
h6,
p,
a,
li {
    display: block;
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
}

/*--------------------------------------------------------------
3.0 Heading 2
--------------------------------------------------------------*/
.heading-2 {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
    font-size: 24px;
    font-weight: 500;
    line-height: 40px;
    color: #000;
    letter-spacing: 0.3px;
}

.heading-2--white {
    color: #FFF;
}

/*--------------------------------------------------------------
4.0 Serif
--------------------------------------------------------------*/
.serif {
    font-family: "Arnhem";
    font-size: 24px;
    font-weight: 100;
    line-height: 38px;
    color: #3E4954;
}

@media (max-width: 400px) {
    .serif {
        font-size: 20px;
        line-height: 32px;
    }
}

/*--------------------------------------------------------------
5.0 Heading-3
--------------------------------------------------------------*/
.heading-3 {
    font-size: 18px;
    font-weight: 600;
    line-height: 25px;
    color: #3E4954;
    letter-spacing: 0.3px;
}

.heading-3 + .heading-3 {
    margin-top: 5px;
}

.heading-3--white {
    color: #FFF;
}

.heading-3--light {
    color: #848995;
}

/*--------------------------------------------------------------
6.0 Text
--------------------------------------------------------------*/
.text {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
    font-size: 18px;
    font-weight: 400;
    line-height: 30px;
    color: #848995;
    letter-spacing: 0px;
}

@media (max-width: 600px) {
    .text {
        font-size: 16px;
        line-height: 30px;
    }
}

.text--light-white {
    color: rgba(255, 255, 255, 0.7);
}

/*--------------------------------------------------------------
7.0 Links
--------------------------------------------------------------*/
.link {
    position: relative;
    display: inline-block;
    font-weight: 600;
    color: #5050FF;
    float: right;
}

.link:before {
    position: absolute;
    top: 50%;
    left: -32px;
    display: block;
    width: 24px;
    height: 24px;
    background: url("../images/slider/rubber_stamp.svg") no-repeat center;
    background-size: cover;
    content: '';
    transform: translateY(-55%);
    transition: all 0.12s ease-in-out;
}

.link:hover:before {
    top: -5px;
}

/*--------------------------------------------------------------
2.0 General
--------------------------------------------------------------*/
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 General
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 General
--------------------------------------------------------------*/

/*--------------------------------------------------------------
3.0 Slider
--------------------------------------------------------------*/
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Variables
2.0 Cards
3.0 Bullets
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Variables
--------------------------------------------------------------*/
/*--------------------------------------------------------------
2.0 Cards
--------------------------------------------------------------*/
.slider__item {
    transition: all 0.2s ease;
}

<?php
for ($i = 1; $i < $SLIDER_ITEMS_COUNT; $i++) {
    for ($j = 1; $j < $SLIDER_ITEMS_COUNT; $j++) {
?>

#slide-<?php echo $i;?>:checked ~ .slider__holder .slider__item--<?php echo $j;?> {

<?php if ($i == $j) {
	echo "position: relative;";
    echo "z-index: 2;";
    echo "transform-origin: top center;";
    echo "transform: translate(0) scale(1);";
} elseif ($i-1 == $j) {
    echo "z-index: 1;";
    echo "transform-origin: top center;";
    echo "transform: translateX(-100px) translateY(50px) scale(0.85);";
} elseif ($i-2 == $j) {
    echo "z-index: 0;";
    echo "transform-origin: top center;";
    echo "transform: translateX(-210px) translateY(100px) scale(0.65);";
} elseif ($i == $j-1) {
    echo "z-index: 1;";
    echo "transform-origin: top center;";
    echo "transform: translateX(100px) translateY(50px) scale(0.85);";
} elseif ($i == $j-2) {
    echo "z-index: 0;";
    echo "transform-origin: top center;";
    echo "transform: translateX(210px) translateY(100px) scale(0.65);";
} elseif ($i-3 >= $j) {
    echo "z-index: -1;";
    echo "opacity: 0;";
    echo "transform-origin: top center;";
    echo "transform: translateX(-210px) translateY(150px) scale(0.65);";
} elseif ($i <= $j-3) {
    echo "z-index: -1;";
    echo "opacity: 0;";
    echo "transform-origin: top center;";
    echo "transform: translateX(210px) translateY(150px) scale(0.65);";
}
?>

}

<?php
    }
}
?>

/*--------------------------------------------------------------
3.0 Bullets
--------------------------------------------------------------*/
.bullets__item {
    transition: all 0.2s ease;
}

<?php
for ($i = 1; $i < $SLIDER_ITEMS_COUNT; $i++) {
?>
#slide-<?php echo $i ?>:checked ~ .bullets .bullets__item--<?php echo $i ?> {
    background: #FFF;
}

<?php
}
?>

/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Card
2.0 Slider
3.0 Bullets
4.0 Section
5.0 Button
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Card
--------------------------------------------------------------*/
.card {
    position: relative;
    display: block;
    border-radius: 8px;
    background: #FFF;
    box-shadow: 0 5px 20px 0 rgba(0, 0, 0, 0.05), 0 2px 4px 0 rgba(0, 0, 0, 0.1);
}

/*--------------------------------------------------------------
2.0 Slider
--------------------------------------------------------------*/
.slider {
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

.slider__radio {
    display: none;
}

.slider__holder {
    position: relative;
    width: 100%;
    max-width: 600px;
    margin: 0 auto;
    margin-top: 80px;
    text-align: left;
}

@media (max-width: 900px) {
    .slider__holder {
        max-width: 540px;
    }
}

@media (max-width: 600px) {
    .slider__holder {
        margin-top: 60px;
    }
}

.slider__item {
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    overflow: hidden;
    width: 100%;
    opacity: 1;
    cursor: pointer;
}

.slider__item-content {
    padding: 40px;
}

@media (max-width: 600px) {
    .slider__item-content {
        padding: 32px 32px;
    }
}

@media (max-width: 375px) {
    .slider__item-content {
        padding: 40px 24px;
    }
}

.slider__item-text {
    padding: 60px 0;
}

/*--------------------------------------------------------------
3.0 Bullets
--------------------------------------------------------------*/
.bullets {
    z-index: 10;
    display: block;
    width: auto;
    height: 10px;
    height: 10px;
    margin: 0 auto;
    margin-top: 8px;
    margin-bottom: 16px;
    text-align: center;
}

@media (max-width: 600px) {
    .bullets {
        margin-top: 32px;
    }
}

.bullets__item {
    display: inline-block;
    width: 10px;
    height: 10px;
    margin: 0 4px;
    border-radius: 6px;
    background: rgba(0, 0, 0, 0.2);
    cursor: pointer;
}

.bullets__item:hover {
    background: #FFF;
}

/*--------------------------------------------------------------
4.0 Section
--------------------------------------------------------------*/
.section {
    background: #dbdbdb;
    position: relative;
    width: 100%;
    padding: 50px 24px;
    text-align: center;
}

.section__entry {
    width: 100%;
    max-width: 380px;
    margin: 0 auto;
}

.section__entry--center {
    text-align: center;
}

.section__title {
    display: block;
    padding-bottom: 12px;
}

.section__text {
    display: block;
}

/*--------------------------------------------------------------
5.0 Button
--------------------------------------------------------------*/
.button {
    display: inline-block;
    height: 44px;
    padding: 12px 16px;
    font-weight: 500;
    line-height: 20px;
    color: #FFF;
    border-radius: 3px;
    background: rgba(0, 0, 0, 0.2);
}

.button:hover {
    background: rgba(0, 0, 0, 0.1);
}

/*--------------------------------------------------------------
4.0 All Tutorials
--------------------------------------------------------------*/
/*--------------------------------------------------------------
>>> TABLE OF CONTENTS:
----------------------------------------------------------------
1.0 Labs bar
--------------------------------------------------------------*/
/*--------------------------------------------------------------
1.0 Labs bar
--------------------------------------------------------------*/
.labs-bar {
    position: fixed;
    z-index: 1000;
    top: 0;
    right: 0;
    left: 0;
    display: block;
    width: 100%;
    padding: 8px 0px;
    background: rgba(0, 0, 0, 0.2);
}

.labs-bar__inner {
    position: relative;
    display: block;
    width: 100%;
    max-width: 1200px;
    margin: 0 auto;
    padding: 0 20px;
}

.labs-bar__nav {
    position: relative;
    display: block;
    float: left;
}

.labs-bar__link {
    position: relative;
    display: inline-block;
    padding: 10px;
    padding-left: 30px;
    font-size: 12px;
    font-weight: 700;
    line-height: 20px;
    color: #FFF;
    letter-spacing: 0.5px;
    text-transform: uppercase;
}

.labs-bar__link:hover:before {
    left: -16px;
}

.labs-bar__link:before {
    position: absolute;
    top: 50%;
    left: 0;
    display: block;
    width: 24px;
    height: 14px;
    background-image: url("/images/general/arrow__right--white.svg");
    background-repeat: no-repeat;
    background-position: left;
    background-size: cover;
    content: '';
    transform: translateY(-50%);
    transition: all 0.2s ease;
}

.labs-bar__branding {
    display: inline-block;
    font-size: 20px;
    line-height: 20px;
    color: #FFF;
}

.labs-bar__logo {
    width: auto;
    height: 18px;
    margin-top: 5px;
    margin-left: 10px;
}

.labs-bar__logo:hover {
    opacity: 0.8;
}

.labs-bar__button {
    float: right;
    padding: 10px 16px;
    font-size: 12px;
    font-weight: 700;
    line-height: 20px;
    color: #FFF;
    border-radius: 3px;
    background: rgba(0, 0, 0, 0.2);
    letter-spacing: 0.5px;
    text-transform: uppercase;
}

.labs-bar__button:hover {
    background: rgba(0, 0, 0, 0.1);
}

.hidden {
    display: none;
}

.stamp {
    position: relative;
}

.stamp:after {
    border: solid .1em red;
    border-radius: .2em;
    color: red;
    content: 'current';
    font-size: 50px;
    font-weight: bold;
    line-height: 1;
    opacity: 0;
    position: absolute;
    padding: .1em .5em;
    margin: 0 auto;
    top: 80px;
    left: 10%;
    text-transform: uppercase;
    opacity: 0;
    transform-origin: 50% 50%;
    transform: rotate(-2deg) scale(5);
    transition: all .3s cubic-bezier(0.6, 0.04, 0.98, 0.335);
}

.loaded .stamp:after {
    opacity: .55;
    transform: rotate(-15deg) scale(1);
    z-index: -1;
}

.stamped {
    position: absolute;
    border: solid .1em red;
    border-radius: .2em;
    color: red;
    font-size: 50px;
    font-weight: bold;
    line-height: 1;
    opacity: .55;
    padding: .1em .5em;
    margin: 0 auto;
    top: 80px;
    left: 10%;
    text-transform: uppercase;
    transform-origin: 50% 50%;
    transform: rotate(-15deg) scale(1);
    z-index: -1;
}

div.positioner {position: absolute; right: 0;} /*may not be needed: see below*/

.cd-top {
    display: inline-block;
    height: 40px;
    width: 40px;
    margin-left: 35px;
    position: fixed;
    bottom: 100px;
    z-index: 10;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.05);
    /* image replacement properties */
    overflow: hidden;
    text-indent: 100%;
    white-space: nowrap;
    background: rgba(232, 98, 86, 0.8) url(../images/slider/cd-top-arrow.svg) no-repeat center 50%;
    visibility: hidden;
    opacity: 0;
    -webkit-transition: opacity .3s 0s, visibility 0s .3s;
    -moz-transition: opacity .3s 0s, visibility 0s .3s;
    transition: opacity .3s 0s, visibility 0s .3s;
}

.cd-top.cd-is-visible, .cd-top.cd-fade-out, .no-touch .cd-top:hover {
    -webkit-transition: opacity .3s 0s, visibility 0s 0s;
    -moz-transition: opacity .3s 0s, visibility 0s 0s;
    transition: opacity .3s 0s, visibility 0s 0s;
}

.cd-top.cd-is-visible {
    /* the button becomes visible */
    visibility: visible;
    opacity: 1;
}

.cd-top.cd-fade-out {
    /* if the user keeps scrolling down, the button is out of focus and becomes less visible */
    opacity: .5;
}

.no-touch .cd-top:hover {
    background-color: #e86256;
    opacity: 1;
}

.clarification p {
    background: #dbdbdb;
    margin-top: 5em;
    font-size: 24px;
    font-weight: 400;
    line-height: 42px;
    color: #3E4954;
}

.clarification a {
    position: relative;
    display: inline-block;
    font-size: 24px;
    color: #3E4954;
}

.cl-effect-7 a {
    padding: 0px 5px;
    margin-bottom:5px;
    color: #566473;
    text-shadow: none;
    font-weight: 700;
}

.cl-effect-7 a::before,
.cl-effect-7 a::after {
    position: absolute;
    top: 100%;
    left: 0;
    width: 100%;
    height: 3px;
    background: #566473;
    content: '';
    -webkit-transition: -webkit-transform 0.3s;
    -moz-transition: -moz-transform 0.3s;
    transition: transform 0.3s;
    -webkit-transform: scale(0.85);
    -moz-transform: scale(0.85);
    transform: scale(0.85);
}

.cl-effect-7 a::after {
    opacity: 0;
    -webkit-transition: top 0.3s, opacity 0.3s, -webkit-transform 0.3s;
    -moz-transition: top 0.3s, opacity 0.3s, -moz-transform 0.3s;
    transition: top 0.3s, opacity 0.3s, transform 0.3s;
}

.cl-effect-7 a:hover::before,
.cl-effect-7 a:hover::after,
.cl-effect-7 a:focus::before,
.cl-effect-7 a:focus::after {
    -webkit-transform: scale(1);
    -moz-transform: scale(1);
    transform: scale(1);
}

.cl-effect-7 a:hover::after,
.cl-effect-7 a:focus::after {
    top: 0%;
    opacity: 1;
}
