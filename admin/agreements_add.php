<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
session_start();

$page = "company-data";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/admin.css" />
        
        <style type="text/css">
            .content {
                height: 50rem;
                width: 80%;
                padding: 1rem;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_device" action="agreements_process.php?action=add">
                                <textarea class="content" name="agreement_content" class="box">
                                </textarea> 
                                <br/>
                                <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $("#data_device").validate({
            rules: {
                serial_number: "required"
            }, messages: {
                serial_number: "Please insert serial number"
            },
            errorLabelContainer: $("#data_device div.error")
        });
    </script>
</html>   