<?php

/*
ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );
//*/

include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$idCompany = $_GET['companyId'];
if ( is_null( $idCompany ) ) {
	header( "Location: index.php" );
	exit();
}
$servicios = new Servicios();
$oCompany  = $servicios->getCompanyById( $idCompany );

$vAgreements = $servicios->getAllCompanyAgreements( $oCompany->getId() );

$page = "agreement history";
$path = '../';

?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
<head>
    <title>NORAD | Radon Detection System</title>
    <meta name="keywords" content=""/>
    <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css"/>
    <link type="text/css" rel="stylesheet" href="company_agreement_history.css"/>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src='../js/jquery-1.9.1.js'></script>
    <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
</head>
<body>
<div id="container">
    <header><?php include_once( $path . 'includes/header-admin.php' ); ?></header>
    <div id="wrapper" class="page-company-data">
        <div id="main">
            <!--                <div id="menu-admin">-->
			<?php //include_once( $path . 'includes/menu-admin.php' ); ?><!--</div>-->
            <div>
                <div class="clarification">
                    <p>This is <span style="font-weight: 700;"><?php echo $oCompany->getName(); ?></span> agreement
                        history of changes.</p>
                </div>
				<?php if ( count( $vAgreements ) > 0 ) { ?>
                    <div class="fullwidth">
                        <div class="timeline group">
							<?php
							foreach ( $vAgreements as $oAgreement ) {
								?>
                                <div class="item">
                                    <h2><?php echo date( "F jS, Y · g:i a", strtotime( $oAgreement->date ) ); ?></h2>
                                    <h3>Agreement #<?php echo $oAgreement->agreement_id; ?></h3>
                                    <h4><?php if ($oAgreement->agreement_accepted) {
                                        echo "<span class='accepted'>Accepted on ".date( "F jS, Y · g:i a", strtotime( $oAgreement->agreement_accepted ) )."</span>";
	                                    }?></h4>
                                    <p><?php echo $oAgreement->agreement_content; ?></p>
                                </div>
								<?php
							}
							?>
                        </div>
                    </div>
				<?php } else { ?>
                    <div class="emptystate">
                        <p>There aren't any agreement previously selected.</p>
                    </div>
				<?php } ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
