<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';
session_start();

$servicios = new Servicios();

//Pagination
$perpage = $GLOBAL_SETTINGS['admin.devices.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
//$total = count($servicios->getDevices());


$orderBy = "";
$dir = "";

if (isset($_GET['orderBy'])) {
    $orderBy = $_GET['orderBy'];
    $dir = $_GET['dir'];
}

$filter = "";
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
}

$vDevices = $servicios->getDevicesPag($from, $perpage, $orderBy, $dir, $filter);

$vDevices_count = $servicios->getDevicesPag(0, 9999999, $orderBy, $dir, $filter);
$total = count($vDevices_count);

$pagination = new Pagination($currentPage, $total, 5, $perpage);

$page = "devices";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/admin.css" />
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <div class="conttxt" style="display:none;">
                                <input class="box" type="text" placeholder="filter" id="inputFilter" /> 

                                <input id="btnFilterContains" class="button" type="button" value="{..}" />
                                <input id="btnFilterEquals" class="button" type="button" value="=" />
                                <input id="btnFilterMajor" class="button" type="button" value=">" />
                                <input id="btnFilterMinor" class="button" type="button" value="<" />
                                <input id="btnFilterClear" class="button" type="button" value="Clear" style="background-color: #FF8784" />

<!--<input id="buttonFilter" class="button" type="button" value="Filter" />-->



                                <!--<p><a href="devices_add.php">New device</a></p>-->
                            </div>

                            <div class="conttxt" style="display:none;">
                                <?php
                                $str_filter = trim(str_replace('WHERE', '', $_GET['filter']));
                                $str_filter = str_replace('*', '', $str_filter);
                                $str_filter = str_replace('like', 'like ', $str_filter);
                                $str_filter = str_replace('%)', ' ', $str_filter);

                                echo $str_filter;
                                ?>
                            </div>

                            <table id="table-t">
                                <tr>
                                    <?php
                                    $serial = "device_serial_number";
                                    $name = "device_serial_number";
                                    $company = "company_name";
                                    $calDate = "calibration_date_time";

                                    $dirSerial = "ASC";
                                    $dirName = "ASC";
                                    $dirCompany = "ASC";
                                    $dirCalDate = "ASC";

                                    if ($_GET['orderBy'] == $serial) {
                                        if ($_GET['dir'] == "ASC") {
                                            $dirSerial = "DESC";
                                        } else {
                                            $dirSerial = "ASC";
                                        }
                                    } else if ($_GET['orderBy'] == $name) {
                                        if ($_GET['dir'] == "ASC") {
                                            $dirName = "DESC";
                                        } else {
                                            $dirName = "ASC";
                                        }
                                    } else if ($_GET['orderBy'] == $company) {
                                        if ($_GET['dir'] == "ASC") {
                                            $dirCompany = "DESC";
                                        } else {
                                            $dirCompany = "ASC";
                                        }
                                    } else if ($_GET['orderBy'] == $calDate) {
                                        if ($_GET['dir'] == "ASC") {
                                            $dirCalDate = "DESC";
                                        } else {
                                            $dirCalDate = "ASC";
                                        }
                                    }
                                    ?>


                                    <th><a href="<?php echo 'devices.php?pag=' . $currentPage . '&orderBy=' . $serial . '&dir=' . $dirSerial . '&filter=' . $filter ?>" >S/N</a></th>
                                    <th><a href="<?php echo 'devices.php?pag=' . $currentPage . '&orderBy=' . $name . '&dir=' . $dirName . '&filter=' . $filter ?>" >Name</a></th>
                                    <th><a href="<?php echo 'devices.php?pag=' . $currentPage . '&orderBy=' . $company . '&dir=' . $dirCompany . '&filter=' . $filter ?>" >Company</a></th>
                                    <th><a href="<?php echo 'devices.php?pag=' . $currentPage . '&orderBy=' . $calDate . '&dir=' . $dirCalDate . '&filter=' . $filter ?>" >Cal date</a></th>
                                    <th>Actions</th>
                                </tr>
                                <?php
                                foreach ($vDevices as $oDevice) {
                                    $oDeviceCompany = $servicios->getCompanyByIdDevice($oDevice->getId());
                                    $calibrationDate = $servicios->getCalibrationDate($servicios->getDeviceById($oDevice->getId()));
                                    $oCompany = NULL;
                                    if (!is_null($oDeviceCompany)) {
                                        $oCompany = $servicios->getCompanyById($oDeviceCompany->getIdCompany());
                                    }
                                    ?>
                                    <tr>
                                        <td><?php echo $oDevice->getSerialNumber(); ?></td>
                                        <td><?php echo $oDevice->getName(); ?></td>
                                        <td><?php echo (!is_null($oCompany)) ? $oCompany->getName() : "-"; ?></td>
                                        <td> <?php echo (!is_null($calibrationDate)) ? Utilidades::dateFormat($calibrationDate, "m-d-Y H:i:s") : "-"; ?> </td>
                                        <td>
                                            <div class="actions-container">
                                                <img class='actions-img' src='../images/actions.png' />
                                                <div class='actions-list'>
                                                    <ul>
                                                        <?php
                                                        if (!is_null($oCompany)) {
                                                            echo '<li><a href="devices_unassign.php?id=' . $oDevice->getId() . '&company_id='. $oCompany->getId() .'">Unassign</a></li>';
                                                        } else {
                                                            echo '<li><a href="devices_assign.php?id=' . $oDevice->getId() . '">Assign</a></li>';
                                                        }
                                                        ?>
                                                        <li><a href="devices_calibrate.php?id=<?php echo $oDevice->getId() ?>">Calibrate</a></li>
                                                        <li><a href="device_history.php?id=<?php echo $oDevice->getId() ?>">Device History</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>


                                    <?php
                                }
                                ?>

                            </table>
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>


        <script type='text/javascript'>
            $(document).ready(function () {
                var orderBy = '<?php echo $_GET['orderBy'] ?>';

                if (orderBy != '') {
                    $('.conttxt').show();
                    $("#inputFilter").attr("placeholder", orderBy);
                }
            });

            $("#btnFilterContains").click(function () {
                var orderBy = '<?php echo $_GET['orderBy'] ?>';
                var filter = '<?php echo $_GET['filter'] ?>';

                if (filter == '') {
                    filter = " WHERE " + orderBy + " like" + $("#inputFilter").val() + "%*)";
                }
                else {
                    filter = filter + " AND " + orderBy + " like" + $("#inputFilter").val() + "%*)";
                }

                window.location.href = "devices.php?pag=1&orderBy=<?php echo $_GET['orderBy'] ?>&dir=<?php echo $_GET['dir'] ?>&filter=" + filter;
            });

            $("#btnFilterEquals").click(function () {
                var orderBy = '<?php echo $_GET['orderBy'] ?>';
                var filter = '<?php echo $_GET['filter'] ?>';

                if (filter == '') {
                    filter = " WHERE " + orderBy + '=*' + $("#inputFilter").val() + '*';
                }
                else {
                    filter = filter + " AND " + orderBy + '=*' + $("#inputFilter").val() + '*';
                }

                window.location.href = "devices.php?pag=1&orderBy=<?php echo $_GET['orderBy'] ?>&dir=<?php echo $_GET['dir'] ?>&filter=" + filter;
            });

            $("#btnFilterMajor").click(function () {
                var orderBy = '<?php echo $_GET['orderBy'] ?>';
                var filter = '<?php echo $_GET['filter'] ?>';

                if (filter == '') {
                    filter = " WHERE " + orderBy + ' > ' + $("#inputFilter").val();
                }
                else {
                    filter = filter + " AND " + orderBy + ' > ' + $("#inputFilter").val();
                }

                window.location.href = "devices.php?pag=1&orderBy=<?php echo $_GET['orderBy'] ?>&dir=<?php echo $_GET['dir'] ?>&filter=" + filter;
            });

            $("#btnFilterMinor").click(function () {
                var orderBy = '<?php echo $_GET['orderBy'] ?>';
                var filter = '<?php echo $_GET['filter'] ?>';

                if (filter == '') {
                    filter = " WHERE " + orderBy + ' < ' + $("#inputFilter").val();
                }
                else {
                    filter = filter + " AND " + orderBy + ' < ' + $("#inputFilter").val();
                }

                window.location.href = "devices.php?pag=1&orderBy=<?php echo $_GET['orderBy'] ?>&dir=<?php echo $_GET['dir'] ?>&filter=" + filter;
            });

            $("#btnFilterClear").click(function () {
                window.location.href = "devices.php?pag=1&orderBy=<?php echo $_GET['orderBy'] ?>&dir=<?php echo $_GET['dir'] ?>&filter=";
            });




            $.each($("#pagination a"), function () {
//                if (this  ) {
                var orderBy = '<?php echo $_GET['orderBy'] ?>';
                var dir = '<?php echo $_GET['dir'] ?>';
                var filter = '<?php echo $_GET['filter'] ?>';

                var value = $(this).attr('href');

                if (filter != '') {
                    value = value + '&orderBy=&' + orderBy + 'dir=' + dir + '&filter=' + filter;
                    $(this).attr('href', value);
                }
//                }
            });

        </script>

    </body>
</html>   