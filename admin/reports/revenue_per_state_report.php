<div class="logo"><img src="../../img/logo_envirolabs.png" width="240" /></div>
<div class="right" style="margin-top:0.8em;">
    <div class="title">Devices not used</div>
    <div class="text"><strong><?php echo date('l, d F Y h:i:s A'); ?></strong></div>

</div>

<div id="wrapper" class="informe">
    <div id="container">

        <div id="main" style="margin-top:5px; height: 83%;">
            <div class="rayastxt" style="border:none; font:normal 12pt Arial, Helvetica, sans-serif;">

                <?php
                if ($filter != '') {
                    echo "<p style='font-size: 0.8rem; text-transform: uppercase; text-align:center;'>";
                    echo $filter;
                    echo "</p>";
                }
                ?>

                <div id="reports_table_content">
                    <table style="width:85%">
                        <tr>
                            <th>Device S/N</th>
                            <th>Company</th>
                            <th>Last Used</th>
                        </tr>
                        <tbody>
                            <?php
                            foreach ($data as $row) {
                                echo '<tr>';
                                echo'   <td>' . $row->device_serial_number . '</td>';
                                echo'   <td>' . $row->company_name . '</td>';
                                echo'   <td>' . $row->last_used . '</td>';
                                echo '</tr>';
                            }
                            ?>
                        </tbody>
                    </table>
                </div>

            </div>

        </div>	

        <div class='footer' style="border-top:1px solid black;">
            <div>13-15 East Deer Park Drive, Suite 202, Gaithersburg, MD 20877</div>
            <div style='text-align: right;'>info@envirolabs-inc.com</div>
            <div>Tel. (301) 468-6666, (703) 242-0000</div>
            <div style='text-align: right;'>www.envirolabs-inc.com</div>
        </div>
    </div>