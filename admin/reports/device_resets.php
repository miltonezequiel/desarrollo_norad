<?php

include( "reports.php");
$report = new Reports();

$dateFrom = $_GET['dateFrom'];
$dateTo = $_GET['dateTo'];
$device = $_GET['deviceSerialNumber'];

$select = " SELECT dr_date_time, ax_devices.device_serial_number, ax_companies.company_name ";

$from = "   FROM ax_devices_resets
            INNER JOIN ax_devices ON device_id = dr_device_id
            INNER JOIN ax_devices_companies on dr_device_id = dc_device_id and dc_end_date is null
            INNER JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
        ";

$where = "";

if ($dateFrom != '' || $dateTo != '') {
    $where = " WHERE 1 = 1 ";
    $filter = "Number of resets per device in the date range ";

    if ($dateFrom != '') {
        $where = $where . " AND dr_date_time >= '" . $dateFrom . "' ";
        $filter = $filter . " from " . date('l, d F Y', strtotime($dateFrom)) . ". ";
    }

    if ($dateTo != '') {
        $where = $where . " AND dr_date_time <= '" . $dateTo . "' ";
        $filter = $filter . " to " . date('l, d F Y', strtotime($dateTo)) . ". ";
    }
}

if ($device == '') {
    $device = 'All';
} else {
    $where = $where . " AND device_serial_number = '" . $device . "' ";
}

$filter = $filter . " Filtering Device: " . $device;



$groupBy = "";
$orderBy = "";

$fileName = "device_resets_report.php";

$sql = $select . $from . $where . $groupBy . $orderBy;

$html = $report->getHtml($sql, $fileName, $filter);
$report->getReport($html, 'device_resets_report' . date('d_F_Y') . '.pdf');
?>