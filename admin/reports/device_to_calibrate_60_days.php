<?php

include( "reports.php");
$report = new Reports();

$dateFrom = $_GET['dateFrom'];
$dateTo = $_GET['dateTo'];




# WHERE test_report_date > ''



$select = " SELECT ax_devices.device_serial_number,
            ax_companies.company_name, 
            date(MAX(calibration_date_time)) as LAST_CALIBRATION_DATE,
            DATE_ADD(date(MAX(calibration_date_time)), INTERVAL 1 YEAR) as NEXT_CALIBRATION_DATE, 
            DATE_ADD(DATE_ADD(date(MAX(calibration_date_time)), INTERVAL 1 YEAR), INTERVAL - 60 DAY) as 60_DAYS_BEFORE_NEXT_CALIBRATION,
            DATEDIFF(DATE_ADD(date(MAX(calibration_date_time)), INTERVAL 1 YEAR), NOW()) DAYS_LEFT_TO_CALIBRATE ";

$from = " 
            FROM ax_devices_calibration
            INNER JOIN ax_devices ON device_id = calibration_device_id
            INNER JOIN ax_devices_companies on device_id = dc_device_id and dc_end_date is null
            INNER JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
        ";

$where = "
            GROUP BY device_serial_number, company_name
            HAVING NOW()  >= DATE_ADD(DATE_ADD(date(max(calibration_date_time)), INTERVAL 1 YEAR), INTERVAL - 60 DAY)
        ";

$filter = "";


$groupBy = "";
$orderBy = "";

$fileName = "device_to_calibrate_60_days_report.php";

$sql = $select . $from . $where . $groupBy . $orderBy;

$html = $report->getHtml($sql, $fileName, $filter);
$report->getReport($html, 'device_to_calibrate_60_days_report_' . date('d_F_Y') . '.pdf');
?>