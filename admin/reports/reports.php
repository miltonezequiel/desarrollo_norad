<?php

include( "../../datos/general.php");
include( "../../mpdf/mpdf.php");

class Reports {

    public static function getHtml($sql, $fileName, $filter) {
        $report = new DataGeneral();
        $data = $report->executeQuery($sql);

        ob_start();

        include $fileName;
        $html = ob_get_clean();
        return $html;
    }

    public function getReport($html, $reportTitle) {
        $mpdf = new mPDF('c', 'Letter');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list
        $stylesheet = file_get_contents('../../css/report.css');
        $mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

        header('Content-Type: application/pdf');
        $mpdf->WriteHTML($html, 2);
        //$mpdf->Output();
        ob_clean();
        header('Content-Type: application/pdf');
        $mpdf->Output($reportTitle,'I');
    }

}

?>