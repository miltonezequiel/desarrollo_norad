<?php

include( "reports.php");
$report = new Reports();

$dateFrom = $_GET['dateFrom'];
$dateTo = $_GET['dateTo'];

$select = " SELECT device_serial_number, c.company_name, COUNT(test_id) count";

$from = " FROM ax_tests 
		RIGHT JOIN ax_devices_companies ON dc_id = test_di_dc_id 
		INNER JOIN ax_companies c ON dc_company_id = c.company_id 
		INNER JOIN ax_devices ON ax_devices.device_id = dc_device_id
        ";

$where = "";

if ($dateFrom != '' || $dateTo != '') {
    $where = "WHERE test_report_date IS NULL OR ";
    $filter = "Number of test per device in the date range ";

	if ($dateFrom != '' && $dateTo != '') $where = $where . " ( ";
    if ($dateFrom != '') {
        $where = $where . " test_report_date >= '" . $dateFrom . "' ";
        $filter = $filter . " from " . date('l, d F Y', strtotime($dateFrom)) . ". ";
    }

	if ($dateFrom != '' && $dateTo != '') $where = $where . " AND ";
    if ($dateTo != '') {
        $where = $where . " test_report_date <= '" . $dateTo . "' ";
        $filter = $filter . " to " . date('l, d F Y', strtotime($dateTo)) . ". ";
    }

	if ($dateFrom != '' && $dateTo != '') $where = $where . " ) ";
}


$groupBy = " GROUP BY device_id, device_serial_number, dc_company_id";
$orderBy = " ORDER BY device_serial_number ";

$fileName = "tests_per_device_report.php";

$sql = $select . $from . $where . $groupBy . $orderBy;

$html = $report->getHtml($sql, $fileName, $filter);
$report->getReport($html, 'tests_per_device_' . date('d_F_Y') . '.pdf');
?>