<?php

include( "reports.php");
$report = new Reports();

$dateFrom = $_GET['dateFrom'];
$dateTo = $_GET['dateTo'];




# WHERE test_report_date > ''



$select = " SELECT device_serial_number, company_name, max(test_report_date) last_used ";

$from = " FROM ax_tests
          INNER JOIN ax_devices_companies ON ax_devices_companies.dc_id = test_di_dc_id
          INNER JOIN ax_devices on ax_devices.device_id = ax_devices_companies.dc_device_id
          INNER JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
        ";

$where = "
            GROUP BY device_id, dc_company_id, device_serial_number
            HAVING max(test_report_date) <= '". $dateFrom ."'
            ORDER BY test_report_date DESC
        ";

$filter = " Devices not used since " . date('l, d F Y', strtotime($dateFrom))  . ". ";


$groupBy = "";
$orderBy = "";

$fileName = "device_not_used_report.php";

$sql = $select . $from . $where . $groupBy . $orderBy;

$html = $report->getHtml($sql, $fileName, $filter);
$report->getReport($html, 'device_not_used_report_' . date('d_F_Y') . '.pdf');
?>