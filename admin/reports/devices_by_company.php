<?php

include( "reports.php");
$report = new Reports();

$dateFrom = $_GET['dateFrom'];
$companyName = $_GET['companyName'];
        
$select = " SELECT device_serial_number, company_name, dc_start_date as start_date, dc_end_date as end_date ";

$from = " FROM ax_tests
          INNER JOIN ax_devices_companies ON ax_devices_companies.dc_id = test_di_dc_id
          INNER JOIN ax_devices on ax_devices.device_id = ax_devices_companies.dc_device_id
          INNER JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
        ";

$groupBy = " GROUP BY device_id, dc_company_id, device_serial_number ";
$orderBy = " ORDER BY device_serial_number ";

if ($dateFrom == '') {
  $dateFrom = date('YYYY-MM-dd');
}

$where = " WHERE ax_companies.company_name like '" . $companyName . "' AND dc_start_date <= '" . $dateFrom . "' AND (dc_end_date IS NULL OR dc_end_date >= '" . $dateFrom . "')";

$filter = "Company " . $companyName . " on date " . date('l, d F Y', strtotime($dateFrom)) ;


$fileName = "devices_by_company_report.php";

$sql = $select . $from . $where . $groupBy . $orderBy;

$html = $report->getHtml($sql, $fileName, $filter);
$report->getReport($html, 'devices_by_company_report' . date('d_F_Y') . '.pdf');
?>