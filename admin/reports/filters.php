<?php
include '../../login_check.php';
include '../login_admin_check.php';
include_once '../../init.php';
session_start();

$page = "filters";
$path = '../../';
$report = $_GET['report'];
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../../js/jquery-1.9.1.js'></script>

        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/jquery.ui.timepicker.css" />

        <script src='../../js/jquery-ui.js'></script>
        <script src='../../js/jquery.inputmask.bundle.js'></script>

        <style type='text/css'>
            .title {
                font-size: 1.5rem;
                text-transform: uppercase;
                margin-bottom: 1rem;
            }

            #inputDeviceSerialNumber, #inputCompanyName {
                display: none;
            }
            #divButtonFilter {
                float:left;
                text-align: right;
                width:100%;
            }

            #divButtonFilter input{
                margin-right: 9rem;
            }

            .page-company-data #container2 #main .form-cd input.box {
                width:10rem;
            }

            .conttxt p {
                font: 1.8rem/25px robotobold,Geneva,sans-serif !important;
                text-transform: uppercase !important;
            }
        </style>


    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <div class='conttxt'><p><?php echo $report ?> </p></div> 
                            <!--<div class="conttxt">-->
                            <input class="box" type="text" placeholder="Date from" id="inputDateFrom" /> 
                            <input class="box" type="text" placeholder="Date to" id="inputDateTo" /> 

                            <input class="box" type="text" placeholder="Device Serial Number" id="inputDeviceSerialNumber" /> 

                            <input class="box" type="text" placeholder="Company Name" id="inputCompanyName" />


                            <!--</div>-->
                        </div>

                        <div id="divButtonFilter">
                            <input id='btnFilter' type="button" class="button" value="Filter" />
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>


        <script type='text/javascript'>
            $(document).ready(function () {
                $("#inputDateFrom").datepicker();
                $("#inputDateTo").datepicker();

                $.datepicker.regional['en'] = {
                    closeText: 'Cerrar',
                    prevText: '<Ant',
                    nextText: 'Sig>',
                    currentText: 'Hoy',
                    dateFormat: 'yy-mm-dd',
                    changeMonth: true,
                    changeYear: true,
                    firstDay: 1,
                    isRTL: false,
                    showMonthAfterYear: false,
                    yearSuffix: ''
                };
                $.datepicker.setDefaults($.datepicker.regional['en']);

                var rpt = '<?php echo $report ?>';

                if (rpt == 'device_not_used') {
                    $("#inputDateFrom").attr("placeholder", "Date since...");
                    $("#inputDateTo").hide();
                }
                else if (rpt == 'devices_by_company') {
                    $("#inputCompanyName").show();
                    $("#inputDateTo").hide();
                }
                else if (rpt == 'device_resets') {
                    $("#inputDeviceSerialNumber").show();
                }

                $("#btnFilter").click(function () {
                    var report = '<?php echo $report ?>';
                    var href;
                    if (report == 'tests_per_device') {
                        href = report + '.php?dateFrom=' + $("#inputDateFrom").val() + "&dateTo=" + $("#inputDateTo").val();
                        window.open(href, '_blank');
                    } else if (report == 'device_not_used') {
                        href = report + '.php?dateFrom=' + $("#inputDateFrom").val();
                        window.open(href, '_blank');
                    } else if (report == 'device_resets') {
                        href = report + '.php?dateFrom=' + $("#inputDateFrom").val() + "&dateTo=" + $("#inputDateTo").val() + "&deviceSerialNumber=" + $("#inputDeviceSerialNumber").val();
                        window.open(href, '_blank');
                    } else if (report == 'devices_by_company') {
                        href = report + '.php?dateFrom=' + $("#inputDateFrom").val() + "&companyName=" + $("#inputCompanyName").val();
                        window.open(href, '_blank');
                    }
                });


            });
        </script>

    </body>
</html>   