<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();

if (!isset($_GET['d']) || $_GET['d'] == 1) {
    $disabled = true;
} else {
    $disabled = false;
}

//Pagination
$perpage = $GLOBAL_SETTINGS['admin.companies.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
$total = count($servicios->getCompanies($disabled));
$vCompanies = $servicios->getCompaniesPag($from, $perpage, $disabled);

$pagination = new Pagination($currentPage, $total, 5, $perpage, "d=" . ($disabled) ? "1" : "0");
$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/admin.css" />
        <style>
           
            .actions-list li a {
                padding: 0.4rem;
            }
            
            .actions-list ul {
                width: 10rem;
            }

        </style>
        
        
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <!--<div class="conttxt"><a href="<?php echo $path; ?>admin/companies_add.php"><img src="<?php echo $path; ?>images/admin-btn-create.jpg" class="img"></a></div>-->
                            <table id="table-t">
                                <tr>
                                    <th>ID</th>
                                    <th class="mid">Name</th>
                                    <th>Actions</th>
                                </tr>
                                <?php
                                foreach ($vCompanies as $oCompany) {
                                    ?>
                                    <tr <?php if (!$oCompany->getStatus()) echo 'id="disable"'; ?>>
                                        <td><?php echo $oCompany->getId(); ?></td>
                                        <td><?php echo $oCompany->getName(); ?></td>
                                        <td>
                                            <div class="actions-container">
                                                <img class='actions-img' src='../images/actions.png' />
                                                <div class='actions-list'>
                                                    <ul>
                                                        <li><a href="companies_data_edit.php?idc=<?php echo $oCompany->getId(); ?>">Data edit</a></li>
                                                        <li><a href="comp_change_username.php?idc=<?php echo $oCompany->getId(); ?>">Change Username</a></li>
                                                        <li><a href="comp_change_password.php?idc=<?php echo $oCompany->getId(); ?>">Change Password</a></li>
                                                        <li><a href="companies_status.php?id=<?php echo $oCompany->getId(); ?>"><?php echo ($oCompany->getStatus() == 1) ? "Disable" : "Enable"; ?></a></li>
                                                        <li><a href="inspectors.php?idc=<?php echo $oCompany->getId(); ?>">Inspectors</a></li>
                                                        <li><a href="tests.php?idc=<?php echo $oCompany->getId(); ?>">Tests</a></li>
                                                        <li><a href="devices_history.php?idc=<?php echo $oCompany->getId(); ?>">Device history</a></li>
                                                        <li><a href="companies_payment.php?idc=<?php echo $oCompany->getId(); ?>">Payment</a></li>
                                                        <li><a href="company_agreement_selection.php?idc=<?php echo $oCompany->getId(); ?>">Agreements</a></li>
                                                        <li><a href="company_cc_payment.php?idc=<?php echo $oCompany->getId(); ?>">Generate Payment</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                    <?php
                                }
                                ?>

                            </table>
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   