<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/controllers/users_controller.php';
include_once ROOT_DIR . '/controllers/companies_controller.php';
include_once ROOT_DIR . '/controllers/companies_fee_controller.php';
include_once ROOT_DIR . '/controllers/agreements_controller.php';

include_once ROOT_DIR . '/servicios/servicios.php';

$action = $_GET['action'];
$redirectLocation = 'index.php';
if (!isset($action) || empty($action)) { //si no hay accion
    header('Location:' . $redirectLocation);
    return;
}

$usersController = new UsersController();
$companiesController = new CompaniesController();
$agreementController = new AgreementsController();
$companiesFeeController = new CompaniesFeeController();

$servicios = new Servicios();
if ($action == "add") {
    $oUser = $usersController->uploadUser();
    $oCompany = $companiesController->uploadCompany($oUser->getId());
    $agreementController->setCurrentAgreementFromDefault($oCompany->getId());

    
    $companiesFeeController->addCompanyFee(0, 1, 50, $oCompany->getId());
    $companiesFeeController->addCompanyFee(2, 4, 25, $oCompany->getId());
    $companiesFeeController->addCompanyFee(5, 9999, 10, $oCompany->getId());
    
    $redirectLocation = 'companies.php';
} elseif ($action == "edit") {
    //TODO
} elseif ($action == "status") {
    $idCompany = $_GET['idCompany'];
    $oCompany = $companiesController->changeStatusCompany($idCompany);
    $redirectLocation = 'companies.php';
}

header('Location:' . $redirectLocation);
