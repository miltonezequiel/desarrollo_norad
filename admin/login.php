<?php
session_start();
$page = "admin";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
        
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">
                        <div class="form-login">
                        	<div class="tit">
                            	<h1>ADMIN NORAD LOGIN</h1>
                            </div>
                            <?php
                            if (!isset($_SESSION['estadoLogin']) || !$_SESSION['estadoLogin']) {
                                ?>
                                <form id="data_login" method="post" action="index.php">
                                    <input type="text" name="username"  class="box" placeholder="User" />
                                    <input type="password" name="password"  class="box" placeholder="Password" />
                                    <div id="login"><input type="submit" class="submit" value=""/></div>
                                    <div class="txt"><p>Forgot your Password? <a href="../users/forgot_password.php?type=admin">Click Here to reset</a></p></div>
                                    
                                    <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                                </form>
                                <?php
                            } else {
                                echo "Welcome";
                                echo '<pre>';
                                var_dump($_SESSION['user']);
                                echo '</pre>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   

<script type="text/javascript">
    $("#data_login").validate({
        rules: {
            username: "required",
            password: "required"
        }, messages: {
            username: "Please insert username",
            password: "Please insert password"
        },
        errorLabelContainer: $("#data_login div.error")
    });
    
    $(document).ready(function() {
        debugger;
       <?php if(isset($_GET['msg']) && $_GET['msg'] != '') {
           echo "$('.error').append('Incorrect username or password');";
       } ?> 
    });
</script>