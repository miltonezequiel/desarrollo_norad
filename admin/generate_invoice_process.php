<?php

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

include( "../mpdf/mpdf.php");
include_once '../init.php';
include_once '../util/utilidades.php';
include_once '../entidades/payment.php';
include_once '../entidades/payment_detail.php';
include_once '../servicios/servicios.php';

$servicios = new Servicios();
$_SESSION['oCompany'] = $servicios->getCompanyById($companyId);

//get invoice total
$total = 0;
foreach (explode(",", $data) as $statement) {
    $cur_statement = $servicios->getPaymentById($statement);
    $total += $cur_statement->getPaymentTotal();
}

//create new payment type invoice
$date = date('Y-m-d H:i:s');
$oPayment = new Payment(0, $date, $total, 1, 'Invoice', 0, 1, 0);
$paymentId = $servicios->saveTestPayment($oPayment);
$oPayment = $servicios->getPaymentById($paymentId);

$aPaymentDetail = Array();

foreach (explode(",", $data) as $statement) {
    $cur_statement = $servicios->getPaymentById($statement);

    //update the payment_id in the test
    $oTest = $servicios->getTestByPaymentId($cur_statement->getPaymentId());
    $oTest->setPaymentId($paymentId);
    $servicios->updateTestPaymentId($oTest);

    $statement_details = $servicios->getPaymentDetailByPaymentId($statement);

    foreach ($statement_details as $stm_detail) {
        $site = $stm_detail->payment_details_desc;
        $testFee = $stm_detail->payment_details_subtotal;
        $quantity = $stm_detail->payment_details_quantity;
        $oPaymentDetail = new Payment_detail($site, $testFee, $paymentId, $quantity);
        $servicios->saveTestPaymentDetail($oPaymentDetail);
        array_push($aPaymentDetail, $oPaymentDetail);
    }
    
    $servicios->updatePaymentAsPayed($statement);
}

$_SESSION['oPayment'] = $oPayment;
$_SESSION['oPaymentsDetails'] = $aPaymentDetail;

$html = Utilidades::generateReceiptReport();

$mpdf = new mPDF('c', 'Letter');
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

$stylesheet = file_get_contents('../css/report.css');
$mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html, 2);


//$oTest = $_SESSION['oTest'];
//$strDocType = "Invoice";
//$paymentReportFileName = $strDocType . '_' . $oTest->getAddress() . '-' . $oTest->getCity() . '-' . $oTest->getState() . '-' . $oTest->getZip() . '_' . date('d-m-Y') . '.pdf';

$mpdf->Output();
//$mpdf->Output(ROOT_DIR . "/files/reports/" . $paymentReportFileName, 'F');
?>