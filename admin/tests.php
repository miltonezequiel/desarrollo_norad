<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';
session_start();

$servicios = new Servicios();
$idCompany = $_GET['idc'];
$oCompany = $servicios->getCompanyById($idCompany);
//Pagination
$perpage = $GLOBAL_SETTINGS['admin.tests.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
$total = count($servicios->getTestsByIdCompany($idCompany));
$vTests = $servicios->getTestsByIdCompanyPag($idCompany, $from, $perpage);

$pagination = new Pagination($currentPage, $total, 5, $perpage, "idc=" . $idCompany);

$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <style>
            
            #table-t{
                width: 100%;
                border-collapse: collapse;
				margin:10px 0 0 0;
            }

            #table-t th{
                border: 1px solid #a9a9a9;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			 #table-t th.mid {
                border-bottom:1px solid #a9a9a9;
				border-top:1px solid #a9a9a9;
				border-left:hidden;
				border-right:hidden;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }

            #table-t td{
                border: 1px solid #a9a9a9;
                background-color: #fff;
				font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			#table-t td a {
               font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:none;
            }
			
			#table-t td a:hover {
                font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:underline;
            }
       
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <div class="conttxt"><p>Company: <?php echo $oCompany->getName(); ?></p></div>
                            <table id="table-t">
                                <tr>
                                    <th>ID</th>
                                    <th class="mid">Client</th>
                                     <th class="mid">Site</th>
                                     <th class="mid">City</th>
                                     <th class="mid">Zip</th>
                                     <th class="mid">ST</th>
                                     <th class="mid">Date</th>
                                     <th class="mid">Inspector</th>
                                     <th class="mid">Device</th>
                                    <th>File</th>
                                </tr>
                                <?php
                                foreach ($vTests as $oTest) {
                                    $oInspector = $servicios->getInspectorById($oTest->getIdInspector());
                                    $oClient = $servicios->getClientById($oTest->getIdClient());
                                    ?>
                                    <tr>
                                        <td><?php echo $oTest->getId(); ?></td>
                                        <td><?php echo $oClient->getName(); ?></td>
                                        <td><?php echo $oTest->getAddress(); ?></td>
                                        <td><?php echo $oTest->getCity(); ?></td>
                                        <td><?php echo $oTest->getZip(); ?></td>
                                        <td><?php echo $oTest->getState(); ?></td>
                                        <td><?php echo Utilidades::dateFormat($oTest->getReportDate()); ?></td>
                                        <td><?php echo $oInspector->getName(); ?></td>
                                        <td><?php echo $oTest->getDeivceSerialNumber() ?></td>
                                        <td>
                                            <a href="../<?php echo $GLOBAL_SETTINGS['report.pdf.path'] . "/" . $oTest->getFile(); ?>" target="_blank">
                                                <?php echo $oTest->getFile(); ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </table>
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   