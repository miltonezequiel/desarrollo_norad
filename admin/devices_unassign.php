<?php

include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';

$idDevice = $_GET['id'];
$currentDate = date("Y-m-d");
$companyId = $_GET['company_id'];

$redirectLocation = "devices_process.php?action=unassign&id={$idDevice}&date={$currentDate}&company_id={$companyId}";
header("Location: " . $redirectLocation);
?>