<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/device.php';
session_start();

$idDevice = $_GET['id'];
if (!isset($idDevice) || is_null($idDevice)) {
    header("Location: devices.php");
}

$servicios = new Servicios();
$oDevice = $servicios->getDeviceById($idDevice);

$page = "devices";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_device" action="devices_process.php?action=calibrate">
                                <input type="hidden" name="id" class="box" value="<?php echo $oDevice->getId(); ?>"/><br/>
                                <input type="text" name="name" class="box" value="<?php echo $oDevice->getName(); ?>" readonly="true"/><br/>
                                <input type="text" name="constant" class="box" value="<?php echo (!is_null($oDevice->getError())) ? $oDevice->getError() : ""; ?>"/><br/>
                                <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $( "#data_device" ).validate({
            rules: {
                constant: "required"
            },messages: {
                name: "Please insert constant calibration"
            },
            errorLabelContainer: $("#data_device div.error")
        });
    </script>
</html>