<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company_fee.php';
include_once ROOT_DIR . '/controllers/companies_fee_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

$action = $_GET['action'];
$redirectLocation = 'company_fee.php?idc=' . $_POST['company_id'];

$companiesFeeController = new CompaniesFeeController();
$servicios = new Servicios();

$from = $_POST['company_fee_from'];
$to = $_POST['company_fee_to'];
$fee = $_POST['company_fee_fee'];
$company_id = $_POST['company_id'];

$companiesFeeController->addCompanyFee($from, $to, $fee, $company_id);

//if ($oDevice == null) {
//    
//    $redirectLocation = 'company_fee_add.php';
//} else {
//    $redirectLocation = 'company_fee_add.php?error=An error has ocurred';
//}


header('Location:' . $redirectLocation);
?>