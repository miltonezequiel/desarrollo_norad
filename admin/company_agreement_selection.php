<?php

//*
//ini_set( 'display_errors', 1 );
//ini_set( 'display_startup_errors', 1 );
//error_reporting( E_ALL );
//*/

include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$idCompany = $_GET['idc'];
if ( is_null( $idCompany ) ) {
	header( "Location: index.php" );
}
$servicios = new Servicios();
$oCompany  = $servicios->getCompanyById( $idCompany );

$vAgreements       = $servicios->getAgreements();
$oCurrentAgreement = $servicios->getCurrentAgreement( $idCompany );

$page = "agreement selection";
$path = '../';

//Post back
if ( $_POST ) {
	if ( isset( $_POST['companyId'] ) && isset( $_POST['agreementId'] ) ) {
		$servicios->setCurrentAgreement( $_POST['companyId'], $_POST['agreementId'] );
	}

	//Prevent resubmissions as reload
	header( "Location: " . $_SERVER['REQUEST_URI'] );
	exit();
}

?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
<head>
    <title>NORAD | Radon Detection System</title>
    <meta name="keywords" content=""/>
    <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css"/>
    <link type="text/css" rel="stylesheet" href="company_agreement_selection.css.php"/>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src='../js/jquery-1.9.1.js'></script>
    <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
    <script src="./company_agreement_selection.js"></script>
</head>
<body>
<div id="container">
    <header><?php include_once( $path . 'includes/header-admin.php' ); ?></header>
    <div id="wrapper" class="page-company-data">
        <div id="main">
            <!--                <div id="menu-admin">-->
			<?php //include_once( $path . 'includes/menu-admin.php' ); ?><!--</div>-->
            <div>
                <div class="clarification">
                    <p>You can change the selected <span
                                style="font-weight: 700;"><?php echo $oCompany->getName(); ?></span> current agreement
                        by choosing a different sheet. Also, <span class="cl-effect-7"><a
                                    href="company_agreement_history.php?companyId=<?php echo $oCompany->getId(); ?>">here</a></span>
                        is
                        the history of changes for
                        the same company.</p>
                </div>
                <section class="section slider">
					<?php
					$num = 1;
					foreach ( $vAgreements as $oAgreement ) {
						?>
                        <input type="radio" name="slider" id="slide-<?php echo $num ?>"
                               class="slider__radio" <?php if ( ! is_null( $oCurrentAgreement ) && $oAgreement->agreement_id == $oCurrentAgreement->getId() ) {
							echo "checked";
						} elseif ( is_null( $oCurrentAgreement ) && $num == 1 ) {
							echo "checked";

						} ?>>
						<?php
						$num ++;
					}
					?>

                    <div class="bullets">
						<?php
						$how_many = sizeof( $vAgreements );
						for ( $i = 1; $i <= $how_many; $i ++ ) {
							?>
                            <label for="slide-<?php echo $i ?>"
                                   class="bullets__item bullets__item--<?php echo $i ?>"></label>
							<?php
						}
						?>
                    </div> <!-- Bullets -->

                    <div class="slider__holder">
						<?php
						$num     = 1;
						$stamped = false;
						foreach ( $vAgreements as $oAgreement ) { ?>
                            <label id="stamping<?php echo $oAgreement->agreement_id ?>" for="slide-<?php echo $num ?>"
                                   class="slider__item slider__item--<?php echo $num ?> card">
                                <div class="slider__item-content">
                                    <div class="stamp"></div>
									<?php if ( ! is_null( $oCurrentAgreement ) && ( $oAgreement->agreement_id == $oCurrentAgreement->getId() ) ) {
										echo '<div id="' . $oAgreement->agreement_id . '" class="stamped">Current</div>';
									} ?>
									<?php echo '<a id="setLink' . $oAgreement->agreement_id . '" class="heading-3 link' . ( ! is_null( $oCurrentAgreement ) && ( $oAgreement->agreement_id == $oCurrentAgreement->getId() ) ? ' hidden' : '' ) . '" onclick="setCurrentAgreement(' . $oCompany->getId() . ', ' . $oAgreement->agreement_id . ')">Select as current</a>'; ?>
                                    <p class="heading-3 heading-3--light">Agreement
                                        #<?php echo $oAgreement->agreement_id; ?></p>
                                    <p class="slider__item-text serif"><?php echo $oAgreement->agreement_content; ?></p>
                                </div>
                            </label>
							<?php
							$num += 1;
						} ?>
                        <div class="positioner">
                            <a href="#0" class="cd-top">Top</a>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
</body>
</html>
