<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/company_fee.php';
include_once ROOT_DIR . '/entidades/entity.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();

$idCompany = $_GET['idc'];
if (is_null($idCompany)) {
    header("Location: index.php");
}
$oCompany = $servicios->getCompanyById($idCompany);

//Pagination
$perpage = $GLOBAL_SETTINGS['admin.inspectors.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
$total = count($servicios->getCompanyFeeByCompanyId($idCompany));
$vCompaniesFee = $servicios->getCompanyFeeByCompanyIdPag($idCompany, $from, $perpage);

$pagination = new Pagination($currentPage, $total, 5, $perpage, "idc=" . $idCompany);

$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <style>
            #table-t{
                width: 70%;
                border-collapse: collapse;
				margin:10px 0 0 15%;
            }

            #table-t th{
                border: 1px solid #a9a9a9;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			 #table-t .rojo {
               
                background-color: #ffc1c1;
				
            }
			
			 #table-t th.mid {
                border-bottom:1px solid #a9a9a9;
				border-top:1px solid #a9a9a9;
				border-left:hidden;
				border-right:hidden;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }

            #table-t td {
                border: 1px solid #a9a9a9;
                background-color: #fff;
				font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			#table-t td a {
               font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:none;
            }
			
			#table-t td a:hover {
                font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:underline;
            }
            
            .button {
                float: right;
                margin-top: 1rem;
            }
       
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <div class="conttxt"><p>Company: <?php echo $oCompany->getName(); ?></p></div>
                            <table id="table-t">
                                <tr>
                                    <th>ID</th>
                                     <th class="mid">Test amount from</th>
                                     <th class="mid">Fee amount to</th>
                                     <th class="mid">Fee</th>
                                    <th>Actions</th>
                                </tr>
                                <?php
                                foreach ($vCompaniesFee as $oCompanyFee) {
                                    ?>
                                    <tr>
                                        <td class="rojo"><?php echo $oCompanyFee->getCompanyFeeId(); ?></td>
                                        <td class="rojo"><?php echo $oCompanyFee->getCompanyFeeFrom(); ?></td>
                                        <td class="rojo"><?php echo $oCompanyFee->getCompanyFeeTo(); ?></td>
                                        <td class="rojo"><?php echo $oCompanyFee->getCompanyFeeFee(); ?></td>
                                        <td class="rojo">
<!--                                            <a href="inspectors_edit.php?id=<?php echo $oCompanyFee->getCompanyFeeId(); ?>">Edit</a>
                                            |-->
                                            <a href="company_fee_delete_process.php?idc=<?php echo $oCompany->getId(); ?>&company_fee_id=<?php echo $oCompanyFee->getCompanyFeeId(); ?>">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                            
                            <input type="button" class="button" value="Add" onclick="window.location.replace('company_fee_add.php?idc=<?php echo $oCompany->getId(); ?>');" />
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   