<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$idCompany = $_GET['idc'];
if (is_null($idCompany)) {
    header("Location: index.php");
}
$servicios = new Servicios();
$oCompany = $servicios->getCompanyById($idCompany);

$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
        <style type="text/css">
            select {
                margin: 10px 72px 0 0 !important;
                padding: 10px !important;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_company" onsubmit="return validateForm()" action="companies_payment_process.php">
                                <input type="hidden" name="idc" value="<?php echo $idCompany; ?>"/>
                                <input type="hidden" name="test_fee" class="box" placeholder="Test Fee" value="<?php echo ($oCompany->getCompanyTestFee() != NULL) ? $oCompany->getCompanyTestFee() : ""; ?>"/>
                                
                                <input type="button" class="button" value="Fees" onclick="window.location.replace('company_fee.php?idc=<?php echo $oCompany->getId();  ?>');" />
                                
                                <select name="payment_type" id="payment_type">
                                    <option selected disabled>Select</option>
                                    <option <?php
                                    if ($oCompany->getCompanyPaymentType() != NULL && $oCompany->getCompanyPaymentType() == "Credit Card") {
                                        echo " selected=true ";
                                    }
                                    ?>>Credit Card</option>
                                    <option <?php
                                    if ($oCompany->getCompanyPaymentType() != NULL && $oCompany->getCompanyPaymentType() == "Invoice") {
                                        echo " selected=true ";
                                    }
                                    ?>>Invoice</option>
                                </select>

                                <div class="errorContainer">
                                    <div class="error"></div>
                                </div>

                                <div id="savechanges"><input type="submit" class="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>



            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script type="text/javascript">
        $("#data_company").validate({
            rules: {
                test_fee: {
                    required: true,
                    number: true
                },
                payment_type: "required"
            }, messages: {
                test_fee: " Please insert a numeric test fee ",
                payment_type: " Please select a payment type "
            },
            errorLabelContainer: $("#data_company div.error")
        });
    </script>
</html>   