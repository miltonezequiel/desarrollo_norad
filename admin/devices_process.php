<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/controllers/devices_controller.php';
include_once ROOT_DIR . '/controllers/devices_companies_controller.php';
include_once ROOT_DIR . '/controllers/devices_inspectors_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

$action = $_GET['action'];
$redirectLocation = 'devices.php';
if (!isset($action) || empty($action)) { //si no hay accion
    header('Location:' . $redirectLocation);
    return;
}

$devicesController = new DevicesController();
$devicesCompaniesController = new DevicesCompaniesController();
$devicesInspectorsController = new DevicesInspectorsController();
$servicios = new Servicios();
if ($action == "add") {
    $oDevice = $servicios->getDeviceBySerialNumber(trim($_POST['serial_number']));
    if($oDevice == null) {
        $devicesController->uploadDevice();
        $redirectLocation = 'devices.php';
    } else {
        $redirectLocation = 'devices_add.php?error=The Device Serial Number already exists in the system';
    }
} elseif ($action == "assign") {
    $devicesCompaniesController->uploadDeviceCompany();
    $redirectLocation = 'devices.php';
} elseif ($action == "unassign") {
    $idDevice = $_GET['id'];
    $endDate = $_GET['date'];
    $idCompany = $_GET['company_id'];
    
    $idDeviceCompany = $_POST['idDevComp'];

    $devicesInspectorsController->unassignDeviceInspectorFromAdmin($idDevice, $idCompany); //Cambio fecha de fin al inspector que esté usando el dispositivo
    $devicesCompaniesController->unassignDeviceCompany($idDevice, $endDate); //Cambio fecha de fin a la empresa que esté usando el dispositivo

    $redirectLocation = 'devices.php';
} elseif ($action == "calibrate") {
    $idDevice = $_POST['id'];

    $devicesController->calibrateDevice($idDevice);

    $redirectLocation = 'devices.php';
} elseif ($action == "edit") {
    //TODO
} elseif ($action == "delete") {
    //TODO
}

header('Location:' . $redirectLocation);
?>