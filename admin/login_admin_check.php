<?php

session_start();

if ($_SESSION['user']['type'] != "admin") {
    header("Location: ../index.php");
    exit;
}
?>
