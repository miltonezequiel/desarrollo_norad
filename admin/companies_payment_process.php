<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/controllers/companies_controller.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$companiesController = new CompaniesController();

$idCompany = $_POST['idc'];

$oCompany = $companiesController->savePayment($idCompany);

header('Location: companies_payment.php?idc=' . $oCompany->getId());
?>