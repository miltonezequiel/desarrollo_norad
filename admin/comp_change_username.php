<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();

$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#username').keyup(username_check);
            });
     
            function username_check(){
                var username = $('#username').val();
                if(username == "" || username.length < 4){
                    $('#username').css('border', '3px #CCC solid');
                    $('#tick').hide();
                }else{
                    jQuery.ajax({
                        type: "POST",
                        url: "companies_add_check_user.php",
                        data: 'username='+ username,
                        cache: false,
                        success: function(response){
                            if(response > 0){
                                $('#username').css('border', '3px #C33 solid');
                                $('#tick').hide();
                                $('#cross').fadeIn();
                                $('#submit').attr('disabled','disabled');
                                $('#submit').css('opacity','0.5');
                            }else{
                                $('#username').css('border', '3px #090 solid');
                                $('#cross').hide();
                                $('#tick').fadeIn();
                                $('#submit').removeAttr('disabled');
                                $('#submit').css('opacity','1');
                            }
                        }
                    });
                } 
            }
        </script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_user" action="companies_change_username_process.php">
                                <input type="hidden" name="id" class="box" placeholder="" value="<?php echo $_GET['idc']; ?>" /><br/>
                                <input type="text" name="username" id="username" class="box" placeholder="User" /><br/>
                                <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $( "#data_user" ).validate({
            rules: {
                username: "required"
            },messages: {
                username: "Please insert username"
            },
            errorLabelContainer: $("#data_user div.error")
        });
    </script>
</html>   