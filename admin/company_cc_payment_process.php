<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

ini_set('max_execution_time', 300);

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
//include_once ROOT_DIR . '/util/utilidades.php';
include '../authorizenet/authorize.php';

$servicios = new Servicios();

$redirectLocation = "company_cc_payment.php";
$oCompany = $servicios->getCompanyById($_POST['company_id']);

if ($oCompany->getCompanyPaymentType() == 'Invoice') {
    echo '<script type="text/javascript">';
    echo 'window.location.href="company_cc_payment.php?error=A payment can not be generated for an invoice customer";';
    echo '</script>';
} else {
    $aAuthorize = new Authorizenet();
    $result = null;
    $result = $aAuthorize->chargeCustomerProfile($oCompany->getCustomerProfileId(), $oCompany->getCustomerPaymentProfileId(), $_POST['company_cc_payment_total'], '1', $oCompany->getName(), $_POST['company_cc_payment_detail']);

    if ($result['result'] == "Fail") {
        echo '<script type="text/javascript">';
        echo 'window.location.href="company_cc_payment.php?idc='.$_POST['company_id'].'&error=' . $result['error'] . '";';
        echo '</script>';
    } else {
        $authCode = $result['authCode'];
        $transId = $result['transId'];

        //generate the test receipt/statement
        $date = date('Y-m-d H:i:s');

        $oPayment = new Payment(0, $date, $_POST['company_cc_payment_total'], 2, '', 1, 1, 0);

        $paymentId = $servicios->saveTestPayment($oPayment);
        $oPayment = $servicios->getPaymentById($paymentId);
        $aPaymentDetail = Array();

        //generate the payments details
        $oPaymentDetail = new Payment_detail($_POST['company_cc_payment_detail'], $_POST['company_cc_payment_total'], $paymentId, 1);
        $servicios->saveTestPaymentDetail($oPaymentDetail);
        
        echo '<script type="text/javascript">';
        echo 'window.location.href="index.php";';
        echo '</script>';
    }
}
?>