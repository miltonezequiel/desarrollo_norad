<?php

/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//*/

session_start();

include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/entidades/company.php';

$estadoLogin = $_SESSION['estadoLogin'];
if (isset($estadoLogin) && $estadoLogin) {
    header("Location: devices.php");
    return;
} else {
    $estadoLogin = FALSE;
}

$user = $_POST['username'];
$password = $_POST['password'];

$servicios = new Servicios();
$oUser = $servicios->getUserByUsername($user);

if (isset($oUser) && $oUser->getUsername() == $user && $oUser->getPassword() == md5($password)) {
    $oInspector = $servicios->getInspectorByUser($oUser->getId());
    $oCompany = $servicios->getCompanyByUser($oUser->getId());

    if (is_null($oInspector) && is_null($oCompany)) { //No debe ser inspector ni company para que sea admin
        $_SESSION['user']['type'] = "admin";
        $_SESSION['estadoLogin'] = $estadoLogin = TRUE;
        $redirectLocation = "devices.php";
    } else {
        $redirectLocation = "login.php?msg=error";
    }
} elseif ($user == NULL || $password == NULL) {
    $redirectLocation = "login.php";
} else {
    $redirectLocation = "login.php?msg=error";
}

header("Location: " . $redirectLocation);
?>