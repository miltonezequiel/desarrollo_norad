<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/agreement.php';
include_once ROOT_DIR . '/controllers/agreements_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

$action = $_GET['action'];
$redirectLocation = 'devices.php';
if (!isset($action) || empty($action)) { //si no hay accion
    header('Location:' . $redirectLocation);
    return;
}

$agreementsController = new AgreementsController();
$servicios = new Servicios();
if ($action == "add") {
    $agreementsController->addAgreement();
    $redirectLocation = 'agreements.php';
} elseif ($action == "edit") {
    //TODO
} elseif ($action == "delete") {
    //TODO
}

header('Location:' . $redirectLocation);
?>