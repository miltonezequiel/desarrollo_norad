<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
session_start();

$page = "company-data";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/admin.css" />
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header-admin.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_device" action="company_cc_payment_process.php">
                                <input type="text" name="company_cc_payment_detail" class="box" placeholder="Details" /><br/>
                                <input type="text" name="company_cc_payment_total" class="box" placeholder="Total" /><br/>
                                <input type="hidden" name="company_id" class="box" value="<?php echo $_GET['idc']; ?>"/><br/>
                                <div class="errorContainer"> <div class="error"></div>                                     </div>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                            <div <?php if(!isset($_GET['error'])) echo "style='display:none;'"; ?> class="error"><?php if(isset($_GET['error'])) { echo $_GET['error']; } ?> </div> 
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $( "#data_device" ).validate({
            rules: {
                company_cc_payment_detail: "required",
                company_cc_payment_total: "required"
            },messages: {
                company_cc_payment_detail: "Please enter a detail",
                company_cc_payment_total: "Please enter a total"
            },
            errorLabelContainer: $("#data_device div.error")
        });
    </script>
</html>   