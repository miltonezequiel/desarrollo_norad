<?php 
$page = "tester-resources";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
     <div id="container">
    	<header><?php include_once($path.'includes/header.php'); ?></header>
           <div id="wrapper" class="page-testerresources">
                     <div id="container2">
                           <div id="main">
                                <div class="info">
                                	<div class="txt">
                                    	<h1>Tester Resources</h1>
                                        <input type="button" value="NORAD Drivers" class="button" onclick="window.location.href = 'drivers.php'"/>
                                    </div>
                                </div>
                           </div>
                    </div>
              </div>	
        </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   