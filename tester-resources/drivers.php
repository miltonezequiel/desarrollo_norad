<?php
include '../../login_check.php';
include '../login_inspector_check.php';
include_once '../../init.php';
include ROOT_DIR . '/inspector/session_clean.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$page = "drivers";
$path = '../../';
?>

<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />

        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>

        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

        <style type="text/css">

            .style3 {
                color: #ff0000;
            }
            .style16 {
                border: thin solid #c0c0c0;
                font-weight: bold;
                text-align: center;
            }
            .TableBorder1 {
                border-color: #c0c0c0;
                border-style: solid none none solid;
                border-width: thin;
                text-align: left;
            }
            .style22 {
                border-color: #c0c0c0;
                border-style: none none solid;
                border-width: medium medium thin;
                font-weight: bold;
                text-align: center;
            }
            .style23 {
                border-left-width: 0;
                border-right-width: 0;
                border-top-width: 0;
            }
            .style24 {
                border-color: #c0c0c0;
                border-style: none solid solid none;
                border-width: medium thin thin medium;
                font-weight: bold;
                text-align: center;
            }
            .style25 {
                border-color: #c0c0c0;
                border-style: none none solid solid;
                border-width: medium medium thin thin;
                font-weight: bold;
                text-align: center;
            }
            .style29 {
                border: thin solid #c0c0c0;
                text-align: center;
            }
            .style30 {
                color: #0000ff;
            }
            .style31 {
                border: thin solid #c0c0c0;
                color: #0000ff;
                text-align: center;
            }
            .style32 {
                border: thin solid #c0c0c0;
                font-size: x-small;
                text-align: center;
            }
            .style1 {
                color: #0000ff;
                text-decoration: none;
            }

            .tableContainer {
                display: block;
                float: left;
                position: relative;
                text-align: center;
                width: 100%
            }

            .tit {
                display: block;
                float: left;
                margin: 0 0 10px;
                position: relative;
                text-align: center;
                width: 100%;
            }

            .tit h1 {
                color: #000;
                font: 18px/22px robotobold,Geneva,sans-serif;
                text-align: center;
            }

            table {
                margin-left:8em;
                margin-top:2em;
                float:left;
                
            }

            #main {
                margin-top: 3em !important;
            }

            .notes {
                margin-left:8em;
                margin-top:2em;
                float:left;
                width:80%;
                font-size:0.8em;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once( $path . 'includes/header.php' ); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">

                        <div id="divLogin" class="tableContainer">
                            <div class="tit">
                                <h1>DRIVERS DOWNLOAD</h1>
                            </div>
                            <p>In most cases the NORAD drivers are installed automatically by your computer. In the rare occasions where they are not these drivers should be used at the direction of a NORAD Customer Support Specialist</p>
                            <table style="font-family: Arial; font-size: 10pt" class="style23" width="80%" cellspacing="0" cellpadding="4" align="center">
                                <tbody><tr>
                                        <td class="style22" style="height: 36px"></td>
                                        <td class="style24" style="height: 36px"></td>
                                        <td class="style16" colspan="7" style="height: 36px">Processor Architecture</td>
                                        <td class="style25" style="height: 36px"></td>
                                    </tr>
                                    <tr>
                                        <td class="style16" style="height: 36px">Operating System</td>
                                        <td class="style16" style="height: 36px">Release Date</td>
                                        <td class="style16" style="height: 36px">x86 (32-bit)</td>
                                        <td class="style16" style="height: 36px">x64 (64-bit)</td>
                                        <td class="style16" style="height: 36px">PPC</td>
                                        <td class="style16" style="height: 36px">ARM</td>
                                        <td class="style16" style="height: 36px">MIPSII</td>
                                        <td class="style16" style="height: 36px">MIPSIV</td>
                                        <td class="style16" style="height: 36px">SH4</td>
                                    </tr>
                                    <tr>
                                        <td class="style29" style="height: 84px">Windows*</td>
                                        <td class="style29" style="height: 84px">2016-10-10</td>
                                        <td class="style29" style="height: 84px">
                                            <a class="style1" href="../inspector/drivers/CDM%20v2.12.24%20WHQL%20Certified.zip">2.12.24</a></td>
                                        <td class="style29" style="height: 84px">
                                            <span lang="EN-GB"><font size="2">
                                                <a class="style1" href="../inspector/drivers/CDM%20v2.12.24%20WHQL%20Certified.zip">2.12.24</a></font></span></td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>

                                    </tr>
                                    <tr>
                                        <td class="style29" style="height: 84px">Linux</td>
                                        <td class="style29" style="height: 84px">2009-05-14</td>
                                        <td class="style29" style="height: 84px"><a class="style1" href="../inspector/drivers/ftdi_sio.tar.gz">1.5.0</a></td>
                                        <td class="style29" style="height: 84px"><a class="style1" href="../inspector/drivers/ftdi_sio.tar.gz">1.5.0</a></td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                    <tr>
                                        <td class="style29">Mac OS X 10.3 to 10.8</td>
                                        <td class="style29">2012-08-10</td>
                                        <td class="style29"><a class="style1" href="../inspector/drivers/FTDIUSBSerialDriver_v2_2_18.dmg" target="_blank">2.2.18</a></td>
                                        <td class="style29"><a class="style1" href="../inspector/drivers/FTDIUSBSerialDriver_v2_2_18.dmg" target="_blank">2.2.18</a></td>
                                        <td class="style29"><a class="style1" href="../inspector/drivers/FTDIUSBSerialDriver_v2_2_18.dmg" target="_blank">2.2.18</a></td>
                                        <td class="style29">-</td>
                                        <td class="style29">-</td>
                                        <td class="style29">-</td>
                                        <td class="style29">-</td>
                                    </tr>
                                    <tr>
                                        <td class="style29" style="height: 22px">
                                            <span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
                                                  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-ansi-language:
                                                  EN-GB;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">Mac OS X 10.9 and 
                                                above</span></td>
                                        <td class="style29" style="height: 22px">
                                            <span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
                                                  mso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;mso-ansi-language:
                                                  EN-GB;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">2015-04-15</span></td>
                                        <td class="style29" style="height: 22px">-</td>				
                                        <td class="style29" style="height: 22px">
                                            <a class="style1" href="../inspector/drivers/FTDIUSBSerialDriver_v2_3.dmg" target="_blank">2.3</a></td>
                                        <td class="style29" style="height: 22px">-</td>			
                                        <td class="style29" style="height: 22px">-</td>
                                        <td class="style29" style="height: 22px">-</td>
                                        <td class="style29" style="height: 22px">-</td>
                                        <td class="style29" style="height: 22px">-</td>
                                    </tr>

                                    <tr>
                                        <td class="style29">
                                            Windows CE 4.2-5.2**</td>
                                        <td class="style29">2012-01-0<font size="2">6</font></td>
                                        <td class="style29"><a href="../inspector/drivers/x86VCPDriver.zip">
                                                <span class="style30">1.1.0.20</span></a></td>
                                        <td class="style29">-</td>
                                        <td class="style29">-</td>
                                        <td class="style29">
                                            <a href="../inspector/drivers/ARMv4VCPDriver_1.1.0.20.zip">
                                                <span class="style30">1.1.0.20</span></a></td>
                                        <td class="style29">
                                            <a href="../inspector/drivers/MIPSIIVCPDriver.zip">
                                                <span class="style30">1.1.0.10</span></a></td>
                                        <td class="style29">
                                            <a href="../inspector/drivers/MIPSIVVCPDriver.zip">
                                                <span class="style30">1.1.0.10</span></a></td>
                                        <td class="style29"><a href="../inspector/drivers/SH4VCPDriver.zip">
                                                <span class="style30">1.1.0.10</span></a></td>
                                    </tr>
                                    <tr>
                                        <td class="style29" style="height: 84px">
                                            Windows CE 6.0/7.0</td>
                                        <td class="style29" style="height: 84px">
                                            <span lang="EN-GB"><font size="2">
                                                2012-01-06</font></span></td>
                                        <td class="style29" style="height: 84px">
                                            <a href="../inspector/drivers/x86VCPDriver60.zip">
                                                <span class="style30">1.1.0.20</span></a><br>
                                            <a href="../inspector/drivers/WinCE%206.0%20x86.zip"><span class="style30">CE 6.0 CAT</span></a><br>
                                            <a href="../inspector/drivers/WinCE%207.0%20x86.zip">
                                                <span class="style30">CE 7.0 CAT</span></a></td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">-</td>
                                        <td class="style29" style="height: 84px">
                                            <a href="../inspector/drivers/ARMv4VCPDriver60_1.1.0.20.zip">
                                                <span class="style30">1.1.0.20</span></a><br>
                                            <a href="../drivers/WinCE 6.0 ARM.zip">
                                                <span class="style30">CE 6.0 CAT</span></a><br>
                                            <a href="../drivers/WinCE 7.0 ARM.zip">
                                                <span class="style30">CE 7.0 CAT</span></a></td>
                                        <td class="style29" style="height: 84px">
                                            <a href="../inspector/drivers/MIPSIIVCPDriver60.zip">
                                                <span class="style30">1.1.0.10</span></a></td>
                                        <td class="style29" style="height: 84px">
                                            <a href="../inspector/drivers/MIPSIVVCPDriver60.zip">
                                                <span class="style30">1.1.0.10</span></a></td>
                                        <td class="style29" style="height: 84px"><a href="../inspector/drivers/SH4VCPDriver60.zip">
                                                <span class="style30">1.1.0.10</span></a></td>
                                    </tr>
                                    <tr>
                                        <td class="style29">
                                            Windows CE 2013</td>
                                        <td class="style29">
                                            2015-03-06</td>
                                        <td class="style29">
                                            <a href="../inspector/drivers/x86VCPDriver2013.zip"><span class="style30">1.0.0</span></a></td>
                                        <td class="style29">&nbsp;</td>
                                        <td class="style29">&nbsp;</td>
                                        <td class="style29">
                                            <a href="../inspector/drivers/ARMVCPDriver2013.zip"><span class="style30">1.0.0</span></a></td>
                                        <td class="style29">
                                            &nbsp;</td>
                                        <td class="style29">
                                            &nbsp;</td>
                                        <td class="style29">&nbsp;</td>
                                    </tr>
                                </tbody></table>


                            <div class="notes">
                                *Includes the following version of of the Windows operating system: 
                                Windows 7, Windows Server 2008 R2 and Windows 8, 8.1, Windows server 
                                2012 R2 and Windows 10. Also, 
                                as Windows 8 RT is a closed system not allowing for 3rd party driver 
                                installation our Windows 8 driver will not support this variant of the 
                                OS.
                            </div>


                            <div class="notes">
                                *Includes the following version of of the Windows operating system: 
                                Windows 7, Windows Server 2008 R2 and Windows 8, 8.1, Windows server 
                                2012 R2 and Windows 10. 
                            </div>

                            <div class="notes">
                                **includes the following versions of Windows CE 4.2-5.2 based operating 
                                systems: Windows Mobile 2003, Windows Mobile 2003 SE, Windows Mobile 5, 
                                Windows Mobile 6, Windows Mobile 6.1 ,Windows Mobile 6.5
                            </div>

                            <table align="center" cellpadding="4" cellspacing="0" style="font-family: Arial; font-size: 10pt; margin-bottom:4em;" width="80%" class="style23">
                                <tr>
                                    <td class="style22">&nbsp;</td>
                                    <td class="style22">&nbsp;</td>
                                    <td class="style16" colspan="7">Processor Architecture</td>
                                    <td class="style22">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style16">Operating System</td>
                                    <td class="style16">Release Date</td>
                                    <td class="style16">x86 (32-bit)</td>
                                    <td class="style16">x64 (64-bit)</td>
                                    <td class="style16">PPC</td>
                                    <td class="style16">ARM</td>
                                    <td class="style16">MIPSII</td>
                                    <td class="style16">MIPSIV</td>
                                    <td class="style16">SH4</td>
                                </tr>
                                <tr>
                                    <td class="style29">Previous Windows Release</td>
                                    <td class="style29">2016-06-23</td>
                                    <td class="style29">
                                        <span LANG="EN-GB"><font SIZE="2">
                                            <a class="style1" href="../inspector/drivers/CDM%20v2.12.18%20WHQL%20Certified.zip">2.12.18</a></font></span></td>
                                    <td class="style29">
                                        <span LANG="EN-GB"><font SIZE="2">
                                            <a class="style1" href="../inspector/drivers/CDM%20v2.12.18%20WHQL%20Certified.zip">2.12.18</a></font></span></td>
                                    <td class="style29">&nbsp;</td>
                                    <td class="style29">&nbsp;</td>
                                    <td class="style29">&nbsp;</td>
                                    <td class="style29">&nbsp;</td>
                                    <td class="style29">&nbsp;</td>
                                    &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="style29">Windows XP, Vista, Server&nbsp;2003, Server&nbsp;2008</td>
                                    <td class="style29">2012-04-13</td>
                                    <td class="style29"><a class="style1" href="../inspector/drivers/CDM%202.08.24%20WHQL%20Certified.zip">2.08.24</a></td>
                                    <td class="style29"><a class="style1" href="../inspector/drivers/CDM%202.08.24%20WHQL%20Certified.zip">2.08.24</a></td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>

                                </tr>
                                <tr>
                                    <td class="style29">Windows 2000</td>
                                    <td class="style29">2009-10-22</td>
                                    <td class="style29"><a class="style1" href="../inspector/drivers/CDM%202.06.00%20WHQL%20Certified.zip">2.06.00</a></td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>

                                </tr>
                                <tr>
                                    <td class="style29">Windows 98/ME</td>
                                    <td class="style29">2004-11-25</td>
                                    <td class="style29"><a class="style1" href="../inspector/drivers/R10906.zip">1.09.06</a></td>
                                    <td class="style32">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                </tr>
                                <tr>
                                    <td class="style29">Windows 98/ME</td>
                                    <td class="style29">2004-03-12</td>
                                    <td class="style29"><a class="style1" href="../inspector/drivers/FTC100103.zip">1.0.3</a></td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                </tr>
                                <tr>
                                    <td class="style29">
                                        Mac OS 9<br>
                                        Mac OS 8</td>
                                    <td class="style29">2004-05-18</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29"><a class="style1" href="../inspector/drivers/USBSerialConverter_040E2.sit.hqx">1.0f4</a></td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                    <td class="style29">-</td>
                                </tr>
                            </table>
                            

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>	
</div>
<footer><?php include_once( $path . 'includes/footer.php' ); ?></footer>
</body>
</html>    