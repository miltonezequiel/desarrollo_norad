<?php

use App\Core\Request;
use App\Core\Router;

require __DIR__ . '/../vendor/autoload.php';
require 'core/bootstrap.php';

Router::load( 'app/routes.php' )
      ->direct( Request::uri(), Request::method() );
