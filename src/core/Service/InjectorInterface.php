<?php

namespace NoradService;

/**
 * Interface InjectorInterface
 * @package NoradService
 */
interface InjectorInterface {
	public function create();
}
