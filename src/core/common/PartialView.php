<?php

namespace NoradCommon;

/**
 * Class PartialView
 * @package NoradViews
 */
class PartialView extends AbstractView implements CommonRenderable {
	/**
	 * Render the partial view
	 */
	public function render() {
		return $this->_doRender();
	}
}
