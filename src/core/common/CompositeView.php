<?php
namespace NoradCommon;

use InvalidArgumentException;

/**
 * Class CompositeView
 * @package NoradViews
 */
class CompositeView extends AbstractView implements CommonAggregable, CommonRemovable, CommonRenderable {
	protected $_views = array();

	/**
	 * Add a view
	 */
	public function add($view, $key = null) {
		if (!$view instanceof AbstractView) {
			throw new InvalidArgumentException("La vista a agregar debe ser una instancia de AbstractView");
		}
		if(isset($key)) {
			$this->_views[$key] = $view;
		}
		else {
			$this->_views[] = $view;
		}
		return $this;
	}

	/**
	 * Add multiple views
	 */
	public function addMultiple(array $views) {
		foreach($views as $key => $view) {
			$this->add($view, $key);
		}
		return $this;
	}

	/**
	 * Remove a view
	 */
	public function remove($view) {
		if (!$view instanceof AbstractView) {
			throw new InvalidArgumentException("La vista a agregar debe ser una instancia de AbstractView");
		}
		$this->_views = array_filter( $this->_views, function ($v) use ($view) {
			return $v !== $view;
		});
		return true;
	}

	/**
	 * Render both the injected view and the current one
	 */
	public function render() {
		$inner_content = "";
		//render the injected views
		foreach ($this->_views as $_view) {
			$inner_content .= $_view->render();
		}
		$this->content = $inner_content;
		//render the current view
		$output = "";
		$output .= $this->_doRender();
		//replace placeholder with partial content for nesting
		return $output;
	}
}
