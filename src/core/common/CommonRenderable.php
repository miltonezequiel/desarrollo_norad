<?php

namespace NoradCommon;

/**
 * Interface CommonRenderable
 * @package NoradCommon
 */
interface CommonRenderable {
	public function render();
}