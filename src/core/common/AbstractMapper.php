<?php

namespace NoradModelMapper;

use NoradDatabase,
	NoradCommon;

/**
 * Class AbstractMapper
 * @package NoradModelMapper
 */
abstract class AbstractMapper {
	protected $_adapter;
	protected $_entityTable;

	/**
	 * AbstractMapper constructor.
	 *
	 * @param DatabaseDatabaseAdapterInterface $adapter
	 * @param null $entityTable
	 */
	public function __construct( DatabaseDatabaseAdapterInterface $adapter, $entityTable = null ) {
		$this->_adapter = $adapter;
		// Set the entity table if the option has been specified
		if ( isset( $entityTable ) ) {
			$this->setEntityTable( $entityTable );
		}
		// check if the entity table has been set
		if ( ! isset( $this->_entityTable ) ) {
			throw new InvalidArgumentException( 'The entity table has been not set yet.' );
		}
	}

	/**
	 * Get the database adapter
	 * @return DatabaseDatabaseAdapterInterface
	 */
	public function getAdapter() {
		return $this->_adapter;
	}

	/**
	 * Set the entity table
	 *
	 * @param $entityTable
	 */
	public function setEntityTable( $entityTable ) {
		if ( ! is_string( $table ) || empty( $entityTable ) ) {
			throw new InvalidArgumentException( 'The entity table is invalid.' );
		}
		$this->_entityTable = $entityTable;
	}

	/**
	 * Get the entity table
	 * @return mixed
	 */
	public function getEntityTable() {
		return $this->_entityTable;
	}

	/**
	 * Find an entity by its ID
	 *
	 * @param $id
	 *
	 * @return null
	 */
	public function findById( $id ) {
		$this->_adapter->select( $this->_entityTable, "id = $id" );
		if ( $data = $this->_adapter->fetch() ) {
			return $this->_createEntity( $data );
		}

		return null;
	}

	/**
	 * Find all the entities that match the specified criteria (or all when no criteria are given)
	 *
	 * @param $criteria
	 *
	 * @return array|CommonEntityCollection
	 */
	public function find( $criteria = ” ) {
		$collection = new CommonEntityCollection;
		$this->_adapter->select( $this->_entityTable, $criteria );
		while ( $data = $this->_adapter->fetch() ) {
			$collection[] = $this->_createEntity( $data );
		}

		return $collection;
	}

	/**
	 * Reconstitute the corresponding entity with the supplied data
	 * (implementation delegated to child mappers)
	 *
	 * @param array $fields
	 *
	 * @return mixed
	 */
	abstract protected function _createEntity( array $fields );
}
