<?php

namespace NoradCommon;

use NoradModel;

/**
 * Class EntityCollection
 * @package NoradCommon
 */
class EntityCollection implements CollectionInterface {
	protected $_entities = array();

	/**
	 * EntityCollection constructor.
	 *
	 * @param array $entities
	 */
	public function __construct( array $entities = array() ) {
		$this->_entities = $entities;
		$this->reset();
	}

	/**
	 * Get the entities stored in the collection
	 * @return array
	 */
	public function toArray() {
		return $this->_entities;
	}

	/**
	 * Clear the collection
	 */
	public function clear() {
		$this->_entities = array();
	}

	/**
	 * Rewind the collection
	 */
	public function reset() {
		reset( $this->_entities );
	}

	/**
	 * Add an entity to the collection
	 *
	 * @param $key
	 * @param ModelAbstractEntity $entity
	 *
	 * @return bool
	 */
	public function add( $key, ModelAbstractEntity $entity ) {
		return $this->offsetSet( $key, $entity );
	}

	/**
	 * Get from the collection the entity with the specified key
	 *
	 * @param $key
	 *
	 * @return mixed|null
	 */
	public function get( $key ) {
		return $this->offsetGet( $key );
	}

	/**
	 * Remove from the collection the entity with the specified key
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function remove( $key ) {
		return $this->offsetUnset( $key );
	}

	/**
	 * Check if the entity with the specfied key exists in the collection
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function exists( $key ) {
		return $this->offsetExists( $key );
	}

	/**
	 * Count the number of entities in the collection
	 * @return int
	 */
	public function count() {
		return count( $this->_entities );
	}

	/**
	 * Get the external array iterator
	 * @return ArrayIterator
	 */
	public function getIterator() {
		return new ArrayIterator( $this->toArray() );
	}

	/**
	 * Add an entity to the collection
	 *
	 * @param $key
	 * @param $entity
	 *
	 * @return bool
	 */
	public function offsetSet( $key, $entity ) {
		if ( ! $entity instanceof ModelAbstractEntity ) {
			throw new InvalidArgumentException( 'To add an entity to the collection, it must be an instance of EntityAbstract.' );
		}
		if ( ! isset( $key ) ) {
			$this->_entities[] = $entity;
		} else {
			$this->_entities[ $key ] = $entity;
		}

		return true;
	}

	/**
	 * Remove an entity from the collection
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function offsetUnset( $key ) {
		if ( $key instanceof ModelAbstractEntity ) {
			$this->_entities = array_filter( $this->_entities, function ( $v ) use ( $key ) {
				return $v !== $key;
			} );

			return true;
		}
		if ( isset( $this->_entities[ $key ] ) ) {
			unset( $this->_entities[ $key ] );

			return true;
		}

		return false;
	}

	/**
	 * Get the specified entity in the collection
	 *
	 * @param $key
	 *
	 * @return mixed|null
	 */
	public function offsetGet( $key ) {
		return isset( $this->_entities[ $key ] )
			? $this->_entities[ $key ] : null;
	}

	/**
	 * Check if the specified entity exists in the collection
	 *
	 * @param $key
	 *
	 * @return bool
	 */
	public function offsetExists( $key ) {
		return isset( $this->_entities[ $key ] );
	}
}
