<?php

namespace NoradCommon;

/**
 * Class LayoutBuilder
 * @package NoradAppUtility
 */
class LayoutBuilder {
	protected $_views = array();

	/**
	 * Attach a single view
	 *
	 * @param CommonAgregable $view
	 * @param null $key
	 *
	 * @return LayoutBuilder
	 */
	public function attachView( $view, $key = null ) {
		if ( isset( $key ) ) {
			$this->_views[ $key ] = $view;
		} else {
			$this->_views[] = $view;
		}

		return $this;
	}

	/**
	 * Attach multiple views
	 */
	public function attachViews( array $views ) {
		foreach ( $views as $key => $view ) {
			$this->attachView( $view, $key );
		}

		return $this;
	}

	/**
	 * Get the attached views
	 */
	public function getViews() {
		return $this->_views;
	}

	/**
	 * Build the layout(s)
	 */
	public function build() {
		foreach ( $this->_views as $_view ) {
			//create a predefined layout with the attached views
			$_view->addMultiple( array(
				new PartialView( array(
					'templateFile' => 'header.php'
				) ),
				new PartialView( array(
					'templateFile' => 'body.php'
				) ),
				new PartialView( array(
					'templateFile' => 'footer.php'
				) )
			) );
		}
	}
}

