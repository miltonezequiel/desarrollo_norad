<?php

namespace NoradCommon;

use Exception;

/**
 * Class ClassNotFoundException
 * @package NoradUtils
 */
class ClassNotFoundException extends Exception {
}
