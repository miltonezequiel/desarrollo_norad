<?php

namespace NoradCommon;

use NoradModel;

/**
 * Class EntityProxy
 * @package NoradCommon
 */
class EntityProxy extends AbstractProxy implements LoadableInterface {
	protected $_entity;

	/**
	 * Load an entity via the 'findById()' method of the injected mapper
	 * @return ModelAbstractEntity
	 */
	public function load() {
		if ( $this->_entity === null ) {
			$this->_entity = $this->_mapper->findById( $this->_params );
			if ( ! $this->_entity instanceof ModelAbstractEntity ) {
				throw new RunTimeException( 'Unable to load the specified entity . ' );
			}
		}

		return $this->_entity;
	}
}
