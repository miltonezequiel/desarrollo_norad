<?php

namespace NoradCommon;

require_once __DIR__ . '/ClassNotFoundException.php';

/**
 * Class Autoloader
 * @package NoradUtils
 */
class Autoloader {
	private static $_instance;

	// get the Singleton instance of the autoloader
	public static function getInstance() {
		if ( ! self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;
	}

	// private constructor
	private function __construct() {
		spl_autoload_register( array( $this, 'load' ) );
	}

	// prevent cloning instance of the autoloader
	private function __clone() {
	}

	// autoload classes on demand

	/**
	 * @param $class
	 *
	 * @throws \Exception
	 * @throws ClassNotFoundException
	 */
	public static function load( $class ) {
		//$file = $class . '.php';
		$file = __DIR__ . DIRECTORY_SEPARATOR .substr($class, strrpos($class, '\\') + 1).'.php';
		if ( ! file_exists( $file ) ) {
			throw new ClassNotFoundException( 'The file ' . $file . ' containing the requested class ' . $class . ' was not found.' );
		}
		include $file;
		unset( $file );
		/*if ( ! class_exists( substr($class, strrpos($class, '\\') + 1), false ) ) {
			throw new ClassNotFoundException( 'The requested class ' . $class . ' was not found.' );
		}
		*/
	}

}