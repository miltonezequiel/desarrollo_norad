<?php

namespace NoradCommon;

/**
 * Interface CommonAggregable
 * @package NoradCommon
 */
interface CommonAggregable {

	public function add( $view, $key = null );

	public function addMultiple( array $views );

}