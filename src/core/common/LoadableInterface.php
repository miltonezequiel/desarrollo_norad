<?php

namespace NoradCommon;

/**
 * Interface LoadableInterface
 * @package NoradCommon
 */
interface LoadableInterface
{
	public function load();
}
