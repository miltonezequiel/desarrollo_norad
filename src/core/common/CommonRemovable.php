<?php

namespace NoradCommon;

/**
 * Interface CommonRemovable
 * @package NoradCommon
 */
interface CommonRemovable {
	public function remove( $view );
}