<?php

namespace NoradCommon;
use InvalidArgumentException;
use NoradModelMapper;

/**
 * Class AbstractProxy
 * @package NoradCommon
 */
abstract class AbstractProxy
{
	protected $_mapper;
	protected $_params;

	/**
	 * Constructor
	 *
	 * @param MapperAbstractMapper $mapper
	 * @param $params
	 *
	 * @throws \InvalidArgumentException
	 */
	public function __construct(MapperAbstractMapper $mapper, $params)
	{
		if (!is_string($params) || empty($params)) {
			throw new InvalidArgumentException('The mapper parameters are invalid.');
        }
		$this->_mapper = $mapper;
		$this->_params = $params;
	}
}
