<?php

namespace NoradHelper;

/**
 * Class EmailValidator
 * @package NoradHelper
 */
class EmailValidator extends AbstractValidator implements ValidatorInterface {
	const DEFAULT_ERROR_MESAGE = 'The supplied email address is invalid.';

	/**
	 * Validate an email address
	 * @return mixed
	 */
	public function validate() {
		return filter_var( $this->_value, FILTER_VALIDATE_EMAIL );
	}
}
