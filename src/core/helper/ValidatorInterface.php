<?php

namespace NoradHelper;

/**
 * Interface ValidatorInterface
 * @package NoradHelper
 */
interface ValidatorInterface {
	/**
	 * Validate the given value
	 */
	public function validate();
}


