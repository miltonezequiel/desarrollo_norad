<?php

namespace NoradHelper;

/**
 * Class AbstractValidator
 * @package NoradHelper
 */
abstract class AbstractValidator {
	const DEFAULT_ERROR_MESAGE = 'The supplied value is invalid.';
	protected $_value;
	protected $_errorMessage = self::DEFAULT_ERROR_MESAGE;

	/**
	 * AbstractValidator constructor.
	 *
	 * @param null $value
	 * @param null $errorMessage
	 */
	public function __construct( $value = null, $errorMessage = null ) {
		if ( $value !== null ) {
			$this->setValue( $value );
		}
		if ( $errorMessage !== null ) {
			$this->setErrorMessage( $errorMessage );
		}
	}

	/**
	 * Set the value to be validated
	 *
	 * @param $value
	 */
	public function setValue( $value ) {
		$this->_value = $value;
	}

	/**
	 * Get the inputted value
	 * @return mixed
	 */
	public function getValue() {
		return $this->_value;
	}

	/**
	 * Set the error message
	 *
	 * @param $errorMessage
	 */
	public function setErrorMessage( $errorMessage ) {
		if ( ! is_string( $errorMessage ) || empty( $errorMessage ) ) {
			throw new InvalidArgumentException( 'The error message is invalid. It must be a non-empty string.' );
		}
		$this->_errorMessage = $errorMessage;
	}

	/**
	 * Get the error message
	 * @return string
	 */
	public function getErrorMessage() {
		return $this->_errorMessage;
	}

	/**
	 * Reset the error message to the default value
	 */
	public function resetErrorMessage() {
		$this->_errorMessage = self::DEFAULT_ERROR_MESAGE;
	}
}
