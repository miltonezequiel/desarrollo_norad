<?php

namespace NoradHelper;

/**
 * Class FloatValidator
 * @package NoradHelper
 */
class FloatValidator extends AbstractValidator implements ValidatorInterface {
	const DEFAULT_ERROR_MESAGE = 'The supplied value is an invalid float number.';

	/**
	 * Validate a float number
	 */
	public function validate() {
		return filter_var( $this->_value, FILTER_VALIDATE_FLOAT );
	}
}
