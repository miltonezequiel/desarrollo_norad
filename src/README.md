# Observaciones

Cuando se usa autoload de composer para importar las clases automáticamente hay que ejecutar el comando `$ composer dump-autoload`

Se generarán los archivos necesarios para cargar las clases sin la necesidad del uso de la palabra clave `use`

Notar que se pueden agregar además archivos que no tengan clases para que autoload los cargue. Para eso hay que modificar la sección files del archivo `composer.json`

```json
{
  "autoload": {
    "classmap": [
      "./"
    ],
    "files": ["src/core/utils/util_functions.php"]
  }
}
```
