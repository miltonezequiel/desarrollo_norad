<?php

/*
use NoradAppUtility\LayoutBuilder as LayoutBuilder, NoradViews\CompositeView as CompositeView;

//include the autoloader
//require_once 'Autoloader.php';

//$autoloader = new Autoloader;
//create an instance of the layout builder
$layoutBuilder = new LayoutBuilder();
//add a view to the layout builder
$layoutBuilder->attachView( new CompositeView);
//generate a layout with the attached view
$layoutBuilder->build();

*/

use NoradCommon\Autoloader;
use NoradCommon\CompositeView;
use NoradCommon\PartialView;

try {
	// include the autoloader class and grab an instance of it
	require_once __DIR__ . '/../../core/common/Autoloader.php';
	$autoloader = Autoloader::getInstance();

	$directory = __DIR__ . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR . 'partials';

	// create a partial view using the previous ‘partial1.php’ file
	$partialA          = new PartialView( array( 'templateFile' => 'partial1.php', 'templateDirectory' => $directory ) );
	$partialA->message = 'This is a message from partial view A.';

	// create another partial view using the previous ‘partial2.php’ file
	$partialB          = new PartialView( array( 'templateFile' => 'partial2.php', 'templateDirectory' => $directory ) );
	$partialB->message = 'This is a message from partial view B.';

	// create a composite View and add the partials to it
	$compositeView = new CompositeView( array('templateFile' => 'layout.php' ));
	echo $compositeView->add( $partialA )
	                   ->add( $partialB )
	                   ->render();
} catch ( Exception $e ) {
	echo $e->getMessage();
	exit();
}



