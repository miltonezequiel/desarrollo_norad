<?php

namespace App\Controllers;

/**
 * Class PagesController
 * @package App\Controllers
 */
class PagesController {
	/**
	 * Show the home page.
	 * @return mixed
	 */
	public function home() {
		return view( 'index' );
	}
}
//todo: usar la función que está en helpers
/**
 * Require a view.
 *
 * @param  string $name
 * @param  array $data
 *
 * @return mixed
 */
function view( $name, $data = [] ) {
	extract( $data );

	return require "app/views/{$name}.view.php";
}
