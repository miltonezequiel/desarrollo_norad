<?php

namespace NoradModel;

use NoradCommon;
use NoradCommon\CommonCollectionProxy;

/**
 * Class User
 * @package NoradModel
 */
class User extends AbstractEntity {
	protected $_allowedFields = array( 'id', 'username', 'password', 'inspectors' );

	/**
	 * Set the user id
	 *
	 * @param $id
	 */
	public function setId( $id ) {
		if ( ! filter_var( $id, FILTER_VALIDATE_INT, array(
			'options' => array(
				'min_range' => 1,
				'max_range' => 999999
			)
		) ) ) {
			throw new InvalidArgumentException( 'The user ID is invalid.' );
		}
		$this->_values['id'] = $id;
	}

	/**
	 * Set the user name
	 *
	 * @param $username
	 */
	public function setUsername( $username ) {
		if ( ! is_string( $username ) || strlen( $username ) < 2 || strlen( $username ) > 64 ) {
			throw new InvalidArgumentException( 'The username is invalid.' );
		}
		$this->_values['username'] = $username;
	}

	/**
	 * Set the password
	 *
	 * @param $password
	 */
	public function setPassword( $password ) {
		if ( ! is_string( $password ) || strlen( $password ) < 2 || strlen( $password ) > 64 ) {
			throw new InvalidArgumentException( 'The password is invalid.' );
		}
		$this->_values['password'] = $password;
	}

	public function setInspectors( CommonCollectionProxy $inspectors ) {
		$this->_values['inspectors'] = $inspectors;
	}
}