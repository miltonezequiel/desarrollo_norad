<?php
$target_dir = "../../uploads/custom_logos/";
$target_file = $target_dir . $oCompany->getId();
$file = "../../img/envirolabs_letterhead.jpg";

if (file_exists($target_file . '.jpg')) {
    $file = $target_file . '.jpg';
} elseif (file_exists($target_file . '.jpeg')) {
    $file = $target_file . '.jpeg';
}
?>


<div class="logo"><img src="<?php echo $file; ?>" width="100%" /></div>

<div class="logoNorad"><img src="../../img/logo_small.jpg"/></div>

<div class="sublogo">
    <div class="title">Indoor Air Radon Test Report</div>
    <div class="text"><strong>Analytical Services Provided by Envirolabs Incorporated</strong></div>
    <div class="text"><strong>AARST- NRPP Accredited Laboratory 101147 AL</strong></div>
</div>

<div id="wrapper" class="informe">
    <div id="container">
        <div id="main" style="margin-top:5px;">
            <div class="tabla">
                <div class="column-list-left">
                    <div class="column-list">
                        <div class="title"><span><b>TEST PERFORMED FOR:</b></span></div>
                        <div class="texto"><span><?php echo $oClient->getName(); ?></span></div>
                    </div>
                    <div class="column-list">
                        <div class="title"><span><b>TEST SITE:</b></span></div>
                        <div class="texto">
                            <span><?php echo$oTest->getAddress(); ?></span><br>
                            <span><?php echo $oTest->getCity() . ', ' . $oTest->getState() . ', ' . $oTest->getZip(); ?></span>
                        </div>
                    </div>
                    <div class="column-list">
                        <div class="title"><span><b>TEST LOCATION:</b></span></div>
                        <div class="texto"><span><?php echo $oTest->getFloorLevel() . ' ' . $oTest->getRoom(); ?></span></div>
                    </div>
                    <div class="column-list">
                        <div class="title"><span><b>TEST PERFORMED BY:</b></span></div>
                        <div class="texto"><span><?php echo $oInspector->getName(); ?></span></div>
                        <div class="title"><span><b>INSPECTION COMPANY:</b></span></div>
                        <div class="texto"><span><?php echo $oCompany->getName(); ?></span></div>
                        <div class="title"><span><b>TESTER ID:</b></span></div>
                        <div class="texto"><span><?php echo $oInspector->getIdentifier(); ?></span></div>
                    </div>
                </div>
                <div class="column-list-right">
                    <div class="column-list">
                        <div class="title"><span><b>REPORT DATE:</b></span></div>
                        <div class="texto"><span><?php echo date("F d, Y"); ?></span></div>
                        <div class="title"><span><b>TEST DEVICE:</b></span></div>
                        <div class="texto"><span><?php echo $oDevice->getName(); ?></span></div>
                    </div>

                    <div class="column-list">
                        <div class="title"><span><b>AARST-NRPP DEVICE CODE:</b></span></div>
                        <div class="texto"><span>8300</span></div>
                    </div>

                    <div class="column-list">
                        <div class="title"><span><b>TEST DEVICE S/N:</b></span></div>
                        <div class="texto"><span><?php echo $oDevice->getSerialNumber(); ?></span></div>
                        
                        <div class="title"><span><b>CALIBRATION DATE:</b></span></div>
                        <div class="texto"><span><?php echo $oCalibration; ?></span></div>
                        <div class="title"><span><b>CALIBRATION EXPIRES:</b></span></div>
                        <div class="texto"><span><?php echo $oCalibrationExpires; ?></span></div>
                        
                        <div class="title"><span><b>TEST ID:</b></span></div>
                        <div class="texto"><span class="testid"><?php echo $idTest; ?></span></div>
                    </div>
                </div>
            </div>

            <div class="rayastxt">
                The USEPA recommends action be taken to reduce radon concentrations if test results are at or exceed the USEPA Action Level.
                The attached sheet, "About Your Radon Test Results" provides additional information regarding the interpretation of these test
                results. The following data are the result of a short-term screening test and are representative of the levels measured during
                the test period only. Radon levels may change from room to room, day to day and season to season.
            </div>


            <div class="tablaFull">
                <div class="column-list-full">
                    <div class="column-list">
                        <div class="title"><span><b>RESULTS:</b></span></div>
                        <div class="texto"><span>The average radon level measured is 
                                <?php echo ($avgRadon < 4.0) ? "below" : "at or excedes"; ?>
                                the US EPA Action Level of 4.0 pCi/l.</span></div>
                    </div>
                </div>

                <div class="column-list-full">
                    <div class="column-list">
                        <div class="main_title"><span><b>TEST SUMMARY:</b></span></div>
                        <div class="description"><span><b>AVERAGE RADON CONCENTRATION (pCi/l):</b></span></div>
                        <div class="value"><span><?php echo Utilidades::floatFormat($avgRadon, 1); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>AVERAGE TEMPERATURE (°F):</b></span></div>
                        <div class="value"><span><?php echo Utilidades::floatFormat($avgTemperature, 1); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>AVERAGE BAROMETRIC PRESSURE (inHg):</b></span></div>
                        <div class="value"><span><?php echo Utilidades::floatFormat($avgPressure, 1); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>HUMIDITY (%):</b></span></div>
                        <div class="value"><span><?php echo Utilidades::floatFormat($avgHumidity, 0); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>TEST START DATE:</b></span></div>
                        <div class="value"><span><?php echo Utilidades::dateFormat($datetime[0], "m/d/Y"); ?></span></div>

                        <?php
                        $temp_start_time = strtotime($datetime[0]);
                        $new_start_time = date("Y-m-d H:i:s", strtotime('-1hours', $temp_start_time));
                        ?>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>TEST START TIME:</b></span></div>
                        <div class="value"><span><?php echo Utilidades::dateFormat($new_start_time, "H:i:s"); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>TEST END DATE:</b></span></div>
                        <div class="value"><span><?php echo Utilidades::dateFormat($datetime[$index - 1], "m/d/Y"); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>TEST END TIME:</b></span></div>
                        <div class="value"><span><?php echo Utilidades::dateFormat($datetime[$index - 1], "H:i:s"); ?></span></div>

                        <div class="main_title"><span style="color:white;">. </span></div>
                        <div class="description"><span><b>TEST DURATION:</b></span></div>
                        <?php
//                        $date2 = new DateTime($datetime[$index - 1]);
//                        $date1 = new DateTime($datetime[0]);
//
//                        $diff = $date1->diff($date2);
//                        $hours = $diff->h;
//                        $minutes = $diff->i;
//                        
//                        if($minutes > 30) {
//                            $hours = $hours + 1;
//                        }
//                        
//                        $hours = $hours + ($diff->days * 24);

                        $hours = $index;
                        ?>
                        <div class="value"><span><?php echo ' ' . $hours . ' h.'; ?></span></div>
                    </div>
                </div>


                <div class="column-list-full">
                    <div class="column-list">
                        <div class="title_full"><span><b>COMMENTS OR OBSERVATIONS CONCERNING CONDITIONS AT BEGINNING OF TEST:</b></span></div>
                        <div class="text_full"><span><?php echo $oTest->getCommentsBegin(); ?></span></div>
                    </div>
                </div>

                <div class="column-list-full">
                    <div class="column-list">
                        <div class="title_full"><span><b>COMMENTS OR OBSERVATIONS CONCERNING CONDITIONS AT CONCLUSION OF TEST:</b></span></div>
                        <div class="text_full"><span><?php echo $oTest->getCommentsEnd(); ?></span></div>
                    </div>
                </div>

            </div>


            <div class="rayastxt" style="border-bottom: none;">
                This test has been performed in accordance with EPA testing protocols, which include the requirement to maintain "Closed
                Building Conditions." If the home is occupied during the test, the tester has notified the occupants of the home or a seller’s
                representative of these requirements and requested their cooperation. While certain procedures, precautions and quality
                controls have been taken to ensure that these and other conditions for the test have been met, EnviroLabs Incorporated cannot
                guarantee the absence of circumstances beyond its control which may have affected the outcome of the test. If you have any
                questions regarding this test or have concerns about radon, please call EnviroLabs Incorporated at (301) 468-6666.
            </div>

            <pagebreak sheet-size="Letter" />
            <div class="testid" style="font: 11px Arial,Helvetica,sans-serif;margin:0 0 10px 400px;">
                <strong>TEST ID:&nbsp;</strong><?php echo $idTest; ?>
            </div>
            <div class="cuadro" style="margin: 20px 0 20px 45px;">
                <table class="hourly">
                    <tr>
                        <th class="tit-none">&nbsp;</th>
                        <th colspan="6" class="tit">HOURLY&nbsp;&nbsp;RESULTS</th>
                    </tr>
                    <tr>
                        <th width="42px" class="tit-none">&nbsp;</th>
                        <td width="88px" class="indTit">DATE</td>
                        <td width="75px" class="indTit">TIME</td>
                        <td width="105px" class="indTit">Radon <br /> (pCi/L)</td>
                        <td width="75px" class="indTit">Temp. <br /> (ºF) </td>
                        <td width="85px" class="indTit">Humidity (%H) </td>
                        <td width="105px" class="indTit borderRight">Pressure (inMg) </td>
                        <td width="25px" style="border-style: none;">&nbsp;</td>

    <!--                        <th width="37px" class="tit-none">&nbsp;</th>
                            <td width="83px" class="indTit">DATE</td>
                            <td width="70px" class="indTit">TIME</td>
                            <td width="100px" class="indTit">Radon <br /> (pCi/L)</td>
                            <td width="70px" class="indTit">Temp. <br /> (ºF) </td>
                            <td width="80px" class="indTit">Humidity (%H) </td>
                            <td width="100px" class="indTit borderRight">Pressure (inMg) </td>
                            <td width="20px" style="border-style: none;">&nbsp;</td>-->
                    </tr>
                    <?php
                    $index = 1;
                    $hasE = false;
                    $hasM = false;
                    $hasP = false;

                    foreach ($vHourlyResults as $oHourlyResult) {
                        ?>
                        <tr>

                            <td class="tblIndex" style=""><?php echo $index++; ?></td>
                            <td class="tblItem"><?php echo Utilidades::dateFormat($oHourlyResult->getDateTime(), "m/d/Y"); ?></td>
                            <td class="tblItem"><?php echo Utilidades::dateFormat($oHourlyResult->getDateTime(), "H:i:s"); ?></td>
                            <td class="tblItem" style="background:#F2F2F2;"><?php echo Utilidades::floatFormat($oHourlyResult->getRadon(), 1); ?></td>
                            <td class="tblItem" style="border-left-width:1px;"><?php echo Utilidades::floatFormat(((($oHourlyResult->getTemperature() * 9) / 5) + 32), 1); ?></td>
                            <td class="tblItem"><?php echo Utilidades::floatFormat($oHourlyResult->getHumidity(), 0); ?></td>
                            <td class="tblItem borderRight"><?php echo Utilidades::floatFormat(($oHourlyResult->getPressure() * 0.295299830714), 1); ?></td>
                            <td style="vertical-align:middle;text-align:left;font:bold 12px Arial, Helvetica, sans-serif;color: #7f3f3f;line-height:18px;border-style: none">
                                <?php
                                $hrE = $oHourlyResult->getError(); // Error
                                $hrT = $oHourlyResult->getTilt(); // Tilt
                                $hrP = $oHourlyResult->getPowerOutage(); // PowerOutage
                                $O = "&nbsp;";

                                if (!empty($hrE)) {
                                    $O .= "E";
                                    $hasE = true;
                                } else {
                                    $O .= "";
                                }

                                if (!empty($hrT)) {
                                    $O .= "M";
                                    $hasM = true;
                                } else {
                                    $O .= "";
                                }

                                if (!empty($hrP)) {
                                    $O .= "P";
                                    $hasP = true;
                                } else {
                                    $O .= "";
                                }

//                                $O .= (!empty($hrE)) ? "E" : "";
//                                $O .= (!empty($hrT)) ? "M" : "";
//                                $O .= (!empty($hrP)) ? "P" : "";
                                echo $O;
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>

            <?php
            if ($hasM || $hasE || $hasP) {
                echo "<ul class='references'>";
            }

            if ($hasM) {
                echo '<li>M = Device was moved.</li>';
            }

            if ($hasE) {
                echo '<li>E = Errors of the device while performing the test.</li>';
            }

            if ($hasP) {
                echo '<li>P = Power to device was cut off. </li>';
            }

            if ($hasM || $hasE || $hasP) {
                echo "</ul>";
            }
            ?>


            <pagebreak sheet-size="Letter" />


            <div class="testid" style="font: 11px Arial,Helvetica,sans-serif;margin:0 0 10px 400px;">
                <strong>TEST ID:&nbsp;</strong><?php echo $idTest; ?>
            </div>

            <div class='txtGraphics'>
                <p style="font-size:14pt;">
                    Radon Data Graphs:
                </p>

                <p>
                    Radon concentrations vary from season to season, week to week, day by day, and hour by hour. US EPA
                    guidelines state that action should be taken to reduce radon levels when the average during the test period is
                    equal to or greater than 4.0 pCi/L.
                </p>
                <p>
                    The following graph shows the hour by hour changes in the radon level. Large fluctuations in the average radon
                    level may be observed when taking hour by hour readings. Concentrations above the action level during short
                    intervals are not indicative of a situation that necessarily warrants remediation. One should base a decision to
                    undertake remediation upon observing an average over at least 48 hours.
                </p>
            </div>

            <div class="radon-concentrations">
                <img src="../../files/reports/tmp/img/<?php echo $imgRAvg; ?>" class="report-img" />
            </div>

            <div class="txtGraphics">
                <p>
                    The following page provides graphs of the radon level along with barometric pressure, humidity, and temperature
                    which indicate the conditions the under which the radon test was performed. Changes in barometric pressure can
                    have an effect on conditions in the ground around the house and may correlate with changes in the radon level.
                    Drastic sudden changes in temperature and humidity may indicate a change in the operation of the HVAC system
                    or the introduction of outdoor air and may warrant further investigation.
                </p>
            </div>

            <pagebreak sheet-size="Letter" />

            <div class="radon-vs-baro">
                <img  height="250" width="700" src="../../files/reports/tmp/img/<?php echo $imgRP; ?>" class="report-img" />
            </div>
            <div class="radon-vs-baro">
                <img height="250" width="700" src="../../files/reports/tmp/img/<?php echo $imgRH; ?>" class="report-img" />
            </div>
            <div class="radon-vs-baro">
                <img height="250" width="700" src="../../files/reports/tmp/img/<?php echo $imgRT; ?>" class="report-img" />
            </div>

            <pagebreak sheet-size="Letter" />

            <?php
            $vImages = $oTest->getImages();
            if (!empty($vImages)) {
                if ($preview) {
                    $path = '../../uploads/tmp/';
                } else {
                    $path = '../../files/images/';
                }
                ?>
                <div class="testid" style="font: 11px Arial,Helvetica,sans-serif;margin:0 0 10px 400px;">
                    <strong>TEST ID:&nbsp;</strong><?php echo $idTest; ?>
                </div>
                <div style="width: 640px;display:block;position:relative;float: left;">
                    <?php
                    foreach ($vImages as $oImage) {
                        ?>
                        <img class="report-img" src="<?php echo $path . $oImage['name']; ?>"/>
                        <div><?php echo $oImage['description']; ?></div>
                        <pagebreak sheet-size="Letter" />
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>



            <div class="information">
                <div class="information-half">
                    <div class="title scratched">About Your Radon Test Results</div>
                    <div class="content">
                        <p>
                            Radon is a naturally occurring radioactive gas found
                            in homes throughout the United States. Nearly 1 in
                            15 homes in the United States is estimated to have
                            elevated levels of radon. Radon is measured in
                            picocuries per liter, or pCi/L. The US EPA estimates
                            the average indoor radon level to be about 1.3 pCi/L,
                            and a level of 0.4 pCi/L is typical in outdoor air. The
                            US Surgeon General has warned that radon is the
                            second leading cause of lung cancer in the United
                            States today. The EPA recommends that action be
                            taken to reduce the radon level if the test result is 4.0
                            pCi/L or higher. A qualified radon reduction
                            contractor can take steps to reduce the radon level.
                        </p>

                        <p>
                            The test that has been performed is a short term
                            screening test, which measures the radon level only
                            during the test period. Radon levels vary from room
                            to room, day to day, and season to season. If time
                            permits (more than 90 days) long-term tests can be
                            used to confirm short-term test results. A long term
                            test is more likely to give a reading closer to the yearround
                            average.
                        </p>

                        <p>
                            The test has been performed in accordance with US
                            EPA testing protocols, including the requirement to
                            maintain “Closed Building Conditions”. The tester has
                            left notice for occupants of the house. While certain
                            precautions and quality controls have been taken to
                            ensure these conditions and the integrity of the test,
                            EnviroLabs Incorporated cannot guarantee the
                            absence of circumstances beyond its control that
                            have affected the outcome of the test.
                        </p>
                    </div>

                    <div class="title">What to do if your results are
                        above the USEPA Action
                        Level?</div>
                    <div class="content">
                        <p>
                            The US EPA recommends having the problem fixed
                            by a qualified radon mitigation contractor if the results
                            of this test is 4.0pCi/L or higher. Most homes can be
                        </p>
                    </div>
                </div>

                <div class="information-half">
                    <div class="content" style="padding-top:0;">
                        <p style="padding-top:0;">
                            fixed for about the same cost as other common home
                            repairs. Many states require radon mitigation
                            professionals to be licensed, certified, or registered.
                            EnviroLabs makes no recommendations regarding
                            mitigation contractors and suggests verifying
                            credentials with the National Radon Proficiency
                            Program (NRPP), the National Radon Safety Board
                            (NRSB), and any relevant local building authorities.
                        </p>

                        <p>
                            Depending on the type of construction of the house
                            the radon mitigation contractor may recommend
                            different methods of radon reduction which will
                            typically involve venting the radon outdoors before it
                            enters the house.
                        </p>

                        <p>
                            You can find a listing of all NRPP Certified Radon
                            Mitigation Contractors by clicking on the previous link
                            and narrowing your search to Mitigation Providers for
                            your state.
                        </p>

                        <div class="title">For More Information About
                            Radon…</div>
                        <p>
                            The following sources also provide extensive
                            information regarding radon.


                        <ul class="listFirstLevel">
                            <li>National Safety Council (800) SOS-RADON (1-800-767-7236)</li>
                            <li>The Radon FIX-IT Program, (800) 644-6999</li>
                            <li>National Radon Proficiency Program (NRPP) 800-269-4174 <a href="www.aarst-nrpp.com/wp/">www.aarst-nrpp.com/wp/</a></li>
                            <li>National Radon Safety Board (NRSB) 866-329-3474 <a href='www.nrsb.org'>www.nrsb.org</a></li>
                            <li>USEPA publications:</li>
                        </ul>

                        <ul class="listSecondLevel">
                            <li>Home Buyers and Sellers Guide
                                to Radon</li>
                            <li>Consumer’s Guide to Radon
                                Reduction</li>
                            <li>A Citizen's Guide to Radon: The
                                Guide to Protecting Yourself and
                                Your Family from Radon</li>
                        </ul>

                        </p>

                        <div class="logo" style='float:right;'><img src="../../img/logo_envirolabs.png" width="200" /></div>
                    </div>
                </div>
            </div>

            <div class='footer'>
                <div>13-15 East Deer Park Drive, Suite 202, Gaithersburg, MD 20877</div>
                <div style='text-align: right;'>info@envirolabs-inc.com</div>
                <div>Tel. (301) 468-6666, (703) 242-0000</div>
                <div style='text-align: right;'>www.envirolabs-inc.com</div>
            </div>


        </div>	
    </div>