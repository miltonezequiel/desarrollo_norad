<?php

require_once ROOT_DIR . '/libs/jpgraph/src/jpgraph.php';
require_once ROOT_DIR . '/libs/jpgraph/src/jpgraph_line.php';
require ROOT_DIR . '/libs/jpgraph/trend-line_regression-analysis.php';

function generateGraph( $d1, $d2, $datay1, $datay2 ) {
	//Asignaciones de variables de datos a comparar
	switch ( $d1 ) {
		case 'r':
			$name1  = 'Radon';
			$color1 = "#6495ED";
			break;
		case 'h':
			$name1  = 'Humidity';
			$color1 = "#00B050";
			break;
		case 'p':
			$name1  = 'Pressure';
			$color1 = "#BE4B48";
			break;
		case 't':
			$name1  = 'Temperature';
			$color1 = "#FF0000";
			break;
	}

	switch ( $d2 ) {
		case 'r':
			$name2  = 'Radon';
			$color2 = "#6495ED";
			break;
		case 'h':
			$name2  = 'Humidity';
			$color2 = "#00B050";
			break;
		case 'p':
			$name2  = 'Pressure';
			$color2 = "#BE4B48";
			break;
		case 't':
			$name2  = 'Temperature';
			$color2 = "#FF0000";
			break;
		case 'ravg':
			$name2  = 'Average Radon';
			$color2 = "#BE4E4D";
			break;
	}

	// Setup the graph
	$graph = new Graph( 1300, 600 );
	$graph->SetScale( "textlin" );

	$theme_class = new UniversalTheme;
	$graph->SetTheme( $theme_class );
	$graph->img->SetAntiAliasing( false );
	$graph->title->Set( $name1 . ' vs ' . $name2 );
	$graph->SetBox( false );

	$graph->img->SetAntiAliasing();

	$graph->yaxis->HideZeroLabel();
	$graph->yaxis->HideLine( false );
	$graph->yaxis->HideTicks( false, false );

	$graph->xgrid->Show();
	$graph->xgrid->SetLineStyle( "solid" );
	$graph->xaxis->SetTickLabels( Utilidades::generateXAxisTicks( 0, count( $datay1 ), 6 ) );
	$graph->xgrid->SetColor( '#E3E3E3' );

	// Create the first line
	$p1 = new LinePlot( $datay1 );
	$graph->Add( $p1 );
	$p1->SetColor( $color1 );
	$p1->SetLegend( $name1 );

	// Create the second line
	$p2 = new LinePlot( $datay2 );
	if ( ( $d1 === 'h' or $d1 === 'p' or $d1 === 't' ) or ( $d2 === 'h' or $d2 === 'p' or $d2 === 't' ) ) {
		$graph->SetYScale( 0, 'lin',(min($datay2)-1),(max($datay2)+1) );
		$graph->AddY( 0, $p2 );
		$graph->ynaxis[0]->SetColor( 'teal' );
	} else {
		$graph->Add( $p2 );
	}

	$p2->SetColor( $color2 );
	$p2->SetLegend( $name2 );

	$graph->legend->SetFrameWeight( 1 );
	$graph->SetMargin(33,33,0,0);

	// Output line
	$filename = Utilidades::generateImgGraphFileName( $_SESSION['idInspector'], $d1, $d2, "png" );
	$graph->Stroke( ROOT_DIR . "/files/reports/tmp/img/" . $filename );

	return $filename;
}

function roundUp( $numToRound, $multiple ) {
	if ( $multiple == 0 ) {
		return $numToRound;
	}

	$remainder = abs( $numToRound ) % $multiple;
	if ( $remainder == 0 ) {
		return $numToRound;
	}

	if ( $numToRound < 0 ) {
		return - ( abs( $numToRound ) - $remainder );
	} else {
		return $numToRound + $multiple - $remainder;
	}
}

?>