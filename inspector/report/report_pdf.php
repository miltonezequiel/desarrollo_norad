<?php
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';
include_once ROOT_DIR . '/entidades/hourly_result.php';
include_once ROOT_DIR . '/entidades/test.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/client.php';
include_once ROOT_DIR . '/entidades/inspector.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/inspector/report/report_graph.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/entidades/device_company.php';

$servicios      = new Servicios();
$oTest          = $servicios->getTestById( $idTest );
$vHourlyResults = $servicios->getHourlyResultsByIdTest( $idTest );

$oDeviceInspector = $servicios->getDevicesInspectorById( $oTest->getTestDiDcId() );
$oDeviceCompany   = $servicios->getDevicesCompanyById( $oDeviceInspector->getIdDeviceCompany() );
$oInspector       = $servicios->getInspectorById( $oDeviceInspector->getIdInspector() );
$oCompany         = $servicios->getCompanyById( $oDeviceCompany->getIdCompany() );
$oDevice          = $servicios->getDeviceById( $oDeviceCompany->getIdDevice() );

$oClient = $servicios->getClientById( $oTest->getIdClient() );

$oCalibration = $servicios->getCalibrationDate( $oDevice );
$oCalibrationExpires;
if ( ! $oCalibration ) {
	$oCalibration        = "N/S";
	$oCalibrationExpires = "N/S";
} else {
	$oCalibrationExpires = date( 'F d, Y', strtotime( '+1 year', strtotime( $oCalibration ) ) );
	$oCalibration        = date( 'F d, Y', strtotime( $oCalibration ) );
}

$html = Utilidades::generateHtmlReport( $oClient, $oTest, $oInspector, $oCompany, $oDevice, $idTest, $vHourlyResults, false, $oCalibration, $oCalibrationExpires );

//==============================================================
//==============================================================
//==============================================================
$testFileName    = 'Radon Test Report_' . $oTest->getId() . '_' . $oTest->getAddress() . '-' . $oTest->getCity() . '-' . $oTest->getState() . '-' . $oTest->getZip() . '_' . date( 'd-m-Y' );
$escapedTestFileName = preg_replace( '/[^A-Za-z0-9_\-]/', '_', $testFileName ) . '.pdf';

$oTest->setFile( $escapedTestFileName );
$servicios->updateTestFile( $oTest );

include( ROOT_DIR . "/mpdf/mpdf.php" );

$mpdf = new mPDF( 'c', 'Letter' );
$mpdf->SetDisplayMode( 'fullpage' );
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

$stylesheet = file_get_contents( ROOT_DIR . '/css/report.css' );
$mpdf->WriteHTML( $stylesheet, 1 ); // The parameter 1 tells that this is css/style only and no body/html/text


$mpdf->WriteHTML( $html, 2 );
//$mpdf->Output();
$mpdf->Output( ROOT_DIR . "/files/reports/" . $escapedTestFileName, 'F' );

//==============================================================
//==============================================================
//==============================================================
?>