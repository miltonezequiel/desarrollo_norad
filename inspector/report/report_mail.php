<?php

require_once ROOT_DIR . '/libs/PHPMailer/PHPMailerAutoload.php';

function sendReportEmail($address, $name, $testFileName, $subject, $body) {
    $mail = new PHPMailer();
    $mail->setFrom('reports@noradonline.com', 'NORAD');
    $mail->addAddress($address, $name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->addAttachment(ROOT_DIR . "/files/reports/" . $testFileName);
    $mail->send();
}

function sendReportEmailAndMailReport($address, $name, $testFileName, $subject, $body, $mailReportFile) {
    $mail = new PHPMailer();
    $mail->setFrom('reports@noradonline.com', 'NORAD');
    $mail->addAddress($address, $name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->addAttachment(ROOT_DIR . "/files/reports/" . $testFileName);
    $mail->addAttachment(ROOT_DIR . "/files/reports/" . $mailReportFile);
    $mail->send();
}

function sendReportEmailAndMailReportAndReceipt($address, $name, $testFileName, $subject, $body, $mailReportFile, $receiptReportFile) {
    $mail = new PHPMailer();
    $mail->setFrom('reports@noradonline.com', 'NORAD');
    $mail->addAddress($address, $name);
    $mail->Subject = $subject;
    $mail->Body = $body;
    $mail->addAttachment(ROOT_DIR . "/files/reports/" . $testFileName);
    $mail->addAttachment(ROOT_DIR . "/files/reports/" . $mailReportFile);
    $mail->addAttachment(ROOT_DIR . "/files/reports/" . $receiptReportFile);
    $mail->send();
}

foreach ($vHourlyResults as $oHourlyResult) {
    $rn[$index] = $oHourlyResult->getRadon();
    $index++;
}

if ($index == 0) {
    $index = 1;
}

$avgRadon = array_sum($rn) / ($index);
$avgRadon = Utilidades::floatFormat($avgRadon, 1);

$addressString = $oTest->getAddress() . ", " . $oTest->getCity() . ", " . $oTest->getState() . ", " . $oTest->getZip();
$subject = "Radon Test Results, " . $addressString;

$radonLevel = ($avgRadon < 4.0) ? "BELOW" : "AT OR EXCEDES";

$body = "The average radon concentration for the test performed at " . $addressString . " was " . $avgRadon . " pCi/l.\n\n";
$body = $body . "This value is " . $radonLevel . " the USEPA Action Level of 4.0 pCi/l \n\n";
$body = $body . "Your detailed radon test report is included as an attachment to this email.\n\n";
$body = $body . "Best Regards,\n\n";
$body = $body . "The NORAD Team \n\n";
$body = $body . "Envirolabs Incorporated \n\n";

sendReportEmail($oClient->getEmail(), $oClient->getName(), $escapedTestFileName, $subject, $body);
sendReportEmail($oClient->getEmail2(), $oClient->getName(), $escapedTestFileName, $subject, $body);

sendReportEmail($oClient->getSecondEmail(), $oClient->getSecondName(), $escapedTestFileName, $subject, $body);
sendReportEmail($oClient->getSecondEmail2(), $oClient->getSecondName(), $escapedTestFileName, $subject, $body);

sendReportEmail($oClient->getAgentEmail(), $oClient->getNameAgent(), $escapedTestFileName, $subject, $body);
sendReportEmail($oClient->getAgentEmail2(), $oClient->getNameAgent(), $escapedTestFileName, $subject, $body);

sendReportEmail($oClient->getSecondAgentEmail(), $oClient->getSecondAgentName(), $escapedTestFileName, $subject, $body);
sendReportEmail($oClient->getSecondAgentEmail2(), $oClient->getSecondAgentName(), $escapedTestFileName, $subject, $body);

//send message to company e-mail and inspector e-mail
require ROOT_DIR . '/inspector/step5/report_emails.php';
require ROOT_DIR . '/inspector/step5/report_receipts.php';

sendReportEmailAndMailReportAndReceipt($oCompany->getEmail(), $oCompany->getName(), $escapedTestFileName, $subject, $body, $escapedMailReportFileName, $escapedPaymentReportFileName);

sendReportEmailAndMailReport($oInspector->getEmail(), $oInspector->getName(), $escapedTestFileName, $subject, $body, $escapedMailReportFileName);

$_SESSION['inspectorMail'] = $oInspector->getEmail();


//Envio de copias opcionales
if (!empty($vContactsCopy)) {
    $mailBody = "Test id: {$idTest}\n\n";
    foreach ($vContactsCopy as $oContactCopy) {
        $mailBody.="Contact copy:\n";
        $mailBody.="Name:{$oContactCopy->getName()}\n";
        $mailBody.="Street:{$oContactCopy->getStreet()}\n";
        $mailBody.="City:{$oContactCopy->getCity()}\n";
        $mailBody.="State:{$oContactCopy->getState()}\n";
        $mailBody.="Zip:{$oContactCopy->getZip()}\n\n\n\n";
    }

    $mail = new PHPMailer();
    $mail->setFrom('reports@noradonline.com', 'NORAD');
    $mail->addAddress('reports@noradonline.com', 'NORAD');
    $mail->Subject = 'Contacts copy test id ' . $idTest;
    $mail->Body = $mailBody;
    $mail->send();
}
?>