<?php

include_once '../../util/utilidades.php';
include_once '../../entidades/payment.php';

$oPayment = $_SESSION['oPayment'];

$html = Utilidades::generateReceiptReport();

$mpdf = new mPDF( 'c', 'Letter' );
$mpdf->SetDisplayMode( 'fullpage' );
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

$stylesheet = file_get_contents( '../../css/report.css' );
$mpdf->WriteHTML( $stylesheet, 1 ); // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML( $html, 2 );


$oTest = $_SESSION['oTest'];


$strDocType = "";
if ( $oPayment->getPaymentType() == 1 ) {
	$strDocType = "Invoice";
} else if ( $oPayment->getPaymentType() == 2 ) {
	$strDocType = "Receipt";
} else {
	$strDocType = "Statement";
}

$paymentReportFileName = $strDocType . '_' . $oTest->getAddress() . '-' . $oTest->getCity() . '-' . $oTest->getState() . '-' . $oTest->getZip() . '_' . date( 'd-m-Y' );

$escapedPaymentReportFileName = preg_replace( '/[^A-Za-z0-9_\-]/', '_', $paymentReportFileName ) . '.pdf';
//$mpdf->Output();
$mpdf->Output( ROOT_DIR . "/files/reports/" . $escapedPaymentReportFileName, 'F' );
?>