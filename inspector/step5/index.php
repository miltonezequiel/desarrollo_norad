<?php
include_once '../../init.php';
include_once ROOT_DIR . '/entidades/client.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include '../login_inspector_check.php';
include '../../login_check.php';

$page = "step5";
$path = '../../';
if (!isset($_SESSION['oTest']) || !isset($_SESSION['oClient']) || !isset($_SESSION['vHourlyResults'])) {
    header("Location: ../step1");
}
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
    <body>
        <iframe NAME="_jnlp_" STYLE="visibility:hidden;display:none"></iframe>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="menu-step"><?php include_once($path . 'includes/menu-step.php'); ?></div>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div class="text">
                            <h1>DONE!</h1>
                            <p>Your test has been completed successfully and the Test Reports have been submitted to the following addresses:</p>
                            <p>
                            <ul class="mailedList">
                                <?php
                                if ($_SESSION['oClient']->getEmail() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getEmail() . "</li>";
                                }

                                if ($_SESSION['oClient']->getEmail2() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getEmail2() . "</li>";
                                }

                                if ($_SESSION['oClient']->getSecondEmail() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getSecondEmail() . "</li>";
                                }

                                if ($_SESSION['oClient']->getSecondEmail2() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getSecondEmail2() . "</li>";
                                }

                                if ($_SESSION['oClient']->getAgentEmail() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getAgentEmail() . "</li>";
                                }

                                if ($_SESSION['oClient']->getAgentEmail2() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getAgentEmail2() . "</li>";
                                }

                                if ($_SESSION['oClient']->getSecondAgentEmail() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getSecondAgentEmail() . "</li>";
                                }

                                if ($_SESSION['oClient']->getSecondAgentEmail2() != '') {
                                    echo "<li>" . $_SESSION['oClient']->getSecondAgentEmail2() . "</li>";
                                }
                                ?>
                            </ul>
                            </p>

<!--<p>Please click <a href="report_emails.php" target="_blank">HERE</a> to generate a report containing a list of the sent E-mail addresses.</p>-->

                            <p>A copy of the test report has also been sent to you at <span class="negrita"><?php echo $_SESSION['inspectorMail'] ?></span></p>
                            <br/>
                            <?php
                            if (trim($_SESSION['oCompany']->getCompanyPaymentType()) != 'Invoice') {
                                echo '<p>Your Credit Card <b>' . $_SESSION['creditCard'] . '</b> has been charged: <b>$ ' . $_SESSION['testFee'] . '</b></p>';
                            } else {
                                echo '<p>The <b>$ ' . $_SESSION['testFee'] . '</b> fees for this test have been added to your invoice.</p>';
                            }
                            ?>
                            <br/>

                            <br/>
                            <br/>
                            <p>Please do not unplug the test device from the USB Connection until the device has reset and returned to READY MODE</p>
                            <br/>
                            <form method="post" action="reset_device.php" target="_jnlp_">
                                <input type="image" src="<?php echo $path; ?>images/step1-btn-reset.jpg" value="RESET"/>
                            </form>
                            <br/>
                        </div>
                        <div class="btn">
                            <form method="post">
                                <div id="return"><input type="submit" class="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   
<?php
//delete img files
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRP']));
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRH']));
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRT']));
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRAvg']));

array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRP2']));
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRH2']));
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRT2']));
array_map('unlink', glob($path . 'files/reports/tmp/img/' . $_SESSION['imgRAvg2']));

include ROOT_DIR . '/inspector/session_clean.php';

$servicios = new Servicios();
$id = $_SESSION['user']['id'];
if (isset($id) && !empty($id)) {
    //$servicios = new Servicios();
    $oUser = $servicios->getUserById($id);
    if (isset($oUser)) {
        $tid = $oUser->getDTestID();
        if (isset($tid) && !empty($tid)) {
            if ((int) $tid > 0) {
                $servicios->eraseTemplyResultsByIdTest($tid);
            }
        }

        $oUser->setDeviceID(0);
        $oUser->setDeviceSN(0);
        $oUser->setDTestID(0);
        $oUser->setLogged(1); // userIsOn
        $servicios->editUserDevice($oUser);
    }
}
?>