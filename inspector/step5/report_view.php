<?php
include_once '../../entidades/client.php';
include_once '../../entidades/company.php';
include_once '../../entidades/test.php';
include '../login_inspector_check.php';
include '../../login_check.php';
?>

<div class="logo"><img src="../../img/logo_envirolabs.png" width="240" /></div>
<div class="right" style="margin-top:0.8em;">
    <div class="title">E-Mail sent Report</div>
    <div class="text"><strong>Indoor Air Radon Test</strong></div>
</div>
<div id="wrapper" class="informe">
    <div id="container">
        <div id="main" style="margin-top:5px; height: 83%;">
            <div class="rayastxt" style="border:none; font:normal 12pt Arial, Helvetica, sans-serif;">


                <p><?php echo date('l, d F Y h:i:s A'); ?></p>

                <p>
                    The Indoor Air Radon Report performed at: <?php echo $_SESSION['oTest']->getAddress() . ', ' . $_SESSION['oTest']->getCity() . ', ' . $_SESSION['oTest']->getState() . ', ' . $_SESSION['oTest']->getZip(); ?> 
                    <br> for: <?php echo $_SESSION['oClient']->getName(); ?> has been sent to the following email addresses:
                </p>

                <div style="padding-left:4em; font:normal 12pt Arial, Helvetica, sans-serif;">
                    <ul class="mailedList" style="list-style-type: square">
                        <?php
                        if ($_SESSION['oClient']->getEmail() != '') {
                            echo "<li>" . $_SESSION['oClient']->getName() . ' - ' . $_SESSION['oClient']->getEmail() . "</li>";
                        }

                        if ($_SESSION['oClient']->getEmail2() != '') {
                            echo "<li>" . $_SESSION['oClient']->getName() . ' - ' . $_SESSION['oClient']->getEmail2() . "</li>";
                        }

                        if ($_SESSION['oClient']->getSecondEmail() != '') {
                            echo "<li>" . $_SESSION['oClient']->getSecondName() . ' - ' . $_SESSION['oClient']->getSecondEmail() . "</li>";
                        }

                        if ($_SESSION['oClient']->getSecondEmail2() != '') {
                            echo "<li>" . $_SESSION['oClient']->getSecondName() . ' - ' . $_SESSION['oClient']->getSecondEmail2() . "</li>";
                        }

                        if ($_SESSION['oClient']->getAgentEmail() != '') {
                            echo "<li>" . $_SESSION['oClient']->getNameAgent() . ' - ' . $_SESSION['oClient']->getAgentEmail() . "</li>";
                        }

                        if ($_SESSION['oClient']->getAgentEmail2() != '') {
                            echo "<li>" . $_SESSION['oClient']->getNameAgent() . ' - ' . $_SESSION['oClient']->getAgentEmail2() . "</li>";
                        }

                        if ($_SESSION['oClient']->getSecondAgentEmail() != '') {
                            echo "<li>" . $_SESSION['oClient']->getSecondAgentName() . ' - ' . $_SESSION['oClient']->getSecondAgentEmail() . "</li>";
                        }

                        if ($_SESSION['oClient']->getSecondAgentEmail2() != '') {
                            echo "<li>" . $_SESSION['oClient']->getSecondAgentName() . ' - ' . $_SESSION['oClient']->getSecondAgentEmail2() . "</li>";
                        }
                        ?>
                    </ul>
                </div>

            </div>

        </div>	

        <div class='footer' style="border-top:1px solid black;">
            <div>13-15 East Deer Park Drive, Suite 202, Gaithersburg, MD 20877</div>
            <div style='text-align: right;'>info@envirolabs-inc.com</div>
            <div>Tel. (301) 468-6666, (703) 242-0000</div>
            <div style='text-align: right;'>www.envirolabs-inc.com</div>
        </div>
    </div>