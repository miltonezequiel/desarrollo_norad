<?php
include_once '../../init.php';
include_once '../../entidades/client.php';
include_once '../../entidades/company.php';
include_once '../../entidades/test.php';

include_once '../../entidades/payment.php';
include_once '../../entidades/payment_detail.php';

include '../login_inspector_check.php';
include '../../login_check.php';
include '/authorizenet/authorize.php';

$oPayment = $_SESSION['oPayment'];
$oPaymentsDetails = $_SESSION['oPaymentsDetails'];

$payment_mode = "Cash";

//$site = $_SESSION['oTest']->getAddress() . ', ' . $_SESSION['oTest']->getCity() . ', ' . $_SESSION['oTest']->getState() . ' ' . $_SESSION['oTest']->getZip() . ' on ' . date('d-m-Y');

//if ($_SESSION['oCompany']->getCustomerProfileId() != '' && $_SESSION['oCompany']->getCustomerProfileId() != "0") {
//    $authorize = new Authorizenet();
//
//    $result = array(
//        "result" => "",
//        "cardNumber" => "",
//        "name" => "",
//        "error" => ""
//    );
//
//
//    $result = $authorize->getCustomerPaymentProfile($_SESSION['oCompany']->getCustomerProfileId(), $_SESSION['oCompany']->getCustomerPaymentProfileId());
//    $payment_mode = "Credit card (" . $result['cardNumber'] . ")";
//}
?>

<div class="logo"><img src="<?php echo ROOT_DIR ?>/img/logo_envirolabs.png" width="240" /></div>
<div class="right" style="margin-top:0.8em;">
    <div class="title">
        <?php
        $documentType = "";
        if ($oPayment->getPaymentType() == 1) {
            $documentType = "Invoice";
        } else if ($oPayment->getPaymentType() == 2) {
            $documentType = "Receipt";
        } else {
            $documentType = "Statement";
        }
        echo $documentType;
        ?>
    </div>
    <div class="text"><strong>Indoor Air Radon Test</strong></div>
</div>

<div id="wrapper" class="informe">
    <div id="container">
        <div id="main" style="margin-top:5px; height: 83%;">
            <div id="receiptHeader">
                <div id="receiptHeaderLeft">
                    <strong>Billed To:</strong><BR>
                    <?php echo strtoupper($_SESSION['oCompany']->getName()) . " - " . $_SESSION['oCompany']->getContactPerson(); ?><BR>
                    <?php echo $_SESSION['oCompany']->getAddress1(); ?><BR>
                    <?php echo $_SESSION['oCompany']->getCity() . ", " . $_SESSION['oCompany']->getState() . ", " . $_SESSION['oCompany']->getZip(); ?><BR>
                </div>
                <div id="receiptHeaderRight">

                    <strong><?php echo $documentType; ?></strong> # <?php echo $oPayment->getPaymentNumber(); ?> <BR>
                    <strong>PAID WITH:</strong><BR>
                    <?php echo $payment_mode; ?><BR>
                    <?php //echo $_SESSION['oCompany']->getCompanyPaymentType() . " " . $result['cardNumber'];  ?><BR>
                </div>
            </div>

            <div class="cuadro" style="margin: 0; margin-top:3rem;">
                <table class="hourly">
                    <tr>
                        <th class="receiptTit large">Description</th>
                        <th class="receiptTit">Quantity</th>
                        <th class="receiptTit">Price</th>
                    </tr>
                    <?php
                    foreach ($oPaymentsDetails as $payDetail) {
                        echo '<tr>';
                        echo '    <td class="receiptTblItemDesc">' . $payDetail->getPaymentDetailsDesc() . '</td>';
                        echo '    <td class="receiptTblItem">' . $payDetail->getPaymentDetailsQuantity() . '</td>';
                        echo '    <td class="receiptTblItem">$' . $payDetail->getPaymentDetailsSubtotal() . '</td>';
                        echo '</tr>';
                    }
                    ?>

                    <tr>
                        <td class="receiptTblItem"></td>
                        <td class="receiptTot">TOTAL</td>
                        <td class="receiptTot">$<?php echo $oPayment->getPaymentTotal(); ?></td>
                    </tr>
                </table>
            </div>

            <p class="receipt_legend">We are very glad to have you as a customer. Please, feel free to share any feedback that might help us improve your experience.</p>

        </div>	

        <div class='receiptFooter' style="border-top:1px solid black;">
            <div>13-15 East Deer Park Drive, Suite 202, Gaithersburg, MD 20877</div>
            <div style='text-align: right;'>info@envirolabs-inc.com</div>
            <div>Tel. (301) 468-6666, (703) 242-0000</div>
            <div style='text-align: right;'>www.envirolabs-inc.com</div>
        </div>
    </div>
</div>