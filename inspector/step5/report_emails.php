<?php

include_once '../../util/utilidades.php';

$html = Utilidades::generateEmailsReport();

//include( "../../mpdf/mpdf.php");

$mpdf = new mPDF( 'c', 'Letter' );
$mpdf->SetDisplayMode( 'fullpage' );
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

$stylesheet = file_get_contents( '../../css/report.css' );
$mpdf->WriteHTML( $stylesheet, 1 ); // The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML( $html, 2 );
//$mpdf->Output();

$oTest = $_SESSION['oTest'];

$mailReportFileName = 'Email Sent Report_' . $oTest->getAddress() . '-' . $oTest->getCity() . '-' . $oTest->getState() . '-' . $oTest->getZip() . '_' . date( 'd-m-Y' );
$escapedMailReportFileName    = preg_replace( '/[^A-Za-z0-9_\-]/', '_', $mailReportFileName ) . '.pdf';

$mpdf->Output( ROOT_DIR . "/files/reports/" . $escapedMailReportFileName, 'F' );


?>