<?php

include_once '../../init.php';
include_once ROOT_DIR . '/entidades/contact_copy.php';
$vContactsCopy = array();
if (!empty($check_1)) {
    $oContactCopy1 = new ContactCopy("", $name_contact_copy_1, $street_contact_copy_1, $city_contact_copy_1, $zip_contact_copy_1, $state_contact_copy_1, "");
    array_push($vContactsCopy, $oContactCopy1);
}
if (!empty($check_2)) {
    $oContactCopy2 = new ContactCopy("", $name_contact_copy_2, $street_contact_copy_2, $city_contact_copy_2, $zip_contact_copy_2, $state_contact_copy_2, "");
    array_push($vContactsCopy, $oContactCopy2);
}
if (!empty($check_3)) {
    $oContactCopy3 = new ContactCopy("", $name_contact_copy_3, $street_contact_copy_3, $city_contact_copy_3, $zip_contact_copy_3, $state_contact_copy_3, "");
    array_push($vContactsCopy, $oContactCopy3);
}

$_SESSION['vContactsCopy'] = $vContactsCopy;
?>