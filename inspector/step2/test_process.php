<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//*/

include '../login_inspector_check.php';
include '../../login_check.php';
include_once '../../init.php';
include_once ROOT_DIR . '/util/utilidades.php';

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

//form variables
//Test
$streetAddress1Test = $_POST['street_address_1'];
$streetAddress2Test = $_POST['street_address_2'];
$cityTest = $_POST['city'];
$stateTest = $_POST['state'];
$zipTest = $_POST['zip'];
$floorLevelTest = $_POST['floor_level'];
$roomTest = $_POST['room'];
$commentsBeginTest = $_POST['comments_begin'];
$commentsEndTest = $_POST['comments_end'];

$idInspector = $_SESSION['user']['inspector']['id'];
$idDevice = $_SESSION['selectedDeviceId'];
$startDateDevice = $_SESSION['selectedDeviceStartDate'];

//Test img
$vImagesTest = array();
for ($i = 1; $i <= $GLOBAL_SETTINGS['test.img.max']; $i++) {
    $imageField = "image_" . $i;
    $imageDescriptionField = "image_description_" . $i;
    if (isset($_FILES[$imageField]["name"]) && ($_FILES[$imageField]["name"]) != NULL) {
        $imgName = Utilidades::generateImgTestFileName($idInspector, utf8_decode($_FILES[$imageField]["name"]), $i);
        copy(utf8_decode($_FILES[$imageField]["tmp_name"]), ROOT_DIR . "/" . $GLOBAL_SETTINGS['test.img.path.tmp'] . "/" . $imgName);
        $image = array("name" => $imgName, "description" => $_POST[$imageDescriptionField]);
        array_push($vImagesTest, $image);
    }
}
$_SESSION['vImagesTest'] = $vImagesTest;

//Client 1
$nameClient = $_POST['name_client'];
$emailClient = $_POST['email_client'];
$email2Client = $_POST['email_client_2'];

//Client 2
$second_nameClient = $_POST['2_name_client'];
$second_emailClient = $_POST['2_email_client'];
$second_email2Client = $_POST['2_email_client_2'];

//Agent 1
$nameAgent = $_POST['name_agent'];
$emailAgent = $_POST['email_agent'];
$email2Agent = $_POST['email_agent_2'];

//Agent 2
$second_nameAgent = $_POST['2_name_agent'];
$second_emailAgent = $_POST['2_email_agent'];
$second_email2Agent = $_POST['2_email_agent_2'];


if (isset($_POST['check_1'])){
	$check_1 = $_POST['check_1'];
}
$name_contact_copy_1 = $_POST['name_contact_copy_1'];
$street_contact_copy_1 = $_POST['street_contact_copy_1'];
$city_contact_copy_1 = $_POST['city_contact_copy_1'];
$state_contact_copy_1 = $_POST['state_contact_copy_1'];
$zip_contact_copy_1 = $_POST['zip_contact_copy_1'];

if (isset($_POST['check_2'])) {
	$check_2 = $_POST['check_2'];
}
$name_contact_copy_2 = $_POST['name_contact_copy_2'];
$street_contact_copy_2 = $_POST['street_contact_copy_2'];
$city_contact_copy_2 = $_POST['city_contact_copy_2'];
$state_contact_copy_2 = $_POST['state_contact_copy_2'];
$zip_contact_copy_2 = $_POST['zip_contact_copy_2'];

if (isset($_POST['check_3'])) {
	$check_3 = $_POST['check_3'];
}
$name_contact_copy_3 = $_POST['name_contact_copy_3'];
$street_contact_copy_3 = $_POST['street_contact_copy_3'];
$city_contact_copy_3 = $_POST['city_contact_copy_3'];
$state_contact_copy_3 = $_POST['state_contact_copy_3'];
$zip_contact_copy_3 = $_POST['zip_contact_copy_3'];


include 'client_add_session.php';
include 'contact_copy_add_session.php';
include 'test_add_session.php';
header('Location: ../step3');
?>