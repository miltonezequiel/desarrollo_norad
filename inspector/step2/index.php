<?php
include_once '../../init.php';
include_once ROOT_DIR . '/entidades/test.php';
include_once ROOT_DIR . '/entidades/client.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/entidades/contact_copy.php';
include '../login_inspector_check.php';
include '../../login_check.php';
include_once ROOT_DIR . '/servicios/servicios.php';

if ( session_status() == PHP_SESSION_NONE ) {
	session_start();
}

if ( isset( $_SESSION['oClient'] ) && ! is_null( $_SESSION['oClient'] ) ) {
	$oClient = $_SESSION['oClient'];
}
if ( isset( $_SESSION['oTest'] ) && ! is_null( $_SESSION['oTest'] ) ) {
	$oTest = $_SESSION['oTest'];
}
if ( isset( $_SESSION['vContactsCopy'] ) && ! is_null( $_SESSION['vContactsCopy'] ) ) {
	$vContactsCopy = $_SESSION['vContactsCopy'];
}

$page      = "step2";
$path      = '../../';
$servicios = new Servicios();

$oInspector = $servicios->getInspectorById( $_SESSION['user']['inspector']['id'] );

$oUser          = $servicios->getUserById( $_SESSION['user']['id'] );
$InternalTestID = $oUser->getDTestID();
if ( ( (int) $InternalTestID == 0 ) || ( (int) $oUser->getLogged() != 2 ) ) {
	header( "Location: ../index.php" );
}
$_SESSION['internalTestId']             = $InternalTestID;
$_SESSION['selectedDeviceId']           = $oUser->getDeviceID();
$_SESSION['selectedDeviceSerialNumber'] = $oUser->getDeviceSN();

$oDeviceInspector = $servicios->getDeviceInspectorByDeviceId( $_SESSION['selectedDeviceId'] );
if ( ! isset( $oDeviceInspector ) || is_null( $oDeviceInspector ) ) {
	header( "Location: ../index.php" );
}
$_SESSION['selectedDeviceStartDate'] = $oDeviceInspector->getStartDate();

$vStates = $servicios->getStates();
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
<head>
    <title>NORAD | Radon Detection System</title>
    <meta name="keywords" content=""/>
    <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css"/>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src='../../js/jquery-1.9.1.js'></script>
    <link rel="stylesheet" href="../../css/jquery-ui.css">
    <script src="../../js/jquery-ui.js"></script>
    <script src="../../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../../libs/jquery-validation/dist/additional-methods.min.js"></script>
    <script type="text/javascript">
        function clearContents(element) {
            element.value = '';
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.reporting').click(function () {
                $('#site_information').css({'display': 'none'});
                $('#reporting_requeriments').css({'display': 'block'});
                $('.reporting').addClass("reporting_activo").removeClass("reporting");
                $('.testsite_activo').addClass("testsite").removeClass("testsite_activo");
            });
            $('.testsite_activo').click(function () {
                $('#reporting_requeriments').css({'display': 'none'});
                $('#site_information').css({'display': 'block'});
                $('.testsite').addClass("testsite_activo").removeClass("testsite");
                $('.reporting_activo').addClass("reporting").removeClass("reporting_activo");
            });
            $('.reporting_activo').click(function () {
                //Do nothing
            });
        });

        $(function () {
            var rooms = [
                "Bedroom",
                "Family Room",
                "Kitchen",
                "Dining Room",
                "Mud Room",
                "Workroom",
                "Basement"
            ];
            $("#room").autocomplete({
                source: rooms,
                minLength: 0
            }).focus(function () {
                $(this).autocomplete('search');
            });
            var floorLevels = [
                "Lower level",
                "Basement",
                "First Floor"
            ];
            $("#floor_level").autocomplete({
                source: floorLevels,
                minLength: 0
            }).focus(function () {
                $(this).autocomplete('search');
            });
        });

        $(document).ready(function () {
            $('#photos-check').change(function () {
                $('#photos').slideToggle();
            });
        });
    </script>
    <script>
        function showCityAndState(str) {
            if (str.length == 0) {
                document.getElementById("zipcode").value = "";
                return;
            } else {
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        var r = JSON.parse(this.responseText);
                        var place = r['places'][0];
                        document.getElementById('city').value = place['place name'];
                        document.getElementById('state').value = place['state abbreviation'];
                    }
                }
                xmlhttp.open("GET", "http://api.zippopotam.us/us/" + str, true);
                xmlhttp.send();
            }
        }
    </script>
    <style>
        .testsite_activo, .testsite, .reporting_activo, .reporting {
            cursor: pointer;
        }
    </style>
</head>
<body>
<div id="container">
    <header><?php include_once( $path . 'includes/header.php' ); ?></header>
    <div id="menu-step"><?php include_once( $path . 'includes/menu-step.php' ); ?></div>
    <div id="wrapper" class="page-<?php echo $page ?>">
        <div id="container2">
            <div id="main">
                <div class="stform">
                    <form id="data_test" method="post" action="test_process.php" enctype="multipart/form-data"
                          autocomplete="off">
                        <div class="form-pp">
                            <div class="btn"><span>Test Conducted by <?php echo $oInspector->getName(); ?></span></div>
                            <div class="btn"><span>Tester ID <?php echo $oInspector->getIdentifier(); ?></Respan></div>
                        </div>
                        <div class="form-botonera">
                            <nav>
                                <a class="testsite_activo">test site information<span></span></a>
                                <a class="reporting">reporting requeriments<span></span></a>
                            </nav>
                        </div>
                        <div class="form-sp" id="site_information">
                            <div style="position: relative">
                                <input type="button" class="button" value="Continue" id="continue"
                                       style="right: 0; top: -60px; position: absolute"/>
                            </div>
                            <input type="text" name="street_address_1" class="box" placeholder="Street Address Line 1"
                                   value="<?php echo ( isset( $oTest ) ) ? $oTest->getAddress() : ""; ?>"/>
                            <input type="text" name="street_address_2" class="box" placeholder="Street Address Line 2"
                                   value="<?php echo ( isset( $oTest ) ) ? $oTest->getAddress2() : ""; ?>"/>
                            <input id='city' type="text" name="city" class="box" placeholder="City"
                                   value="<?php echo ( isset( $oTest ) ) ? $oTest->getCity() : ""; ?>"/>

                            <select name="state" id="state" >
								<?php
								foreach ( $vStates as $oState ) {
									echo "<option value='{$oState->getId()}'>{$oState->getId()}</option>";
								}
								?>
                            </select>
                            <input id="zipcode" type="text" name="zip" class="box3" placeholder="ZIP"
                                   value="<?php echo ( isset( $oTest ) ) ? $oTest->getZip() : ""; ?>" onkeyup="showCityAndState(this.value)"/>
                            <input type="text" name="floor_level" id="floor_level" class="box"
                                   placeholder="Floor Level Tested"
                                   value="<?php echo ( isset( $oTest ) ) ? $oTest->getFloorLevel() : ""; ?>"/>
                            <input type="text" name="room" id="room" class="box" placeholder="Room Tested"
                                   value="<?php echo ( isset( $oTest ) ) ? $oTest->getRoom() : ""; ?>"/>

                            <textarea name="comments_begin" class="box" rows="4"
                                      placeholder="Comments/Observations at Begining of Test"><?php echo ( isset( $oTest ) ) ? $oTest->getCommentsBegin() : ""; ?></textarea>
                            <textarea name="comments_end" class="box" rows="4"
                                      placeholder="Comments Observations at End of Test"><?php echo ( isset( $oTest ) ) ? $oTest->getCommentsEnd() : ""; ?></textarea>

                            <input type="checkbox" id="photos-check" value=""><label> Insert Photo Page?</label>
                            <br/>
                            <div style="display:none" id="photos">
                                <input type="file" size="1" name="image_1" accept="image/*"/><br/>
                                <input type="text" name="image_description_1"
                                       value="<?php echo ( isset( $oTest ) ) ? $oTest->getImageDescription1() : ""; ?>"/><br/>
                                <input type="file" size="1" name="image_2" accept="image/*"/><br/>
                                <input type="text" name="image_description_2"
                                       value="<?php echo ( isset( $oTest ) ) ? $oTest->getImageDescription2() : ""; ?>"/><br/>
                                <input type="file" size="1" name="image_3" accept="image/*"/><br/>
                                <input type="text" name="image_description_3"
                                       value="<?php echo ( isset( $oTest ) ) ? $oTest->getImageDescription3() : ""; ?>"/><br/>
                            </div>
                            <div class="error_site">
                            </div>
                            <!--<div id="savechanges"><input type="submit" class="submit" value=""/></div>-->
                        </div>

                        <div class="form-sp" id="reporting_requeriments" style="display:none">
                            <div id="save-button-and-error-message" style="display: block; position: relative">
                                <input type="submit" class="submit" value=""
                                       style="display: block; right: 0; top:-80px; z-index: 3; position: absolute"/>
                                <div class="error_rep">
                                </div>
                            </div>
                            <input type="text" name="name_client" class="box" placeholder="Client Name"
                                   value="<?php echo ( isset( $oClient ) ) ? $oClient->getName() : ""; ?>"/>
                            <input type="text" name="email_client" class="box" placeholder="Client Email 1"
                                   value="<?php echo ( isset( $oClient ) ) ? $oClient->getEmail() : ""; ?>"/>
                            <input type="text" name="email_client_2" class="box" placeholder="Client Email 2"
                                   value="<?php echo ( isset( $oClient ) ) ? $oClient->getEmail2() : ""; ?>"/>

                            <input type="text" name="2_name_client" class="box" placeholder="Client Name" value=""/>
                            <input type="text" name="2_email_client" class="box" placeholder="Client Email 1" value=""/>
                            <input type="text" name="2_email_client_2" class="box" placeholder="Client Email 2"
                                   value=""/>

                            <input type="text" name="name_agent" class="box" placeholder="Agent Name" value=""/>
                            <input type="text" name="email_agent" class="box" placeholder="Agent Email 1" value=""/>
                            <input type="text" name="email_agent_2" class="box" placeholder="Agent Email 2" value=""/>

                            <input type="text" name="2_name_agent" class="box" placeholder="Agent Name" value=""/>
                            <input type="text" name="2_email_agent" class="box" placeholder="Agent Email 1" value=""/>
                            <input type="text" name="2_email_agent_2" class="box" placeholder="Agent Email 2" value=""/>

							<?php
							$maxCC = 3;
							for ( $i = 0; $i < $maxCC; $i ++ ) {
								?>
                                <div class="ckeck-mgn"><input name="check_<?php echo( $i + 1 ); ?>"
                                                              type="checkbox" <?php echo ( isset( $vContactsCopy[ $i ] ) ) ? "checked" : ""; ?>><label>
                                        Optional Hardcopy Mailed to: ($5.00 charge applies) :</label></div>
                                <div class="show-input">
                                    <input type="text" name="name_contact_copy_<?php echo( $i + 1 ); ?>" class="box"
                                           placeholder="Additional Report to (name) :"
                                           value="<?php echo ( isset( $vContactsCopy[ $i ] ) ) ? $vContactsCopy[ $i ]->getName() : ""; ?>"/>
                                    <input type="text" name="street_contact_copy_<?php echo( $i + 1 ); ?>" class="box"
                                           placeholder="Street"
                                           value="<?php echo ( isset( $vContactsCopy[ $i ] ) ) ? $vContactsCopy[ $i ]->getStreet() : ""; ?>"/>
                                    <input type="text" name="city_contact_copy_<?php echo( $i + 1 ); ?>" class="box"
                                           placeholder="City"
                                           value="<?php echo ( isset( $vContactsCopy[ $i ] ) ) ? $vContactsCopy[ $i ]->getCity() : ""; ?>"/>
                                    <input type="text" name="state_contact_copy_<?php echo( $i + 1 ); ?>" class="box"
                                           placeholder="State"
                                           value="<?php echo ( isset( $vContactsCopy[ $i ] ) ) ? $vContactsCopy[ $i ]->getState() : ""; ?>"/>
                                    <input type="text" name="zip_contact_copy_<?php echo( $i + 1 ); ?>" class="box"
                                           placeholder="Zip"
                                           value="<?php echo ( isset( $vContactsCopy[ $i ] ) ) ? $vContactsCopy[ $i ]->getZip() : ""; ?>"/>
                                </div>
								<?php
							}
							?>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<footer><?php include_once( $path . 'includes/footer.php' ); ?></footer>
<script>
    $("#data_test").validate({
        rules: {
            street_address_1: "required",
            city: "required",
            state: "required",
            zip: "required",
            floor_level: "required",
            room: "required",
            name_client: "required",
            email_client: {
                required: true,
                email: true
            }
        }, messages: {
            street_address_1: "Please insert street address",
            city: "Please insert city",
            state: "Please insert state",
            zip: "Please insert zip code",
            floor_level: "Please insert floor level",
            room: "Please insert room",
            name_client: "Please insert name",
            email_client: {
                required: "Please insert email",
                email: "Please enter a valid email address"
            }
        },
        errorLabelContainer: $("#data_test div.error_rep")
    });
    $('#continue').click(function () {
        //Faltan las validaciones
        $('#site_information').css({'display': 'none'});
        $('#reporting_requeriments').css({'display': 'block'});
        $('.reporting').addClass("reporting_activo").removeClass("reporting");
        $('.testsite_activo').addClass("testsite").removeClass("testsite_activo");
    });
</script>
</body>
</html>