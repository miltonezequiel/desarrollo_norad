<?php

include_once '../../init.php';
include_once ROOT_DIR . '/entidades/hourly_result.php';
include_once ROOT_DIR . '/entidades/test.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

$servicios  = new Servicios();
$utilidades = new Utilidades();

$oDeviceCompany = $servicios->getCompanyByIdDevice( $idDevice );

$oTest = new Test( "", $streetAddress1Test, $streetAddress2Test,
	$cityTest, $stateTest, $zipTest, $floorLevelTest,
	$roomTest, $commentsBeginTest, $commentsEndTest,
	date( "Y-m-d" ), $oDeviceCompany->getId(), "" );

if ( isset( $vImagesTest[0] ) ) {
	$oTest->setImage1( $vImagesTest[0]['name'] );
	$oTest->setImageDescription1( $vImagesTest[0]['description'] );
}
if ( isset( $vImagesTest[1] ) ) {
	$oTest->setImage2( $vImagesTest[1]['name'] );
	$oTest->setImageDescription2( $vImagesTest[1]['description'] );
}
if ( isset( $vImagesTest[2] ) ) {
	$oTest->setImage3( $vImagesTest[2]['name'] );
	$oTest->setImageDescription3( $vImagesTest[2]['description'] );
}

$_SESSION['oTest'] = $oTest;

$InternalTestID = $_SESSION['internalTestId'];
if ( (int) $InternalTestID > 0 ) {
	$vHourlyResults = $servicios->getTemplyResultsByIdTest( $InternalTestID ); //lo cambio para que me genere el reporte (donde dice 1 va $InternalTestID)
}

$vHRDateTime = null;
if ( property_exists( get_class( $vHourlyResults[0] ), 'getDateTime' ) ) {
	$vHRDateTime = $vHourlyResults[0]->getDateTime;
}
list( $startDateTest, $startTimeTest ) = explode( " ", $vHRDateTime );
$oDevice = $servicios->getDeviceById( $idDevice, $startDateTest ); //Busco el device para encontrar el error en la fecha del test

if ( $oDevice->getError() == 0 ) {//Asigno radon
	foreach ( $vHourlyResults as $oHourlyResult ) {
		$oHourlyResult->setRadon( $oHourlyResult->getAlpha() );
	}
} else {
	foreach ( $vHourlyResults as $oHourlyResult ) {
		$oHourlyResult->setRadon( $oHourlyResult->getAlpha() / $oDevice->getError() );
	}
}


//delete extreme values (error values)

$index = 0;
foreach ( $vHourlyResults as $oHourlyResult ) {
	$rn[ $index ] = $oHourlyResult->getRadon();
	$index ++;
}

$stop_condition = 0;
$umbral         = 20;
$min_val        = min( $rn );
$replace_index  = array();

$quartile_1        = $utilidades->get_percentile( 25, $rn );
$quartile_3        = $utilidades->get_percentile( 75, $rn );
$range_multiplier  = 2.5;
$upper_limit       = $quartile_1 + $range_multiplier * ( $quartile_3 - $quartile_1 );
$replacement_value = ( $quartile_1 + $quartile_3 ) / 2;


while ( true ) {
	if ( $min_val < 1 ) {
		$min_val = 1;
	}

	$stop_condition = $max_val / $min_val;

	if ( $stop_condition > $umbral ) {
		//replace max_val with null in order to replace it when finishing
		$index           = array_search( $max_val, $rn );
		$rn[ $index ]    = null;
		$replace_index[] = $index;
	} else {
		break;
	}
}

$index = 0;
foreach ( $rn as &$valor ) {
	if ( $valor > $upper_limit ) {
		$rn[ $index ]    = null;
		$replace_index[] = $index;
	}
	$index ++;
}
unset ( $valor ); //rompe la referencia con el último elemento

if ( count( $replace_index ) > 0 ) {
	//replace NULL values in array with replacement value
	$clearArray = array_filter( $rn, function ( $var ) {
		return ! is_null( $var );
	} );
	foreach ( $replace_index as $idx ) {
		$vHourlyResults[ $idx ]->setRadon( $replacement_value );
	}
}

$_SESSION['vHourlyResults'] = $vHourlyResults;
?>
