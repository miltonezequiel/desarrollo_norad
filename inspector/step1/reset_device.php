<?php
include '../../login_check.php';
include '../login_inspector_check.php';
include_once '../../init.php';

$page = "step1";
$path = '../../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../../js/jquery-1.9.1.js'></script>
    </head>
    <body>
    <iframe NAME="_jnlp_" STYLE="visibility:hidden;display:none"></iframe>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="menu-step"><?php include_once($path . 'includes/menu-step.php'); ?></div>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div class="text">
                            <p><span class="negrita">WARNING</span></p>
                            <br/>
                            <p>RESETING the NORAD Device will return it READY MODE and all Data will be deleted.</p>
                            <p>It should only be used when a test has been cancelled or is considered invalid.</p>
                            <p>Once the data has been deleted it CANNOT be recovered.</p>
                            <br/>
                            <br/>
                            <p>If you wish to proceed and RESET the NORAD DEVICE press below.</p>
                            <br/>
                            <form method="post" action="reset_device_process.php" target="_jnlp_">
                                <input type="image" src="<?php echo $path; ?>images/step1-btn-reset.jpg" value="RESET"/>
                            </form>
                            <br/>
                            <p>Otherwise Go Back to Test Information Entry</p>
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   