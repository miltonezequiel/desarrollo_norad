<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//*/
include '../../login_check.php';
include '../login_inspector_check.php';
include_once '../../init.php';
include ROOT_DIR . '/inspector/session_clean.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$page      = "step1";
$path      = '../../';
$servicios = new Servicios();


$oUser = $servicios->getUserById( $_SESSION['user']['id'] );

$InternalTestID = $oUser->getDTestID();


if ( ( (int) $InternalTestID != 0 ) && ( (int) $oUser->getLogged() == 2 ) ) {


	/* para que ande local */
//    include_once '../../init.php';
//    include_once ROOT_DIR . '/entidades/client.php';
//
//    $nameClient = "Mariana Busse";
//    $emailClient = "martingra@gmail.com";
//    $email2Client = "marubusse@gmail.com";
//    $_SESSION['idInspector'] = 19;
//
//    $oClient = new Client("", $nameClient, $emailClient, $email2Client);
//    $_SESSION['oClient'] = $oClient;

	/*     * ****************** */

	$pnext = "../step2/index.php";

	$psec = "1";
} else {

	$pnext = "index.php";

	$psec = "5";
}


$vDevicesInspectors = $servicios->getDevicesByIdInspector( $_SESSION['user']['inspector']['id'] ); //Search devices by id inspectors
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta http-equiv="refresh" content="<?php echo $psec ?>;URL='<?php echo $pnext ?>'">

<html>
<head>
    <title>NORAD | Radon Detection System</title>
    <meta name="keywords" content=""/>
    <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css"/>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <script src='../../js/jquery-1.9.1.js'></script>

    <script src="../../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="../../libs/jquery-validation/dist/additional-methods.min.js"></script>
    <style> input.error {
            border: 1px dotted red;
        }

        .alert-box {
            color: #555;
            border-radius: 10px;
            font-family: Tahoma, Geneva, Arial, sans-serif;
            font-size: 11px;
            padding: 10px 36px;
            margin: 10px;
        }

        .alert-box span {
            font-weight: bold;
            text-transform: uppercase;
        }

        .alert-error {
            background: #ffecec url('../../images/error.png') no-repeat 10px 50%;
            border: 1px solid #f5aca6;
        }

        .button {
            background-color: black;
            border-radius: 8px;
            color: white;
            /*padding:1em;*/
            width: 130px;
            height: 40px;
            font-size: 1em;
            border: 1px solid black;
            cursor: pointer;
        }

    </style>
    <script>
        $(document).ready(function () {
            changeDevice($("#device_data").val());
            $("#device_data").change(function () {
                changeDevice($(this).val());
            });
        });

        function changeDevice(device) {
            $("#idDevice").val(device)
        }
    </script>
</head>
<body>
<iframe NAME="_jnlp_" STYLE="visibility:hidden;display:none"></iframe>
<div id="container">
    <header><?php include_once( $path . 'includes/header.php' ); ?></header>
    <div id="menu-step"><?php include_once( $path . 'includes/menu-step.php' ); ?></div>
    <div id="wrapper" class="page-<?php echo $page ?>">
        <div id="container2">
            <div id="main">
                <div class="text">
					<?php
					if ( empty( $vDevicesInspectors ) ) {
						echo '<div class="alert-box alert-error"><span>error: </span>You don\'t have assigned devices</div>';
					}
					?>
                    <p>Please plug in the NORAD Test Device to your computers USB Port and make sure it is in <span
                                class="negrita">Upload Mode</span></p>
                    <br/>
                    <p>When you are ready to proceed, press <span class="negrita">Upload</span> to run the Norad app</p>
                    <br/>
                </div>
                <div class="btn">
                    <form id="upload_test" method="post" action="test_upload.php" target="_jnlp_"
                          enctype="multipart/form-data">
                        <input class="button" type="submit" value="Upload" alt="Upload test"/>
                    </form>
                    <br/>
                    <form method="post" action="reset_device.php">
                        <input class="button" type="submit" value="Reset Device" alt="Reset Device"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<footer><?php include_once( $path . 'includes/footer.php' ); ?></footer>
</body>
</html>   