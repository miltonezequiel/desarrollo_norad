<?php
include '../login_inspector_check.php';
include '../../login_check.php';
$page = "step3";
$path = '../../';

if (!isset($_SESSION['oTest']) || !isset($_SESSION['oClient']) || !isset($_SESSION['vHourlyResults'])) {
    header("Location: ../step2");
}
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="menu-step"><?php include_once($path . 'includes/menu-step.php'); ?></div>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div class="text">
                            <p>Your report will not be submitted and we will be able to make changes above if necessary.</p>
                        </div>
                        <div style="text-align: center;width: 100%;">
                            <iframe src="report_preview.php" width="700" height="500" style="border: none;">
                            </iframe>
                        </div>
                        <div class="text">
                            <a href="<?php echo $path; ?>inspector/step4"><img src="<?php echo $path; ?>images/btn-continue.jpg" class="btncont"></a>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   