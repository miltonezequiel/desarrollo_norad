<?php
include_once '../../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';
include_once ROOT_DIR . '/entidades/hourly_result.php';
include_once ROOT_DIR . '/entidades/test.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/client.php';
include_once ROOT_DIR . '/entidades/inspector.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/inspector/report/report_graph.php';
include_once ROOT_DIR . '/util/utilidades.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/entidades/device_company.php';
session_start();

$servicios = new Servicios();

$oClient = $_SESSION['oClient'];
$oTest = $_SESSION['oTest'];
$vHourlyResults = $_SESSION['vHourlyResults'];


$oDeviceInspector = $servicios->getDevicesInspectorById($oTest->getTestDiDcId());
$oDeviceCompany = $servicios->getDevicesCompanyById($oDeviceInspector->getIdDeviceCompany());

$oInspector = $servicios->getInspectorById($oDeviceInspector->getIdInspector());
$oCompany = $servicios->getCompanyById($oDeviceCompany->getIdCompany());
$oDevice = $servicios->getDeviceById($oDeviceCompany->getIdDevice());

$oCalibration = $servicios->getCalibrationDate($oDevice);
$oCalibrationExpires;
if (!$oCalibration) {
    $oCalibration = "N/S";
    $oCalibrationExpires = "N/S";
} else {
    $oCalibrationExpires = date('F d, Y', strtotime('+1 year', strtotime($oCalibration)));
    $oCalibration = date('F d, Y', strtotime($oCalibration));
}

?>
<html>
    <head>
        <script src='../../js/jquery-1.9.1.js'></script>
        <script type="text/javascript">
            $(window).load(function() {
                $(".loader").fadeOut("slow");
            })
        </script>
        <style>
            body{
                /*                zoom: 0.5;
                                -moz-transform: scale(0.5);
                                -moz-transform-origin: 0 0;*/
            }
            #preview{
                position: absolute;
                z-index:1;
                overflow: hidden;
            }
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('../../images/page-loader.gif') 50% 50% no-repeat rgb(249,249,249);
            }

            .preventSelect {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1;
            }
            
            #watermark{
                position:absolute;
                z-index:0;
                background:white;
                display:block;
                min-height:50%; 
                min-width:50%;
                color:yellow;
            }

            #wm-text {
                color:lightgrey;
                font-size:120px;
                transform:rotate(300deg);
                -webkit-transform:rotate(300deg);
            }
        </style>

        <link rel="stylesheet" href="../../css/report.css">
        <script>
            var paras = document.getElementsByTagName('div');
            for (var i=0, len=paras.length; i<len; i++){
                paras[i].onmousedown = function(){
                    return false;
                };
            }
        </script>
    </head>
    <body>
        <div class="loader"></div>
        <div id="watermark">
            <p id="wm-text" style="margin-top: 400px;">Draft Report</p>
            <p id="wm-text" style="margin-top: 800px;">Draft Report</p>
            <p id="wm-text" style="margin-top: 1200px;">Draft Report</p>
            <p id="wm-text" style="margin-top: 1600px;">Draft Report</p>
        </div>
        <div id="preview">
            <div class="preventSelect"></div>
            <?php
            echo Utilidades::generateHtmlReport($oClient, $oTest, $oInspector, $oCompany, $oDevice, $oTest->getId(), $vHourlyResults, TRUE, $oCalibration, $oCalibrationExpires);
            ?> 
        </div>
    </body>
</html>

