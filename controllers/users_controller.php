<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

class UsersController {

    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    private function addUser() {
        $username = trim($_POST['username']);
        $password = $_POST['password'];

        $oUser = new User("", $username, md5($password),0,0,0,0);
        $idUser = $this->servicios->addUser($oUser);
        $oUser->setId($idUser);

        return $oUser;
    }

    public function uploadUser() {
        $oUser = $this->addUser();
        return $oUser;
    }

    public function editUser($idUser) {
        $oUser = $this->servicios->getUserById($idUser);
        $password = $_POST['password'];
        $oUser->setPassword(md5($password));
        $this->servicios->editPasswordUser($oUser);
        return $oUser;
    }
    
    public function getUsername($type, $email) {
        $oUser = $this->servicios->getUsername($type, $email);
        return $oUser;
    }
    
    public function getUserByUsername($username) {
        $oUser = $this->servicios->getUserByUsername($username);
        return $oUser;
    }
    
    public function getEmail($type, $username) {
        $email = $this->servicios->getEmail($type, $username);
        return $email;
    }
    
    public function getUserPassword($type, $email, $username) {
        $oUser = $this->servicios->getUserPassword($type, $email, $username);
        return $oUser;
    }
    
    public function getUserByKey($md5Key) {
        $oUser = $this->servicios->getUserByKey($md5Key);
        return $oUser;
    }
    
    public function saveKeyPassRecover($idUser, $md5Key) {
        $oUser = $this->servicios->saveKeyPassRecover($idUser, $md5Key);
        return $oUser;
    }
}

?>