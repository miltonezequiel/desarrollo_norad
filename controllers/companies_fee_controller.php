<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/company_fee.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

class CompaniesFeeController {
    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    public function addCompanyFee($from, $to, $fee, $company_id) {
        $oCompanyFee = new CompanyFee(0, $from, $to, $fee, $company_id);
        $idCompanyFee = $this->servicios->addCompanyFee($from, $to, $fee, $company_id);
        $oCompanyFee->setCompanyFeeId($idCompanyFee);
        return $oCompanyFee;
    }
    
    public function deleteCompanyFee($company_fee_id) {
        $idCompanyFee = $this->servicios->deleteCompanyFee($company_fee_id);
    }
    
}
?>