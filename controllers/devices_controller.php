<?php



include_once '../init.php';

include_once ROOT_DIR . '/entidades/device.php';

include_once ROOT_DIR . '/servicios/servicios.php';

include_once ROOT_DIR . '/util/utilidades.php';



class DevicesController {



    protected $servicios;



    public function __construct() {

        $this->servicios = new Servicios();

    }



    private function addDevice() {

        $name = trim($_POST['name']);

        $serialNumber = trim($_POST['serial_number']);



        $oDevice = new Device("", $name, "", $serialNumber);



        $idDevice = $this->servicios->addDevice($oDevice);



        $oDevice->setId($idDevice);



        return $oDevice;

    }



    public function uploadDevice() {

        $oDevice = $this->addDevice();

        return $oDevice;

    }



    public function calibrateDevice($idDevice) {

        $constant = Utilidades::floatValue($_POST['constant']);



        $oDevice = new Device($idDevice, "", $constant);



        $this->servicios->addCalibration($oDevice);



        return $oDevice;

    }


}



?>