<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/inspector.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

class InspectorsController {

    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    private function addInspector($companyId, $userId) {
        $name = trim($_POST['name']);
        $certificationNumber = trim($_POST['certificationNumber']);
        $entity = ($_POST['entitiesList'] == "Other") ? $_POST['entity'] : $_POST['entitiesList'];
        $email = $_POST['email'];

        $oInspector = new Inspector();
        $oInspector->setName($name);
        $oInspector->setIdCompany($companyId);
        $oInspector->setUser($userId);
        $oInspector->setEmail($email);
        $oInspector->setEntity($entity);
        $oInspector->setCertificationNumber($certificationNumber);

        $idInspector = $this->servicios->addInspector($oInspector);

        $oInspector->setId($idInspector);

        return $oInspector;
    }

    public function uploadInspector($companyId, $userId) {
        $oInspector = $this->addInspector($companyId, $userId);
        return $oInspector;
    }

    public function editInspector($idInspector) {
        $oInspector = $this->servicios->getInspectorById($idInspector);

        $name = trim($_POST['name']);
        $certificationNumber = trim($_POST['certificationNumber']);
        $entity = ($_POST['entitiesList'] == "Other") ? $_POST['entity'] : $_POST['entitiesList'];
        $email = $_POST['email'];
        $oInspector->setName($name);
        $oInspector->setCertificationNumber($certificationNumber);
        $oInspector->setEntity($entity);
        $oInspector->setEmail($email);
        $this->servicios->editInspector($oInspector);

        return $oInspector;
    }

    public function changeStatusInspector($idInspector, $companyId = NULL) {
        $oInspector = $this->servicios->getInspectorById($idInspector);

        if ($oInspector->getStatus() == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $oInspector->setStatus($status);
        if (!is_null($companyId) && $companyId != $oInspector->getIdCompany()) {
            die("Illegal operation");
        }
        $this->servicios->editInspector($oInspector);
        return $oInspector;
    }
}

?>