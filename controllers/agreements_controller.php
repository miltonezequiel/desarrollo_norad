<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/agreement.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

class AgreementsController {
    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    public function addAgreement() {
        $content = trim($_POST['agreement_content']);
        $oAgreement = new Agreement(0, $content);
        $idAgreement = $this->servicios->addAgreement($oAgreement);
        $oAgreement->setId($idAgreement);
        return $oAgreement;
    }

	public function setCurrentAgreementFromDefault( $idCompany ) {
    	$this->servicios->setCurrentAgreementFromDefault ( $idCompany );
    	return true;
	}    
}

?>