<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

class DevicesInspectorsController {

    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    public function unassignDeviceInspector($idDevice, $inspectorId) {
        $oDeviceInspector = new DeviceInspector();

        $this->servicios->editEndDateDeviceInspector($idDevice, $inspectorId);

        return $oDeviceInspector;
    }
    
    public function unassignDeviceInspectorFromAdmin($idDevice, $idCompany) {
        $oDeviceInspector = new DeviceInspector();

        $this->servicios->editEndDateDeviceInspectorFromAdmin($idDevice, $idCompany);

        return $oDeviceInspector;
    }

    private function addDeviceInspector($idDeviceCompany) {
        //$idDevice = $_POST['id'];
        $idInspector = $_POST['inspector'];
        $startDate = date("Y-m-d");

        //$oDeviceCompany = $this->servicios->getCompanyByIdDevice($idDevice);
        
        $oDeviceInspector = new DeviceInspector();
        $oDeviceInspector->setIdInspector($idInspector);
        $oDeviceInspector->setStartDate($startDate);
        $oDeviceInspector->setIdDeviceCompany($idDeviceCompany);
        $this->servicios->addDeviceInspector($oDeviceInspector);

        return $oDeviceInspector;
    }

    public function uploadDeviceInspector($idDeviceCompany) {
        $oDeviceInspector = $this->addDeviceInspector($idDeviceCompany);
        return $oDeviceInspector;
    }
}

?>