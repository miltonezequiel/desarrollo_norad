<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';
include '../authorizenet/authorize.php';

class CompaniesController {

    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    private function addCompany($userId) {
        $name = $_POST['name'];

        $oCompany = new Company();
        $oCompany->setName($name);
        $oCompany->setUser($userId);

        $companyId = $this->servicios->addCompany($oCompany);

        $oCompany->setId($companyId);
        return $oCompany;
    }

    public function uploadCompany($userId) {
        $oCompany = $this->addCompany($userId);
        return $oCompany;
    }

    public function changeStatusCompany($companyId) {
        $oCompany = $this->servicios->getCompanyById($companyId);

        if ($oCompany->getStatus() == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $oCompany->setStatus($status);
        $this->servicios->editCompany($oCompany);

        return $oCompany;
    }

    public function editCompany($companyId) {
        $oCompany = $this->servicios->getCompanyById($companyId);

        $name = $_POST['name'];
        $city = $_POST['city'];
        $state = $_POST['state'];
        $tel = $_POST['tel'];
        $address1 = $_POST['address1'];
        $address2 = $_POST['address2'];
        $contactPerson = $_POST['contact_person'];
        $zip = $_POST['zip'];
        $email = $_POST['email'];

        $oCompany->setName($name);
        $oCompany->setCity($city);
        $oCompany->setState($state);
        $oCompany->setPhone($tel);
        $oCompany->setAddress1($address1);
        $oCompany->setAddress2($address2);
        $oCompany->setZip($zip);
        $oCompany->setEmail($email);
        $oCompany->setContactPerson($contactPerson);

        $this->servicios->editCompany($oCompany);

        return $oCompany;
    }

    public function addBillingInfo($companyId) {
        $ccNumber = trim(str_replace(" ", "", $_POST['ccNumber']));
        
        $type = strtolower($_POST['type']);
        $expirationDate = $_POST['expirationDate'];
        $securityCode = trim($_POST['securityCode']);
        $firstName = trim($_POST['firstName']);
        $lastName = trim($_POST['lastName']);

        $expirationDate = substr($_POST['expirationDate'], 3, 4) . '-' . substr($_POST['expirationDate'], 0, 2);
        
        $oCompany = $this->servicios->getCompanyById($companyId);
        $authorize = new Authorizenet();
        
        $autReturn = array(
            "result" => "",
            "customerProfileId" => "",
            "customerPaymentProfileId" => "",
            "error" => ""
        );

        $autReturn = $authorize->createCustomerProfile(
                $companyId, $type, $firstName, $lastName, $oCompany->getName(), $oCompany->getAddress1(), $oCompany->getCity(), $oCompany->getState(), $oCompany->getZip(), "USA", $oCompany->getPhone(), $ccNumber, $expirationDate, $securityCode, "testMode",
                $oCompany->getEmail()
        );

        if ($autReturn['result'] == "Ok") {
            $oCompany->setCustomerProfileId($autReturn['customerProfileId']);
            $oCompany->setCustomerPaymentProfileId($autReturn['customerPaymentProfileId']);
            $this->servicios->saveBillingInfo($oCompany);
            return "Ok";
        } else {
            return $autReturn['error'];
        }
    }

    public function disableBillingInfo($companyId) {
        $authorize = new Authorizenet();
        $oCompany = $this->servicios->getCompanyById($companyId);

        $autReturn = array(
            "result" => "",
            "error" => ""
        );

        $autReturn = $authorize->deleteCustomerPaymentProfile($oCompany->getCustomerProfileId(), $oCompany->getCustomerPaymentProfileId());

        if ($autReturn['result'] == "Ok") {

            $autReturn = $authorize->deleteCustomerProfile($oCompany->getCustomerProfileId());
            if ($autReturn['result'] == "Ok") {
                $oCompany->setCustomerProfileId(null);
                $oCompany->setCustomerPaymentProfileId(null);
                $this->servicios->saveBillingInfo($oCompany);
                return "Ok";
            }
            else {
                return $autReturn['error'];
            }
        } else {
            return $autReturn['error'];
        }
    }
    
    public function savePayment($companyId) {
        $oCompany = $this->servicios->getCompanyById($companyId);

        $test_fee = $_POST['test_fee'];
        $payment_type = $_POST['payment_type'];

        $oCompany->setCompanyPaymentType($payment_type);
        $oCompany->setCompanyTestFee($test_fee);
        
        $this->servicios->savePayment($oCompany);

        return $oCompany;
    }
}

?>