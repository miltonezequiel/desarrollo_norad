<?php

include_once '../init.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/util/utilidades.php';

class DevicesCompaniesController {

    protected $servicios;

    public function __construct() {
        $this->servicios = new Servicios();
    }

    private function addDeviceCompany() {
        $idDevice = $_POST['id'];
        $idCompany = $_POST['company'];
        $startDate = date("Y-m-d");

        $oDeviceCompany = new DeviceCompany();
        $oDeviceCompany->setIdDevice($idDevice);
        $oDeviceCompany->setIdCompany($idCompany);
        $oDeviceCompany->setStartDate($startDate);

        $this->servicios->addDeviceCompany($oDeviceCompany);

        return $oDeviceCompany;
    }

    public function uploadDeviceCompany() {
        $oDeviceCompany = $this->addDeviceCompany();
        return $oDeviceCompany;
    }

    public function unassignDeviceCompany($idDevice, $endDate) {
        $oDeviceCompany = new DeviceCompany();
        $oDeviceCompany->setIdDevice($idDevice);
        $oDeviceCompany->setEndDate($endDate);

        $this->servicios->editEndDateDeviceCompany($oDeviceCompany);

        return $oDeviceCompany;
    }
}

?>