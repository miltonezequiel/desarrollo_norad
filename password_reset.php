<?php
$page = "client-login";
$path = '';

$typeUser = $_GET['t'];
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div class="form-login">
                            <p>Enter your user</p>
                            <form method="post" action="password_reset_process.php">
                                <input type="text" name="username" class="box" placeholder="User" />
                                <input type="hidden" name="typeuser" value="<?php echo $typeUser; ?>"/>
                                <input type="submit" class="submit" value=""/>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>