<?php
$page = "contact-us";
$path = '../';

require_once '../libs/PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer();
$mail->setFrom('info@noradonline.com', 'NORAD');
$mail->addAddress("pmerolla@envirolabs-inc.com", 'Contact us');
$mail->Subject = 'Contact Us';

$body = "The following message has been sent from the contact-us section.\n \n";
$body = $body . "Name: " . $_POST['name'] . "\n";
$body = $body . "Telephone: " . $_POST['tel'] . "\n";
$body = $body . "E-mail: " . $_POST['email'] . "\n";
$body = $body . "Comment: " . $_POST['comment'] . "\n";
$mail->Body = $body;
$mail->send();
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

        <style type="text/css">
            textarea.box {
                background: #fff none repeat scroll 0 0;
                border: 1px solid #cbcbcb;
                color: #060807;
                font: 14px robotolight,Geneva,sans-serif;
                height: 95px;
                margin: 0 0 10px;
                overflow: auto;
                padding: 12px;
                text-align: left;
                width: 97%;
                margin-top:2em;
            }

            .form-login {
                width: 270px;
                margin: 0px 0px 0px 342px;
                text-align: center;
                display: block;
                position: relative;
                float: left;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">

                        <div id="browserValidation" class="form-login" style="display: 'none';">
                            <p>Your comment has been sent successfully</p>
                        </div>

                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   