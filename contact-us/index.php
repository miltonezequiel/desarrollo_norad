<?php
$page = "contact-us";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../js/jquery-ui.js"></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>


        <style type="text/css">
            textarea.box {
                background: #fff none repeat scroll 0 0;
                border: 1px solid #cbcbcb;
                color: #060807;
                font: 14px robotolight,Geneva,sans-serif;
                height: 95px;
                margin: 0 0 10px;
                overflow: auto;
                padding: 12px;
                text-align: left;
                width: 97%;
                margin-top:2em;
            }
            
            .error {
                margin-bottom: 2em;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div class="info">
                            <div class="form-cd">
                                <!--<h2>Contact Us</h2>-->
                                <form method="post" id="contact_us" action="process.php">
                                    <input type="text" name="name" id="name"  class="box" placeholder="Name" value=""/>
                                    <input type="text" name="tel" id="tel"  class="box" placeholder="Tel" value=""/>
                                    <input type="text" name="email" id="email" class="box2" placeholder="E-mail" value=""/>
                                    <textarea class="box" rows="4" name="comment" id="comment" placeholder="Comment"></textarea>

                                    <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                                    <div id="savechanges"><input type="submit" class="btn-html" value="Send"/></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>

        <script type="text/javascript">
            $("#contact_us").validate({
                rules: {
                    name: "required",
                    tel: "required",
                    comment: "required",
                    email: {
                        required: true,
                        email: true
                    }
                }, messages: {
                    name: "Please insert your name. <br>",
                    tel: "Please insert your telephone number. <br>",
                    comment: "Please insert a comment. <br>",
                    email: {
                        required: "Please insert email. <br>",
                        email: "Please enter a valid email address. <br>"
                    }
                },
                errorLabelContainer: $(".error")
            });

        </script>

    </body>
</html>   