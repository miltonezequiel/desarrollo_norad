<?php

/**
 * Description of pagination
 *
 * @author JuanMa
 */
class Pagination {

    private $current;
    private $total;
    private $adjacents;
    private $perpage;
    private $parameters;

    function __construct($current = null, $total = null, $adjacents = null, $perpage = null, $parameters = null) {
        if (!is_null($current)) {
            $this->current = $current;
        }
        if (!is_null($total)) {
            $this->total = $total;
        }
        if (!is_null($adjacents)) {
            $this->adjacents = $adjacents;
        }
        if (!is_null($perpage)) {
            $this->perpage = $perpage;
        }
        if (!is_null($parameters)) {
            $this->parameters = $this->checkUrlParameters($parameters);
        }
    }

    public function getCurrent() {
        return $this->current;
    }

    public function setCurrent($current) {
        $this->current = $current;
    }

    public function getTotal() {
        return $this->total;
    }

    public function setTotal($total) {
        $this->total = $total;
    }

    public function getAdjacents() {
        return $this->adjacents;
    }

    public function setAdjacents($adjacents) {
        $this->adjacents = $adjacents;
    }

    public function getPerpage() {
        return $this->perpage;
    }

    public function setPerpage($perpage) {
        $this->perpage = $perpage;
    }

    public function render() {
        $current = $this->current;
        $total = $this->total;
        $adjacents = $this->adjacents;
        $perpage = $this->perpage;
        $parameters = $this->parameters;
        ob_start();
        include 'pagination.render.php';
        $response = ob_get_contents();
        ob_end_clean();
        return $response;
    }

    private function checkUrlParameters($parameters) {
        if (strrpos($parameters, "?") !== 0) {
            $parameters = "?" . $parameters;
        }
        return $parameters;
    }

}

?>