<div id="pagination" style="clear: both;text-align: center;margin-bottom: 20px;">
    <?php
    $total_pages = ceil($total / $perpage);
    $lpm1 = $total_pages - 1; //last page menos 1
    $pagination = "";
    if (!is_null($parameters)) {
        $urlParameters = $parameters . "&pag=";
    } else {
        $urlParameters = "?pag=";
    }
    if ($total_pages > 1) {
        //previous button
        if ($current > 1) {
            $prev = ($current - 1);
            $pagination.= "<a href=\"$urlParameters$prev\">&lt;&lt;</a>";
        } else {
            $pagination.= "<span class=\"disabled_pagination\">&lt;&lt;</span>";
        }
        //pages 
        if ($total_pages < 7 + ($adjacents * 2)) { //not enough pages to bother breaking it up
            for ($counter = 1; $counter <= $total_pages; $counter++) {
                if ($counter == $current) {
                    $pagination.= "<a href=\"#\" class=\"current_link\">$counter</a>";
                } else {
                    $pagination.= "<a href=\"$urlParameters$counter\">$counter</a>";
                }
            }
        } elseif ($total_pages > 5 + ($adjacents * 2)) { //enough pages to hide some
            //close to beginning; only hide later pages
            if ($current < 1 + ($adjacents * 2)) {
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                    if ($counter == $current) {
                        $pagination.= "<a href=\"#\" class=\"current_link\">$counter</a>";
                    } else {
                        $pagination.= "<a href=\"$urlParameters$counter\">$counter</a>";
                    }
                }
                $pagination.= "<span class=\"disabled_pagination\">...</span>";
                $pagination.= "<a href=\"$urlParameters$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"$urlParameters$total_pages\">$total_pages</a>";
            }
            //in middle; hide some front and some back
            elseif ($total_pages - ($adjacents * 2) > $current && $current > ($adjacents * 2)) {
                $pagination.= "<a href=\"$urlParameters1\">1</a>";
                $pagination.= "<a href=\"$urlParameters2\">2</a>";
                $pagination.= "<span class=\"disabled_pagination\">...</span>";
                for ($counter = $current - $adjacents; $counter <= $current + $adjacents; $counter++) {
                    if ($counter == $current) {
                        $pagination.= "<a href=\"#\" class=\"current_pagination\">$counter</a>";
                    } else {
                        $pagination.= "<a href=\"$urlParameters$counter\">$counter</a>";
                    }
                }
                $pagination.= "<span class=\"disabled_pagination\">...</span>";
                $pagination.= "<a href=\"$urlParameters$lpm1\">$lpm1</a>";
                $pagination.= "<a href=\"$urlParameters$total_pages\">$total_pages</a>";
            }
            //close to end; only hide early pages
            else {
                $pagination.= "<a href=\"$urlParameters1\">1</a>";
                $pagination.= "<a href=\"$urlParameters2\">2</a>";
                $pagination.= "<span class=\"disabled_pagination\">...</span>";
                for ($counter = $total_pages - (2 + ($adjacents * 2)); $counter <= $total_pages; $counter++) {
                    if ($counter == $current)
                        $pagination.= "<a href=\"#\" class=\"current_pagination\">$counter</a>";
                    else {
                        $pagination.= "<a href=\"$urlParameters$counter\">$counter</a>";
                    }
                }
            }
        }
        //next button
        if ($current < $counter - 1) {
            $next = $current + 1;
            $pagination.= "<a href=\"$urlParameters$next\"> >></a>";
        } else {
            $pagination.= "<span class=\"disabled_pagination\"> >></span>";
        }
    }

    echo $pagination;
    ?>
</div>