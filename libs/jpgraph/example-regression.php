<?php
require 'trend-line_regression-analysis.php';
// sales data for the last 30 quarters
$vSales = array(
    637381, 700986, 641305, 660285, 604474, 565316, 598734, 688690, 723406, 697358,
    669910, 605636, 526655, 555165, 625800, 579405, 588317, 634433, 628443, 570597,
    662584, 763516, 742150, 703209, 669883, 586504, 489240, 648875, 692212, 586509
);
$vRegression = new CRegressionLinear($vSales);
$vNextQuarter = $vRegression->predict(1); // return the forecast for next period

var_dump($vRegression);
var_dump($vNextQuarter);
?>