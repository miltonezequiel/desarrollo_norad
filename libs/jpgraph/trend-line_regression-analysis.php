<?php

/** perform regression analysis on the input data, make the trend line y=ax+b
 * @author Son Nguyen
 * @since 11/18/2005
 * @package Framework
 * @subpackage Math
 */
class CRegressionLinear {

    private $mDatas; // input data, array of (x1,y1);(x2,y2);... pairs, or could just be a time-series (x1,x2,x3,...)

    /** constructor */

    function __construct($pDatas) {
        $this->mDatas = $pDatas;
    }

    /** compute the coeff, equation source: http://people.hofstra.edu/faculty/Stefan_Waner/RealWorld/calctopic1/regression.html */
    function calculate() {
        $n = count($this->mDatas);
        $vSumXX = $vSumXY = $vSumX = $vSumY = 0;
        //var_dump($this->mDatas);
        $vCnt = 0; // for time-series, start at t=0
        foreach ($this->mDatas AS $vOne) {
            if (is_array($vOne)) { // x,y pair
                list($x, $y) = $vOne;
            } else { // time-series
                $x = $vCnt;
                $y = $vOne;
            } // fi
            $vSumXY += $x * $y;
            $vSumXX += $x * $x;
            $vSumX += $x;
            $vSumY += $y;
            $vCnt++;
        } // rof
        $vTop = ($n * $vSumXY - $vSumX * $vSumY);
        $vBottom = ($n * $vSumXX - $vSumX * $vSumX);
        $a = $vBottom != 0 ? $vTop / $vBottom : 0;
        $b = ($vSumY - $a * $vSumX) / $n;
        //var_dump($a,$b);
        return array($a, $b);
    }

    /** given x, return the prediction y */
    function predict($x) {
        list($a, $b) = $this->calculate();
        $y = $a * $x + $b;
        return $y;
    }

}

?>
