<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();
$idCompany = $_SESSION['user']['company']['id'];

//Pagination
$perpage = $GLOBAL_SETTINGS['company.devices.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
$total = count($servicios->getDevicesByIdCompany($idCompany));
$vDevicesCompany = $servicios->getDevicesByIdCompanyPag($idCompany, $from, $perpage);

$pagination = new Pagination($currentPage, $total, 5, $perpage);

$page = "company-data6";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <style>
            #table-t{
                width: 50%;
                border-collapse: collapse;
				margin:10px 0 0 25%;
            }
			
			 #table-t .rojo {
               
                background-color: #ffc1c1;
				
            }

            #table-t th{
                border: 1px solid #a9a9a9;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			 #table-t th.mid {
                border-bottom:1px solid #a9a9a9;
				border-top:1px solid #a9a9a9;
				border-left:hidden;
				border-right:hidden;
                background-color: #dedede;
				font:normal 14.5px robotobold, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }

            #table-t td{
                border: 1px solid #a9a9a9;
                background-color: #fff;
				font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				padding:0 6px 0 6px;
            }
			
			#table-t td a {
               font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:none;
            }
			
			#table-t td a:hover {
                font:normal 13.5px robotolight, Geneva, sans-serif;
				color: #060807;
				line-height:26px;
				text-align:center;
				text-decoration:underline;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-company.php'); ?></div>
                        <div class="form-cd">
                            <table id="table-t">
                                <tr>
                                    <th>S/N</th>
                                    <th class="mid">Name</th>
                                    <th class="mid">Inspector</th>
                                    <th>Actions</th>
                                </tr>
                                <?php
                                foreach ($vDevicesCompany as $oDeviceCompany) {
                                    $oDevice = $servicios->getDeviceById($oDeviceCompany->getIdDevice());
                                    $oDeviceInspector = $servicios->getDevicesInspectorById($oDeviceCompany->getId());
                                    
                                    $oInspector = NULL;
                                    if (!is_null($oDeviceInspector)) {
                                        $oInspector = $servicios->getInspectorById($oDeviceInspector->getIdInspector());
                                    }
                                    ?>
                                    <tr>
                                        <td class="rojo"><?php echo $oDevice->getSerialNumber(); ?></td>
                                        <td class="rojo"><?php echo $oDevice->getName(); ?></td>
                                        <td class="rojo"><?php echo (!is_null($oInspector)) ? $oInspector->getName() : "-"; ?></td>
                                        <td class="rojo">
                                            <?php
                                            if (!is_null($oInspector)) {
                                                echo '<a href="devices_unassign.php?id=' . $oDeviceCompany->getId() . '&inspectorId='.$oInspector->getId().'">Unassign</a>';
                                            } else {
                                                echo '<a href="devices_assign.php?id=' . $oDevice->getId() . '&idDevComp=' . $oDeviceCompany->getId() . '">Assign</a>';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </table>
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   