<?php

$deviceId = $_GET['device_id'];

include( "reports.php");
//include( "../../datos/general.php");
$report = new Reports();

$select = "SELECT ax_devices.device_serial_number, calibration_device_id, date_format(date(MAX(calibration_date_time)), '%b %D, %Y') calibration_date, date_format(date(DATE_ADD(date(MAX(calibration_date_time)), INTERVAL 1 YEAR)), '%b %D, %Y')  calibration_expires";

$from = " 
    FROM ax_devices_calibration
    INNER JOIN ax_devices ON ax_devices_calibration.calibration_device_id = ax_devices.device_id
    
        ";

$where = "WHERE ax_devices.device_id = " . $deviceId;

$groupBy = " GROUP BY calibration_device_id";
$orderBy = "  ";

$fileName = "calibration_certificate_report.php";

$sql = $select . $from . $where . $groupBy . $orderBy;

$general = new DataGeneral();
$data = $general->executeQuery($sql);

$html = $report->getHtml($data, $fileName, $filter);
$report->getReport($html, 'NORAD Certificate of Calibration SN ' . $data[0]->device_serial_number . '.pdf');
?>