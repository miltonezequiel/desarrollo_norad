<div class="logo" style="text-align: center;" ><img src="../../img/logo_envirolabs.png" width="240" /></div>
<!--<div class="center" style="margin-top:0.8em;">-->
<div class="certificate-title">CERTIFICATE OF CALIBRATION</div>
<!--</div>-->

<div id="wrapper" class="informe">
    <div id="container">

        <div id="main" style="margin-top:5px; height: 83%;">
            <div class="rayastxt" style="border:none; font:normal 12pt Arial, Helvetica, sans-serif;">

                <?php
                if ($filter != '') {
                    echo "<p style='font-size: 0.8rem; text-transform: uppercase; text-align:center;'>";
                    echo $filter;
                    echo "</p>";
                }
                ?>

                <div id="reports_table_no_lines">
                    <table style="width:85%">
                        <tr><th>Device:</th><th>NORAD Radon Detection System</th></tr>
                        <tr><th>Manufacturer:</th><th>Envirolabs Incorporated</th></tr>
                        <tr><th>Type of Device:</th><th>Continuous Radon Monitor</th></tr>
                        <tr><th>Device Serial Number:</th><th><?php echo $data[0]->device_serial_number; ?></th></tr>

                        <tr><th></th><th></th></tr>
                        <tr><th></th><th></th></tr>
                        <tr><th>Date of Calibration:</th><th><?php echo $data[0]->calibration_date; ?></th></tr>
                        <tr><th>Next Calibration Date:</th><th><?php echo $data[0]->calibration_expires; ?></th></tr>
                    </table>

                    <table style="width:85%">
                    </table>
                    <br><br>

                    <p>
                        This device has been calibrated using a calibrated instrument traceable to NIST SRM 4973 Radon Emanation Standard at the manufacturers facilities.
                    </p>

                    <p>
                        The accuracy of this radon monitor per USEPA recommendations is ± 25% or 1 pCi/l, whichever is greater after a 24-hour period of deployment.
                    </p>

                    <p>
                        This device is a blind monitor and no corrections or input is required by the instrument operator. Frequency of calibration may vary depending on local or state requirements.
                    </p>

                    <p>
                        The sensitivity of this device is within its operating specifications. 
                    </p>

                    <p>
                        Certified by.
                    </p>

                    <div class="signature"><img src="../../img/signature_paul.png" width="240" /></div>

                    <p style="margin-left: 1rem;">
                        Dr. Paul Merolla, PhD. P.E.
                    </p>

                    <p style="margin-left: 1rem;">
                        Envirolabs Incorporated
                    </p>
                </div>

            </div>

        </div>	
    </div>

    <div class='footer' style="border-top:1px solid black; margin-top:-20px;">
        <div>13-15 East Deer Park Drive, Suite 202, Gaithersburg, MD 20877</div>
        <div style='text-align: right;'>info@envirolabs-inc.com</div>
        <div>Tel. (301) 468-6666, (703) 242-0000</div>
        <div style='text-align: right;'>www.envirolabs-inc.com</div>
    </div>
</div>