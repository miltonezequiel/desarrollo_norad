<?php
/*
ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );
//*/

include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();
$companyId = $_SESSION['user']['company']['id'];
$agreement = $servicios->getCurrentAgreement($companyId);

$success = $servicios->acceptAgreement($companyId, $agreement->getId());

if ($success == null) {
    header("Location: acept_agreement.php?error='An error has ocurred. Please, try again.'");
} else {
    header("Location: data_edit.php");
}
?>