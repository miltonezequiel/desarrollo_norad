<?php

session_start();

if (empty($_SESSION['user']['company'])) {
    session_destroy();
    header("Location: index.php");
    exit;
}
?>
