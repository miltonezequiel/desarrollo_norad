<div class="logo"><img src="<?php echo ROOT_DIR ?>/img/logo_envirolabs.png" width="240" /></div>
<div class="right" style="margin-top:0.8em;">
    <div class="title">
        Agreement Report
    </div>
    <div class="text"><strong>Envirolabs-inc</strong></div>
</div>

<div id="wrapper" class="informe">
    <div id="container">
        <div id="main" style="margin-top:5px; height: 75%; font-size: 8pt;">
            <p>
                <?php echo $_SESSION['agreement_content']; ?>
            </p>
        </div>	

        <div class='receiptFooter' style="border-top:1px solid black;">
            <div>13-15 East Deer Park Drive, Suite 202, Gaithersburg, MD 20877</div>
            <div style='text-align: right;'>info@envirolabs-inc.com</div>
            <div>Tel. (301) 468-6666, (703) 242-0000</div>
            <div style='text-align: right;'>www.envirolabs-inc.com</div>
        </div>
    </div>
</div>