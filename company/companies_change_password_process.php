<?php

include '../login_check.php';
include 'login_admin_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/controllers/users_controller.php';
include_once ROOT_DIR . '/controllers/companies_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$usersController = new UsersController();
$companiesController = new CompaniesController();
$servicios = new Servicios();

$currentPassword = $_POST['current_password'];
$password = $_POST['password'];
$idCompany = $_SESSION['user']['company']['id'];
$oCompany = $servicios->getCompanyById($idCompany);
$oUser = $servicios->getUserById($_SESSION['user']['id']);

$hashCurrentPass = md5($currentPassword);

$resultado = '';
if ($hashCurrentPass == $oUser->getPassword()) {
    $oUser->setPassword(md5($password));
    $servicios->editPasswordUser($oUser);
    $redirectLocation = "../logout.php";
}
else {
    $redirectLocation = 'data_edit.php?resultado=The cmopany password has not been updated. Current password does not match.';
}

header('Location:' . $redirectLocation);
?>