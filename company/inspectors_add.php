<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/entity.php';
session_start();

$servicios = new Servicios();

$vEntities = $servicios->getEntities();

$page = "company-data";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src='../js/entity-other.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#username').keyup(username_check);
            });

            function username_check() {
                var username = $('#username').val();
                if (username == "" || username.length < 4) {
                    $('#username').css('border', '3px #CCC solid');
                    $('#tick').hide();
                } else {
                    jQuery.ajax({
                        type: "POST",
                        url: "inspectors_add_check_user.php",
                        data: 'username=' + username,
                        cache: false,
                        success: function (response) {
                            if (response > 0) {
                                $('#username').css('border', '3px #C33 solid');
                                $('#submit').attr('disabled', 'disabled');
                                $('#submit').css('opacity', '0.5');
                            } else {
                                $('#username').css('border', '3px #090 solid');
                                $('#submit').removeAttr('disabled');
                                $('#submit').css('opacity', '1');
                            }
                        }
                    });
                }
            }
        </script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-company.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_user" action="inspectors_process.php?action=add">

                                <input type="text" name="name" class="box" placeholder="Inspector name" /><br/>
                                <select name="entitiesList" id="entitiesList">
                                    <option selected disabled>Entity</option>
                                    <?php
                                    foreach ($vEntities as $oEntity) {
                                        echo "<option>{$oEntity->getName()}</option>";
                                    }
                                    ?>
                                    <option>Other</option>
                                </select><br/>
                                <input type="text" name="entity" id="entity" class="box" placeholder="Other entity" style="display: none;"/><br/>
                                <input type="text" name="certificationNumber" class="box" placeholder="Certification Number" /><br/>
                                <input type="text" name="email" class="box" placeholder="Inspector email" /><br/>
                                <input type="text" name="username" id="username" class="box" placeholder="User" /><br/>
                                <input type="password" name="password"  class="box" placeholder="Password" /><br/>

                                <div class="errorContainer">                                         
                                    <div class="error"></div>                                     
                                </div>

                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $("#data_user").validate({
            rules: {
                name: "required",
                username: "required",
                password: "required",
                email: "required"
            }, messages: {
                name: "Please insert name. ",
                username: "Please insert username. ",
                password: "Please insert password. ",
                email: "Please insert email. "
            },
            errorLabelContainer: $("#data_user div.error")
        });
    </script>
</html>   