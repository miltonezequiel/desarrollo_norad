<?php

include '../login_check.php';
include 'login_company_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/device_company.php';
include_once ROOT_DIR . '/entidades/device_inspector.php';
include_once ROOT_DIR . '/controllers/devices_inspectors_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/Util/utilidades.php';

$action = $_GET['action'];
$redirectLocation = 'devices.php';
if (!isset($action) || empty($action)) { //si no hay accion
    header('Location:' . $redirectLocation);
    return;
}

$devicesInspectorsController = new DevicesInspectorsController();
$servicios = new Servicios();


if ($action == "assign") {
    $idDeviceCompany = $_POST['idDevComp'];
    $devicesInspectorsController->uploadDeviceInspector($idDeviceCompany);
} elseif ($action == "unassign") {
    $idDeviceCompany = $_GET['id'];
    $inspectorId = $_GET['inspectorId'];
    $devicesInspectorsController->unassignDeviceInspector($idDeviceCompany, $inspectorId);
}

header('Location:' . $redirectLocation);
?>