<?php
/*
ini_set( 'display_errors', 1 );
ini_set( 'display_startup_errors', 1 );
error_reporting( E_ALL );
//*/
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();
$oCompany = $servicios->getCompanyById($_SESSION['user']['company']['id']);

$page = "company-data2";
$path = '../';

$agreement = $servicios->getCurrentAgreement($oCompany->getId());
$agreement_content = $agreement->getContent();

$agreement_content = str_replace("<FEE>", "U\$D " . $oCompany->getCompanyTestFee(), $agreement_content);
$agreement_content = str_replace("<COMPANY NAME>", $oCompany->getName(), $agreement_content);

?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <!--<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>-->
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>

        <style type="text/css">
            #savechanges {
                /*margin-top: 3em;*/
            }

            #btnChangePassword {
                background-color: black;
                font-size: 0.8em;
                height: 3em;
                left: 10em;
                margin-top: 1.5em;
                position: absolute;
            }

            #btnSaveChanges {
                font-size: 0.8em;
                height: 3em;
                right: 10em;
                margin-top: 1.5em;
                position: absolute;
            }
            
            #imgCustomLogo {
                height: 80%;
                width: 80%;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div class="form-cd">
                            <form method="post" id="data_company" action="accept_agreement_process.php" enctype="multipart/form-data">
                                <textarea style="width: 100%; height: 20rem;"><?php echo $agreement_content ?> </textarea>
                                <div id="savechanges">
                                    <input type="submit" class="button" id="btnSaveChanges" value="I accept"/>
                                </div>
                            </form>
                        </div>
                        <?php 
                            if(isset($error)) {
                                echo "<div class='error'>". $error ."</div>";
                            }
                        ?>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $("#data_company").validate({
            rules: {
                name: "required",
                address1: "required",
                city: "required",
                state: "required",
                zip: "required",
                tel: "required",
                email: {
                    required: true,
                    email: true
                }
            }, messages: {
                name: "Please insert name",
                address1: "Please insert street address",
                city: "Please insert city",
                state: "Please insert state",
                zip: "Please insert zip code",
                tel: "Please insert phone",
                email: {
                    required: "Please insert email",
                    email: "Please enter a valid email address"
                }
            },
            errorLabelContainer: $("#data_company div.error")
        });

        $("#checkPersonalizeLogo").click(function () {
            if (this.checked) {
                $("#uploadFileDiv").show();
            } else {
                $("#uploadFileDiv").hide();
            }
        });
        
        function removeLogo() {
            $("#fileUploader").show();
            $("#fileUploaded").hide();
            $("#checkPersonalizeLogo").click();
            $("#deletedLogo").val('yes');
        }
    </script>
</html>   