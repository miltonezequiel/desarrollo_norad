<?php

include '../login_check.php';
include 'login_company_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/entidades/inspector.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/controllers/users_controller.php';
include_once ROOT_DIR . '/controllers/inspectors_controller.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$action = $_GET['action'];
$redirectLocation = 'index.php';
if (!isset($action) || empty($action)) { //si no hay accion
    header('Location:' . $redirectLocation);
    return;
}

$companyId = $_SESSION['user']['company']['id'];

$usersController = new UsersController();
$inspectorsController = new InspectorsController();
$servicios = new Servicios();
if ($action == "add") {
    $oUser = $usersController->uploadUser();
    $oInspector = $inspectorsController->uploadInspector($companyId, $oUser->getId());

    $redirectLocation = 'inspectors.php';
} elseif ($action == "edit") {
    $idInspector = $_POST['idInspector'];
    $idUser = $_POST['idUser'];

    $oInspector = $inspectorsController->editInspector($idInspector);

    if ($_POST['password'] != "") {
        $oUser = $usersController->editUser($idUser);
    }
    $redirectLocation = 'inspectors.php';
} elseif ($action == "status") {
    $idInspector = $_GET['idInspector'];
    $oInspector = $inspectorsController->changeStatusInspector($idInspector, $companyId);

    $redirectLocation = 'inspectors.php';
}

header('Location:' . $redirectLocation);
?>