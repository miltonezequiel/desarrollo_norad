<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();
$oCompany = $servicios->getCompanyById($_SESSION['user']['company']['id']);

$page = "company-data2";
$path = '../';

$oCompayAgreementAccepted = $servicios->isCurrentAgreementAccepted($_SESSION['user']['company']['id']);
if(!$oCompayAgreementAccepted) {
    header("Location: " . "acept_agreement.php");
}

?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <!--<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>-->
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>

        <style type="text/css">
            #savechanges {
                /*margin-top: 3em;*/
            }

            #btnChangePassword {
                background-color: black;
                font-size: 0.8em;
                height: 3em;
                left: 10em;
                margin-top: 1.5em;
                position: absolute;
            }

            #btnSaveChanges {
                font-size: 0.8em;
                height: 3em;
                right: 10em;
                margin-top: 1.5em;
                position: absolute;
            }
            
            #btnPrintAgreement {
                margin-top: 1.5em;
                font-size: 0.8em;
                height: 3em;
            }
            
            #imgCustomLogo {
                height: 80%;
                width: 80%;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-company.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_company" action="data_edit_process.php" enctype="multipart/form-data">
                                <input type="text" name="name"  class="box" placeholder="Company name" value="<?php echo ($oCompany->getName() != NULL) ? $oCompany->getName() : ""; ?>"/>
                                <input type="text" name="city"  class="box" placeholder="City" value="<?php echo ($oCompany->getCity() != NULL) ? $oCompany->getCity() : ""; ?>" />
                                <input type="text" name="tel"  class="box2" placeholder="Tel" value="<?php echo ($oCompany->getPhone() != NULL) ? $oCompany->getPhone() : ""; ?>"/>

                                <input type="text" name="address1"  class="box" placeholder="Address Line 1" value="<?php echo ($oCompany->getAddress1() != NULL) ? $oCompany->getAddress1() : ""; ?>"/>
                                <input type="text" name="state"  class="box" placeholder="State" value="<?php echo ($oCompany->getState() != NULL) ? $oCompany->getState() : ""; ?>"/>
                                <input type="text" name="contact_person"  class="box2" placeholder="Contact Person" value="<?php echo ($oCompany->getContactPerson() != NULL) ? $oCompany->getContactPerson() : ""; ?>"/>

                                <input type="text" name="address2"  class="box" placeholder="Address Line 2" value="<?php echo ($oCompany->getAddress2() != NULL) ? $oCompany->getAddress2() : ""; ?>"/>
                                <input type="text" name="zip"  class="box" placeholder="ZIP" value="<?php echo ($oCompany->getZip() != NULL) ? $oCompany->getZip() : ""; ?>"/>
                                <input type="text" name="email"  class="box2" placeholder="E-mail" value="<?php echo ($oCompany->getEmail() != NULL) ? $oCompany->getEmail() : ""; ?>"/>

                                <?php
                                $customLogoUrl = "../uploads/custom_logos/" . $_SESSION['user']['company']['id'];
                                $showUploader = 'block';
                                $showUploaded = 'block';
                                $fileExtension = ".";

                                if (file_exists($customLogoUrl . '.jpg') || file_exists($customLogoUrl . '.jpeg')) {
                                    $showUploader = 'none';
                                } else {
                                    $showUploaded = 'none';
                                }
                                
                                if (file_exists($customLogoUrl . '.jpg')) {
                                    $fileExtension = '.jpg';
                                }
                                
                                if (file_exists($customLogoUrl . '.jpeg')) {
                                    $fileExtension = '.jpeg';
                                }
                                
                                ?>

                                <div id="fileUploader" class="fileUpload" style="display:<?php echo $showUploader ?>;">
                                    <div>
                                        <input id="checkPersonalizeLogo" class="fileUpload" type="checkbox" style="float:none;"/> <b>Personalize test report with your company logo</b>
                                    </div>
                                    <div id="uploadFileDiv" style="margin-top:0.5rem; display:none;">
                                        Upload your desired letterhead or Company Logo as a 2200 pixel x 250 pixel jpg file <input type="file" name="fileToUpload" id="fileToUpload">
                                    </div>
                                </div>

                                <div id="fileUploaded" class="fileUpload" style="display:<?php echo $showUploaded ?>;">
                                    <div>
                                        <b>This is your personalized letterhead for your test reports</b>
                                    </div>
                                    <div id="uploadFileDiv" style="margin-top:0.5rem;">
                                        <img id="imgCustomLogo" src="<?php echo $customLogoUrl . $fileExtension ?>" />
                                        <a onclick="removeLogo();" href="#" style="float:right;">Remove Logo </a>  
                                    </div>
                                </div>


                                <div class="errorContainer">
                                    <div class="error"><?php if (isset($_GET['resultado'])) echo $_GET['resultado']; ?></div>
                                </div>
                                
                                <input type="hidden" id="deletedLogo" name="deletedLogo" value="no" />

                                <div id="savechanges">
                                    <input type="button" class="button" id="btnChangePassword" value="Change Password" onclick="window.location = 'comp_change_password.php'"></input>
                                    <input type="button" class="button" id="btnPrintAgreement" value="View Agreement" onclick="window.open('agreement_pdf.php','_blank');"></input>
                                    <input type="submit" class="button" id="btnSaveChanges" value="Save Changes"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $("#data_company").validate({
            rules: {
                name: "required",
                address1: "required",
                city: "required",
                state: "required",
                zip: "required",
                tel: "required",
                email: {
                    required: true,
                    email: true
                }
            }, messages: {
                name: "Please insert name",
                address1: "Please insert street address",
                city: "Please insert city",
                state: "Please insert state",
                zip: "Please insert zip code",
                tel: "Please insert phone",
                email: {
                    required: "Please insert email",
                    email: "Please enter a valid email address"
                }
            },
            errorLabelContainer: $("#data_company div.error")
        });

        $("#checkPersonalizeLogo").click(function () {
            if (this.checked) {
                $("#uploadFileDiv").show();
            } else {
                $("#uploadFileDiv").hide();
            }
        });
        
        function removeLogo() {
            $("#fileUploader").show();
            $("#fileUploaded").hide();
            $("#checkPersonalizeLogo").click();
            $("#deletedLogo").val('yes');
        }
    </script>
</html>   