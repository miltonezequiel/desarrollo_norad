<?php
include '../login_check.php';
include 'login_admin_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$idCompany = $_GET['idc'];
if (is_null($idCompany)) {
    header("Location: index.php");
}
$servicios = new Servicios();
$oCompany = $servicios->getCompanyById($idCompany);
$oUser = $servicios->getUserById($oCompany->getUser());

$page = "companies";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
           <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-admin"><?php include_once($path . 'includes/menu-admin.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_company" action="companies_data_edit_process.php">
                                <input type="hidden" name="idc" value="<?php echo $idCompany; ?>"/>
                                
                                <input type="text" name="username" disabled style="margin-right: 100%; background-color: lightgray; border:1px solid black;"  class="box" placeholder="Company Username" value="<?php echo ($oUser->getUsername() != NULL) ? $oUser->getUsername() : ""; ?>"/>
                                
                                <input type="text" name="name"  class="box" placeholder="Company name" value="<?php echo ($oCompany->getName() != NULL) ? $oCompany->getName() : ""; ?>"/>
                                <input type="text" name="city"  class="box" placeholder="City" value="<?php echo ($oCompany->getCity() != NULL) ? $oCompany->getCity() : ""; ?>" />
                                <input type="text" name="tel"  class="box2" placeholder="Tel" value="<?php echo ($oCompany->getPhone() != NULL) ? $oCompany->getPhone() : ""; ?>"/>

                                <input type="text" name="address1"  class="box" placeholder="Address Line 1" value="<?php echo ($oCompany->getAddress1() != NULL) ? $oCompany->getAddress1() : ""; ?>"/>
                                <input type="text" name="state"  class="box" placeholder="State" value="<?php echo ($oCompany->getState() != NULL) ? $oCompany->getState() : ""; ?>"/>
                                <input type="text" name="contact_person"  class="box2" placeholder="Contact Person" value="<?php echo ($oCompany->getContactPerson() != NULL) ? $oCompany->getContactPerson() : ""; ?>"/>

                                <input type="text" name="address2"  class="box" placeholder="Address Line 2" value="<?php echo ($oCompany->getAddress2() != NULL) ? $oCompany->getAddress2() : ""; ?>"/>
                                <input type="text" name="zip"  class="box" placeholder="ZIP" value="<?php echo ($oCompany->getZip() != NULL) ? $oCompany->getZip() : ""; ?>"/>
                                <input type="text" name="email"  class="box2" placeholder="E-mail" value="<?php echo ($oCompany->getEmail() != NULL) ? $oCompany->getEmail() : ""; ?>"/>
                                <div class="error"></div>
                                <div id="savechanges"><input type="submit" class="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $( "#data_company" ).validate({
            rules: {
                name: "required",
                address1: "required",
                city: "required",
                state: "required",
                zip: "required",
                tel: "required",
                email: {
                    required: true,
                    email: true
                }
            },messages: {
                name: "Please insert name",
                address1: "Please insert street address",
                city: "Please insert city",
                state: "Please insert state",
                zip: "Please insert zip code",
                tel: "Please insert phone",
                email: {
                    required: "Please insert email",
                    email: "Please enter a valid email address"
                }
            },
            errorLabelContainer: $("#data_company div.error")
        });
    </script>
</html>   