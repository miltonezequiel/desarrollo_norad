<?php

session_start();

include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/entidades/company.php';

$estadoLogin = $_SESSION['estadoLogin'];
if (isset($estadoLogin) && $estadoLogin) {
    header("Location: data_edit.php");
    return;
} else {
    $estadoLogin = FALSE;
}

$user = $_POST['username'];
$password = $_POST['password'];

$servicios = new Servicios();
$oUser = $servicios->getUserByUsername($user);

if (isset($oUser) && $oUser->getUsername() == $user && $oUser->getPassword() == md5($password)) {
    $oCompany = $servicios->getCompanyByUser($oUser->getId());
    if (isset($oCompany) && !is_null($oCompany) && $oCompany->getStatus() != 0) {
        $_SESSION['user']['type'] = "company";
        $_SESSION['user']['company']['id'] = $oCompany->getId();
        $_SESSION['user']['company']['name'] = $oCompany->getName();
        $_SESSION['user']['id'] = $oUser->getId();
        $_SESSION['estadoLogin'] = $estadoLogin = TRUE;
        $redirectLocation = "data_edit.php";
    } else {
        $redirectLocation = "login.php?msg=error";
    }
} elseif ($user == NULL || $password == NULL) {
    $redirectLocation = "login.php";
} else {
    $redirectLocation = "login.php?msg=error";
}

header("Location: " . $redirectLocation);
?>