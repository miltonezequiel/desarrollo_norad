<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include '../authorizenet/authorize.php';

session_start();

$page = "company-data7";
$path = '../';

$servicios = new Servicios();
$oCompany = $servicios->getCompanyById($_SESSION['user']['company']['id']);
$isCreditCard = true;

if($oCompany->getCompanyPaymentType() == "Invoice") {
    $isCreditCard = false;
}

if ($oCompany->getCustomerProfileId() != '' && $oCompany->getCustomerProfileId() != "0") {
    $authorize = new Authorizenet();

    $result = array(
        "result" => "",
        "cardNumber" => "",
        "name" => "",
        "error" => ""
    );

    $result = $authorize->getCustomerPaymentProfile($oCompany->getCustomerProfileId(), $oCompany->getCustomerPaymentProfileId());
}
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='<?php echo $path; ?>js/jquery-1.9.1.js'></script>
        <script src="<?php echo $path; ?>libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo $path; ?>libs/jquery-validation/dist/additional-methods.min.js"></script>

        <script src="../js/jquery.inputmask.bundle.js"></script>


        <style type="text/css">
            select {
                margin: 10px 72px 0 0 !important;
                padding: 10px !important;
            }

            #table-t{
                width: 80%;
                border-collapse: collapse;
                margin:10px 0 0 10%;
            }

            #table-t th{
                border: 1px solid #a9a9a9;
                background-color: #dedede;
                font:normal 14.5px robotobold, Geneva, sans-serif;
                color: #060807;
                line-height:26px;
                text-align:center;
                padding:0 6px 0 6px;
            }

            #table-t th.mid {
                border-bottom:1px solid #a9a9a9;
                border-top:1px solid #a9a9a9;
                border-left:hidden;
                border-right:hidden;
                background-color: #dedede;
                font:normal 14.5px robotobold, Geneva, sans-serif;
                color: #060807;
                line-height:26px;
                text-align:center;
                padding:0 6px 0 6px;
            }

            #table-t td{
                border: 1px solid #a9a9a9;
                background-color: #fff;
                font:normal 13.5px robotolight, Geneva, sans-serif;
                color: #060807;
                line-height:26px;
                text-align:center;
                padding:0 6px 0 6px;
            }

            #table-t td a {
                font:normal 13.5px robotolight, Geneva, sans-serif;
                color: #060807;
                line-height:26px;
                text-align:center;
                text-decoration:none;
            }

            #table-t td a:hover {
                font:normal 13.5px robotolight, Geneva, sans-serif;
                color: #060807;
                line-height:26px;
                text-align:center;
                text-decoration:underline;
            }
            
            #divInvoice {
                width:100%;
                float: left;
                margin-left: 5rem;
            }
        </style>
    </head>
    <body>

        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-company-data">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-company.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_company" action="billing_info_process.php" onsubmit="return validateForm();" <?php
                            if (($oCompany->getCustomerProfileId() == '' || $oCompany->getCustomerProfileId() == "0") && $isCreditCard) {
                                echo "style='display:block;'";
                            } else {
                                echo "style='display:none;'";
                            }
                            ?>>
                                <input type="text" name="ccNumber" id="ccNumber" maxlength="19" placeholder="Card Number"  class="box"/>


                                <select name="type" id="type">
                                    <option selected disabled>Type</option>
                                    <option>Individual</option>
                                    <option>Business</option>
                                </select>


                                <input type="text" name="expirationDate" id="expirationDate" maxlength="7" class="box2" placeholder="Expiration Date"/>

                                <input type="text" name="securityCode" id="securityCode" maxlength="4"  class="box" placeholder="Security Code"/>
                                <input type="text" name="firstName" id="firstName" class="box" maxlength="50" placeholder="First name on Card"/>
                                <input type="text" name="lastName" id="lastName"  class="box2" maxlength="50" placeholder="Last name on Card"/>

                                <div class="errorContainer"><div class="error"></div></div>
                                <div id="savechanges"><input type="submit" class="submit" value=""/></div>
                            </form>
                        </div>

                        <div id='divRegistredBillingData' <?php
                        if ($oCompany->getCustomerProfileId() != '' && $oCompany->getCustomerProfileId() != "0" && $isCreditCard) {
                            echo "style='display:block;'";
                        } else {
                            echo "style='display:none;'";
                        }
                        ?>'>
                            <div class="form-cd">
                                <table id="table-t">
                                    <tr>
                                        <th class="">Registered Card</th>
                                        <th class="">Actions</th>
                                    </tr>
                                    <tr>
                                        <td><?php echo $result['name'] . " - " . $result['cardNumber']; ?></td>
                                        <td><a style="text-decoration:underline;" href='billing_info_disable.php?companyId=<?php echo $_SESSION['user']['company']['id']; ?>'>Disable</a>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        
                        
                        <div id='divInvoice' <?php
                        if (!$isCreditCard) {
                            echo "style='display:block;'";
                        } else {
                            echo "style='display:none;'";
                        }
                        ?>'> 
                            An invoice will be generated when performing a test.
                        </div>
                        

                        <?php
                        if (isset($_GET['result'])) {
                            if ($_GET['result'] == "Ok") {
                                echo "<div class='divSuccess'>The billing information has been saved</div>";
                            } else {
                                echo "<div class='divError'>An ERROR has ocurred: " . $_GET['result'] . "</div>";
                            }
                        }
                        ?>

                        <div id="validationError" class='divError' style="display: none;"></div>

                    </div>
                </div>	
            </div>
            <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>


    <script type="text/javascript">
        $("#ccNumber").inputmask("9999 9999 9999 9999", {"placeholder": " ", clearMaskOnLostFocus: true});
        $("#expirationDate").inputmask("mm/yyyy", {"placeholder": "MM/YYYY", clearMaskOnLostFocus: true});
        $("#securityCode").inputmask("9999", {"placeholder": " ", clearMaskOnLostFocus: true});
        
         $("#data_company").validate({
            rules: {
                ccNumber: "required",
                expirationDate: "required",
                securityCode: "required",
                type: "required",
                firstName: "required",
                lastName: "required"
            }, messages: {
                ccNumber: "Please insert card number. ",
                expirationDate: "Please insert expiration date. ",
                securityCode: "Please insert security code. ",
                type: "Please insert type. ",
                firstName: "Please insert first name. ",
                lastName: "Please insert last name. "
            },
            errorLabelContainer: $("#data_company div.error")
        });
        
        

//        function validateForm() {
//            //validate card numbre
//            var error = '';
//            var separator = '';
//
//            if ($("#ccNumber").val() == 'Credit Card Number') {
//                error += separator + "- Please insert a Credit Card Number";
//                separator = "</BR>";
//            }
//
//            if ($("#ccNumber").val().includes("-")) {
//                error += separator + "- The Credit Card Number should contain only numbers";
//                separator = "</BR>";
//            }
//
//            //validate type
//            if ($("#type option:selected").text() == 'Type') {
//                error += separator + "- Please insert a Type";
//                separator = "<BR>";
//            }
//
//            //validate expiration date
//            $("#expirationDate").val(formatedExpDate);
//            if ($("#expirationDate").val() == 'Expiration Date') {
//                error += separator + "- Please insert an expiration date";
//                separator = "</BR>";
//            }
//
//            if (!$("#expirationDate").val().includes("-") || !$("#expirationDate").val().substring(4, 1)) {
//                error += separator + "- The expiration date must follow the format YYYY-MM";
//                separator = "<BR>";
//            }
//
//            //validate security code
//
//            if ($("#securityCode").val() == 'Security Code') {
//                error += separator + "- Please insert a Security Code";
//                separator = "<BR>";
//            }
//
//            //validate first name
//
//            if ($("#firstName").val() == 'First name on Card') {
//                error += separator + "- Please insert a First Name";
//                separator = "<BR>";
//            }
//
//            //validate last name
//
//            if ($("#lastName").val() == 'Last name on Card') {
//                error += separator + "- Please insert a Last Name";
//                separator = "<BR>";
//            }
//
//            if (error == '') {
//                $("#validationError").hide();
//                return true;
//            }
//            else {
//                $("#validationError").html(error);
//                $("#validationError").show();
//                return false;
//            }
//        }
    </script>


</html>   