<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/entity.php';
session_start();

$idInspector = $_GET['id'];

if (!isset($idInspector) || is_null($idInspector)) {
    header("Location: inspectors.php");
}

$servicios = new Servicios();
$vEntities = $servicios->getEntities();

$oInspector = $servicios->getInspectorById($idInspector);
$oUser = $servicios->getUserById($oInspector->getUser());
$page = "company-data";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <script src='../js/entity-other.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-company.php'); ?></div>
                        <div class="form-cd">
                            <form method="post" id="data_user" action="inspectors_process.php?action=edit">
                                <input type="hidden" name="idInspector" class="box" value="<?php echo $oInspector->getId(); ?>"/><br/>
                                <input type="hidden" name="idUser" class="box" value="<?php echo $oUser->getId(); ?>"/><br/>
                                <input type="text" name="name" class="box" placeholder="Inspector name" value="<?php echo $oInspector->getName(); ?>"/><br/>
                                <select name="entitiesList" id="entitiesList">
                                    <option disabled>Entity</option>
                                    <?php
                                    $selected = null;
                                    foreach ($vEntities as $oEntity) {
                                        if (is_null($selected)) {
                                            $selected = ($oEntity->getName() == $oInspector->getEntity()) ? "selected" : null;
                                        }
                                        echo "<option {$selected}>{$oEntity->getName()}</option>";
                                    }
                                    ?>
                                    <option <?php echo (empty($selected)) ? "selected" : ""; ?>>Other</option>
                                </select><br/>
                                <input type="text" name="entity" id="entity" class="box" placeholder="Other entity" value="<?php echo (empty($selected)) ? $oInspector->getEntity() : ""; ?>" style="display: none;" /><br/>
                                <input type="text" name="email" class="box" placeholder="Inspector email" value="<?php echo $oInspector->getEmail(); ?>"/><br/>
                                <input type="text" name="certificationNumber" class="box" placeholder="Certification number" value="<?php echo $oInspector->getCertificationNumber(); ?>"/><br/>
                                <input type="text" name="username" id="username" class="box" placeholder="User" value="<?php echo $oUser->getUsername(); ?>" readonly="true"/><br/>
                                <input type="password" name="password"  class="box" placeholder="New password" /><br/>
                                <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
    <script>
        $( "#data_user" ).validate({
            rules: {
                name: "required",
                identifier: "required",
                email: "required",
                username: "required"
            },messages: {
                name: "Please insert name",
                identifier: "Please insert identifier",
                email: "Please insert email",
                username: "Please insert username"
            },
            errorLabelContainer: $("#data_user div.error")
        });
    </script>
</html>   