<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/device.php';
include_once ROOT_DIR . '/entidades/inspector.php';
session_start();

$idDevice = $_GET['id'];
if (!isset($idDevice) || is_null($idDevice)) {
    header("Location: devices.php");
}

$servicios = new Servicios();
$idCompany = $_SESSION['user']['company']['id'];
$vInspectors = $servicios->getInspectorsByIdCompany($idCompany);
$oDevice = $servicios->getDeviceById($idDevice);

$page = "company-data";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>

        <style type='text/css'>
            .button {
                height: 2.2em;
                width: 4em;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div id="divNoInspectors" style="text-align: center; display:none;">
                            There are currently no inspectors available to assign to a device.
                        </div>
                        <div class="form-cd">
                            <form method="post" id="data_device" action="devices_process.php?action=assign">
                                <input type="hidden" name="id" class="box" value="<?php echo $oDevice->getId(); ?>"/><br/>
                                <input type="hidden" name="idDevComp" class="box" value="<?php echo $_GET['idDevComp']; ?>"/><br/>
                                <input type="text" name="name" id="txtName" class="box" value="<?php echo $oDevice->getName(); ?>" readonly="true"/><br/>
                                <select id="selectInspectors" name="inspector">
                                    <?php
                                    foreach ($vInspectors as $oInspector) {
                                        echo "<option value='{$oInspector->getId()}'>{$oInspector->getName()}</option>";
                                    }
                                    ?>
                                </select><br/>
                                <div id="savechanges"><input type="submit" class="submit" id="submit" value=""/>
                                    <input type="button" class="button" id="btnVolver" value="Back" onclick="window.location = '../company/devices.php';"/>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>

        <script type="text/javascript" >
<?php if (count($vInspectors) == 0) { ?>
                $(document).ready(function () {
                    $("#selectInspectors").hide();
                    $("#submit").hide();
                    $("#txtName").hide();
                    $("#divNoInspectors").show();
                });
<?php } ?>
        </script>
    </body>
</html>   