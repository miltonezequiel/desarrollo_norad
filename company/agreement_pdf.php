<?php

include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include_once ROOT_DIR . '/entidades/agreement.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/company_agreements.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();
$companyId = $_SESSION['user']['company']['id'];
$oCompany = $servicios->getCompanyById($companyId);
$company_agreement = $servicios->getAgreementsCompany($companyId);
$agreement = $servicios->getAgreementById($company_agreement->getAgreementId());
$agreement_content = $agreement->getContent();

$agreement_content = str_replace("<FEE>", "U\$D " . $oCompany->getCompanyTestFee(), $agreement_content);
$agreement_content = str_replace("<COMPANY NAME>", $oCompany->getName(), $agreement_content);

include(ROOT_DIR . "/mpdf/mpdf.php");

$mpdf = new mPDF('c', 'Letter');
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0; // 1 or 0 - whether to indent the first level of a list

$stylesheet = file_get_contents(ROOT_DIR . '/css/report.css');
$mpdf->WriteHTML($stylesheet, 1); // The parameter 1 tells that this is css/style only and no body/html/text

$_SESSION['agreement_content'] = nl2br($agreement_content);
ob_start();
include ROOT_DIR . '/company/agreement_pdf_html.php';
$html = ob_get_clean();
//$_SESSION['agreement_content'] = "";

$mpdf->WriteHTML($html, 2);
//$mpdf->Output();
$mpdf->Output();
?>

