<?php


include '../login_check.php';
include 'login_company_check.php';
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/controllers/companies_controller.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/servicios/servicios.php';

$companiesController = new CompaniesController();

$idCompany = $_SESSION['user']['company']['id'];

$result = $companiesController->addBillingInfo($idCompany);

//header('Location: billing-info.php?result=' . $result);


echo '<script type="text/javascript">';
echo 'window.location.href="billing-info.php?result=' . $result.'";';
echo '</script>';

?>