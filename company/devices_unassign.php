<?php

include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';

$idDevice = $_GET['id'];
$inspectorId = $_GET['inspectorId'];
$currentDate = date("Y-m-d");
$redirectLocation = "devices_process.php?action=unassign&id={$idDevice}&date={$currentDate}&inspectorId={$inspectorId}";
header("Location: " . $redirectLocation);
?>