<?php
include '../login_check.php';
include 'login_company_check.php';
include_once '../init.php';
include ROOT_DIR . '/libs/pagination/pagination.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/inspector.php';
include_once ROOT_DIR . '/entidades/entity.php';
include_once ROOT_DIR . '/servicios/servicios.php';
session_start();

$servicios = new Servicios();

//Pagination
$perpage = $GLOBAL_SETTINGS['company.inspectors.max'];
$currentPage = isset($_GET['pag']) ? ((int) $_GET['pag']) : 1;
$from = (($currentPage * $perpage - $perpage));
$total = count($servicios->getInspectorsByIdCompany($_SESSION['user']['company']['id']));
$vInspectors = $servicios->getInspectorsByIdCompanyPag($_SESSION['user']['company']['id'], $from, $perpage);

$pagination = new Pagination($currentPage, $total, 5, $perpage);

$page = "company-data";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <link type="text/css" rel="stylesheet" href="../libs/pagination/css/pagination.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../js/jquery-1.9.1.js'></script>
        <style>
            #table-t{
                width: 100%;
                border-collapse: collapse;
            }

            #table-t th{
                border: 1px solid #000;
                background-color: #dedede;
            }

            #table-t td{
                border: 1px solid #000;
                background-color: #fff;
            }

            #table-t #disable td{
                background-color: #f94141;
            }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div id="menu-client"><?php include_once($path . 'includes/menu-company.php'); ?></div>
                        <div class="form-cd">
                            <br>
                            <a href="inspectors_add.php">New inspector</a>
                            <br>

                            <table id="table-t">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Entity</th>
                                    <th>Username</th>
                                    <th>Actions</th>
                                </tr>
                                <?php
                                foreach ($vInspectors as $oInspector) {
                                    $oUser = $servicios->getUserById($oInspector->getUser());
                                    ?>
                                    <tr <?php if (!$oInspector->getStatus()) echo 'id="disable"'; ?>>
                                        <td><?php echo $oInspector->getIdentifier(); ?></td>
                                        <td><?php echo $oInspector->getName(); ?></td>
                                        <td><?php echo $oInspector->getEntity(); ?></td>
                                        <td><?php echo $oUser->getUsername(); ?></td>
                                        <td>
                                            <a href="inspectors_edit.php?id=<?php echo $oInspector->getId(); ?>">Edit</a>
                                            |
                                            <a href="inspectors_status.php?id=<?php echo $oInspector->getId(); ?>"><?php echo ($oInspector->getStatus() == 1) ? "Disable" : "Enable"; ?></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </table>
                            <?php echo $pagination->render(); ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   