$(document).ready(function(){
    changeEntity($("#entitiesList").val());
    $( "#entitiesList" ).change(function() {
        changeEntity($(this).val());
    });
});

function changeEntity(entity){
    if(entity=="Other"){
        $("#entity").css("display","initial");
    }else{
        $("#entity").css("display","none");
    }
}