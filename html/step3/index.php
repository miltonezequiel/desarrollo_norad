<?php 
$page = "step3";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
     	<div id="container">
    		<header><?php include_once($path.'includes/header.php'); ?></header>
          		<div id="menu-step"><?php include_once($path.'includes/menu-step.php'); ?></div>
            		<div id="wrapper" class="page-<?php echo $page ?>">
                        <div id="container">
                                <div id="main">
                                	<div class="text">
                                    	<p>Press the following button to generate a preview. Your report will not be submitted and we will be able to make changes above if necessary. You will need to press PREVIEW to see any changes you make before finalizing.</p>
                                    </div>
                                    <div class="btn">
                                    	<form method="post">
                                             <div id="preview"><input type="submit" class="submit" value=""/></div>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>	
         </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   