<?php 
$page = "client-login";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
     <div id="container">
    	<header><?php include_once($path.'includes/header.php'); ?></header>
           <div id="wrapper" class="page-<?php echo $page ?>">
                     <div id="container">
                           <div id="main">
                                <div class="form-login">
                                	<form method="post">
                                                	<input type="text" name=""  class="box" value="Usuario" onfocus="if (this.value == 'Usuario') this.value = ''"	onblur="if (this.value == '') this.value = 'Usuario'" />
                                                    <input type="text" name=""  class="box" value="Password" onfocus="if (this.value == 'Password') this.value = ''"	onblur="if (this.value == '') this.value = 'Password'" />
                                                    <div id="login"><input type="submit" class="submit" value=""/></div>
                                                    <div class="txt"><p>Forgot your Password? <a href="#">Click Here to reset</a></p></div>
                                    </form>
                                 </div>
                           </div>
                    </div>
              </div>	
        </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   