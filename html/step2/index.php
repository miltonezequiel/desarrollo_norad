<?php 
$page = "step2";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script type="text/javascript">
				function clearContents(element) {
				  element.value = '';
				}
        </script> 
    </head>
	<body>
    	 <div id="container">
    		<header><?php include_once($path.'includes/header.php'); ?></header>
          		<div id="menu-step"><?php include_once($path.'includes/menu-step.php'); ?></div>
            		<div id="wrapper" class="page-<?php echo $page ?>">
                        <div id="container">
                                <div id="main">
                                	 <div class="stform">
                                            <form method="post">
                                            	<div class="form-pp">
                                                	<div class="btn"><span>Test Conducted by (Inspector)</span></div>
                                                 	<div class="btn"><span>Tester ID</span></div>
                                                </div>
                                                <div class="form-botonera">
                                                	<nav>
                                                        <a href="<?php echo $path; ?>step2" class="testsite<?php if($page == 'step2'){ echo '_activo'; } ?>">test site information<span></span></a>
                                                        <a href="<?php echo $path; ?>step2/reporting-requeriments.php" class="reporting<?php if($page == 'step2-reporting'){ echo '_activo'; } ?>">reporting requeriments<span></span></a>
                                                    </nav>
                                                </div>
                                                <div class="form-sp">
                                                	<input type="text" name=""  class="box" value="Street Address Line 1" onfocus="if (this.value == 'Street Address Line 1') this.value = ''"	onblur="if (this.value == '') this.value = 'Street Address Line 1'" />
                                                    <input type="text" name=""  class="box" value="Street Address Line 2" onfocus="if (this.value == 'Street Address Line 2') this.value = ''"	onblur="if (this.value == '') this.value = 'Street Address Line 2'" />
                                                    <input type="text" name=""  class="box" value="City" onfocus="if (this.value == 'City') this.value = ''"	onblur="if (this.value == '') this.value = 'City'" />
                                                	
                                                    <input type="text" name=""  class="box2" value="State" onfocus="if (this.value == 'State') this.value = ''"	onblur="if (this.value == '') this.value = 'State'" />
                                                    <input type="text" name=""  class="box3" value="ZIP" onfocus="if (this.value == 'ZIP') this.value = ''"	onblur="if (this.value == '') this.value = 'ZIP'" />
                                                    <input type="text" name=""  class="box" value="Floor Level Tested" onfocus="if (this.value == 'Floor Level Tested') this.value = ''"	onblur="if (this.value == '') this.value = 'Floor Level Tested'" />
                                                    <input type="text" name=""  class="box" value="Room Tested" onfocus="if (this.value == 'Room Tested') this.value = ''"	onblur="if (this.value == '') this.value = 'Room Tested'" />
                                               		
                                                    <textarea name="" class="box" rows="4" onfocus="clearContents(this);" onblur="if (this.value == '') this.value = 'Comments/Observations at Begining of Test'">Comments/Observations at Begining of Test</textarea>
                                                    <textarea name="" class="box" rows="4" onfocus="clearContents(this);" onblur="if (this.value == '') this.value = 'Comments Observations at End of Test'">Comments Observations at End of Test</textarea>
                                               		
                                                    <input type="checkbox" value=""><label> Insert Photo Page?</label>
                                                    
                                                    <div id="savechanges"><input type="submit" class="submit" value=""/></div>
                                               </div>
                                           </form>
                                       </div>
                                </div>
                        </div>
                    </div>	
         </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   