<?php 
$page = "step2-reporting";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script type="text/javascript">
				function clearContents(element) {
				  element.value = '';
				}
        </script> 
    </head>
	<body>
    	 <div id="container">
    		<header><?php include_once($path.'includes/header.php'); ?></header>
          		<div id="menu-step"><?php include_once($path.'includes/menu-step.php'); ?></div>
            		<div id="wrapper" class="page-step2">
                        <div id="container">
                                <div id="main">
                                	 <div class="stform">
                                            <form method="post">
                                            	<div class="form-pp">
                                                	<div class="btn"><span>Test Conducted by (Inspector)</span></div>
                                                 	<div class="btn"><span>Tester ID</span></div>
                                                </div>
                                                <div class="form-botonera">
                                                	<nav>
                                                        <a href="<?php echo $path; ?>step2" class="testsite<?php if($page == 'step2'){ echo '_activo'; } ?>">test site information<span></span></a>
                                                        <a href="<?php echo $path; ?>step2/reporting-requeriments.php" class="reporting<?php if($page == 'step2-reporting'){ echo '_activo'; } ?>">reporting requeriments<span></span></a>
                                                    </nav>
                                                </div>
                                                <div class="form-sp">
                                                	<input type="text" name=""  class="box" value="Client Name" onfocus="if (this.value == 'Client Name') this.value = ''"	onblur="if (this.value == '') this.value = 'Client Name'" />
                                                    <input type="text" name=""  class="box" value="Client Email 1" onfocus="if (this.value == 'Client Email 1') this.value = ''"	onblur="if (this.value == '') this.value = 'Client Email 1'" />
                                                    <input type="text" name=""  class="box" value="Client Email 2" onfocus="if (this.value == 'Client Email 2') this.value = ''"	onblur="if (this.value == '') this.value = 'Client Email 2'" />
                                                	
                                                    <div class="ckeck-mgn"><input type="checkbox" value=""><label> Optional Hardcopy Mailed to: ($5.00 charge applies) :</label></div>
                                                        <div class="show-input">
                                                            <input type="text" name=""  class="box" value="Additional Report emailed to (name) :" onfocus="if (this.value == 'Additional Report emailed to (name) :') this.value = ''"	onblur="if (this.value == '') this.value = 'Additional Report emailed to (name) :'" />
                                                            <input type="text" name=""  class="box" value="Email" onfocus="if (this.value == 'Email') this.value = ''"	onblur="if (this.value == '') this.value = 'Email'" />
                                                        </div>
                                                    <div class="ckeck-mgn"><input type="checkbox" value=""><label> Optional Hardcopy Mailed to: ($5.00 charge applies) :</label></div>
                                                        <div class="show-input">
                                                            <input type="text" name=""  class="box" value="Additional Report emailed to (name) :" onfocus="if (this.value == 'Additional Report emailed to (name) :') this.value = ''"	onblur="if (this.value == '') this.value = 'Additional Report emailed to (name) :'" />
                                                            <input type="text" name=""  class="box" value="Email" onfocus="if (this.value == 'Email') this.value = ''"	onblur="if (this.value == '') this.value = 'Email'" />
                                                        </div>
                                                    <div class="ckeck-mgn"><input type="checkbox" value=""><label> Optional Hardcopy Mailed to: ($5.00 charge applies) :</label></div>
                                                        <div class="show-input">
                                                            <input type="text" name=""  class="box" value="Additional Report emailed to (name) :" onfocus="if (this.value == 'Additional Report emailed to (name) :') this.value = ''"	onblur="if (this.value == '') this.value = 'Additional Report emailed to (name) :'" />
                                                            <input type="text" name=""  class="box" value="Email" onfocus="if (this.value == 'Email') this.value = ''"	onblur="if (this.value == '') this.value = 'Email'" />
                                                        </div>    
                                                    
                                                    <div id="savechanges"><input type="submit" class="submit" value=""/></div>
                                               </div>
                                           </form>
                                      </div>
                                </div>
                        </div>
                    </div>	
         </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   