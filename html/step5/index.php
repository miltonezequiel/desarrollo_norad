<?php 
$page = "step5";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
    	<div id="container">
    		<header><?php include_once($path.'includes/header.php'); ?></header>
          		<div id="menu-step"><?php include_once($path.'includes/menu-step.php'); ?></div>
             		<div id="wrapper" class="page-<?php echo $page ?>">
                        	<div id="container">
                                <div id="main">
                                	<div class="text">
                                    	<h1>DONE!</h1>
                                    	<p>Your test has been completed successfully and the Test Reports have been submitted.</p>
                                        <p>A copy of the test report has also been sent to you at <span class="negrita"><a href="mailto:email@email.com">email@email.com</a></span></p>
                                        <p>Your Credit Card has been charged: <span class="negrita">$100.00</span></p>
                                        <p>Please click HERE to generate a Receipt for your records.</p>
                                        <p>Please do not unplug the test device from the USB Connection until the device has reset and returned to READY MODE</p>
                                    </div>
                                    <div class="btn">
                                    	<form method="post">
                                             <div id="return"><input type="submit" class="submit" value=""/></div>
                                        </form>
                                    </div>
                                </div>
                        	</div>
                    </div>	
        </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   