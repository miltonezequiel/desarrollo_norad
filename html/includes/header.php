<div class="container">
	<div class="logo"><a href="<?php echo $path; ?>"><img src="<?php echo $path; ?>images/header-logo.jpg"/></a></div>
</div>
<div class="menu">
    <div class="container">
            <nav>
                <a href="<?php echo $path; ?>client-login" class="client-login<?php if($page == 'client-login' or $page == 'company-data'){ echo '_activo'; } ?>">client login<span></span></a>
                <a href="<?php echo $path; ?>step1" class="upload_test<?php if($page == 'upload_test' or $page == 'step1' or $page == 'step2' or $page == 'step3' or $page == 'step4' or $page == 'step5' or $page == 'step2-reporting'){ echo '_activo'; } ?>">upload test<span></span></a>
                <a href="<?php echo $path; ?>tester-resources" class="tester-resources<?php if($page == 'tester-resources'){ echo '_activo'; } ?>">tester resources<span></span></a>
                <a href="<?php echo $path; ?>about-norad" class="about-norad<?php if($page == 'about-norad'){ echo '_activo'; } ?>">about norad<span></span></a>
                <a href="<?php echo $path; ?>contact-us" class="contact-us<?php if($page == 'contact-us'){ echo '_activo'; } ?>">contact us<span></span></a>
            </nav>
    </div>
</div>