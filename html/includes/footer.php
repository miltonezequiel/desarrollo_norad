<div class="container">
       <div class="mgn">
            <div class="txt1">
                <p>
                	This website is for use by home inspection and environmental professionals using the NORAD Radon Detection System<br />
                    | If you are looking for general information on radon or radon testing please use the following link: <a href="http://www.envirolabs-inc.com/" target="_blank">www.envirolabs-inc.com</a>
                </p>
            </div>
            <div class="txt2">
                <p>This website and its content is copyright of Envirolabs Incorporated, Copyright © 2014, Envirolabs Incorporated. All Rights Reserved.</p>
            </div>
       </div>
</div>
