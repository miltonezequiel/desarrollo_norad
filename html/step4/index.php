<?php 
$page = "step4";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
   		<div id="container">
    		<header><?php include_once($path.'includes/header.php'); ?></header>
          		<div id="menu-step"><?php include_once($path.'includes/menu-step.php'); ?></div>
            		 <div id="wrapper" class="page-<?php echo $page ?>">
                        <div id="container">
                                <div id="main">
                                	<div class="text">
                                    	<h1>DISCLAIMER</h1>
                                    	<p>The test report will be generated and submitted in accordance with the information you have provided. Accordingly Envirolabs Incorporated can make no assertion regarding its accuracy.</p>
                                        <p>By clicking below you agree to hold Envirolabs Incorporated harmless in the event of any dispute between you and your client regarding this test.</p>
                                        <p>Our liability is limited to the amount you are billed for this service.</p>
                                        <p>Your Credit card on file will automatically be charged as per your service agreement once the test has been submited.</p>
                                    </div>
                                    <div class="btn">
                                    	<form method="post">
                                             <input type="checkbox" value=""><label>I HAVE READ AND AGREE TO THESE TERMS</label>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    </div>	
         </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   