<?php 
$page = "step1";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
     <div id="container">
    	<header><?php include_once($path.'includes/header.php'); ?></header>
          <div id="menu-step"><?php include_once($path.'includes/menu-step.php'); ?></div>
             <div id="wrapper" class="page-<?php echo $page ?>">
                        <div id="container">
                                <div id="main">
                                	<div class="text">
                                    	<p>Please plug inthe NORAD Test Device to your computers USB Port and make sure it is in <span class="negrita">UPLOAD Mode</span><img src="<?php echo $path; ?>images/step1-quest.jpg" class="quest"/></p>
										<br />
                                        <p>When you are ready to proceed press upload</p>
                                    </div>
                                    <div class="btn">
                                    	<form method="post">
                                             <div class="custom-input-file">
                                             	<input type="file" size="1" class="input-file" />
   												<img src="<?php echo $path; ?>images/step1-btn-upload.jpg" class="upload"/>
											</div>
                                             <img src="<?php echo $path; ?>images/step1-mas.jpg" class="mas"/>
                                        </form>
                                    </div>
                                </div>
                        </div>
              </div>	
        </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   