<?php 
$page = "about-norad";
$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
	<head>
		<title>NORAD | Radon Detection System</title>
		<meta name="keywords" content="" />	
		<link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
  		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
	<body>
     <div id="container">
    	<header><?php include_once($path.'includes/header.php'); ?></header>
           <div id="wrapper" class="page-<?php echo $page ?>">
                     <div id="container">
                           <div id="main">
                                <div class="info">
                                	<div class="txt">
                                    	<h1>Accurate Results and Quick Turnaround</h1>
                                        <p>The NORAD Radon Detection System is available exclusively through Envirolabs Incorporated to home inspectors, environmental and real estate professionals (not available in all states).</p>
                                        <p>Using NORAD you can provide your clients with the most advanced radon continuos monitor testing available without incurring any costs. We will provide you with a supply of NORAD test devices for you to place during your home inspection and you pay only for the for the tests performed*.</p>
                                        <p>The following parameters are monitored on an hourly basis: <br /><br />
                                            > Radon concentrations<br />
                                            > Barometric pressure<br />
                                            > Temperature<br />
                                            > Humidity<br />
                                        </p>
                                        <p>Additionally devices are equipped with tilt detectors and record interruptions of power supply to make sure devices have not been moved or tampered with.</p>
                                        <p>Designed to meet the rigorous protocols of real estate transactions, in a timely manner, a detailed test reports is emailed to designated parties immeadiately after you upload the data to our server using your computers USB port. Once this is done the device is reset and ready to be used again at your next test.</p>
                                        <p>If you are a real estate professional please use the Contact link above and we will be happy to provide you with more information regarding how the Norad Radon Detection Syetem can assist you.</p>
                                        <p class="sub">* Certain conditions apply. Contact us for details</p>
                                    </div>
                                	<div class="img"><img src="<?php echo $path; ?>images/about-norad-img.jpg"></div>
                                </div>
                           </div>
                    </div>
              </div>	
        </div>
        <footer><?php include_once($path.'includes/footer.php'); ?></footer>
    </body>
</html>   