<?php

class CompanyAgreement {

    private $id;
    private $company_id;
    private $agreement_id;
    private $date;
    
    function __construct($company_id, $agreement_id, $date) {
        $this->company_id = $company_id;
        $this->agreement_id = $agreement_id;
        $this->date = $date;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getCompanyId() {
        return $this->company_id;
    }

    public function setCompanyId($company_id) {
        $this->company_id = $company_id;
    }
    
    public function getAgreementId() {
        return $this->agreement_id;
    }

    public function setAgreementId($agreement) {
        $this->agreement_id = $agreement;
    }
    
    public function getDate() {
        return $this->date;
    }

    public function setDate($date) {
        $this->date = $date;
    }
    
}

?>