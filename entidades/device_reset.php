<?php

class DeviceReset {

    private $device;
    private $datetime;

    public function getDevice() {
        return $this->device;
    }

    public function setDevice($device) {
        $this->device = $device;
    }

    public function getDatetime() {
        return $this->datetime;
    }

    public function setDatetime($datetime) {
        $this->datetime = $datetime;
    }
}

?>