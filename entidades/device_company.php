<?php

class DeviceCompany {

    private $idDevice;
    private $idCompany;
    private $startDate;
    private $endDate;
    
    private $id;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getIdDevice() {
        return $this->idDevice;
    }

    public function setIdDevice($idDevice) {
        $this->idDevice = $idDevice;
    }

    public function getIdCompany() {
        return $this->idCompany;
    }

    public function setIdCompany($idCompany) {
        $this->idCompany = $idCompany;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }
}

?>