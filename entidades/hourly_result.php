<?php

class HourlyResult {

    private $idTest;
    private $dateTime;
    private $alpha;
    private $radon;
    private $temperature;
    private $humidity;
    private $pressure;
    private $error;
    private $tilt;
    private $powerOutage;

    public function getIdTest() {
        return $this->idTest;
    }
    public function setIdTest($idTest) {
        $this->idTest = $idTest;
    }

    public function getDateTime() {
        return $this->dateTime;
    }
    public function setDateTime($dateTime) {
        $this->dateTime = $dateTime;
    }

    public function getAlpha() {
        return $this->alpha;
    }
    public function setAlpha($alpha) {
        $this->alpha = $alpha;
    }

    public function getRadon() {
        return $this->radon;
    }
    public function setRadon($radon) {
        $this->radon = $radon;
    }

    public function getTemperature() {
        return $this->temperature;
    }
    public function setTemperature($temperature) {
        $this->temperature = $temperature;
    }

    public function getHumidity() {
        return $this->humidity;
    }
    public function setHumidity($humidity) {
        $this->humidity = $humidity;
    }

    public function getPressure() {
        return $this->pressure;
    }
    public function setPressure($pressure) {
        $this->pressure = $pressure;
    }

    public function getError() {
        return $this->error;
    }
    public function setError($error) {
        $this->error = $error;
    }

    public function getTilt() {
        return $this->tilt;
    }
    public function setTilt($tilt) {
        $this->tilt = $tilt;
    }

    public function getPowerOutage() {
        return $this->powerOutage;
    }
    public function setPowerOutage($powerOutage) {
        $this->powerOutage = $powerOutage;
    }
    
}

?>