<?php

class Payment {

    private $payment_id;
    private $payment_date;
    private $payment_total;
    private $payment_type;
    private $payment_type_str;
    private $test_id;
    private $payment_is_payed;
    private $payment_number;

    function __construct($payment_id, $payment_date, $payment_total, $payment_type, $payment_type_str, $test_id, $payment_is_payed, $paymentNumber) {
        $this->payment_id = $payment_id;
        $this->payment_date = $payment_date;
        $this->payment_total = $payment_total;
        $this->payment_type = $payment_type;
        $this->payment_type_str = $payment_type_str;
        $this->test_id = $test_id;
        $this->payment_is_payed = $payment_is_payed;
        $this->payment_number = $paymentNumber;
    }

    public function getPaymentId() {
        return $this->payment_id;
    }
    public function setPaymentId($id) {
        $this->payment_id = $id;
    }
    
    public function getPaymentDate() {
        return $this->payment_date;
    }
    public function setPaymentDate($date) {
        $this->payment_date = $date;
    }
    
    public function getPaymentTotal() {
        return $this->payment_total;
    }
    public function setPaymentTotal($total) {
        $this->payment_total = $total;
    }
    
    public function getPaymentType() {
        return $this->payment_type;
    }
    public function setPaymentType($type) {
        $this->payment_type = $type;
    }
    public function getPaymentTypeStr() {
        return $this->payment_type_str;
    }
    public function setPaymentTypeStr($type) {
        $this->payment_type_str = $type;
    }
    
    public function getTestId() {
        return $this->test_id;
    }
    public function setTestId($test_id) {
        $this->test_id = $test_id;
    }
    
    public function getPaymentIsPayed() {
        return $this->payment_is_payed;
    }
    public function setPaymentIsPayed($payment_is_payed) {
        $this->payment_is_payed = $payment_is_payed;
    }
    
    public function getPaymentNumber() {
        return $this->payment_number;
    }
    public function setPaymentNumber($paymentNumber) {
        $this->payment_number = $paymentNumber;
    }
    
}

?>