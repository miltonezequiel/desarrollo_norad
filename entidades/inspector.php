<?php

class Inspector {

    private $id;
    private $name;
    private $idCompany;
    private $user;
    private $entity;
    private $certificationNumber;
    private $status;
    private $email;
    private $identifier;

    function __construct($id = NULL, $name = NULL, $idCompany = NULL, $user = NULL, $email = NULL) {
        $this->id = $id;
        $this->name = $name;
        $this->idCompany = $idCompany;
        $this->user = $user;
        $this->email = $email;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getIdCompany() {
        return $this->idCompany;
    }

    public function setIdCompany($idCompany) {
        $this->idCompany = $idCompany;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getEntity() {
        return $this->entity;
    }

    public function setEntity($entity) {
        $this->entity = $entity;
    }

    public function getCertificationNumber() {
        return $this->certificationNumber;
    }

    public function setCertificationNumber($certificationNumber) {
        $this->certificationNumber = $certificationNumber;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getIdentifier() {
        return $this->entity . " " . $this->certificationNumber;
    }
}

?>