<?php

class User {

    private $id;
    private $username;
    private $password;
    private $logged;
    private $deviceid;
    private $devicesn;
    private $devicetid;

    function __construct($id, $username, $password, $logged, $deviceid, $devicesn, $devicetid) {
        $this->id = $id;
        $this->username = $username;
        $this->password = $password;

        $this->logged = $logged;
        $this->deviceid = $deviceid;
        $this->devicesn = $devicesn;
        $this->devicetid = $devicetid;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function setUsername($username) {
        $this->username = $username;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
    }
    
    
    
    public function getLogged() {
        return $this->logged;
    }
    public function setLogged($logged) {
        $this->logged = $logged;
    }
   
    public function getDeviceID() {
        return $this->deviceid;
    }
    public function setDeviceID($deviceid) {
        $this->deviceid = $deviceid;
    }
   
    public function getDeviceSN() {
        return $this->devicesn;
    }
    public function setDeviceSN($devicesn) {
        $this->devicesn = $devicesn;
    }

    public function getDTestID() {
        return $this->devicetid;
    }
    public function setDTestID($devicetid) {
        $this->devicetid = $devicetid;
    }
    
}

?>