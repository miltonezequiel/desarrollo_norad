<?php

class CompanyFee {

    private $company_fee_id;
    private $company_fee_from;
    private $company_fee_to;
    private $company_fee_fee;
    private $company_id;
    
    function __construct($company_fee_id, $company_fee_from, $company_fee_to, $company_fee_fee, $company_id) {
        $this->company_fee_id = $company_fee_id;
        $this->company_id = $company_id;
        $this->company_fee_from = $company_fee_from;
        $this->company_fee_to = $company_fee_to;
        $this->company_fee_fee = $company_fee_fee;
    }

    public function getCompanyFeeId() {
        return $this->company_fee_id;
    }
    public function setCompanyFeeId($id) {
        $this->company_fee_id = $id;
    }
    
    public function getCompanyFeeFrom() {
        return $this->company_fee_from;
    }
    public function setCompanyFeeFrom($company_fee_from) {
        $this->company_fee_from = $company_fee_from;
    }
    
    public function getCompanyFeeTo() {
        return $this->company_fee_to;
    }
    public function setCompanyFeeTo($company_fee_to) {
        $this->company_fee_to = $company_fee_to;
    }
    
    public function getCompanyFeeFee() {
        return $this->company_fee_fee;
    }
    public function setCompanyFeeFee($company_fee_fee) {
        $this->company_fee_fee = $company_fee_fee;
    }
    
    public function getCompanyFeeCompanyId() {
        return $this->company_id;
    }
    public function setCompanyFeeCompanyId($company_id) {
        $this->company_id = $company_id;
    }
    
}

?>