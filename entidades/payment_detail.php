<?php

class Payment_detail {

    private $payment_details_id;
    private $payment_details_desc;
    private $payment_details_subtotal;
    private $payment_details_quantity;
    private $payment_id;

    function __construct($payment_details_desc, $payment_details_subtotal, $payment_id, $payment_details_quantity) {
        $this->payment_details_desc = $payment_details_desc;
        $this->payment_details_subtotal = $payment_details_subtotal;
        $this->payment_id = $payment_id;
        $this->payment_details_quantity = $payment_details_quantity;
    }

    public function getPaymentDetailsId() {
        return $this->payment_details_id;
    }
    public function setPaymentDetailsId($id) {
        $this->payment_details_id = $id;
    }
    
    public function getPaymentDetailsDesc() {
        return $this->payment_details_desc;
    }
    public function setPaymentDetailsDesc($desc) {
        $this->payment_details_desc = $desc;
    }
    
    public function getPaymentDetailsSubtotal() {
        return $this->payment_details_subtotal;
    }
    public function setPaymentDetailsSubtotal($subtotal) {
        $this->payment_details_subtotal = $subtotal;
    }
    
    public function getPaymentId() {
        return $this->payment_id;
    }
    public function setPaymentId($paymentId) {
        $this->payment_id = $paymentId;
    }
    
    public function getPaymentDetailsQuantity() {
        return $this->payment_details_quantity;
    }
    public function setPaymentDetailsQuantity($payment_details_quantity) {
        $this->payment_details_quantity = $payment_details_quantity;
    }
    
}

?>