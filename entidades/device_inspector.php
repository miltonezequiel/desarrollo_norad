<?php

class DeviceInspector {

//    private $idDevice;
//    private $idInspector;
//    private $startDate;
    private $id;
    private $endDate;
    private $startDateCompany;  
    private $idCompany;
    private $deviceSerial;
    private $idDeviceCompany;
    
    function __construct($id = NULL, $idDeviceCompany, $idInspector = NULL, $endDate = NULL, $startDateCompany = NULL, $deviceSerial = NULL) {
        $this->id = $id;
        $this->idDeviceCompany = $idDeviceCompany;
        $this->idInspector = $idInspector;
        $this->endDate = $endDate;
        $this->startDateCompany = $startDateCompany;
        $this->deviceSerial = $deviceSerial;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    
    public function getIdDevice() {
        return $this->idDevice;
    }

    public function setIdDeviceCompany($idDeviceCompany) {
        $this->idDeviceCompany = $idDeviceCompany;
    }
    
    public function getIdDeviceCompany() {
        return $this->idDeviceCompany;
    }

    public function setIdDevice($idDevice) {
        $this->idDevice = $idDevice;
    }

    public function getIdInspector() {
        return $this->idInspector;
    }

    public function setIdInspector($idInspector) {
        $this->idInspector = $idInspector;
    }

    public function getStartDate() {
        return $this->startDate;
    }

    public function setStartDate($startDate) {
        $this->startDate = $startDate;
    }

    public function getEndDate() {
        return $this->endDate;
    }

    public function setEndDate($endDate) {
        $this->endDate = $endDate;
    }

    public function getStartDateCompany() {
        return $this->startDateCompany;
    }

    public function setStartDateCompany($startDateCompany) {
        $this->startDateCompany = $startDateCompany;
    }

    public function getIdCompany() {
        return $this->idCompany;
    }

    public function setIdCompany($idCompany) {
        $this->idCompany = $idCompany;
    }
    
    public function getDeviceSerial() {
        return $this->deviceSerial;
    }

    public function setDeviceSerial($deviceSerial) {
        $this->deviceSerial = $deviceSerial;
    }
}

?>