<?php

class Test {

    private $id;
    private $address;
    private $address2;
    private $city;
    private $state;
    private $zip;
    private $floorLevel;
    private $room;
    private $commentsBegin;
    private $commentsEnd;
    private $reportDate;
    private $test_di_dc_id;
    private $idClient;
    private $file;
    private $image1;
    private $image2;
    private $image3;
    private $imageDescription1;
    private $imageDescription2;
    private $imageDescription3;
    private $authCode;
    private $transId;
    private $idInspector;
    private $idDevice;
    private $serialDevice;
    private $receiptId;
    private $payment_id;

    function __construct($id = NULL, $address = NULL, $address2 = NULL, $city = NULL, $state = NULL, $zip = NULL, $floorLevel = NULL, $room = NULL, $commentsBegin = NULL, $commentsEnd = NULL, $reportDate = NULL, $test_di_dc_id = NULL, $idClient = NULL, $authCode = NULL, $transId = NULL, $receiptId = NULL) {
        $this->id = $id;
        $this->address = $address;
        $this->address2 = $address2;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
        $this->floorLevel = $floorLevel;
        $this->room = $room;
        $this->commentsBegin = $commentsBegin;
        $this->commentsEnd = $commentsEnd;
        $this->reportDate = $reportDate;
        $this->test_di_dc_id = $test_di_dc_id;
        $this->idClient = $idClient;
        $this->authCode = $authCode;
        $this->transId = $transId;
        $this->receiptId = $receiptId;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($address) {
        $this->address = $address;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getState() {
        return $this->state;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function getZip() {
        return $this->zip;
    }

    public function setZip($zip) {
        $this->zip = $zip;
    }

    public function getFloorLevel() {
        return $this->floorLevel;
    }

    public function setFloorLevel($floorLevel) {
        $this->floorLevel = $floorLevel;
    }

    public function getRoom() {
        return $this->room;
    }

    public function setRoom($room) {
        $this->room = $room;
    }

    public function getCommentsBegin() {
        return $this->commentsBegin;
    }

    public function setCommentsBegin($commentsBegin) {
        $this->commentsBegin = $commentsBegin;
    }

    public function getCommentsEnd() {
        return $this->commentsEnd;
    }

    public function setCommentsEnd($commentsEnd) {
        $this->commentsEnd = $commentsEnd;
    }

    public function getReportDate() {
        return $this->reportDate;
    }

    public function setReportDate($reportDate) {
        $this->reportDate = $reportDate;
    }

    public function getTestDiDcId() {
        return $this->test_di_dc_id;
    }

    public function setTestDiDcId($test_di_dc_id) {
        return $this->test_di_dc_id = $test_di_dc_id;
    }

    public function getIdClient() {
        return $this->idClient;
    }

    public function setIdClient($idClient) {
        $this->idClient = $idClient;
    }

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        $this->file = $file;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }

    public function getImage1() {
        return $this->image1;
    }

    public function setImage1($image1) {
        $this->image1 = $image1;
    }

    public function getImage2() {
        return $this->image2;
    }

    public function setImage2($image2) {
        $this->image2 = $image2;
    }

    public function getImage3() {
        return $this->image3;
    }

    public function setImage3($image3) {
        $this->image3 = $image3;
    }

    public function getImageDescription1() {
        return $this->imageDescription1;
    }

    public function setImageDescription1($imageDescription1) {
        $this->imageDescription1 = $imageDescription1;
    }

    public function getImageDescription2() {
        return $this->imageDescription2;
    }

    public function setImageDescription2($imageDescription2) {
        $this->imageDescription2 = $imageDescription2;
    }

    public function getImageDescription3() {
        return $this->imageDescription3;
    }

    public function setImageDescription3($imageDescription3) {
        $this->imageDescription3 = $imageDescription3;
    }

    public function getAuthCode() {
        return $this->authCode;
    }

    public function setAuthCode($authCode) {
        $this->authCode = $authCode;
    }

    public function getTransId() {
        return $this->transId;
    }

    public function setTransId($transId) {
        $this->transId = $transId;
    }

    public function setIdInspector($idInspector) {
        $this->idInspector = $idInspector;
    }

    public function getIdInspector() {
        return $this->idInspector;
    }
    
    public function setIdDevice($idDevice) {
        $this->idDevice = $idDevice;
    }

    public function getIdDevice() {
        return $this->idDevice;
    }
    
    public function setDeviceSerialnumber($serialNumber) {
        $this->serialDevice = $serialNumber;
    }

    public function getDeivceSerialNumber() {
        return $this->serialDevice;
    }
    
    public function setReceiptId($receiptId) {
        $this->receiptId = $receiptId;
    }

    public function getReceiptId() {
        return $this->receiptId;
    }

    public function getImages() {
        $vImages = array();
        if (isset($this->image1) && !is_null($this->image1) && $this->image1 != "") {
            $image = array(
                "name" => $this->image1,
                "description" => $this->imageDescription1
            );
            array_push($vImages, $image);
        }
        if (isset($this->image2) && !is_null($this->image2) && $this->image2 != "") {
            $image = array(
                "name" => $this->image2,
                "description" => $this->imageDescription2
            );
            array_push($vImages, $image);
        }
        if (isset($this->image3) && !is_null($this->image3) && $this->image3 != "") {
            $image = array(
                "name" => $this->image3,
                "description" => $this->imageDescription3
            );
            array_push($vImages, $image);
        }
        return $vImages;
    }
    
    public function setPaymentId($idPayment) {
        $this->payment_id = $idPayment;
    }

    public function getPaymentId() {
        return $this->payment_id;
    }
}

?>