<?php



class Device {



    private $id;

    private $name;

    private $error;

    private $serialNumber;



    function __construct($id, $name, $error, $serialNumber) {

        $this->id = $id;

        $this->name = $name;

        $this->error = $error;

        $this->serialNumber = $serialNumber;

    }



    public function getId() {

        return $this->id;

    }



    public function setId($id) {

        $this->id = $id;

    }



    public function getName() {

        return $this->name;

    }



    public function setName($name) {

        $this->name = $name;

    }



    public function getError() {

        return $this->error;

    }



    public function setError($error) {

        $this->error = $error;

    }



    public function getSerialNumber() {

        return $this->serialNumber;

    }



    public function setSerialNumber($serialNumber) {

        $this->serialNumber = $serialNumber;

    }
    
}



?>