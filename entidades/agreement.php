<?php

class Agreement {

    private $id;
    private $content;
    
    function __construct($id, $content) {
        $this->id = $id;
        $this->content = $content;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getContent() {
        return $this->content;
    }

    public function setContent($content) {
        $this->content = $content;
    }
    
}

?>