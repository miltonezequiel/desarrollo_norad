<?php

class Client {

    private $id;
    private $name;
    private $email;
    private $email2;
    private $second_nameClient; 
    private $second_emailClient;
    private $second_email2Client;
    private $nameAgent;
    private $emailAgent;
    private $email2Agent;
    private $second_nameAgent;
    private $second_emailAgent;
    private $second_email2Agent;
    
    function __construct($id, $name, $email, $email2, $second_nameClient, $second_emailClient, $second_email2Client, $nameAgent, $emailAgent, $email2Agent, $second_nameAgent, $second_emailAgent, $second_email2Agent) {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->email2 = $email2;
        $this->second_nameClient = $second_nameClient; 
        $this->second_emailClient = $second_emailClient;
        $this->second_email2Client = $second_email2Client;
        $this->nameAgent = $nameAgent;
        $this->emailAgent = $emailAgent;
        $this->email2Agent = $email2Agent;
        $this->second_nameAgent = $second_nameAgent;
        $this->second_emailAgent = $second_emailAgent;
        $this->second_email2Agent = $second_email2Agent;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getEmail2() {
        return $this->email2;
    }

    public function setEmail2($email2) {
        $this->email2 = $email2;
    }
    
    // second client
    public function getSecondName() {
        return $this->second_nameClient;
    }

    public function setSecondName($name) {
        $this->second_nameClient = $name;
    }

    public function getSecondEmail() {
        return $this->second_emailClient;
    }

    public function setSecondEmail($email) {
        $this->second_emailClient = $email;
    }

    public function getSecondEmail2() {
        return $this->second_email2Client;
    }

    public function setSecondEmail2($email2) {
        $this->second_email2Client = $email2;
    }
    
    // first agent
    public function getNameAgent() {
        return $this->nameAgent;
    }

    public function setNameAgent($name) {
        $this->nameAgent = $name;
    }

    public function getAgentEmail() {
        return $this->emailAgent;
    }

    public function setAgentEmail($email) {
        $this->emailAgent = $email;
    }

    public function getAgentEmail2() {
        return $this->email2Agent;
    }

    public function setAgentEmail2($email2) {
        $this->email2Agent = $email2;
    }
    
    // second agent
    public function getSecondAgentName() {
        return $this->second_nameAgent;
    }

    public function setSecondAgentName($name) {
        $this->second_nameAgent = $name;
    }

    public function getSecondAgentEmail() {
        return $this->second_emailAgent;
    }

    public function setSecondAgentEmail($email) {
        $this->second_emailAgent = $email;
    }

    public function getSecondAgentEmail2() {
        return $this->second_email2Agent;
    }

    public function setSecondAgentEmail2($email2) {
        $this->second_email2Agent = $email2;
    }
}

?>