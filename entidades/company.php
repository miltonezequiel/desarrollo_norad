<?php

class Company {

    private $id;
    private $name;
    private $user;
    private $address1;
    private $address2;
    private $city;
    private $state;
    private $zip;
    private $phone;
    private $email;
    private $contactPerson;
    private $status;
    
    private $company_customer_profile_id;
    private $company_customer_payment_profile_id;
    
    private $company_payment_type;
    private $company_test_fee;

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getAddress1() {
        return $this->address1;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }

    public function getCity() {
        return $this->city;
    }

    public function setCity($city) {
        $this->city = $city;
    }

    public function getState() {
        return $this->state;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function getZip() {
        return $this->zip;
    }

    public function setZip($zip) {
        $this->zip = $zip;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setPhone($phone) {
        $this->phone = $phone;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getContactPerson() {
        return $this->contactPerson;
    }

    public function setContactPerson($contactPerson) {
        $this->contactPerson = $contactPerson;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }
    
    public function getCustomerProfileId() {
        return $this->company_customer_profile_id;
    }
    
    public function setCustomerProfileId($customerProfilId) {
        $this->company_customer_profile_id = $customerProfilId;
    }
    
    public function getCustomerPaymentProfileId() {
        return $this->company_customer_payment_profile_id;
    }
    
    public function setCustomerPaymentProfileId($customerPaymentProfileId) {
        $this->company_customer_payment_profile_id = $customerPaymentProfileId;
    }
    
    public function getCompanyTestFee() {
        return $this->company_test_fee;
    }
    
    public function setCompanyTestFee($companyTestFee) {
        $this->company_test_fee = $companyTestFee;
    }
    
    public function getCompanyPaymentType() {
        return $this->company_payment_type;
    }
    
    public function setCompanyPaymentType($companyPaymentType) {
        $this->company_payment_type = $companyPaymentType;
    }
    
}

?>