<?php
session_start();
$page = "forgot_username";
$path = '../';
?>

<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />

        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">
                        <div class="form-login">
                            <div class="tit">
                                <h1>FORGOT <?php echo strtoupper($_GET['type']); ?> USERNAME</h1>
                            </div>
                            <form id="data-form" method="post" action="f_username_process.php">
                                <input type="email" name="email"  class="box" placeholder="E-Mail" />
                                <input type="hidden" name="type" value="<?php echo $_GET['type']; ?>"/>
                                
                                <div id="send">
                                    <input type="submit" class="btn-html" value="Send"/>
                                </div>
                                <div class="error"><?php if(isset($_GET['error'])) { echo $_GET['error']; } ?></div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html> 

<script type="text/javascript">
    $("#data-form").validate({
        rules: {
            email: "required"
        }, messages: {
            email: "Please insert an e-mail"
        },
        errorLabelContainer: $("#data-form div.error")
    });
</script>