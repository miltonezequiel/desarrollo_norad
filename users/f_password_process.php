<?php
include_once '../controllers/users_controller.php';
require_once ROOT_DIR . '/libs/PHPMailer/PHPMailerAutoload.php';

$email = $_POST['email'];
$username = $_POST['username'];
$type = $_POST['type'];

$oUserController = new UsersController();

$oUser = $oUserController->getUserByUsername($username);
$email = $oUserController->getEmail($type, $username);

$splitMail = explode("@", $email);
$userMaillength = round(strlen($splitMail[0])/2,0,PHP_ROUND_HALF_UP);
$fill = "*";

for ($i=0; $i<$userMaillength-1;$i++) {
 $fill = $fill . "*";   
}

$emailMask = $fill . substr($splitMail[0], $userMaillength) . "@" . $splitMail[1];

if ($oUser == null || $email == null) {
    header('Location: forgot_password.php?type=' . $type . '&error=User not found');
} else {
    //we create a random number base in the user id and the current time
    $randomId = strval($oUser->getId() + intval(date('s')) + intval(date('H')) + intval(date('s')) + intval(date('m')) * 425849846879) . strval($oUser->getId() + intval(date('s')) + intval(date('H')) + intval(date('s')) + intval(date('m')) * 5484516515849) . strval($oUser->getId() + intval(date('s')) + intval(date('H')) + intval(date('s')) + intval(date('m')) * 9876549849);
    $md5Value = md5($randomId);

    $ret = $oUserController->saveKeyPassRecover($oUser->getId(), $md5Value);

    if ($ret > 0) {
        //we send the corresponding e-mail
        $mail = new PHPMailer();
        $mail->setFrom('reports@noradonline.com', 'NORAD');
        $mail->addAddress($email, $email);
        $mail->Subject = '[NORAD] - Password Reset';
        $mail->msgHTML('Please click the following link to reset your password:<br><br><b>http://noradonline.com/users/restore_password.php?key=' . $randomId . '</b>.<br><br>If you did not make this request please ignore this email and your password will remain unchanged.<br><br> Regards, <br>The NORAD team');
        $mail->send();
    }
    else {
        header('Location: forgot_password.php?type=' . $type . '&error=There was an error recovering password');
    }
}

$path = '../';
?>

<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />

        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">
                        <div class="form-login">
                            <div class="tit">
                                <h1>FORGOT USER PASSWORD</h1>
                            </div>
                            An E-mail containing the requested username has been sent to the address: <?php echo $emailMask; ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>