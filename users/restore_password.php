<?php
include_once '../controllers/users_controller.php';
require_once ROOT_DIR . '/libs/PHPMailer/PHPMailerAutoload.php';

$key = $_GET['key'];
$md5Key = md5($key);

$oUserController = new UsersController();

$oUser = $oUserController->getUserByKey($md5Key);

if ($oUser == null) {
    header('Location: forgot_password.php?type=' . $type . '&error=The Key was not found or has expired.');
}

$path = '../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />

        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>

        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">

                        <div class="form-login">
                            <div class="tit">
                                <h1>CHANGE PASSWORD</h1>
                            </div>
                            <form id="data_login" method="post" action="change_password_process.php">
                                <input type="hidden" name="key" id="key" value="<?php echo $key; ?>" />
                                <input type="password" name="password" id="password" class="box" placeholder="Password" />
                                <input type="password" name="password_again"  class="box" placeholder="Repeat Password" />
                                <div id="login"><input type="submit" class="btn-html" value="Send"/></div>
                                <div class="errorContainer">                                         <div class="error"></div>                                     </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>   


<script type="text/javascript">
    $("#data_login").validate({
        rules: {
            password: "required",
            password_again: {
                equalTo: "#password"
            }
        }, messages: {
            password: "Please insert password",
            password_again: "Passwords are not equal."
        },
        errorLabelContainer: $("#data_login div.error")
    });
</script>