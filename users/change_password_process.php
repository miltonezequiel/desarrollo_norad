<?php
include_once '../controllers/users_controller.php';
require_once ROOT_DIR . '/libs/PHPMailer/PHPMailerAutoload.php';

$key = $_POST['key'];
$md5Key = md5($key);

$oUserController = new UsersController();

$oUser = $oUserController->getUserByKey($md5Key);

if ($oUser != null) {
    $oUser = $oUserController->editUser($oUser->getId());

    if ($oUser == null) {
        header('Location: change_password_error.php?error=Error changing password');
    } else {
        //we send the corresponding e-mail
//        $mail = new PHPMailer();
//        $mail->setFrom('reports@noradonline.com', 'NORAD');
//        $mail->addAddress($email, $email);
//        $mail->Subject = '[NORAD] - Change Password';
//        $mail->msgHTML('Dear ' . $oUser->getUsername() . ', <br>Your password has been changed. <br> <br> Regards, <br> the NORAD team');
//        $mail->send();
    }
}
else {
    header('Location: change_password_error.php?error=Error changing password');
}





$path = '../';
?>

<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />

        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">
                        <div class="form-login">
                            <div class="tit">
                                <h1>CHANGE PASSWORD</h1>
                            </div>
                            Your password has been changed successfully.
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>