<?php
include_once '../controllers/users_controller.php';
require_once ROOT_DIR . '/libs/PHPMailer/PHPMailerAutoload.php';

$email = $_POST['email'];
$type = $_POST['type'];

$oUserController = new UsersController();
$oUser = $oUserController->getUsername($type, $email);

if($oUser == null) {
    header('Location: forgot_username.php?type=' . $type . '&error=User not found');
}
else {
    //we send the corresponding e-mail
    $mail = new PHPMailer();
    $mail->setFrom('reports@noradonline.com', 'NORAD');
    $mail->addAddress($email, $email);
    $mail->Subject = '[NORAD] - Username Notification';
    $mail->msgHTML('The NORAD Online Website has received a request for the username associated with this email. <br><br> The requested username is:<b>' . $oUser->getUsername() . '</b>.<br><br>If you did not request this information please disregard this message.<br><br>Regards,<br>The NORAD Team');
    $mail->send();
}

$path = '../';
?>

<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />	
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />

        <script src='../js/jquery-1.9.1.js'></script>
        <script src="../libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="../libs/jquery-validation/dist/additional-methods.min.js"></script>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="wrapper" class="page-client-login">
                <div id="container2">
                    <div id="main">
                        <div class="form-login">
                            <div class="tit">
                                <h1>FORGOT <?php echo strtoupper($type); ?> USERNAME</h1>
                            </div>
                            An E-mail containing the requested username has been sent to the address: <?php echo $email; ?>
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>