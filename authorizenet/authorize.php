<?php

require 'vendor/autoload.php';

use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

define("AUTHORIZENET_LOG_FILE", "phplog");

class Authorizenet {

    function getCustomerPaymentProfileList() {
        // Common setup for API credentials (merchant)
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId = 'ref' . time();

        //Setting the paging
        $paging = new AnetAPI\PagingType();
        $paging->setLimit("1000");
        $paging->setOffset("1");

        //Setting the sorting
        $sorting = new AnetAPI\CustomerPaymentProfileSortingType();
        $sorting->setOrderBy("id");
        $sorting->setOrderDescending("false");

        //Creating the request with the required parameters
        $request = new AnetAPI\GetCustomerPaymentProfileListRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setPaging($paging);
        $request->setSorting($sorting);
//        $request->setSearchType("cardsExpiringInMonth");
//        $request->setMonth("2500-12");

        // Controller
        $controller = new AnetController\GetCustomerPaymentProfileListController($request);
        // Getting the response
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);

        if (($response != null)) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                // Success
                echo "GetCustomerPaymentProfileList SUCCESS: " . "<br>";
                $errorMessages = $response->getMessages()->getMessage();
                echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
                echo "Total number of Results in the result set" . $response->getTotalNumInResultSet() . "<br>";
                // Displaying the customer payment profile list 
                foreach ($response->getPaymentProfiles() as $paymentProfile) {
                    echo "\nCustomer Profile id: " . $paymentProfile->getCustomerProfileId() . "<br>";
                    echo "Payment profile id: " . $paymentProfile->getCustomerPaymentProfileId() . "<br>";
                    echo "Credit Card Number: " . $paymentProfile->getPayment()->getCreditCard()->getCardNumber() . "<br>";
                    if ($paymentProfile->getBillTo() != null)
                        echo "First Name in Billing Address: " . $paymentProfile->getBillTo()->getFirstName() . "<br>";
                }
            }
            else {
                // Error
                echo "GetCustomerPaymentProfileList ERROR :  Invalid response\n";
                $errorMessages = $response->getMessages()->getMessage();
                //echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
                echo $errorMessages[0]->getText() . "<br>";
            }
        } else {
            // Failed to get the response
            echo "NULL Response Error";
        }
        return $response;
    }

    function getCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId) {
        // Common setup for API credentials (merchant)
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId = 'ref' . time();

        //request requires customerProfileId and customerPaymentProfileId
        $request = new AnetAPI\GetCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setCustomerProfileId($customerProfileId);
        $request->setCustomerPaymentProfileId($customerPaymentProfileId);

        $controller = new AnetController\GetCustomerPaymentProfileController($request);
        //$response = $controller->executeWithApiResponse(\net\authorize\api\constants\ANetEnvironment::SANDBOX);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);

        $result = array(
            "result" => "",
            "cardNumber" => "",
            "name" => "",
            "error" => ""
        );

        if (($response != null)) {
            if ($response->getMessages()->getResultCode() == "Ok") {
                $result['result'] = "Ok";
                $result['cardNumber'] = $response->getPaymentProfile()->getPayment()->getCreditCard()->getCardNumber();
                $result['name'] = $response->getPaymentProfile()->getbillTo()->getFirstName() . " " . $response->getPaymentProfile()->getbillTo()->getLastName();
            } else {
                $result['result'] = "Fail";
                $errorMessages = $response->getMessages()->getMessage();
                $result['error'] = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
            }
        } else {
            $result['result'] = "Fail";
            $result['error'] = "Credit Card Authorization Server is not responding.";
        }

//        if (($response != null)) {
//            if ($response->getMessages()->getResultCode() == "Ok") {
//                echo "GetCustomerPaymentProfile SUCCESS: " . "<br>";
//                echo "Customer Payment Profile Id: " . $response->getPaymentProfile()->getCustomerPaymentProfileId() . "<br>";
//                echo "Customer Payment Profile Billing Address: " . $response->getPaymentProfile()->getbillTo()->getAddress() . "<br>";
//                echo "Customer Payment Profile Card Last 4 " . $response->getPaymentProfile()->getPayment()->getCreditCard()->getCardNumber() . "<br>";
//
//                if ($response->getPaymentProfile()->getSubscriptionIds() != null) {
//                    if ($response->getPaymentProfile()->getSubscriptionIds() != null) {
//
//                        echo "List of subscriptions:";
//                        foreach ($response->getPaymentProfile()->getSubscriptionIds() as $subscriptionid)
//                            echo $subscriptionid . "<br>";
//                    }
//                }
//            } else {
//                echo "GetCustomerPaymentProfile ERROR :  Invalid response\n";
//                $errorMessages = $response->getMessages()->getMessage();
//                echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
//            }
//        } else {
//            echo "NULL Response Error";
//        }
        return $result;
    }

    function getCustomerProfileIds() {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId = 'ref' . time();

        // Get all existing customer profile ID's
        $request = new AnetAPI\GetCustomerProfileIdsRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $controller = new AnetController\GetCustomerProfileIdsController($request);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);
        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            echo "GetCustomerProfileId's SUCCESS: " . "<br>";
            $profileIds[] = $response->getIds();
            echo "There are " . count($profileIds[0]) . " Customer Profile ID's for this Merchant Name and Transaction Key" . "<br>";
            foreach ($profileIds as $customer) {
                echo print_r($customer);
            }
        } else {
            echo "GetCustomerProfileId's ERROR :  Invalid response\n";
            $errorMessages = $response->getMessages()->getMessage();
            echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
        }
        return $response;
    }

    function createCustomerProfile(
    $merchantCustomerId, $customerType, $firstName, $lastName, $company, $adress, $city, $state, $zip, $country, $phoneNumber, $cardNumber, $expirationDate, $carCode, $validationMode, $email
    ) {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId = 'ref' . time();

        // Create the payment data for a credit card
        $creditCard = new AnetAPI\CreditCardType();
        $creditCard->setCardNumber($cardNumber);
        $creditCard->setExpirationDate($expirationDate);
        $creditCard->setCardCode($carCode);
        $paymentCreditCard = new AnetAPI\PaymentType();
        $paymentCreditCard->setCreditCard($creditCard);

        // Create the Bill To info
        $billto = new AnetAPI\CustomerAddressType();
        $billto->setFirstName($firstName);
        $billto->setLastName($lastName);
        $billto->setCompany($company);
        $billto->setAddress($adress);
        $billto->setCity($city);
        $billto->setState($state);
        $billto->setZip($zip);
        $billto->setCountry($country);
        $billto->setPhoneNumber($phoneNumber);

        // Create a Customer Profile Request
        //  1. create a Payment Profile
        //  2. create a Customer Profile   
        //  3. Submit a CreateCustomerProfile Request
        //  4. Validate Profiiel ID returned

        $paymentprofile = new AnetAPI\CustomerPaymentProfileType();

        $paymentprofile->setCustomerType($customerType); //individual or business
        $paymentprofile->setBillTo($billto);
        $paymentprofile->setPayment($paymentCreditCard);
        $paymentprofiles[] = $paymentprofile;
        $customerprofile = new AnetAPI\CustomerProfileType();
        //$customerprofile->setDescription("Customer 2 Test PHP");

        $customerprofile->setMerchantCustomerId($merchantCustomerId);
        $customerprofile->setEmail($email);
        $customerprofile->setPaymentProfiles($paymentprofiles);

        $request = new AnetAPI\CreateCustomerProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setProfile($customerprofile);
        $request->setValidationMode($validationMode);

        $controller = new AnetController\CreateCustomerProfileController($request);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);
//        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
//            echo "Succesfully create customer profile : " . $response->getCustomerProfileId() . "<br>";
//            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
//            echo "SUCCESS: PAYMENT PROFILE ID : " . $paymentProfiles[0] . "<br>";
//        } else {
//            echo "ERROR :  Invalid response\n";
//            $errorMessages = $response->getMessages()->getMessage();
//            echo "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText() . "<br>";
//        }


        $result = array(
            "result" => "",
            "customerProfileId" => "",
            "customerPaymentProfileId" => "",
            "error" => ""
        );

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $result['result'] = "Ok";
            $result['customerProfileId'] = $response->getCustomerProfileId();
            $paymentProfiles = $response->getCustomerPaymentProfileIdList();
            $result['customerPaymentProfileId'] = $paymentProfiles[0];
        } else {
            $result['result'] = "Fail";
            $errorMessages = $response->getMessages()->getMessage();
            $result['error'] = "Response : " . $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
        }
        return $result;
    }

    function chargeCustomerProfile($profileid, $paymentprofileid, $amount, $deviceSerialNumber = 0, $clientName = '', $siteAdress = '') {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);

        $refId = 'ref' . time();

        $profileToCharge = new AnetAPI\CustomerProfilePaymentType();
        $profileToCharge->setCustomerProfileId($profileid);
        $paymentProfile = new AnetAPI\PaymentProfileType();
        $paymentProfile->setPaymentProfileId($paymentprofileid);
        $profileToCharge->setPaymentProfile($paymentProfile);
        $lineItem = new AnetAPI\LineItemType();
        
        $lineItem->setItemId($deviceSerialNumber);
        $lineItem->setName($clientName);
        $lineItem->setDescription($siteAdress);
        $lineItem->setQuantity(1);
        $lineItem->setUnitPrice(0);

        $transactionRequestType = new AnetAPI\TransactionRequestType();
        $transactionRequestType->setTransactionType("authCaptureTransaction");
        $transactionRequestType->setAmount($amount);
        $transactionRequestType->setProfile($profileToCharge);
        
        $parLineItems = Array($lineItem);
        
        $transactionRequestType->setLineItems($parLineItems);

        $request = new AnetAPI\CreateTransactionRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setRefId($refId);
        $request->setTransactionRequest($transactionRequestType);
        $controller = new AnetController\CreateTransactionController($request);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);

        $autReturn = array(
            "result" => "",
            "authCode" => "",
            "transId" => "",
            "error" => ""
        );

        if ($response != null) {
            $tresponse = $response->getTransactionResponse();
            if (($tresponse != null) && ($tresponse->getResponseCode() == \SampleCode\Constants::RESPONSE_OK)) {
                $autReturn['result'] = "Ok";
                $autReturn['authCode'] = $tresponse->getAuthCode();
                $autReturn['transId'] = $tresponse->getTransId();
            } else {
                $error = '';
                foreach ($response->getMessages()->getMessage() as $message) {
                    $error = $error . $message->getCode() . '-' . $message->getText() . ' // ';
                }
                $autReturn['result'] = "Fail";
                $autReturn['error'] = $error;
            }
        } else {
            $autReturn['result'] = "Fail";
            $autReturn['error'] = "ERROR: SERVER NOT RESPONDING.";
        }

        return $autReturn;
    }

    function deleteCustomerPaymentProfile($customerProfileId, $customerpaymentprofileid) {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);

        // Use an existing payment profile ID for this Merchant name and Transaction key

        $request = new AnetAPI\DeleteCustomerPaymentProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setCustomerProfileId($customerProfileId);
        $request->setCustomerPaymentProfileId($customerpaymentprofileid);
        $controller = new AnetController\DeleteCustomerPaymentProfileController($request);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);

        $autReturn = array(
            "result" => "",
            "error" => ""
        );

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $autReturn['result'] = "Ok";
        } else {
            $autReturn['result'] = "Fail";
            $errorMessages = $response->getMessages()->getMessage();
            $autReturn['error'] = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
        }

        return $autReturn;
    }

    function deleteCustomerProfile($customerProfileId) {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);
        $refId = 'ref' . time();

        // Delete an existing customer profile  
        $request = new AnetAPI\DeleteCustomerProfileRequest();
        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setCustomerProfileId($customerProfileId);

        $controller = new AnetController\DeleteCustomerProfileController($request);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);

        $autReturn = array(
            "result" => "",
            "error" => ""
        );

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $autReturn['result'] = "Ok";
        } else {
            $autReturn['result'] = "Fail";
            $errorMessages = $response->getMessages()->getMessage();
            $autReturn['error'] = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
        }

        return $autReturn;
    }

    function validateCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId, $validationMode) {
        // Common setup for API credentials
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(\SampleCode\Constants::MERCHANT_LOGIN_ID);
        $merchantAuthentication->setTransactionKey(\SampleCode\Constants::MERCHANT_TRANSACTION_KEY);

        $request = new AnetAPI\ValidateCustomerPaymentProfileRequest();

        $request->setMerchantAuthentication($merchantAuthentication);
        $request->setCustomerProfileId($customerProfileId);
        $request->setCustomerPaymentProfileId($customerPaymentProfileId);
        $request->setValidationMode($validationMode);

        $controller = new AnetController\ValidateCustomerPaymentProfileController($request);
        $response = $controller->executeWithApiResponse(\SampleCode\Constants::ENVIRONMENT);

        $autReturn = array(
            "result" => "",
            "error" => ""
        );

        if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
            $validationMessages = $response->getMessages()->getMessage();
            $autReturn['result'] = "Ok";
            //$autReturn['validationMessage'] = $validationMessages[0]->getCode() . "  " . $validationMessages[0]->getText();
            $autReturn['validationMessage'] = $validationMessages[0]->getText();
        } else {
            $errorMessages = $response->getMessages()->getMessage();
            $autReturn['result'] = "Fail";
            //$autReturn['error'] = $errorMessages[0]->getCode() . "  " . $errorMessages[0]->getText();
            $autReturn['error'] = $errorMessages[0]->getText();
        }
        return $autReturn;
    }

}

?>
