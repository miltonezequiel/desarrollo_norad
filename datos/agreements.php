<?php

@include_once( 'init.php' );
require_once( ROOT_DIR . "/conf/motor.php" );
require_once( ROOT_DIR . "/entidades/agreement.php" );

class DataAgreements {

	public function getCurrentAgreement( $idCompany ) {
		$tabla             = "_companies_agreements";
		$tabla2            = "_agreements";
		$bd                = new conexion();
		$row               = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla AS T1 INNER JOIN " . DB_PREFIJO . "$tabla2 AS T2 ON T1.agreement_id = T2.agreement_id WHERE company_id = $idCompany ORDER BY date DESC" );
		$oCurrentAgreement = null;
		if ( $row[0] ) {
			$oCurrentAgreement = $this->generateAgreement( $row[0] );
		}

		return $oCurrentAgreement;
	}

	public function setCurrentAgreement( $idCompany, $idAgreement ) {
		$tabla = "_companies_agreements";
		$bd    = new conexion();
		$bd->query( "INSERT INTO " . DB_PREFIJO . "$tabla (company_id, agreement_id, date) VALUES ($idCompany, $idAgreement, NOW())" );

		return true;
	}

	public function getAllAgreements() {
		$tabla = "_agreements";
		$bd    = new conexion();
		$row   = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla" );

		return $row;
	}

	public function getAgreementById( $idAgreement ) {
		$tabla = "_agreements";
		$bd    = new conexion();
		$row   = $bd->query( "SELECT * FROM ax_agreements where agreement_id = " . $idAgreement );
		if ( $row[0] ) {
			$oClient = $this->generateAgreement( $row[0] );
		}

		return $oClient;
	}

	private function generateAgreement( $row ) {
		$idAgreement       = $row->agreement_id;
		$agreement_content = $row->agreement_content;

		$oAgreement = new Agreement( $idAgreement, $agreement_content );

		return $oAgreement;
	}

	public function addAgreement( Agreement $oAgreement ) {
		$tabla = "_agreements";
		$bd    = new conexion();
		$bd->query( "INSERT INTO " . DB_PREFIJO . "$tabla (agreement_content)
                            VALUES('{$oAgreement->getContent()}');" );

		return $bd->insert_id();
	}

	public function getAllCompanyAgreements( $idCompany ) {
		$tabla  = "_companies_agreements";
		$tabla2 = "_agreements";
		$bd     = new conexion();
		$rows   = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla AS T1 INNER JOIN " . DB_PREFIJO . "$tabla2 AS T2 ON T1.agreement_id = T2.agreement_id WHERE company_id = $idCompany ORDER BY date DESC" );

		/*
		$results = array();
		foreach ($rows as $row) {
			$oAgreement = $this->generateAgreement($row);
			array_push($results, $oAgreement);
		}
		return $results;
		*/

		return $rows;
	}

	public function setCurrentAgreementFromDefault( $idCompany ) {
		$tabla      = "_companies_agreements";
		$tabla2     = "_agreements";
		$bd         = new conexion();
		$row        = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla2 WHERE agreement_default = 1 LIMIT 1" );
		$oAgreement = new Agreement();
		if ( $row[0] ) {
			$oAgreement = $this->generateAgreement( $row[0] );
		}
		$bd->query( "INSERT INTO " . DB_PREFIJO . "$tabla (company_id, agreement_id, date) VALUES ($idCompany, " . $oAgreement->getId() . ", NOW())" );

		return true;
	}

	public function isCurrentAgreementAccepted( $idCompany ) {
		$db  = new conexion();
		$row = $db->query( "select * from ax_companies_agreements as ca where ca.company_id = $idCompany order by date desc limit 1" );

		return ( $row[0]->agreement_accepted ) ? true : false;
	}


}

?>
