<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/payment.php");

class DataPayments {

    public function getPaymentById($id) {
        $table = "_payments";
        $bd = new conexion();
        $rows = $bd->query("SELECT ax_payments.*, ax_payment_types.payment_type_name as payment_type_str
                            FROM " . DB_PREFIJO . "$table 
                            INNER JOIN ax_payment_types on ax_payment_types.payment_type_id = ax_payments.payment_type_id    
                            WHERE 
                            payment_id = " . $id . ";");

        if ($rows[0] != null) {
            $oPayment = $this->generatePayment($rows[0]);
        }
        return $oPayment;
    }
    
    public function updatePaymentAsPayed($id) {
        $table = "_payments";
        $bd = new conexion();
        $rows = $bd->query("UPDATE ax_payments SET payment_is_payed = 1
                            WHERE 
                            payment_id = " . $id . ";");
    }

    public function getPaymentDetailByPaymentId($paymentId) {
        $bd = new conexion();
        $row = $bd->query("
                            SELECT *
                            FROM ax_payment_details
                            WHERE payment_id = " . $paymentId);

        return $row;
    }

    public function getPendingStatementsCompanies() {
        $bd = new conexion();
        $row = $bd->query("
                            SELECT company_id, company_name
                            FROM ax_payments
                            INNER JOIN ax_tests on ax_tests.payment_id = ax_payments.payment_id
                            INNER JOIN ax_devices_companies on ax_tests.test_di_dc_id = ax_devices_companies.dc_id
                            INNER JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
                            WHERE payment_type_id = 3 and payment_is_payed = 0
                            GROUP BY company_id
                        ");

        return $row;
    }

    public function getPendingStatements($company_id) {
        $bd = new conexion();
        $row = $bd->query("
                            SELECT ax_payments.payment_id, payment_date, ax_tests.test_id, test_address, test_address_2, test_city, company_id, company_name
                            FROM axyoma_norad_web.ax_payments
                            INNER JOIN ax_tests on ax_tests.payment_id = ax_payments.payment_id
                            INNER JOIN ax_devices_companies on ax_tests.test_di_dc_id = ax_devices_companies.dc_id
                            INNER JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
                            WHERE payment_type_id = 3 and payment_is_payed = 0 AND company_id = " . $company_id
        );

        return $row;
    }

    public function savePayment(Payment $oPayment) {
        $tabla = "_payments";
        $bd = new conexion();

        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (payment_date, payment_total, payment_type_id, payment_is_payed, payment_number)
                VALUES('" . $oPayment->getPaymentDate() . "',
                    " . $oPayment->getPaymentTotal() . ",
                    " . $oPayment->getPaymentType() . ",
                    " . $oPayment->getPaymentIsPayed() . ",
                    (SELECT max(coalesce(payment_number, 0)) + 1 FROM ax_payments pay where pay.payment_type_id = " . $oPayment->getPaymentType() . " ));");
        return $bd->insert_id();
    }

    private function generatePayment($row) {
        $payment_id = $row->payment_id;
        $payment_date = $row->payment_date;
        $payment_total = $row->payment_total;
        $payment_type_id = $row->payment_type_id;
        $payment_type_str = $row->payment_type_str;
        $test_id = $row->test_id;
        $payment_is_payed = $row->payment_is_payed;

        $oPayment = new Payment($payment_id, $payment_date, $payment_total, $payment_type_id, $payment_type_str, $test_id, $payment_is_payed);
        $oPayment->setPaymentNumber($row->payment_number);
        
        
        return $oPayment;        
    }
}

?>
