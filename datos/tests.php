<?php

@include_once( 'init.php' );
require_once( ROOT_DIR . "/conf/motor.php" );
require_once( ROOT_DIR . "/entidades/test.php" );
require_once( ROOT_DIR . "/entidades/hourly_result.php" );

class DataTests {
	public function setTest( Test $oTest ) {
		$bd = new conexion();

		$query = 'INSERT INTO ax_tests(
			test_address,
			test_address_2,
			test_city,
			test_state,
			test_zip,
            test_floor_level,
            test_room,
            test_comments_begin,
            test_comments_end,
            test_report_date,
            test_image_1,
            test_image_2,
            test_image_3,
            test_image_description_1,
            test_image_description_2,
            test_image_description_3,
            client_id, 
            test_auth_code, 
            test_transaction_id, 
            test_di_dc_id,
            receipt_id, 
            payment_id
          ) VALUES (
            ?, ?, ?, ?, ?,
            ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?, 
            ?, ?, ?, ?, ?, 
             ( SELECT id from (
                    SELECT coalesce(MAX(receipt_id), 0) +1 as id FROM ax_tests) t ), 
            ?);';

		if ( $stmt = $bd->conn->prepare( $query ) ) {

			$stmt->bind_param( "ssssssssssssssssisiii",
				$oTest->getaddress(),
				$oTest->getaddress2(),
				$oTest->getcity(),
				$oTest->getState(),
				$oTest->getZip(),
				$oTest->getFloorLevel(),
				$oTest->getRoom(),
				$oTest->getCommentsBegin(),
				$oTest->getCommentsEnd(),
				$oTest->getReportDate(),
				$oTest->getImage1(),
				$oTest->getImage2(),
				$oTest->getImage3(),
				$oTest->getImageDescription1(),
				$oTest->getImageDescription2(),
				$oTest->getImageDescription3(),
				$oTest->getIdClient(),
				$oTest->getAuthCode(),
				$oTest->getTransId(),
				$oTest->getTestDiDcId(),
				$oTest->getPaymentId()
			);
			$stmt->execute();
		}
		if ( $bd->conn->errno ) {
			$error = $bd->conn->errno . ' ' . $bd->conn->error;
			echo $error; // 1054 Unknown column 'foo' in 'field list'
			die();
		}

		return $bd->insert_id();
	}

	public function getTests() {
		$tabla  = "_tests";
		$bd     = new conexion();
		$row    = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla" );
		$index  = 0;
		$vTests = array();
		foreach ( $row as $registro ) {
			$oTest            = $this->generaTest( $registro );
			$vTests[ $index ] = $oTest;
			$index            = $index + 1;
		}

		return $vTests;
	}

	private function generaTest( $row ) {
		$id                = $row->test_id;
		$address           = $row->test_address;
		$address2          = $row->test_address_2;
		$city              = $row->test_city;
		$state             = $row->test_state;
		$zip               = $row->test_zip;
		$floorLevel        = $row->test_floor_level;
		$room              = $row->test_room;
		$commentsBegin     = $row->test_comments_begin;
		$commentsEnd       = $row->test_comments_end;
		$reportDate        = $row->test_report_date;
		$test_di_dc_id     = $row->test_di_dc_id;
		$idClient          = $row->client_id;
		$file              = $row->test_file;
		$image1            = $row->test_image_1;
		$image2            = $row->test_image_2;
		$image3            = $row->test_image_3;
		$imageDescription1 = $row->test_image_description_1;
		$imageDescription2 = $row->test_image_description_2;
		$imageDescription3 = $row->test_image_description_3;
		$authCode          = $row->test_auth_code;
		$transId           = $row->test_transaction_id;

		$oTest = new Test( $id, $address, $address2, $city, $state, $zip, $floorLevel, $room, $commentsBegin, $commentsEnd, $reportDate, $test_di_dc_id, $idClient, $authCode, $transId );
		$oTest->setFile( $file );
		$oTest->setImage1( $image1 );
		$oTest->setImage2( $image2 );
		$oTest->setImage3( $image3 );
		$oTest->setImageDescription1( $imageDescription1 );
		$oTest->setImageDescription2( $imageDescription2 );
		$oTest->setImageDescription3( $imageDescription3 );

		$oTest->setReceiptId( $row->receipt_id );

		if ( isset( $row->inspector_id ) ) {
			$oTest->setIdInspector( $row->inspector_id );
		}

		if ( isset( $row->device_id ) ) {
			$oTest->setIdDevice( $row->device_id );
		}

		if ( isset( $row->device_serial_number ) ) {
			$oTest->setDeviceSerialnumber( $row->device_serial_number );
		}

		return $oTest;
	}

	public function getTestsByIdCompany( $idCompany ) {
		$tabla  = "_tests";
		$bd     = new conexion();
		$row    = $bd->query( "
                            SELECT t.*
                            FROM " . DB_PREFIJO . "$tabla t
                            INNER JOIN ax_devices_companies on t.test_di_dc_id = ax_devices_companies.dc_id
                            INNER JOIN ax_devices_inspectors on ax_devices_inspectors.di_dc_id = ax_devices_companies.dc_id
                            INNER JOIN ax_inspectors i ON i.`inspector_id` = ax_devices_inspectors.di_inspector_id
                            WHERE i.`company_id`={$idCompany}
                            ORDER BY t.test_id DESC" );
		$index  = 0;
		$vTests = array();
		if ( $row[0] ) {
			foreach ( $row as $registro ) {
				$oTest            = $this->generaTest( $registro );
				$vTests[ $index ] = $oTest;
				$index            = $index + 1;
			}
		}

		return $vTests;
	}

	public function getTestsByIdCompanyPag(
		$idCompany,
		$offset,
		$limit
	) {
		$tabla  = "_tests";
		$bd     = new conexion();
		$row    = $bd->query( "SELECT t.*, i.`inspector_id`, ax_devices.device_id as device_id, ax_devices.device_serial_number as device_serial_number
                            FROM " . DB_PREFIJO . "$tabla t
                            INNER JOIN
                                ax_devices_inspectors ON t.test_di_dc_id = ax_devices_inspectors.di_dc_id
                            INNER JOIN
                                ax_inspectors i ON i.inspector_id = ax_devices_inspectors.di_inspector_id
                            INNER JOIN
                                ax_devices_companies ON ax_devices_inspectors.di_dc_id = ax_devices_companies.dc_id
                            INNER JOIN
                                ax_devices ON ax_devices.device_id = ax_devices_companies.dc_device_id
                            WHERE i.`company_id`={$idCompany}
                            GROUP BY 1
                            ORDER BY t.test_id DESC
                            LIMIT {$offset},{$limit}" );
		$index  = 0;
		$vTests = array();
		if ( $row[0] ) {
			foreach ( $row as $registro ) {
				$oTest            = $this->generaTest( $registro );
				$vTests[ $index ] = $oTest;
				$index            = $index + 1;
			}
		}

		return $vTests;
	}

	public function getTestById( $idTest ) {
		$tabla = "_tests";
		$bd    = new conexion();
		$row   = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla where test_id=" . $idTest );
		if ( $row[0] ) {
			$oTest = $this->generaTest( $row[0] );
		}

		return $oTest;
	}

	public function getTestByPaymentId( $idPayment ) {
		$tabla = "_tests";
		$bd    = new conexion();
		$row   = $bd->query( "SELECT * FROM " . DB_PREFIJO . "$tabla where payment_id=" . $idPayment );
		if ( $row[0] ) {
			$oTest = $this->generaTest( $row[0] );
		}

		return $oTest;
	}

	public function updateTestFile( Test $oTest ) {
		$bd = new conexion();

		$query = "UPDATE ax_tests SET test_file=? WHERE test_id=?;";
		if ( $stmt = $bd->conn->prepare( $query ) ) {
			$stmt->bind_param( "si",
				$oTest->getFile(), $oTest->getId() );
			$stmt->execute();
		} else {
			$error = $bd->conn->errno . ' ' . $bd->conn->error;
			echo $error; // 1054 Unknown column 'foo' in 'field list'
		}
	}

	public function updateTestPaymentId( Test $oTest ) {
		$tabla = "_tests";
		$bd    = new conexion();
		$bd->query( "UPDATE " . DB_PREFIJO . "$tabla SET payment_id='{$oTest->getPaymentId()}' WHERE test_id={$oTest->getId()};" );
	}

}

?>