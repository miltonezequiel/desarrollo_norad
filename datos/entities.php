<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/entity.php");

class DataEntities {

    public function getEntities() {
        $entities = json_decode(file_get_contents(ROOT_DIR . "/entities.json"));

        $entitiesArray = array();
        foreach ($entities->entities as $entity) {
            $oEntity = $this->generaEntity($entity);
            array_push($entitiesArray, $oEntity);
        }
        return $entitiesArray;
    }

    private function generaEntity($row) {
        $nameEntity = $row->name;

        $oEntity = new Entity();
        $oEntity->setName($nameEntity);
        return $oEntity;
    }
    
}

?>
