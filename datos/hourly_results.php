<?php

@include_once ('init.php');
require_once (ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/hourly_result.php");

class DataHourlyResults {

    public function setHourlyResults($vHourlyResults) {
        $tabla = "_hourly_results";
        $bd = new conexion();
        foreach ($vHourlyResults as $oHourlyResult) {
            $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (test_id,result_date_time,result_alpha,result_radon,
            result_temperature,result_humidity,result_pressure,result_error,result_tilt,result_power_outage)
                VALUES('" . $oHourlyResult->getIdTest() . "','" . $oHourlyResult->getDateTime() . "',
                       '" . $oHourlyResult->getAlpha() . "',
                       '" . $oHourlyResult->getRadon() . "','" . $oHourlyResult->getTemperature() . "',
                       '" . $oHourlyResult->getHumidity() . "','" . $oHourlyResult->getPressure() . "',
                       '" . $oHourlyResult->getError() . "',
                       '" . $oHourlyResult->getTilt() . "','" . $oHourlyResult->getPowerOutage() . "')");
        }
    }

    public function getHourlyResultsByIdTest($idTest) {
        $tabla = "_hourly_results";
        $bd = new conexion();
        $rows = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla WHERE test_id=" . $idTest);
        $index = 0;
        $vHourlyResults = array();
        if ($rows[0] != null) {
            foreach ($rows as $registro) {
                $oHourlyResult = $this->generaHourlyResult($registro);
                $vHourlyResults[$index] = $oHourlyResult;
                $index = $index + 1;
            }
        }
        return $vHourlyResults;
    }
    
    ///////////////////////////////////////////////////////////////// 
    
    public function setTemplyResults($vHourlyResults) {
        $tabla = "_temply_results";
        $bd = new conexion();
        foreach ($vHourlyResults as $oHourlyResult) {
            $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (test_id,result_date_time,result_alpha,result_radon,
            result_temperature,result_humidity,result_pressure,result_error,result_tilt,result_power_outage)
                VALUES('" . $oHourlyResult->getIdTest() . "','" . $oHourlyResult->getDateTime() . "',
                       '" . $oHourlyResult->getAlpha() . "',
                       '" . $oHourlyResult->getRadon() . "','" . $oHourlyResult->getTemperature() . "',
                       '" . $oHourlyResult->getHumidity() . "','" . $oHourlyResult->getPressure() . "',
                       '" . $oHourlyResult->getError() . "',
                       '" . $oHourlyResult->getTilt() . "','" . $oHourlyResult->getPowerOutage() . "')");
        }
    }

    public function getTemplyResultsByIdTest($idTest) {
        $tabla = "_temply_results";
        $bd = new conexion();
        $rows = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla WHERE test_id=" . $idTest);
        $index = 0;
        $vHourlyResults = array();
        if ($rows[0] != null) {
            foreach ($rows as $registro) {
                $oHourlyResult = $this->generaHourlyResult($registro);
                $vHourlyResults[$index] = $oHourlyResult;
                $index = $index + 1;
            }
        }
        return $vHourlyResults;
    }
    
    public function eraseTemplyResultsByIdTest($idTest) {
        $tabla = "_temply_results";
        $bd = new conexion();
        $rows = $bd->query("DELETE FROM " . DB_PREFIJO . "$tabla WHERE test_id=" . $idTest);
    }
    
    /////////////////////////////////////////////////////////////////
    
    private function generaHourlyResult($row) {
        $idTest = $row->test_id;
        $dateTime = $row->result_date_time;
        $alpha = $row->result_alpha;
        $radon = $row->result_radon;
        $temperature = $row->result_temperature;
        $humidity = $row->result_humidity;
        $pressure = $row->result_pressure;
        $error = $row->result_error;
        $tilt = $row->result_tilt;
        $powerOutage = $row->result_power_outage;

        $oHourlyResult = new HourlyResult();
        $oHourlyResult->setIdTest($idTest);
        $oHourlyResult->setDateTime($dateTime);
        $oHourlyResult->setAlpha($alpha);
        $oHourlyResult->setRadon($radon);
        $oHourlyResult->setTemperature($temperature);
        $oHourlyResult->setHumidity($humidity);
        $oHourlyResult->setPressure($pressure);
        $oHourlyResult->setError($error);
        $oHourlyResult->setTilt($tilt);
        $oHourlyResult->setPowerOutage($powerOutage);

        return $oHourlyResult;
    }
    
}

?>
