<?php

@include_once( 'init.php' );
require_once( ROOT_DIR . "/conf/motor.php" );
require_once( ROOT_DIR . "/entidades/client.php" );

class DataClients {
	public function setClient( Client $oClient ) {
		$bd = new conexion();

		$query = 'INSERT INTO ax_clients (
					client_name,
					client_email,
					client_email_2,
					second_nameClient,
					second_emailClient,
					second_email2Client,
					nameAgent,
					emailAgent,
					email2Agent,
					second_nameAgent,
					second_emailAgent,
					second_email2Agent
				  ) VALUES(
				  	?, ?, ?, ?, ?, 
				  	?, ?, ?, ?, ?, 
				  	?, ?
				  )';
		if ( $stmt = $bd->conn->prepare( $query ) ) {
			$stmt->bind_param( "ssssssssssss",
				  $oClient->getName(),
				$oClient->getEmail(),
				$oClient->getEmail2(),
				$oClient->getSecondName(),
				$oClient->getSecondEmail(),
				$oClient->getSecondEmail2(),
				$oClient->getNameAgent(),
				$oClient->getAgentEmail(),
				$oClient->getAgentEmail2(),
				$oClient->getSecondAgentName(),
				$oClient->getSecondAgentEmail(),
				$oClient->getSecondAgentEmail2()
			);
			$stmt->execute();
		} else {
			$error = $bd->conn->errno . ' ' .$bd->conn->error;
			echo $error; // 1054 Unknown column 'foo' in 'field list'
		}
        return $bd->insert_id();
    }

    public function getClientById($idClient) {
        $tabla = "_clients";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where client_id = " . $idClient);
        if ($row[0]) {
            $oClient = $this->generaClient($row[0]);
        }
        return $oClient;
    }

    private function generaClient($row) {
        $idClient = $row->client_id;
        $nameClient = $row->client_name;
        $emailClient = $row->client_email;
        $email2Client = $row->client_email_2;
        
        $second_nameClient = $row->second_nameClient; 
        $second_emailClient = $row->second_emailClient;
        $second_email2Client = $row->second_email2Client;
        
        $nameAgent = $row->nameAgent;
        $emailAgent = $row->emailAgent;
        $email2Agent = $row->email2Agent;
        
        $second_nameAgent = $row->second_nameAgent;
        $second_emailAgent = $row->second_emailAgent;
        $second_email2Agent = $row->second_email2Agent;
        
        $oClient = new Client($idClient, $nameClient, $emailClient, $email2Client, $second_nameClient, $second_emailClient, $second_email2Client, $nameAgent, $emailAgent, $email2Agent, $second_nameAgent, $second_emailAgent, $second_email2Agent);
        return $oClient;
    }
    
}

?>
