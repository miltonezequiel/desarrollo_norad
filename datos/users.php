<?php

@include_once ('init.php');
require_once (ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/user.php");

class DataUsers {

    public function getUserById($idUser) {
        $tabla = "_users";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where user_id=" . $idUser);
        if ($row[0]) {
            $oUser = $this->generaUser($row[0]);
        }
        return $oUser;
    }

    public function getUserByUsername($username) {
        $tabla = "_users";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where user_username='" . $username . "'");
        if ($row[0]) {
            $oUser = $this->generaUser($row[0]);
        }
        return $oUser;
    }

    public function getEmail($type, $username) {
        $tabla = "_users";
        $bd = new conexion();

        $query = "SELECT * FROM " . DB_PREFIJO . "$tabla ";

        if ($type == 'company') {
            $query = $query . "INNER JOIN " . DB_PREFIJO . "_companies on " . DB_PREFIJO . "_companies.company_user_id = " . DB_PREFIJO . "_users.user_id where user_username = '" . $username . "'";
            $row = $bd->query($query);

            if ($row[0]) {
                return $row[0]->company_email;
            }
        } else { //inspector
            $query = $query . "INNER JOIN " . DB_PREFIJO . "_inspectors on " . DB_PREFIJO . "_inspectors.inspector_user_id = " . DB_PREFIJO . "_users.user_id where user_username = '" . $username . "'";
            $row = $bd->query($query);

            if ($row[0]) {
                return $row[0]->inspector_email;
            }
        }
    }

    public function getUsername($type, $email) {
        $tabla = "_users";
        $bd = new conexion();

        $query = "SELECT * FROM " . DB_PREFIJO . "$tabla ";

        if ($type == 'company') {
            $query = $query . "INNER JOIN " . DB_PREFIJO . "_companies on " . DB_PREFIJO . "_companies.company_user_id = " . DB_PREFIJO . "_users.user_id where company_email = '" . $email . "'";
        } else { //inspector
            $query = $query . "INNER JOIN " . DB_PREFIJO . "_inspectors on " . DB_PREFIJO . "_inspectors.inspector_user_id = " . DB_PREFIJO . "_users.user_id where inspector_email = '" . $email . "'";
        }

        $row = $bd->query($query);

        if ($row[0]) {
            $oUser = $this->generaUser($row[0]);
        }
        return $oUser;
    }

    public function getUserPassword($type, $email, $username) {
        $tabla = "_users";
        $bd = new conexion();

        $query = "SELECT * FROM " . DB_PREFIJO . "$tabla ";

        if ($type == 'company') {
            $query = $query . "INNER JOIN " . DB_PREFIJO . "_companies on " . DB_PREFIJO . "_companies.company_user_id = " . DB_PREFIJO . "_users.user_id where company_email = '" . $email . "' and user_username = '" . $username . "'";
        } else { //inspector
            $query = $query . "INNER JOIN " . DB_PREFIJO . "_inspectors on " . DB_PREFIJO . "_inspectors.inspector_user_id = " . DB_PREFIJO . "_users.user_id where inspector_email = '" . $email . "' and user_username = '" . $username . "'";
        }

        $row = $bd->query($query);

        if ($row[0]) {
            $oUser = $this->generaUser($row[0]);
        }
        return $oUser;
    }

    public function getUserByKey($md5Key) {
        $tabla = "_pass_forgotten_request";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla INNER JOIN " . DB_PREFIJO . "_users on " . DB_PREFIJO . "_users.user_id = " . DB_PREFIJO . "_pass_forgotten_request.user_id where now() <= DATE_ADD(ax_time,INTERVAL 1 DAY) and ax_key='" . $md5Key . "'");
        if ($row[0]) {
            $oUser = $this->generaUser($row[0]);
        }
        return $oUser;
    }

    public function addUser(User $oUser) {
        $tabla = "_users";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (user_username,user_password)
                    VALUES ('{$oUser->getUsername()}','{$oUser->getPassword()}')");
        return $bd->insert_id();
    }

    public function saveKeyPassRecover($userId, $md5Key) {
        $tabla = "_pass_forgotten_request";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (user_id,ax_time, ax_key)
                    VALUES ('{$userId}', NOW() ,'{$md5Key}')");
	    return $bd->insert_id();
    }

    public function editPasswordUser(User $oUser) {
        $tabla = "_users";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET user_password='" . $oUser->getPassword() . "' where user_id= " . $oUser->getId());
	    return $bd->insert_id();
    }

    public function editUserLogged(User $oUser) {
        $tabla = "_users";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET `user_logged`='{$oUser->getLogged()}
                   ' WHERE `user_id`={$oUser->getId()}");
	    return $bd->insert_id();
    }

    public function editUserDevice(User $oUser) {
        $tabla = "_users";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET `user_logged`='{$oUser->getLogged()}
                   ',device_id='{$oUser->getDeviceID()}
                   ',device_sn='{$oUser->getDeviceSN()}
                   ',device_test_id='{$oUser->getDTestID()}
                   ' WHERE `user_id`={$oUser->getId()}");
	    return $bd->insert_id();
    }
    
    public function editUserUsername(User $oUser) {
        $tabla = "_users";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET user_username='" . $oUser->getUsername() . "' WHERE user_id={$oUser->getId()}");
	    return $bd->insert_id();
    }

    public function getUserByDTestID($devicetid) {
        $tabla = "_users";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla WHERE `device_test_id`=" . $devicetid);
        if ($row[0]) {
            $oUser = $this->generaUser($row[0]);
        }
        return $oUser;
    }

    private function generaUser($row) {
        $id = $row->user_id;
        $username = $row->user_username;
        $password = $row->user_password;

        $logged = $row->user_logged;
        $deviceid = $row->device_id;
        $devicesn = $row->device_sn;
        $devicetid = $row->device_test_id;

        $oUser = new User($id, $username, $password, $logged, $deviceid, $devicesn, $devicetid);
        return $oUser;
    }
    
}

?>
