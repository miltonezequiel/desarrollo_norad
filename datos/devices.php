<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/device.php");

class DataDevices {

    public function getDeviceById($idDevice, $dateError) {
        $tablaDevices = "_devices";
        $tablaDevicesCalibration = "_devices_calibration";
        $bd = new conexion();
        $calibration = $bd->query("SELECT cal.`calibration_device_id`,cal.`calibration_constant`
                            FROM " . DB_PREFIJO . "$tablaDevicesCalibration cal WHERE 
                            cal.`calibration_device_id`=" . $idDevice . " AND
                            DATE(cal.`calibration_date_time`)<='" . $dateError . "'
                            ORDER BY cal.`calibration_date_time` DESC
                            LIMIT 1;");
        if ($calibration[0]->calibration_constant != NULL) {
            $error = $calibration[0]->calibration_constant;
        } else {
            $error = 0;
        }
        $rows = $bd->query("SELECT dev.`device_id`,dev.`device_serial_number` FROM " . DB_PREFIJO . "$tablaDevices dev
                            WHERE dev.`device_id`=" . $idDevice . ";");
        if ($rows[0] != null) {
            $oDevice = $this->generaDevice($rows[0], $error);
        }
        return $oDevice;
    }
    
    public function getDeviceBySerialNumber($serialNumberDevice) {
        $tablaDevices = "_devices";
        $bd = new conexion();
        $rows = $bd->query("SELECT dev.`device_id`,dev.`device_serial_number` FROM " . DB_PREFIJO . "$tablaDevices dev
                            WHERE dev.`device_serial_number`=" . $serialNumberDevice . ";");
        if ($rows[0] != null) {
            $oDevice = $this->generaDevice($rows[0], $error);
        }
        return $oDevice;
    }

    public function getDevices() {
        $tabla = "_devices";
        $bd = new conexion();
        $row = $bd->query("SELECT d.*
                            FROM " . DB_PREFIJO . "$tabla d
                            ORDER BY d.device_id DESC");
        $index = 0;
        $vDevices = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oDevice = $this->generaDevice($registro);
                $vDevices[$index] = $oDevice;
                $index = $index + 1;
            }
        }
        return $vDevices;
    }

    public function getDevicesPag($offset, $limit, $orderBy, $dir, $filter) {
        $tabla = "_devices";
        $bd = new conexion();
//        $row = $bd->query("SELECT d.*
//                            FROM " . DB_PREFIJO . "$tabla d
//                            LIMIT {$offset},{$limit}");
        
        if($orderBy == "") {
            $orderBy = "device_id";
            $dir = "asc";
        }

        $row = $bd->query("SELECT device_id, 'NORAD', company_name, (
	select max(calibration_date_time)
    from ax_devices_calibration
    where ax_devices_calibration.calibration_device_id = device_id
) calibration_date_time, ax_devices.device_serial_number
                            FROM ax_devices
                            LEFT JOIN ax_devices_companies on ax_devices_companies.dc_device_id = ax_devices.device_id AND (ax_devices_companies.dc_end_date IS NULL OR ax_devices_companies.dc_end_date > CURRENT_DATE)
                            LEFT JOIN ax_companies on ax_companies.company_id = ax_devices_companies.dc_company_id
                            " . $filter . " 
                            ORDER BY " . $orderBy . " " . $dir . "
                            LIMIT {$offset},{$limit}");

        $index = 0;
        $vDevices = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oDevice = $this->generaDevice($registro);
                $vDevices[$index] = $oDevice;
                $index = $index + 1;
            }
        }
        return $vDevices;
    }

    public function addDevice(Device $oDevice) {
        $tabla = "_devices";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (device_serial_number)
                            VALUES('{$oDevice->getSerialNumber()}');");
        return $bd->insert_id();
    }

    public function addCalibration(Device $oDevice, $date) {
        if (is_null($date)) {
            $date = date("Y-m-d H:i:s"); //current date
        }
        $tabla = "_devices_calibration";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (calibration_device_id,calibration_date_time,calibration_constant)
                    VALUES ({$oDevice->getId()},'{$date}','{$oDevice->getError()}')");
    }

    public function getCalibrationDate(Device $oDevice) {
        $tabla = "_devices_calibration";
        $bd = new conexion();
        $row = $bd->query("SELECT dc.`calibration_date_time`
                    FROM " . DB_PREFIJO . "$tabla dc
                    WHERE dc.`calibration_device_id`={$oDevice->getId()}
                    ORDER BY dc.`calibration_date_time` DESC
                    LIMIT 1;");
        $calibrationDate = NULL;
        if ($row[0]) {
            $calibrationDate = $row[0]->calibration_date_time;
        }
        return $calibrationDate;
    }

    private function generaDevice($row, $error = NULL) {
        $idDevice = $row->device_id;
        $nameDevice = "Norad";
        $serialNumber = $row->device_serial_number;
        $oDevice = new Device($idDevice, $nameDevice, $error, $serialNumber);
        return $oDevice;
    }
    
    public function getCountTestDevice($deviceId) {
        $bd = new conexion();
        $row = $bd->query("SELECT COUNT(t.`test_id`) AS count 
                           FROM ax_tests t
                           INNER JOIN ax_devices_companies on dc_id = t.test_di_dc_id
                           WHERE `dc_device_id`={$deviceId}");
        $countTests = 0;
        if ($row[0]) {
            $countTests = $row[0]->count;
        }
        return $countTests;
    }
    
}

?>
