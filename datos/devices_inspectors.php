<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/device_inspector.php");

class DataDevicesInspectors {

    public function getDevicesByIdInspector($idInspector) {
        $tablaDevices = "_devices";
        $tablaDevicesInspectors = "_devices_inspectors";
        $bd = new conexion();
        $rows = $bd->query("SELECT devins.* FROM 
                            " . DB_PREFIJO . "$tablaDevicesInspectors devins
                            WHERE devins.`di_inspector_id`=" . $idInspector . " AND devins.`device_start_date`<=CURRENT_DATE
                            AND (devins.`device_end_date`>CURRENT_DATE OR devins.`device_end_date` IS NULL)");
        $index = 0;
        $vDevicesInspectors = array();
        if ($rows[0] != null) {
            foreach ($rows as $registro) {
                $oDeviceInspector = $this->generaDeviceInspector($registro);
                $vDevicesInspectors[$index] = $oDeviceInspector;
                $index = $index + 1;
            }
        }
        return $vDevicesInspectors;
    }

    public function editEndDateDeviceInspector($idDevice, $idInspector) {
        $tabla = "_devices_inspectors";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla
                    SET device_end_date= curdate()
                    WHERE `di_dc_id`={$idDevice} and di_inspector_id={$idInspector}");
    }
    
    public function editEndDateDeviceInspectorFromAdmin($idDevice, $idCompany) {
        $tabla = "_devices_inspectors";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla
                    SET device_end_date= curdate()
                    WHERE di_dc_id IN " . "(SELECT dc_id FROM ax_devices_companies where dc_device_id = '". $idDevice ."' and dc_company_id = ". $idCompany .")");
    }

    public function getInspectorByIdDeviceIdCompany($idDevice, $idCompany) {
        $tabla = "_devices_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT di.*
                           FROM " . DB_PREFIJO . "$tabla di
                           WHERE di.`di_dc_device_id`={$idDevice}
                           AND di.`di_dc_company_id`={$idCompany}
                           AND (di.`device_end_date` IS NULL OR di.`device_end_date`>CURRENT_DATE)");
        $oDeviceInspector = null;
        if ($row[0]) {
            $oDeviceInspector = $this->generaDeviceInspector($row[0]);
        }
        return $oDeviceInspector;
    }
    
    public function getDevicesInspectorById($id) {
        $tabla = "_devices_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT di.*
                           FROM " . DB_PREFIJO . "$tabla di
                           WHERE di.`di_dc_id`={$id} AND (`device_end_date` IS NULL OR `device_end_date`>CURRENT_DATE)");
        $oDeviceInspector = null;
        if ($row[0]) {
            $oDeviceInspector = $this->generaDeviceInspector($row[0]);
        }
        return $oDeviceInspector;
    }

    public function addDeviceInspector(DeviceInspector $oDeviceInspector) {
        $tabla = "_devices_inspectors";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (di_dc_id,di_inspector_id,device_start_date, di_dc_device_id, di_dc_company_id, di_dc_start_date)
                    VALUES({$oDeviceInspector->getIdDeviceCompany()},
                           {$oDeviceInspector->getIdInspector()},
                           curdate(),
                           (
                                SELECT dc_device_id
                                FROM ax_devices_companies
                                WHERE ax_devices_companies.dc_id = {$oDeviceInspector->getIdDeviceCompany()}
                            ),
                            (
                                SELECT dc_company_id
                                FROM ax_devices_companies
                                WHERE ax_devices_companies.dc_id = {$oDeviceInspector->getIdDeviceCompany()}
                            ),
                            (
                                SELECT dc_start_date
                                FROM ax_devices_companies
                                WHERE ax_devices_companies.dc_id = {$oDeviceInspector->getIdDeviceCompany()}
                            )
                            );");
    }

    public function getDeviceInspectorByDeviceId($idDevice) {
        $tabla = "_devices_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT di.*, dc.dc_start_date FROM " . DB_PREFIJO . "$tabla di
                           INNER JOIN ax_devices_companies dc ON dc.dc_id = di.di_dc_id 
                           WHERE dc.dc_device_id={$idDevice}
                           AND (`device_end_date` IS NULL OR `device_end_date`>CURRENT_DATE)");
                           
        $oDeviceInspector = null;
        if ($row[0]) {
            $oDeviceInspector = $this->generaDeviceInspector($row[0]);
        }
        return $oDeviceInspector;
    }
    
    private function generaDeviceInspector($row) {
        $idInspector = $row->di_inspector_id;
        $endDate = $row->device_end_date;
        $startDateCompany = null;
        if (property_exists(get_class($row), 'dc_start_date')) {
	        $startDateCompany = $row->dc_start_date;
        }
        
        $id = $row->dc_id;
		$idDeviceCompany = $row->di_dc_id;

        $oDeviceInspector = new DeviceInspector($id, $idDeviceCompany, $idInspector, $endDate, $startDateCompany);

        return $oDeviceInspector;
    }
    
}

?>
