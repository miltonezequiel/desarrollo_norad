<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/state.php");

class DataStates {

    public function getStates() {
        $tabla = "_states";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla s 
                           ORDER BY s.state_code ASC");
        $vStates = array();
        foreach ($row as $registro) {
            $oState = $this->generaState($registro);
            array_push($vStates, $oState);
        }
        return $vStates;
    }

    public function getStateById($idState) {
        $tabla = "_states";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where state_code=" . $idState);
        $oState = null;
        if ($row[0]) {
            $oState = $this->generaState($row[0]);
        }
        return $oState;
    }

   
    private function generaState($row) {
        $id = $row->state_code;
        $name= $row->state;

        $oState=new State($id, $name);

        return $oState;
    }
    
}

?>
