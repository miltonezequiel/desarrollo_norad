<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/inspector.php");

class DataInspectors {

    public function getInspectorById($idInspector) {
        $tabla = "_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where inspector_id='{$idInspector}'");
        $oInspector = null;
        if ($row[0]) {
            $oInspector = $this->generaInspector($row[0]);
        }
        return $oInspector;
    }

    public function getInspectorByUser($user) {
        $tabla = "_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where inspector_user_id=" . $user);
        $oInspector = null;
        if ($row[0]) {
            $oInspector = $this->generaInspector($row[0]);
        }
        return $oInspector;
    }

    public function getInspectorsByIdCompany($idCompany) {
        $tabla = "_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT i.*
                            FROM " . DB_PREFIJO . "$tabla i
                            WHERE i.`company_id`={$idCompany}
                            and inspector_status = 1
                            ORDER BY i.inspector_id DESC");
        $index = 0;
        $vInspector = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oInspector = $this->generaInspector($registro);
                $vInspector[$index] = $oInspector;
                $index = $index + 1;
            }
        }
        return $vInspector;
    }

    public function getInspectorsByIdCompanyPag($idCompany, $offset, $limit) {
        $tabla = "_inspectors";
        $bd = new conexion();
        $row = $bd->query("SELECT i.*
                            FROM " . DB_PREFIJO . "$tabla i
                            WHERE i.`company_id`={$idCompany}
                            ORDER BY i.inspector_id DESC
                            LIMIT {$offset},{$limit}");
        $index = 0;
        $vInspector = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oInspector = $this->generaInspector($registro);
                $vInspector[$index] = $oInspector;
                $index = $index + 1;
            }
        }
        return $vInspector;
    }

    public function addInspector(Inspector $oInspector) {
        $tabla = "_inspectors";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (inspector_name,inspector_email,
                    company_id,inspector_user_id,inspector_certification_number,entity_id)
                    VALUES ('{$oInspector->getName()}','{$oInspector->getEmail()}',
                        '{$oInspector->getIdCompany()}','{$oInspector->getUser()}',
                        '{$oInspector->getCertificationNumber()}','{$oInspector->getEntity()}')");
        return $bd->insert_id();
    }

    public function editInspector(Inspector $oInspector) {
        $tabla = "_inspectors";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET 
                    inspector_name='{$oInspector->getName()}',
                    inspector_email='{$oInspector->getEmail()}',
                    inspector_certification_number='{$oInspector->getCertificationNumber()}',
                    inspector_status='{$oInspector->getStatus()}',
                    entity_id='{$oInspector->getEntity()}'
                    WHERE inspector_id={$oInspector->getId()};");
    }

    private function generaInspector($row) {
        $idInspector = $row->inspector_id;
        $companyId = $row->company_id;
        $nameInspector = $row->inspector_name;
        $user = $row->inspector_user_id;
        $entity = $row->entity_id;
        $certificationNumber = $row->inspector_certification_number;
        $status = $row->inspector_status;
        $email = $row->inspector_email;

        $oInspector = new Inspector();
        $oInspector->setId($idInspector);
        $oInspector->setName($nameInspector);
        $oInspector->setIdCompany($companyId);
        $oInspector->setUser($user);
        $oInspector->setEmail($email);
        $oInspector->setEntity($entity);
        $oInspector->setCertificationNumber($certificationNumber);
        $oInspector->setStatus($status);
        return $oInspector;
    }
    
}

?>