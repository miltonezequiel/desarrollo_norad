<?php

@include_once( 'init.php' );
require_once( ROOT_DIR . "/conf/motor.php" );
require_once( ROOT_DIR . "/entidades/payment_detail.php" );

class DataPaymentsDetails {

	public function getPaymentDetailById( $id ) {
		$table = "_payments_details";
		$bd    = new conexion();
		$rows  = $bd->query( "SELECT *
                            FROM " . DB_PREFIJO . "$table 
                            WHERE 
                            payment_details_id = " . $id . ";" );

		if ( $rows[0] != null ) {
			$oPayment = $this->generatePaymentDetail( $rows[0] );
		}

		return $oDevice;
	}

	private function generatePaymentDetail( $row ) {
		$payment_details_id       = $row->payment_details_id;
		$payment_details_desc     = $row->payment_details_desc;
		$payment_details_subtotal = $row->payment_details_subtotal;
		$payment_details_quantity = $row->payment_details_quantity;
		$payment_id               = $row->payment_id;

		$oPayment = new Payment_detail( $payment_details_id, $payment_details_desc, $payment_details_subtotal, $payment_id, $payment_details_quantity );

		return $oDevice;
	}

	public function savePaymentDetail( Payment_detail $oPaymentDetail ) {
		$bd = new conexion();

		$query = 'INSERT INTO ax_payment_details (
					payment_details_desc,
					payment_details_subtotal,
					payment_details_quantity,
					payment_id
				  ) VALUES(?, ?, ?, ?);';
		if ( $stmt = $bd->conn->prepare( $query ) ) {
			$stmt->bind_param( "sdii",
				$oPaymentDetail->getPaymentDetailsDesc(), $oPaymentDetail->getPaymentDetailsSubtotal(), $oPaymentDetail->getPaymentDetailsQuantity(), $oPaymentDetail->getPaymentId()
			);
			$stmt->execute();
		} else {
			$error = $bd->conn->errno . ' ' . $bd->conn->error;
			echo $error; // 1054 Unknown column 'foo' in 'field list'
		}

		return $bd->insert_id();
	}

}

?>
