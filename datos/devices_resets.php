<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/device_reset.php");

class DataDevicesResets {

    public function setDeviceReset(DeviceReset $oDeviceReset) {
        $tabla = "_devices_resets";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla
                VALUES('{$oDeviceReset->getDevice()}','{$oDeviceReset->getDatetime()}')");
    }

    public function getCountResetsByDevice($idDevice) {
        $tabla = "_devices_resets";
        $bd = new conexion();
        $row = $bd->query("SELECT dr_device_id,count(*) count_resets FROM " . DB_PREFIJO . "$tabla 
                            where dr_device_id={$idDevice}
                            GROUP BY 1");
        if ($row[0]) {
            return $row[0]->count_resets;
        }
        return null;
    }

    private function generaDeviceReset($row) {
        $device = $row->dr_device_id;
        $datetime = $row->dr_date_time;
        $oDeviceReset = new DeviceReset();
        $oDeviceReset->setDevice($device);
        $oDeviceReset->setDatetime($datetime);
        return $oDeviceReset;
    }
    
}

?>
