<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/device_company.php");

class DataDevicesCompanies {

    public function getCompanyByIdDevice($idDevice) {
        $tabla = "_devices_companies";
        $bd = new conexion();
        $row = $bd->query("SELECT dc.*
                            FROM " . DB_PREFIJO . "$tabla dc
                            WHERE dc.`dc_device_id`={$idDevice}
                            AND (dc.dc_end_date IS NULL OR dc.`dc_end_date`>CURRENT_DATE)");
        $oDeviceCompany = null;
        if ($row[0]) {
            $oDeviceCompany = $this->generaDeviceCompany($row[0]);
        }
        return $oDeviceCompany;
    }
    
    public function getDevicesCompanyById($id) {
        $tabla = "_devices_companies";
        $bd = new conexion();
        $row = $bd->query("SELECT dc.*
                            FROM " . DB_PREFIJO . "$tabla dc
                            WHERE dc.`dc_id`={$id}");
        $oDeviceCompany = null;
        if ($row[0]) {
            $oDeviceCompany = $this->generaDeviceCompany($row[0]);
        }
        return $oDeviceCompany;
    }

    public function getDevicesByIdCompany($idCompany, $unassigned) {
        $tabla = "_devices_companies";
        if ($unassigned) {
            $query = "SELECT dc.*
                    FROM " . DB_PREFIJO . "$tabla dc
                    WHERE dc.`dc_company_id`={$idCompany}";
        } else {
            $query = "SELECT dc.*
                    FROM " . DB_PREFIJO . "$tabla dc
                    WHERE dc.`dc_company_id`={$idCompany}
                    AND (dc.`dc_end_date` IS NULL OR dc.`dc_end_date`>CURRENT_DATE)";
        }
        $bd = new conexion();
        $row = $bd->query($query);
        $index = 0;
        $vDevicesCompany = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oDeviceCompany = $this->generaDeviceCompany($registro);
                $vDevicesCompany[$index] = $oDeviceCompany;
                $index = $index + 1;
            }
        }
        return $vDevicesCompany;
    }

    public function getDevicesByIdCompanyPag($idCompany, $offset, $limit, $unassigned) {
        $tabla = "_devices_companies";
        if ($unassigned) {
            $query = "SELECT dc.*
                            FROM " . DB_PREFIJO . "$tabla dc
                            WHERE dc.`dc_company_id`={$idCompany}
                            ORDER BY dc.`dc_start_date` DESC
                            LIMIT {$offset},{$limit}";
        } else {
            $query = "SELECT dc.*
                            FROM " . DB_PREFIJO . "$tabla dc
                            WHERE dc.`dc_company_id`={$idCompany}
                            AND (dc.`dc_end_date` IS NULL OR dc.`dc_end_date`>CURRENT_DATE)
                            ORDER BY dc.`dc_start_date` DESC
                            LIMIT {$offset},{$limit}";
        }
        $bd = new conexion();
        $row = $bd->query($query);
        $index = 0;
        $vDevicesCompany = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oDeviceCompany = $this->generaDeviceCompany($registro);
                $vDevicesCompany[$index] = $oDeviceCompany;
                $index = $index + 1;
            }
        }
        return $vDevicesCompany;
    }

    public function addDeviceCompany(DeviceCompany $oDeviceCompany) {
        $tabla = "_devices_companies";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (dc_company_id,dc_device_id,dc_start_date)
                    VALUES(
                    {$oDeviceCompany->getIdCompany()},
                    {$oDeviceCompany->getIdDevice()},
                    curdate()
                    );");
    }

    public function editEndDateDeviceCompany(DeviceCompany $oDeviceCompany) {
        $tabla = "_devices_companies";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla
                    SET dc_end_date= curdate() 
                    WHERE `dc_device_id`={$oDeviceCompany->getIdDevice()}
                    AND `dc_end_date` IS NULL");
    }

    public function getCountTestDeviceCompany(DeviceCompany $oDeviceCompany) {
        $bd = new conexion();
        $row = $bd->query("SELECT COUNT(t.`test_id`) AS count FROM ax_tests t INNER JOIN ax_devices_companies dc on dc.dc_id = t.test_di_dc_id
                    WHERE dc.`dc_company_id`={$oDeviceCompany->getIdCompany()} AND 
                    dc.`dc_device_id`={$oDeviceCompany->getIdDevice()} ");
        $countTests = 0;
        if ($row[0]) {
            $countTests = $row[0]->count;
        }
        return $countTests;
    }

    public function getLastUseDeviceCompany(DeviceCompany $oDeviceCompany) {
        $bd = new conexion();
        $row = $bd->query("SELECT t.`test_id`,t.`test_report_date` FROM ax_tests t INNER JOIN ax_devices_companies dc on dc.dc_id = t.test_di_dc_id
                    WHERE dc.`dc_company_id`={$oDeviceCompany->getIdCompany()} AND 
                    dc.`dc_device_id`={$oDeviceCompany->getIdDevice()} 
                    ORDER BY t.`test_report_date` DESC
                    LIMIT 1;");
        $lastDate = NULL;
        if ($row[0]) {
            $lastDate = $row[0]->test_report_date;
        }
        return $lastDate;
    }
    
    public function getLastUseCompany(Device $oDevice) {
        $bd = new conexion();
        $row = $bd->query("SELECT dc_company_id, t.`test_report_date` FROM ax_tests t
                    INNER JOIN ax_devices_companies on ax_devices_companies.dc_id = t.test_di_dc_id
                    WHERE `dc_device_id`={$oDevice->getId()}
                    ORDER BY t.`test_report_date` DESC
                    LIMIT 1;");

        return $row;
    }

    private function generaDeviceCompany($row) {
        $id = $row->dc_id;
        $idDevice = $row->dc_device_id;
        $idCompany = $row->dc_company_id;
        $startDate = $row->dc_start_date;
        $endDate = $row->dc_end_date;

        $oDeviceCompany = new DeviceCompany();
        $oDeviceCompany->setId($id);
        $oDeviceCompany->setIdDevice($idDevice);
        $oDeviceCompany->setIdCompany($idCompany);
        $oDeviceCompany->setStartDate($startDate);
        $oDeviceCompany->setEndDate($endDate);

        return $oDeviceCompany;
    }
    
}

?>
