<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/company_agreements.php");

class DataCompanyFee {

//    public function getCompanyFeesByCompany($companyId) {
//        $tabla = "_companies_fee";
//        $bd = new conexion();
//        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla" . " WHERE company_id = " . $companyId);
//        return $row;
//    }
    
    public function getCorrespondingCompanyFee($companyId, $device_id) {
        $tabla = "_companies_fee";
        $bd = new conexion();
        
        $query = "
                    SELECT ax_companies_fee.company_fee_fee
                    FROM ax_companies_fee
                    WHERE ax_companies_fee.company_id = ". $companyId ." AND
                    ax_companies_fee.company_fee_from <= 
                    (
                        SELECT COUNT(*) FROM (
                                                SELECT *
                                                FROM ax_tests
                                                INNER JOIN ax_devices_inspectors ON ax_tests.test_di_dc_id = ax_devices_inspectors.di_dc_id
                                                WHERE 
                                                    YEAR(ax_tests.test_report_date) = YEAR(CURDATE()) AND MONTH(ax_tests.test_report_date) = MONTH(CURDATE()) 
                                                    AND ax_devices_inspectors.di_dc_company_id = ". $companyId ."
                                                    AND ax_devices_inspectors.di_dc_device_id = ". $device_id ."
                                                GROUP BY 1)T1
                    ) AND ax_companies_fee.company_fee_to >= 
                    (
                        SELECT COUNT(*) FROM (
                                                SELECT *
                                                FROM ax_tests
                                                INNER JOIN ax_devices_inspectors ON ax_tests.test_di_dc_id = ax_devices_inspectors.di_dc_id
                                                WHERE 
                                                    YEAR(ax_tests.test_report_date) = YEAR(CURDATE()) AND MONTH(ax_tests.test_report_date) = MONTH(CURDATE()) 
                                                    AND ax_devices_inspectors.di_dc_company_id = ". $companyId ."
                                                    AND ax_devices_inspectors.di_dc_device_id = ". $device_id ."
                                                GROUP BY 1)T1
                    )
                ";
        
        $row = $bd->query($query);
        return $row[0]->company_fee_fee;
    }

    public function addAgreement($from, $to, $fee, $companyId) {
        $bd = new conexion();
        $bd->query("INSERT INTO ax_companies_fee(company_fee_from, company_fee_to, company_fee_fee, company_id)
                            VALUES(" . $from . ", " . $to . ", " . $fee . ", " . $companyId . ");");
        return $bd->insert_id();
    }
    
    public function deleteCompanyFee($companyFeeId) {
        $bd = new conexion();
        $bd->query("DELETE FROM ax_companies_fee WHERE company_fee_id = ". $companyFeeId);
    }
    
    public function getCompanyFeeByCompanyId($companyId) {
        $tabla = "_companies_fee";
        $bd = new conexion();
        
        $query = "
                    SELECT *
                    FROM ax_companies_fee
                    WHERE ax_companies_fee.company_id = ". $companyId;
        
        $row = $bd->query($query);
        $index = 0;
        $vCompanyFee = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oCompanyFee = $this->generateCompanyFee($registro);
                $vCompanyFee[$index] = $oCompanyFee;
                $index = $index + 1;
            }
        }
        return $vCompanyFee;
    }
    
    public function getCompanyFeeByCompanyIdPag($companyId, $offset, $limit) {
        $tabla = "_companies_fee";
        $bd = new conexion();
        
        $query = "
                    SELECT *
                    FROM ax_companies_fee
                    WHERE ax_companies_fee.company_id = ". $companyId. " ORDER BY company_fee_from LIMIT {$offset},{$limit}";
        
        $row = $bd->query($query);
        $index = 0;
        $vCompanyFee = array();
        if ($row[0]) {
            foreach ($row as $registro) {
                $oCompanyFee = $this->generateCompanyFee($registro);
                $vCompanyFee[$index] = $oCompanyFee;
                $index = $index + 1;
            }
        }
        return $vCompanyFee;
    }
    
    
    
    private function generateCompanyFee($row) {
        $company_fee_id = $row->company_fee_id;
        $company_fee_from = $row->company_fee_from;
        $company_fee_to = $row->company_fee_to;
        $company_fee_fee = $row->company_fee_fee;
        $company_id = $row->company_id;

        $oCompanyFee = new CompanyFee($company_fee_id, $company_fee_from, $company_fee_to, $company_fee_fee, $company_id);

        return $oCompanyFee;
    }
   
}

?>
