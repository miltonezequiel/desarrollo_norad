<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/contact_copy.php");

class DataContactsCopy {

    public function setContactCopy(ContactCopy $oContactCopy) {
        $tabla = "_contacts_copy";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (contact_copy_name,contact_copy_street,contact_copy_city,
                contact_copy_state,contact_copy_zip,test_id)
                VALUES('{$oContactCopy->getName()}','{$oContactCopy->getStreet()}','{$oContactCopy->getCity()}',
                    '{$oContactCopy->getState()}','{$oContactCopy->getZip()}','{$oContactCopy->getTest()}')");
        return $bd->insert_id();
    }

    private function generaContactCopy($row) {
        $idContactCopy = $row->contact_copy_id;
        $nameContactCopy = $row->contact_copy_name;
        $streetContactCopy = $row->contact_copy_street;
        $cityContactCopy = $row->contact_copy_city;
        $stateContactCopy = $row->contact_copy_state;
        $zipContactCopy = $row->contact_copy_zip;
        $testId = $row->test_id;
        $oContactCopy = new ContactCopy($idContactCopy, $nameContactCopy, $streetContactCopy, $cityContactCopy, $zipContactCopy, $stateContactCopy, $testId);
        return $oContactCopy;
    }
    
}

?>
