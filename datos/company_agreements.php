<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/company_agreements.php");

class DataCompaniesAgreements {

    public function getLastAgreement($company_id) {
        $tabla = "_companies_agreements";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where company_id =" . $company_id . " ORDER BY 1 DESC LIMIT 1");
        if ($row[0]) {
            $oCompAg = $this->generateAgreement($row[0]);
        }
        return $oCompAg;
    }

    public function getAllAgreements() {
        $tabla = "_agreements";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla");
        return $row;
    }

    private function generateAgreement($row) {
        $id = $row->companies_agreements_id;
        $company_id = $row->company_id;
        $agreement_id = $row->agreement_id;
        $date = $row->date;

        $oCompanyAgreement = new CompanyAgreement($company_id, $agreement_id, $date);
        return $oCompanyAgreement;
    }

    public function addAgreement($companyId, $agreementId) {
        $tabla = "_companies_agreements";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (company_id, agreement_id, date)
                            VALUES(" . $companyId . "," . $agreementId . ", curdate());");
        return $bd->insert_id();
    }

	public function acceptAgreement( $companyId, $agreementId ) {
		$tabla = "_companies_agreements";
		$bd = new conexion();
		$row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla WHERE company_id = $companyId AND agreement_id = $agreementId ORDER BY date DESC LIMIT 1");
		if ($row[0]) {
			$companies_agreements_id = $row[0]->companies_agreements_id;

			$bd->query( "UPDATE " . DB_PREFIJO . "$tabla SET agreement_accepted = now() WHERE companies_agreements_id = $companies_agreements_id" );
		}

		return true;
	}
        
}

?>
