<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/conf/motor.php");
require_once (ROOT_DIR . "/entidades/company.php");

class DataCompanies {

    public function getCompanies($disabled) {
        $tabla = "_companies";
        $bd = new conexion();
        if (!$disabled) {
            $query = "SELECT * FROM " . DB_PREFIJO . "$tabla c 
                        WHERE c.company_status=1
                        ORDER BY c.company_name DESC";
        } else {
            $query = "SELECT * FROM " . DB_PREFIJO . "$tabla c 
                           ORDER BY c.company_name DESC";
        }
        
        $row = $bd->query($query);
        $vCompanies = array();
        foreach ($row as $registro) {
            $oCompany = $this->generaCompany($registro);
            array_push($vCompanies, $oCompany);
        }
        return $vCompanies;
    }

    public function getCompaniesPag($offset, $limit, $disabled) {
        $tabla = "_companies";
        $bd = new conexion();
        if (!$disabled) {
            $query = "SELECT * FROM " . DB_PREFIJO . "$tabla c
                           WHERE c.company_status=1
                           ORDER BY c.company_name DESC
                           LIMIT {$offset},{$limit}";
        } else {
            $query = "SELECT * FROM " . DB_PREFIJO . "$tabla c
                           ORDER BY c.company_name DESC
                           LIMIT {$offset},{$limit}";
        }
        $row = $bd->query($query);
        $vCompanies = array();
        foreach ($row as $registro) {
            $oCompany = $this->generaCompany($registro);
            array_push($vCompanies, $oCompany);
        }
        return $vCompanies;
    }

    public function getCompanyById($idCompany) {
        $tabla = "_companies";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where company_id=" . $idCompany);
        $oCompany = null;
        if ($row[0]) {
            $oCompany = $this->generaCompany($row[0]);
        }
        return $oCompany;
    }

    public function getCompanyByUser($idUser) {
        $tabla = "_companies";
        $bd = new conexion();
        $row = $bd->query("SELECT * FROM " . DB_PREFIJO . "$tabla where company_user_id=" . $idUser);
        $oCompany = null;
        if ($row[0]) {
            $oCompany = $this->generaCompany($row[0]);
        }
        return $oCompany;
    }

    public function addCompany(Company $oCompany) {
        $tabla = "_companies";
        $bd = new conexion();
        $bd->query("INSERT INTO " . DB_PREFIJO . "$tabla (company_name,company_user_id,company_payment_type)
                    VALUES ('{$oCompany->getName()}',{$oCompany->getUser()}, 'Invoice')");
        return $bd->insert_id();
    }

    public function editCompany(Company $oCompany) {
        $tabla = "_companies";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET 
                    company_name='{$oCompany->getName()}',
                    company_address_1='{$oCompany->getAddress1()}',
                    company_address_2='{$oCompany->getAddress2()}',
                    company_city='{$oCompany->getCity()}',
                    company_state='{$oCompany->getState()}',
                    company_zip='{$oCompany->getZip()}',
                    company_phone='{$oCompany->getPhone()}',
                    company_email='{$oCompany->getEmail()}',
                    company_contact_person='{$oCompany->getContactPerson()}',
                    company_status='{$oCompany->getStatus()}'
                    WHERE company_id={$oCompany->getId()}");
    }
    
    public function saveBillingInfo(Company $oCompany) {
        $tabla = "_companies";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET 
                    company_customer_profile_id='{$oCompany->getCustomerProfileId()}',
                    company_customer_payment_profile_id='{$oCompany->getCustomerPaymentProfileId()}'
                    WHERE company_id={$oCompany->getId()}");
    }
    
    public function savePayment(Company $oCompany) {

        $tabla = "_companies";
        $bd = new conexion();
        $bd->query("UPDATE " . DB_PREFIJO . "$tabla SET 
                    company_payment_type='{$oCompany->getCompanyPaymentType()}',
                    company_test_fee='{$oCompany->getCompanyTestFee()}'
                    WHERE company_id={$oCompany->getId()}");
    }

    private function generaCompany($row) {
        $idCompany = $row->company_id;
        $nameCompany = $row->company_name;
        $userCompany = $row->company_user_id;
        $address1Company = $row->company_address_1;
        $address2Company = $row->company_address_2;
        $cityCompany = $row->company_city;
        $stateCompany = $row->company_state;
        $zipCompany = $row->company_zip;
        $phoneCompany = $row->company_phone;
        $emailCompany = $row->company_email;
        $contactPersonCompany = $row->company_contact_person;
        $statusCompany = $row->company_status;
        $customerProfileId = $row->company_customer_profile_id;
        $customerPaymentProfileId = $row->company_customer_payment_profile_id;
        $companyTestFee = $row->company_test_fee;
        $companyPaymentType = $row->company_payment_type;
        
        $oCompany = new Company();
        $oCompany->setId($idCompany);
        $oCompany->setName($nameCompany);
        $oCompany->setUser($userCompany);
        $oCompany->setAddress1($address1Company);
        $oCompany->setAddress2($address2Company);
        $oCompany->setCity($cityCompany);
        $oCompany->setState($stateCompany);
        $oCompany->setZip($zipCompany);
        $oCompany->setPhone($phoneCompany);
        $oCompany->setEmail($emailCompany);
        $oCompany->setContactPerson($contactPersonCompany);
        $oCompany->setStatus($statusCompany);
        $oCompany->setCustomerProfileId($customerProfileId);
        $oCompany->setCustomerPaymentProfileId($customerPaymentProfileId);
        $oCompany->setCompanyPaymentType($companyPaymentType);
        $oCompany->setCompanyTestFee($companyTestFee);

        return $oCompany;
    }
    
}

?>
