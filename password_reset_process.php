<?php

include_once 'init.php';
require_once ROOT_DIR . '/libs/PHPMailer/PHPMailerAutoload.php';
include_once ROOT_DIR . '/util/utilidades.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/entidades/company.php';
include_once ROOT_DIR . '/entidades/inspector.php';

$servicios = new Servicios();

$username = $_POST['username'];
if (!isset($username) || empty($username)) {
    header('Location: password_reset.php');
}

$oUser = $servicios->getUserByUsername($username);
$newPassword = Utilidades::generatePassword();
$oUser->setPassword(md5($newPassword));
$servicios->editPasswordUser($oUser);

$typeUser = $_POST['typeuser'];
switch ($typeUser) {
    case 'inspector':
        $oInspector = $servicios->getInspectorByUser($oUser->getId());
        $email = $oInspector->getEmail();
        break;
    case 'company':
        $oCompany = $servicios->getCompanyByUser($oUser->getId());
        $email = $oCompany->getEmail();
        break;
    default:
        header('Location: index.php');
        break;
}

$mail = new PHPMailer();
$mail->setFrom('norad@noradonline.com', 'NORAD');
$mail->addAddress($email, $oUser->getUsername());
$mail->isHTML(TRUE);
$mail->Subject = 'New password';
$mail->Body = "Username: {$oUser->getUsername()} <br/>Password: {$newPassword}";
$mail->send();

header('Location:' . $typeUser);
?>
