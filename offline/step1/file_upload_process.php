<?php
include '../../login_check.php';
include '../login_inspector_check.php';
include_once '../../init.php';

include_once ROOT_DIR . '/servicios/servicios.php';

// Check if the form was submitted
if($_SERVER["REQUEST_METHOD"] == "POST")
{
   $error="";
   // Check if file was uploaded without errors
   if(isset($_FILES["manup"]) && $_FILES["manup"]["error"] == 0)
   {
        $servicios  = new Servicios();

        $file_ext=strtolower(end(explode('.',$_FILES['manup']['name'])));
        $allowed= array("nrd","owl");

        $file_tmp = $_FILES['manup']['tmp_name'];
        $file_name = $_FILES['manup']['name'];
        $file_size = $_FILES['manup']['size'];
        $file_type = $_FILES['manup']['type'];

        //$file_name = preg_replace("/[^A-Za-z0-9 \.\-_]/", '', $file_name);

        if(empty($file_name))
        {
            $error="File name is empty!";
        }
        else
        if(in_array($file_ext,$allowed) === false)
        {
            $error="Please select a valid file format.";
        }
        else
        if(strlen($file_name) > 100)
        {
            $error="File name too long!";
        }
        else
        if ($file_size > (100*1024))
        {
            $error="File size is larger than the allowed limit.";
        }
        else
        {
           $inspectorId = $_SESSION['user']['inspector']['id'];
           if($inspectorId == null || $inspectorId == "")
           {
               $error="Indeterminate inspector";
           }
           else
           {
                $_SESSION['filename'] = $filename = "I". $_SESSION['user']['inspector']['id'] . utf8_decode($file_name);
                $dest = ROOT_DIR . "/" . $GLOBAL_SETTINGS['test.nrd.path'] . "/" . $filename;

                if ( move_uploaded_file($file_tmp, $dest) )
                {
                    $userId = $_SESSION['user']['id'];
                    if($userId == null || $userId == "")
                    {
                        $error="Indeterminate User";
                    }
                    else
                    {
                        $vHourlyResults = array();
                        if (($handle = fopen($dest, 'r')) !== FALSE)
                        {
                            $row=0; $meds=0;
                            $idTest=0; $idDevice=0; $snDevice=0;
                            while (($line = fgets($handle, 256)) !== false)
                            {
                               $row++;
//echo "<br>". $row .": ". $line;
                               if(($line != "") && ($row>7))
                               {
                                    $line = XORdeCipher($line);
//echo " >>> ". $line;
                                    if(($row>18)&&($idTest!=0))
                                    {
                                        $nrd = str_getcsv($line, ";");
                                        if (str_replace(".", "-", $nrd[0]) != NULL)
                                        {
                                            $oHourlyResult = new HourlyResult();
                                            {
                                                                                        $oHourlyResult->setIdTest(int_Val($idTest));
                                               $line = str_replace(".", "-", $nrd[0]); $oHourlyResult->setDateTime($line);
                                               $line = str_replace(",", ".", $nrd[1]); $oHourlyResult->setAlpha(int_Val($line));
                                                                                        $oHourlyResult->setRadon(0.0);
                                               $line = str_replace(",", ".", $nrd[2]); $oHourlyResult->setTemperature(float_Val($line));
                                               $line = str_replace(",", ".", $nrd[3]); $oHourlyResult->setHumidity(float_Val($line));
                                               $line = str_replace(",", ".", $nrd[4]); $oHourlyResult->setPressure(float_Val($line));
                                                                                        $oHourlyResult->setError(int_Val($nrd[5]));
                                                                                        $oHourlyResult->setTilt(int_Val($nrd[6]));
                                                                                        $oHourlyResult->setPowerOutage(int_Val($nrd[7]));
                                               $meds++;
                                            }
                                            array_push($vHourlyResults, $oHourlyResult);
//echo '<pre>'; var_dump($oHourlyResult); echo '</pre>';
                                        }
                                    }
                                    else
                                    if($row==9)
                                    {
                                        $nrd = str_getcsv($line, ":");

                                        if(strpos($nrd[0], 'Log File') !== false)
                                        {
                                            $idTest = hexdec($nrd[1]);
                                            if($idTest==0) $idTest++;
//echo '<pre>';  echo " >>> TestID: ". $idTest; '</pre>';
                                        }
                                    }
                                    else
                                    if($row==12)
                                    {
                                        $nrd = str_getcsv($line, ":");

                                        if(strpos($nrd[0], 'Serial') !== false)
                                        {
                                            $snDevice = trim($nrd[1]);
                                            $oDevice = $servicios->getDeviceBySerialNumber(int_Val($snDevice));
                                            if($oDevice != null)
                                            {
                                                $idDevice = $oDevice->getId();
                                            }
                                        }
//echo '<pre>';  echo " >>> DeviceSN: ". $snDevice; '</pre>';
//echo '<pre>';  echo " >>> DeviceID: ". $idDevice; '</pre>';
                                    }
                               }
                            }
                            if(!feof($handle))
                            {
                                $error="Early end of file";
                            }
                            fclose($handle);

                            if(($meds>0)&&($idDevice!=0)&&($snDevice!=0))
                            {
                                $servicios->eraseTemplyResultsByIdTest($idTest);
//echo '<pre>';  echo "<br>";  echo " >>> Temply Erasing..."; '</pre>';
                                $servicios->setTemplyResults($vHourlyResults);
//echo '<pre>';  echo "<br>";  echo " >>> Temply Results..."; '</pre>';

                                $oUser = $servicios->getUserById($userId);
//echo '<pre>';  echo "<br>";  echo " >>> oUser..."; '</pre>';
                                if($oUser != null)
                                {
                                    $oUser->setLogged(2); // Fake javaRunng
                                    $oUser->setDTestID(int_Val($idTest));
                                    $oUser->setDeviceID(int_Val($idDevice));
                                    $oUser->setDeviceSN(int_Val($snDevice));
//echo '<pre>'; var_dump($oUser); echo '</pre>';
                                    $servicios->editUserDevice($oUser);
                                    $servicios->editUserLogged($oUser);

                                    header('Location: ../../inspector/step2/index.php');
                                    exit;
                                }
                            }
                            else
                            {
                                $error="In the data package";
                            }
                        }
                    }
                }
                else
                {
                    $error="When uploading data";
                }
           }
        }
   }
   else
   {  /*   $_FILES["manup"]["error"] :
                                        UPLOAD_ERR_INI_SIZE:   1
                                        UPLOAD_ERR_FORM_SIZE:  2
                                        UPLOAD_ERR_PARTIAL:    3
                                        UPLOAD_ERR_NO_TMP_DIR: 6
                                        UPLOAD_ERR_CANT_WRITE: 7
                                        UPLOAD_ERR_EXTENSION:  8
       */
       $error="We have a error. Try Again.";
   }
   header('Location: index.php?error=' . $error);
   exit;
}
?>

<?php
function float_Val($data){ return (float)trim($data); }
function int_Val($data){ return (int)trim($data); }
function XORdeCipher($data)
{ $data = base64_decode($data);
  $dataLen = strlen($data);
  $key = "($*%;=#/&)<TheOwl.ar>Phoenix";
  $keyLen = strlen($key);
  $output = $data;
  for ($i = 0; $i < $dataLen; ++$i)
  {  $output[$i] = $data[$i] ^ $key[$i % $keyLen];
  }
  return $output;
}
?>