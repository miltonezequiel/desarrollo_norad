<?php
include '../../login_check.php';
include '../login_inspector_check.php';
include_once '../../init.php';

$error=''; if(isset($_GET['error'])){ $error = $_GET['error']; }

$page = "step1";
$path = '../../';
?>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<html>
    <head>
        <title>NORAD | Radon Detection System</title>
        <meta name="keywords" content="" />
        <link type="text/css" rel="stylesheet" href="<?php echo $path; ?>css/style.css" />
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <script src='../../js/jquery-1.9.1.js'></script>
        <style>
        .owlMsg
        {   /*background-color:#ffeaea*/
            /*border: 0.2em solid red;*/
            border-radius: 0.5em;
            width: 80%;
            float: center;
            margin-left: 2%; margin-top: 0.7em; margin-bottom: 0.7em;
            padding: 1.2em;
            font-family: verdana, sans-serif; font-size: 80%; font-style: normal;
            line-height: 160%;
            text-align: center;
            color: black;
        }
        </style>
    </head>
    <body>
        <div id="container">
            <header><?php include_once($path . 'includes/header.php'); ?></header>
            <div id="menu-step"><?php include_once($path . 'includes/menu-step.php'); ?></div>
            <div id="wrapper" class="page-<?php echo $page ?>">
                <div id="container2">
                    <div id="main">
                        <div class="text" align="center">
                            <br/>
                            <br/>
                            <br/>
                            <p>Choose a <b>.nrd</b> file to upload</p>
                            <br/>
                            <br/>
                              <form enctype="multipart/form-data" method="post" action="file_upload_process.php">
                                <input type="file" accept=".nrd" name="manup" id="manup"/>
                                <br/>
                                <br/>
                                <input class="button" type="submit" value="UPLOAD FILE" alt="Upload File"/>
                              </form>
                            <br/>
                            <?php
                                if($error!='')
                                {
                                    echo '<div class="owlMsg" style="background-color:#ffeaea"><p><b>Error :  </b>'.$error.'</p></div>';
                                }
                                ?>
                            <br/>
                            <br/>
                            <form enctype="multipart/form-data" method="post" action="../../inspector/index.php">
                            <p>Otherwise <input type="submit" value="  Go Back  "> to Test Information Entry</p>
                            </form>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer><?php include_once($path . 'includes/footer.php'); ?></footer>
    </body>
</html>