<?php
session_start();
include_once '../init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/user.php';
include_once ROOT_DIR . '/entidades/company.php';
if(isset($_SESSION['estadoLogin']))
{
  $estadoLogin = $_SESSION['estadoLogin'];
}
if (isset($estadoLogin) && $estadoLogin)
{
    header("Location: step1");
    return;
}
else
{
    $estadoLogin = FALSE;
}

if(isset($_POST['username']))
{
  $user = $_POST['username'];
}
if(isset($_POST['password']))
{
  $password = $_POST['password'];
}

$servicios = new Servicios();
if(isset($user))
{
  $oUser = $servicios->getUserByUsername($user);
}
if (isset($oUser) && $oUser->getUsername() == $user && $oUser->getPassword() == md5($password))
{
  $oInspector = $servicios->getInspectorByUser($oUser->getId());
  if ($oInspector != null)
  {
    $oCompany = $servicios->getCompanyById($oInspector->getIdCompany());
    if (isset($oInspector) && !is_null($oInspector) && $oInspector->getStatus() != 0 && $oCompany->getStatus() != 0)
    {
            $_SESSION['user']['inspector']['id'] = $oInspector->getId();
            $_SESSION['user']['type'] = "inspector";
            $_SESSION['user']['inspector']['name'] = $oInspector->getName();
            $_SESSION['user']['id'] = $oUser->getId();
            $_SESSION['estadoLogin'] = $estadoLogin = TRUE;

            $oUser->setDeviceID(0);
            $oUser->setDeviceSN(0);
            $oUser->setDTestID(0);
            $oUser->setLogged(1); // userIsOn
            $servicios->editUserDevice($oUser);

            $redirectLocation = "step1";
    }
    else
    {
       $redirectLocation = "login.php?msg=error";
    }
  }
  else
  {
     $redirectLocation = "login.php?msg=error";
  }
}
elseif ($user == NULL || $password == NULL)
{
   $redirectLocation = "login.php";
}
else
{
   $redirectLocation = "login.php?msg=error";
}
header("Location: " . $redirectLocation);
?>