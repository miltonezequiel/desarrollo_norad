<?php
include 'configuracion.php';

class conexion{
	
	// Funci�n constructora, se conecta a penas se crea el objeto.
	public function __construct()
	{
		$enlace = @mysqli_connect(DB_HOST,DB_USER,DB_PASSWORD);
		if (!$enlace) {
			echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
			echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
			echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
			exit;
		} else {
			$this->conn = $enlace;
		}

	    mysqli_select_db($this->conn, DB_NAME);
      
	}
	
	// Funci�n query para ejecuci�n de sentencias SQL.
	public function query($query)
	{
		$mysql_result = @mysqli_query($this->conn, $query);
		if (!$mysql_result) {
			die( $query . "\n" . mysqli_error($this->conn) );
		}


		$this->n = @mysqli_num_rows($mysql_result);
		$this->a = @mysqli_affected_rows();
		if($this->n)
			for($i=0;$i<$this->n;$i++) $taula[$i] = @mysqli_fetch_object($mysql_result);
		else $taula = null;
		$this->v = $taula;
		if(  $this->n > 0  ) mysqli_free_result($mysql_result);
		return $taula;
	}
	
	// Funci�n destructura, se desconecta con la base de datos.
	public function __destruct()
	{
		@mysqli_close($this->conn);
	}

	public function insert_id() {
		return mysqli_insert_id($this->conn);
	}

}
?>