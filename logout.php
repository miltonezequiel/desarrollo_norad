<?php
//*
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
//*/

include_once 'init.php';
include_once ROOT_DIR . '/servicios/servicios.php';
include_once ROOT_DIR . '/entidades/user.php';

session_start();

$estadoLogin = $_SESSION['estadoLogin'];
if (isset($estadoLogin) && $estadoLogin) 
{
   $id = $_SESSION['user']['id']; 
   if (isset($id) && !empty($id)) 
   {
      $servicios = new Servicios();
      $oUser = $servicios->getUserById($id);
      if(isset($oUser)) 
      { 
         $tid = $oUser->getDTestID();
         if (isset($tid) && !empty($tid)) 
         { if ((int)$tid > 0) 
           {
              $servicios->eraseTemplyResultsByIdTest($tid);
           }
         }
         
         $oUser->setLogged(0); // userIsOff
         $servicios->editUserLogged($oUser);
      } 
    }
}  

session_destroy();
header("Location: index.php");
?>