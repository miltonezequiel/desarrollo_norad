<div class="container">
	<div class="logo"><a href="<?php echo $path; ?>"><img src="<?php echo $path; ?>images/logo.jpg"></a></div>
    <?php
        if (isset($_SESSION['estadoLogin']) && $_SESSION['estadoLogin'] == TRUE) {
            echo '
                <form action="'. $path .'logout.php">
                    <input type="submit" class="button" value="LOGOUT" style="position: absolute; top: 20px; left: 55%; z-index: 1000;"/>
                </form>
            ';
	        //echo '<a href="' . $path . 'logout.php"><img src="' . $path . 'images/btn-logout-header.jpg" style=" position: absolute; top: 10px; left: 50%; z-index: 1000;"></a>';
        }
        ?>
    <div class="img"><img src="<?php echo $path; ?>images/header-right.jpg"></div>
</div>
<div class="menu">
    <div class="container">
            <nav>
                <a href="<?php echo $path; ?>company" class="client-login<?php if($page == 'client-login' or $page == 'company-data'){ echo '_activo'; } ?>">client login<span></span></a>
                <a href="<?php echo $path; ?>inspector" class="upload_test<?php if($page == 'inspector' or $page == 'step1' or $page == 'step2' or $page == 'step3' or $page == 'step4' or $page == 'step5' or $page == 'step2-reporting'){ echo '_activo'; } ?>">upload test<span></span></a>
                <a href="<?php echo $path; ?>tester-resources" class="tester-resources<?php if($page == 'tester-resources'){ echo '_activo'; } ?>">tester resources<span></span></a>
                <a href="http://www.norad.one" class="about-norad<?php if($page == 'about-norad'){ echo '_activo'; } ?>">about norad<span></span></a>
                <a href="<?php echo $path; ?>contact-us" class="contact-us<?php if($page == 'contact-us'){ echo '_activo'; } ?>">contact us<span></span></a>
            </nav>
    </div>
</div>
