


<style type="text/css">
    ul {
        list-style: none;
        padding: 0;
        margin: 0;
        background-color: #333;
        font: normal 13px robotobold, Geneva, sans-serif;
        z-index: 999;
    }

    ul li {
        display: block;
        position: relative;
        float: left;
        background-color: #333;
        font: normal 13px robotobold, Geneva, sans-serif;
        min-width: 160px;
        height: 2rem;
        
    }

    li ul { display: none; }



    ul li a {
        display: block;
        /*padding: 1em;*/
        text-decoration: none;
        white-space: nowrap;
        color: #fff;
        height: 100%;
        padding-left: 0.5rem;
    }

    ul li a:hover { 
        background-color: black; 
        color: white;
    }

    li:hover > ul {
        display: block;
        position: absolute;
    }

    li:hover li { float: none; }

    li:hover a { 
        /*        background-color: #f9f9f9;
                color: black;*/
        background-color: black;
        color: white;
    }

    li:hover li a:hover { 
        background: #333; 
    }

    .main-navigation li ul li { border-top: 0; }

    ul ul ul {
        left: 100%;
        top: 0;
    }

    ul:before,
    ul:after {
        content: " "; /* 1 */
        display: table; /* 2 */
    }

    ul:after { clear: both; }

    .menu_title .activo {
        background-color: black;
    }
</style>


<ul class="main-navigation">

    <li class="menu_title"><a href="#" class="<?php
        if ($page == 'devices') {
            echo 'activo';
        }
        ?>">Device Management</a>
        <ul>
            <li><a href="devices_add.php">New device</a></li>
            <li><a href="<?php echo $path; ?>admin/devices.php">Select Device</a>
                <!--                <ul>
                                    <li><a href="#">All</a></li>
                                    <li><a href="#">Order by S/N</a></li>
                                    <li><a href="#">By company assignment</a></li>
                                    <li><a href="#">Unassigned</a></li>
                                    <li><a href="#">Calibrated in date...</a></li>
                                </ul>-->
            </li>
            <li><a href="#">Reports</a>
                <ul>
                    <li><a href="<?php echo $path ?>admin/reports/filters.php?report=device_not_used">Devices not used since...</a></li>
                    <li><a href="<?php echo $path ?>admin/reports/filters.php?report=tests_per_device">Tests per device</a></li>
                    <li><a href="<?php echo $path ?>admin/reports/filters.php?report=devices_by_company">By company assignment</a></li>
                    <li><a href="<?php echo $path ?>admin/reports/filters.php?report=device_resets">Device resets</a></li>
                    <li><a href="<?php echo $path ?>admin/reports/device_to_calibrate_60_days.php" target="_blank">Devices due for calibration next 60 days</a></li>
                </ul>
            </li>
        </ul>
    </li>

    <li class="menu_title"><a href="#" class="<?php
        if ($page == 'companies') {
            echo 'activo';
        }
        ?>">Client Management</a>

        <ul>
            <li><a href="<?php echo $path; ?>admin/companies_add.php">New Company</a></li>
            <li><a href="<?php echo $path; ?>admin/devices.php">Select Company</a>
                <ul>
                    <li><a href="companies.php?d=1">All Companies</a></li>
                    <li><a href="companies.php?d=0">Enabled Companies</a></li>
                </ul>
            </li>
        </ul>


    </li>

     <li class="menu_title"><a href="#" class="<?php
        if ($page == 'administration') {
            echo 'activo';
        }
        ?>">Administration</a>

        <ul>
            <li><a href="<?php echo $path; ?>admin/generate_invoice.php">Generate Invoices</a></li>
            <li><a href="<?php echo $path; ?>admin/devices.php">Reports</a>
                <ul>
                    <li><a href="<?php echo $path ?>admin/reports/filters.php?report=revenue_per_state">Revenue per state</a></li>
                    <li><a href="companies.php?d=0">Listing of revenue per Client</a></li>
                </ul>
            </li>
            <li><a href="<?php echo $path; ?>admin/agreements.php">Agreements</a>
                <ul>
                    <li><a href="<?php echo $path ?>admin/agreements.php">List of agreements</a></li>
                    <li><a href="<?php echo $path ?>admin/agreements_add.php">Add new agreement</a></li>
                </ul>
            </li>
        </ul>


    </li>

</ul>