<?php

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}

$estadoLogin = $_SESSION['estadoLogin'];
if (!isset($estadoLogin) || !$estadoLogin) {
    header("Location: ../index.php");
    exit;
}
?>
