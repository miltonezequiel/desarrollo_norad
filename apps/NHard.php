<?php
    header("Content-Type: application/x-java-jnlp-file");
    header('Content-Disposition: attachment; filename="NHard.jnlp"');
    header("Expires: Tue, 24 Mar 2000 05:00:00 GMT"); // Date in the past
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-control: post-check=0, pre-check=0, false");
    header("Pragma: no-cache");
    
    $userId="0";

    $Host="www.noradonline.com";  /// $_SERVER['SERVER_NAME']; 
    if($Host == "localhost") { $Base = "http://localhost:8080/noradonline/apps/"; 
                               $App = "";
                             }
                        else { $Base = "http://".$Host."/";
                               $App = "apps/";
                             }

    $jnlp = $App.'NHard.php'; 
    $Version = "18.22"; // Year.Week
?>
<!-- /****** By TheOwlar'17 *****/ -->
<jnlp spec="6.0+" codebase="<?php print($Base);?>" href="<?php print($jnlp);?>" > 
    <information>
        <title>Norad Hardware Setting</title>
        <vendor>Envirolabs Incorporated</vendor>
        <homepage href="<?php print($Base);?>" />
        <description>Radon Detection System</description>
        <description kind="short">Norad DataLogger</description>
        <icon href="<?php print($App);?>NoRadLoading.gif" width="286" height="183" kind="splash"/>
        <icon href="<?php print($App);?>NoRadFavIcon.gif" width="48" height="48" kind="default"/>       
        <!-- <association>
            <mime-type="application-x/x-java-jnlp-file" />
            <extensions="jnlp" />
        </association> -->
    </information>
    <security>
        <all-permissions/>
    </security>
    <resources>
        <java version="1.7+"/> 
        <property name="jnlp.versionEnabled" value="true"/>         
        <jar href="<?php print($App);?>NHard.jar" version="<?php print($Version);?>" main="true"/>     
        <jar href="<?php print($App);?>lib/norad-linker-18.01-bin.jar"/>
    </resources>
    <application-desc main-class="jHard.runApp"> 
    <argument><?php print($userId);?></argument>  
    </application-desc>
    <update check="background"/>
</jnlp>
