<?php
function html_to_pdf($html , $page = 'A4')
{
    //Now generate pdf from html
    include("mpdf.php");
     
    //A4 paper
    $mpdf = new mPDF('utf-8' , $page , '' , '' , 0 , 0 , 0 , 0 , 0 , 0);
    $mpdf->SetDisplayMode('fullpage');
    $mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
     
    $mpdf->WriteHTML($html);
    $mpdf->Output();
}

html_to_pdf("http://www.lacapital.com.ar");

?>