<?php

@include_once '../../../init.php';

class Utilidades {

	public static function checkExtension( $file ) {
		$path_parts = pathinfo( $file );
		$extension  = $path_parts['extension'];

		return $extension;
	}

	public static function assertExtension( $file, $ext ) {
		$extFile = Utilidades::checkExtension( $file );
		if ( $ext == $extFile ) {
			return true;
		}

		return false;
	}

	public static function generateFileName( $idInspector, $file ) {
		date_default_timezone_set( 'UTC' ); //dev
		$ext      = Utilidades::checkExtension( $file );
		$filename = $idInspector . "-" . date( 'dmYHis' ) . "-" . substr( md5( microtime() ), 1, 6 ) . "." . $ext;

		return $filename;
	}

	public static function generateReportFileName( $idInspector ) {
		date_default_timezone_set( 'UTC' ); //dev
		$filename = $idInspector . "-report-" . date( 'dmYHis' ) . "-" . substr( md5( microtime() ), 1, 6 ) . ".pdf";

		return $filename;
	}

	public static function generateImgTestFileName( $idInspector, $file, $n ) {
		date_default_timezone_set( 'UTC' ); //dev
		$ext      = Utilidades::checkExtension( $file );
		$filename = $idInspector . "-img" . $n . "-" . date( 'dmYHis' ) . "-" . substr( md5( microtime() ), 1, 6 ) . "." . $ext;

		return $filename;
	}

	public static function generateImgGraphFileName( $idInspector, $d1, $d2, $ext ) {
		date_default_timezone_set( 'UTC' ); //dev
		$filename = $idInspector . "-graph_" . $d1 . $d2 . "-" . date( 'dmYHis' ) . "-" . substr( md5( microtime() ), 1, 6 ) . "." . $ext;

		return $filename;
	}

	public static function generateXAxisTicks( $start, $end, $stopover ) {
		$xaxis = array();
		for ( $i = $start; $i <= $end; $i ++ ) {
			if ( $i % $stopover == 0 ) {
				array_push( $xaxis, $i );
			} else {
				array_push( $xaxis, "" );
			}
		}

		return $xaxis;
	}

	public static function generateHtmlReport( Client $oClient, Test $oTest, Inspector $oInspector, Company $oCompany, Device $oDevice, $idTest, $vHourlyResults, $preview = false, $oCalibration = "N/S", $oCalibrationExpires = "N/S" ) {

		//Arreglos por parametros (radon, humedad, etc) para graficar
		$datetime = array();
		$rn       = array();
		$pressure = array();
		$h        = array();
		$temp     = array();
		$index    = 0;
		foreach ( $vHourlyResults as $oHourlyResult ) {
			$datetime[ $index ] = $oHourlyResult->getDateTime();
			$rn[ $index ]       = $oHourlyResult->getRadon();
			$pressure[ $index ] = ( $oHourlyResult->getPressure() * 0.295299830714 );
			$h[ $index ]        = $oHourlyResult->getHumidity();
			$temp[ $index ]     = ( $oHourlyResult->getTemperature() * ( 9 / 5 ) + 32 );
			$index ++;
		}

		//Calculo promedios (se utilizan en report_view.php)
		if ( $index == 0 ) {
			$index = 1;
		}
		$avgRadon       = array_sum( $rn ) / ( $index );
		$avgPressure    = array_sum( $pressure ) / ( $index );
		$avgTemperature = array_sum( $temp ) / ( $index );
		$avgHumidity    = array_sum( $h ) / ( $index );
		$randonAvgLine  = array();
		for ( $i = 0; $i < $index; $i ++ ) {
			array_push( $randonAvgLine, $avgRadon );
		}

		//Graficos comparativos
		$imgRP   = generateGraph( "r", "p", $rn, $pressure );
		$imgRH   = generateGraph( "r", "h", $rn, $h );
		$imgRT   = generateGraph( "r", "t", $rn, $temp );
		$imgRAvg = generateGraph( "r", "ravg", $rn, $randonAvgLine );

		if ( isset( $_SESSION['imgRP'] ) ) {
			$_SESSION['imgRP2']   = $imgRP;
			$_SESSION['imgRH2']   = $imgRH;
			$_SESSION['imgRT2']   = $imgRT;
			$_SESSION['imgRAvg2'] = $imgRAvg;
		} else {
			$_SESSION['imgRP']   = $imgRP;
			$_SESSION['imgRH']   = $imgRH;
			$_SESSION['imgRT']   = $imgRT;
			$_SESSION['imgRAvg'] = $imgRAvg;
		}


		ob_start();
		include ROOT_DIR . '/inspector/report/report_view.php';
		$html = ob_get_clean();

		return $html;
	}

	public static function generateEmailsReport() {
		ob_start();
		include ROOT_DIR . '/inspector/step5/report_view.php';
		//include __DIR__.'/../' . '\inspector\step5\report_view.php';
		$html = ob_get_clean();

		return $html;
	}

	public static function generateReceiptReport() {
		ob_start();
		include ROOT_DIR . '/inspector/step5/report_view_receipts.php';
		$html = ob_get_clean();

		return $html;
	}

	public static function floatFormat( $number, $precision ) {
		$number = floor(($number*10))/10; //Evita que redondee
		return number_format( $number, $precision, ".", "" );
	}

	public static function dateFormat( $date, $format = "m-d-Y" ) {
		$date    = strtotime( $date );
		$newDate = date( $format, $date );

		return $newDate;
	}

	public static function floatValue( $number ) {

		return floatval( str_replace( ',', '.', $number ) );
	}

	public static function generatePassword() {
		$length           = rand( 8, 10 );
		$characters       = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890$=!?#*+-";
		$lengthCharacters = strlen( $characters );
		$password         = "";
		for ( $i = 1; $i <= $length; $i ++ ) {
			$pos      = rand( 0, $lengthCharacters - 1 );
			$password .= substr( $characters, $pos, 1 );
		}

		return $password;
	}

	// Normaliza el path dependiendo del sistema operativo en donde se está sirviendo la app
	public static function normalizePath( $path ) {
		$newpath = ( DIRECTORY_SEPARATOR === '\\' )
			? str_replace( '/', '\\', $path )
			: str_replace( '\\', '/', $path );

		return $newpath;
	}

	// Normaliza los nombres de los archivos para que este correcta la sintaxis de sus nombre
	public static function normalizeFilename( $candidateFilename ) {
		// Remove anything which isn't a word, whitespace, number
		// or any of the following caracters -_~,;[]().
		// If you don't need to handle multi-byte characters
		// you can use preg_replace rather than mb_ereg_replace
		$file = mb_ereg_replace( "([^\w\s\d\-_~,;\[\]\(\)/.])", '', $candidateFilename );
		// Remove any runs of periods
		$file = mb_ereg_replace( "([\.]{2,})", '', $file );

		return $file;
	}

	public static function rm_slash( $string ) {
		return preg_replace( '´/´', '', $string );
	}

	/*
	* Dado un array devuelve el valor en el percentil.
	*/
	function get_percentile( $percentile, $array ) {
		sort( $array );
		$index = ( $percentile / 100 ) * count( $array );
		if ( floor( $index ) == $index ) {
			$result = ( $array[ $index - 1 ] + $array[ $index ] ) /
			          2;
		} else {
			$result = $array[ floor( $index ) ];
		}
		return $result;
	}

	function IsEmptyString($val){
		if (trim($val) === ''){$val = "NULL";}
		return $val;
	}
}
