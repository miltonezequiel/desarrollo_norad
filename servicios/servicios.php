<?php

@include_once ('init.php');
require_once(ROOT_DIR . "/datos/tests.php");
require_once(ROOT_DIR . "/datos/hourly_results.php");
require_once(ROOT_DIR . "/datos/clients.php");
require_once(ROOT_DIR . "/datos/devices_inspectors.php");
require_once(ROOT_DIR . "/datos/devices.php");
require_once(ROOT_DIR . "/datos/companies.php");
require_once(ROOT_DIR . "/datos/inspectors.php");
require_once(ROOT_DIR . "/datos/contacts_copy.php");
require_once(ROOT_DIR . "/datos/users.php");
require_once(ROOT_DIR . "/datos/entities.php");
require_once(ROOT_DIR . "/datos/devices_companies.php");
require_once(ROOT_DIR . "/datos/states.php");
require_once(ROOT_DIR . "/datos/payments.php");
require_once(ROOT_DIR . "/datos/payments_details.php");
require_once(ROOT_DIR . "/datos/devices_resets.php");
require_once(ROOT_DIR . "/datos/agreements.php");
require_once(ROOT_DIR . "/datos/company_agreements.php");
require_once(ROOT_DIR . "/datos/company_fee.php");

class Servicios {

    public function setTest(Test $oTest) {
        $dataTests = new DataTests();
        $idTest = $dataTests->setTest($oTest);
        return $idTest;
    }

    public function updateTestFile(Test $oTest) {
        $dataTests = new DataTests();
        $dataTests->updateTestFile($oTest);
    }

    public function updateTestPaymentId(Test $oTest) {
        $dataTests = new DataTests();
        $dataTests->updateTestPaymentId($oTest);
    }

    public function getTestsByIdCompanyPag($idCompany, $offset, $limit) {
        $dataTests = new DataTests();
        $vTests = $dataTests->getTestsByIdCompanyPag($idCompany, $offset, $limit);
        return $vTests;
    }

    public function getTestsByIdCompany($idCompany) {
        $dataTests = new DataTests();
        $vTests = $dataTests->getTestsByIdCompany($idCompany);
        return $vTests;
    }

    public function getTests() {
        $dataTests = new DataTests();
        $vTests = $dataTests->getTests();
        return $vTests;
    }

    public function getTestById($idTest) {
        $dataTests = new DataTests();
        $oTest = $dataTests->getTestById($idTest);
        return $oTest;
    }

    public function getTestByPaymentId($idPayment) {
        $dataTests = new DataTests();
        $oTest = $dataTests->getTestByPaymentId($idPayment);
        return $oTest;
    }

    ///////////////////////////////////////////////////

    public function setTemplyResults($vHourlyResults) 
    {
        $dataHourlyResults = new DataHourlyResults();
        $dataHourlyResults->setTemplyResults($vHourlyResults);
    }
    
    public function getTemplyResultsByIdTest($idTest) 
    {
        $dataHourlyResults = new DataHourlyResults();
        $vHourlyResults = $dataHourlyResults->getTemplyResultsByIdTest($idTest);
        return $vHourlyResults;
    }

    public function eraseTemplyResultsByIdTest($idTest) 
    {
        $dataHourlyResults = new DataHourlyResults();
        $dataHourlyResults->eraseTemplyResultsByIdTest($idTest);
    }

    ///////////////////////////////////////////////////

    public function setHourlyResults($vHourlyResults) {
        $dataHourlyResults = new DataHourlyResults();
        $dataHourlyResults->setHourlyResults($vHourlyResults);
    }

    public function getHourlyResultsByIdTest($idTest) {
        $dataHourlyResults = new DataHourlyResults();
        $vHourlyResults = $dataHourlyResults->getHourlyResultsByIdTest($idTest);
        return $vHourlyResults;
    }
    
    public function setClient(Client $oClient) {
        $dataClients = new DataClients();
        $idClient = $dataClients->setClient($oClient);
        return $idClient;
    }

    public function getClientById($idClient) {
        $dataClients = new DataClients();
        $oClient = $dataClients->getClientById($idClient);
        return $oClient;
    }

    public function getDevicesByIdInspector($idInspector) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        $vDevicesInspectors = $dataDevicesInspectors->getDevicesByIdInspector($idInspector);
        return $vDevicesInspectors;
    }

    public function editEndDateDeviceInspector($idDevice, $inspectorId) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        $dataDevicesInspectors->editEndDateDeviceInspector($idDevice, $inspectorId);
    }
    
    public function editEndDateDeviceInspectorFromAdmin($idDevice, $idCompany) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        $dataDevicesInspectors->editEndDateDeviceInspectorFromAdmin($idDevice, $idCompany);
    }

    public function getInspectorByIdDeviceIdCompany($idDevice, $idCompany) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        return $dataDevicesInspectors->getInspectorByIdDeviceIdCompany($idDevice, $idCompany);
    }

    public function getDevicesInspectorById($id) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        return $dataDevicesInspectors->getDevicesInspectorById($id);
    }

    public function getDevicesCompanyById($id) {
        $dataDevicesCompany = new DataDevicesCompanies();
        return $dataDevicesCompany->getDevicesCompanyById($id);
    }

    public function getDeviceInspectorByDeviceId($idDevice) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        return $dataDevicesInspectors->getDeviceInspectorByDeviceId($idDevice);
    }

    public function addDeviceInspector(DeviceInspector $oDeviceInspector) {
        $dataDevicesInspectors = new DataDevicesInspectors();
        $dataDevicesInspectors->addDeviceInspector($oDeviceInspector);
    }

    public function getDeviceById($idDevice, $dateError = null) {
        if ($dateError == null) {
            $dateError = date("Y-m-d");
        }
        $dataDevices = new DataDevices();
        $oDevice = $dataDevices->getDeviceById($idDevice, $dateError);
        return $oDevice;
    }

    public function getDeviceBySerialNumber($serialNumberDevice) {
        $dataDevices = new DataDevices();
        $oDevice = $dataDevices->getDeviceBySerialNumber($serialNumberDevice);
        return $oDevice;
    }

    public function getDevices() {
        $dataDevices = new DataDevices();
        $vDevices = $dataDevices->getDevices();
        return $vDevices;
    }

    public function getDevicesPag($offset, $limit, $orderBy, $dir, $filter) {
        $dataDevices = new DataDevices();

        $filter = str_replace("*", "'", $filter);
        $filter = str_replace("like", "like ('%", $filter);

        $vDevices = $dataDevices->getDevicesPag($offset, $limit, $orderBy, $dir, $filter);
        return $vDevices;
    }

    public function addDevice(Device $oDevice) {
        $dataDevices = new DataDevices();
        return $dataDevices->addDevice($oDevice);
    }

    public function addCalibration(Device $oDevice, $date = NULL) {
        $dataDevices = new DataDevices();
        $dataDevices->addCalibration($oDevice, $date);
    }

    public function getCalibrationDate(Device $oDevice) {
        $dataDevices = new DataDevices();
        return $dataDevices->getCalibrationDate($oDevice);
    }

    public function getCompanies($disabled = FALSE) {
        $dataCompanies = new DataCompanies();
        $vCompanies = $dataCompanies->getCompanies($disabled);
        return $vCompanies;
    }

    public function getCompaniesPag($offset, $limit, $disabled = FALSE) {
        $dataCompanies = new DataCompanies();
        $vCompanies = $dataCompanies->getCompaniesPag($offset, $limit, $disabled);
        return $vCompanies;
    }

    public function getCompanyById($idCompany) {
        $dataCompanies = new DataCompanies();
        $oCompany = $dataCompanies->getCompanyById($idCompany);
        return $oCompany;
    }

    public function getCompanyByUser($idUser) {
        $dataCompanies = new DataCompanies();
        $oCompany = $dataCompanies->getCompanyByUser($idUser);
        return $oCompany;
    }

    public function addCompany(Company $oCompany) {
        $dataCompanies = new DataCompanies();
        return $dataCompanies->addCompany($oCompany);
    }

    public function editCompany(Company $oCompany) {
        $dataCompanies = new DataCompanies();
        $dataCompanies->editCompany($oCompany);
    }

    public function savePayment(Company $oCompany) {
        $dataCompanies = new DataCompanies();
        $dataCompanies->savePayment($oCompany);
    }

    public function saveBillingInfo(Company $oCompany) {
        $dataCompanies = new DataCompanies();
        $dataCompanies->saveBillingInfo($oCompany);
    }

    public function getInspectorById($idInspector) {
        $dataInspectors = new DataInspectors();
        $oInspector = $dataInspectors->getInspectorById($idInspector);
        return $oInspector;
    }

    public function getInspectorsByIdCompany($idCompany) {
        $dataInspectors = new DataInspectors();
        $vInspectors = $dataInspectors->getInspectorsByIdCompany($idCompany);
        return $vInspectors;
    }

    public function getInspectorsByIdCompanyPag($idCompany, $offset, $limit) {
        $dataInspectors = new DataInspectors();
        $vInspectors = $dataInspectors->getInspectorsByIdCompanyPag($idCompany, $offset, $limit);
        return $vInspectors;
    }

    public function getInspectorByUser($user) {
        $dataInspectors = new DataInspectors();
        $oInspector = $dataInspectors->getInspectorByUser($user);
        return $oInspector;
    }

    public function addInspector(Inspector $oInspector) {
        $dataInspectors = new DataInspectors();
        return $dataInspectors->addInspector($oInspector);
    }

    public function editInspector(Inspector $oInspector) {
        $dataInspectors = new DataInspectors();
        $dataInspectors->editInspector($oInspector);
    }

    public function deleteInspector(Inspector $oInspector) {
        $dataInspectors = new DataInspectors();
        $dataInspectors->deleteInspector($oInspector);
    }

    public function setContactsCopy(ContactCopy $oContactCopy) {
        $dataContactsCopy = new DataContactsCopy();
        $idContactCopy = $dataContactsCopy->setContactCopy($oContactCopy);
        return $idContactCopy;
    }

    public function getUserById($idUser) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getUserById($idUser);
        return $oUser;
    }

    public function getUserByUsername($username) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getUserByUsername($username);
        return $oUser;
    }

    public function getEmail($type, $username) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getEmail($type, $username);
        return $oUser;
    }

    public function getUsername($type, $email) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getUsername($type, $email);
        return $oUser;
    }

    public function getUserPassword($type, $email, $username) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getUserPassword($type, $email, $username);
        return $oUser;
    }

    public function getUserByKey($md5Key) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getUserByKey($md5Key);
        return $oUser;
    }

    public function saveKeyPassRecover($idUser, $md5Key) {
        $dataUsers = new DataUsers();
        return $dataUsers->saveKeyPassRecover($idUser, $md5Key);
    }

    public function getUserByDTestID($devicetid) {
        $dataUsers = new DataUsers();
        $oUser = $dataUsers->getUserByDTestID($devicetid);
        return $oUser;
    }

    public function addUser(User $oUser) {
        $dataUsers = new DataUsers();
        return $dataUsers->addUser($oUser);
    }

    public function editPasswordUser(User $oUser) {
        $dataUsers = new DataUsers();
        $dataUsers->editPasswordUser($oUser);
    }

    public function editUserLogged(User $oUser) {
        $dataUsers = new DataUsers();
        $dataUsers->editUserLogged($oUser);
    }

    public function editUserDevice(User $oUser) {
        $dataUsers = new DataUsers();
        $dataUsers->editUserDevice($oUser);
    }

    public function editUserUsername(User $oUser) {
        $dataUsers = new DataUsers();
        $dataUsers->editUserUsername($oUser);
    }

    public function getEntities() {
        $dataEntities = new DataEntities();
        $vEntities = $dataEntities->getEntities();
        return $vEntities;
    }

    public function getEntityById($idEntity) {
        $dataEntities = new DataEntities();
        $oEntity = $dataEntities->getEntityById($idEntity);
        return $oEntity;
    }

    public function addDeviceCompany(DeviceCompany $oDeviceCompany) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        $dataDevicesCompanies->addDeviceCompany($oDeviceCompany);
    }

    public function getCompanyByIdDevice($idDevice) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        $oDeviceCompany = $dataDevicesCompanies->getCompanyByIdDevice($idDevice);
        return $oDeviceCompany;
    }

    public function editEndDateDeviceCompany(DeviceCompany $oDeviceCompany) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        $dataDevicesCompanies->editEndDateDeviceCompany($oDeviceCompany);
    }

    public function getDevicesByIdCompany($idCompany, $unassigned = FALSE) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        return $dataDevicesCompanies->getDevicesByIdCompany($idCompany, $unassigned);
    }

    public function getDevicesByIdCompanyPag($idCompany, $offset, $limit, $unassigned = FALSE) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        return $dataDevicesCompanies->getDevicesByIdCompanyPag($idCompany, $offset, $limit, $unassigned);
    }

    public function getCountTestDeviceCompany(DeviceCompany $oDeviceCompany) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        return $dataDevicesCompanies->getCountTestDeviceCompany($oDeviceCompany);
    }

    public function getCountTestDevice($deviceId) {
        $dataDevicesCompanies = new DataDevices();
        return $dataDevicesCompanies->getCountTestDevice($deviceId);
    }

    public function getLastUseDeviceCompany(DeviceCompany $oDeviceCompany) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        return $dataDevicesCompanies->getLastUseDeviceCompany($oDeviceCompany);
    }

    public function getLastUseCompany(Device $oDevice) {
        $dataDevicesCompanies = new DataDevicesCompanies();
        return $dataDevicesCompanies->getLastUseCompany($oDevice);
    }

    public function getStates() {
        $dataStates = new DataStates();
        return $dataStates->getStates();
    }

    public function getStateById($idState) {
        $dataStates = new DataStates();
        return $dataStates->getStateById($idState);
    }

    public function setDeviceReset(DeviceReset $oDeviceReset) {
        $dataDevicesResets = new DataDevicesResets();
        $dataDevicesResets->setDeviceReset($oDeviceReset);
    }

    public function getCountResetsByDevice($idDevice) {
        $dataDevicesResets = new DataDevicesResets();
        return $dataDevicesResets->getCountResetsByDevice($idDevice);
    }

    public function saveTestPayment($oPayment) {
        $dataPayment = new DataPayments();
        $idPayment = $dataPayment->savePayment($oPayment);
        return $idPayment;
    }

    public function updatePaymentAsPayed($id) {
        $dataPayment = new DataPayments();
        $idPayment = $dataPayment->updatePaymentAsPayed($id);
    }

    public function getPaymentById($id) {
        $dataPayment = new DataPayments();
        return $dataPayment->getPaymentById($id);
    }

    public function getPaymentDetailByPaymentId($id) {
        $dataPayment = new DataPayments();
        return $dataPayment->getPaymentDetailByPaymentId($id);
    }

    public function saveTestPaymentDetail($oPaymentDetail) {
        $dataPaymentDetail = new DataPaymentsDetails();
        $idPaymentDetail = $dataPaymentDetail->savePaymentDetail($oPaymentDetail);
        return $idPaymentDetail;
    }

    public function getPendingStatementsCompanies() {
        $dataPayment = new DataPayments();
        return $dataPayment->getPendingStatementsCompanies();
    }

    public function getPendingStatements($company_id) {
        $dataPayment = new DataPayments();
        return $dataPayment->getPendingStatements($company_id);
    }

    public function getAgreements() {
        $dataAgreements = new DataAgreements();
        $vAgreements = $dataAgreements->getAllAgreements();
        return $vAgreements;
    }

    public function getAgreementById($idAgreement) {
        $dataAgreements = new DataAgreements();
        $vAgreements = $dataAgreements->getAgreementById($idAgreement);
        return $vAgreements;
    }

    public function getCurrentAgreement($idCompany) {
        $dataAgreements = new DataAgreements();
        $vAgreements = $dataAgreements->getCurrentAgreement($idCompany);
        return $vAgreements;
    }

    public function getAllCompanyAgreements($idCompany){
	    $dataAgreements = new DataAgreements();
	    $vAgreements = $dataAgreements->getAllCompanyAgreements($idCompany);
	    return $vAgreements;
    }

    public function setCurrentAgreement($idCompany, $idAgreement) {
	    $dataAgreements = new DataAgreements();
	    $vAgreements = $dataAgreements->setCurrentAgreement($idCompany, $idAgreement);
	    return $vAgreements;
    }
    
     public function addAgreement(Agreement $oAgreement) {
        $dataAgreement = new DataAgreements();
        return $dataAgreement->addAgreement($oAgreement);
    }

    public function getAgreementsCompany($idCompany) {
        $companiesAgreement = new DataCompaniesAgreements();
        $vCompAg = $companiesAgreement->getLastAgreement($idCompany);
        return $vCompAg;
    }

    public function acceptAgreement($companyId, $agreementId) {
        $dataCompaniesAgreement = new DataCompaniesAgreements();
        return $dataCompaniesAgreement->acceptAgreement($companyId, $agreementId);
    }

	public function setCurrentAgreementFromDefault( $idCompany ) {
		$dataAgreement = new DataAgreements();
		return $dataAgreement->setCurrentAgreementFromDefault($idCompany);
	}

	public function isCurrentAgreementAccepted( $idCompany ) {
    	$dataAgreement = new DataAgreements();
    	return $dataAgreement->isCurrentAgreementAccepted ( $idCompany );
	}

    public function getTestFee($companyId, $deviceId) {
        $dataCompanyFee = new DataCompanyFee();
        return $dataCompanyFee->getCorrespondingCompanyFee($companyId, $deviceId);
    }
    
    public function addCompanyFee($from, $to, $fee, $company_id) {
        $dataCompanyFee = new DataCompanyFee();
        return $dataCompanyFee->addAgreement($from, $to, $fee, $company_id);
    }
    
    public function deleteCompanyFee($company_fee_id) {
        $dataCompanyFee = new DataCompanyFee();
        return $dataCompanyFee->deleteCompanyFee($company_fee_id);
    }
    
    public function getCompanyFeeByCompanyId($companyId) {
        $dataCompanyFee = new DataCompanyFee();
        return $dataCompanyFee->getCompanyFeeByCompanyId($companyId);
    }
    
    public function getCompanyFeeByCompanyIdPag($companyId, $offset, $limit) {
        $dataCompanyFee = new DataCompanyFee();
        return $dataCompanyFee->getCompanyFeeByCompanyIdPag($companyId, $offset, $limit);
    }
}

?>